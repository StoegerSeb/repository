package com.sap.sepp.cubetasticessentials.commands;

import com.sap.sepp.cubetasticessentials.CubeTasticEssentials;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class LobbyCommand implements CommandExecutor {

    public LobbyCommand(final String command) {
        Bukkit.getPluginCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (args.length == 0) {
            if (sender instanceof Player) {
                Player player = (Player) sender;

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                try {
                    dataOutputStream.writeUTF("Connect");
                    dataOutputStream.writeUTF("Lobby-1");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                player.sendPluginMessage(CubeTasticEssentials.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
                player.sendMessage("§3CubeTastic§7>> §3Verbindung zu §eLobby §3wird hergestellt");
            }
        }
        return false;
    }
}
