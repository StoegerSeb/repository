package com.sap.sepp.cubetasticessentials;

import com.sap.sepp.cubetasticessentials.commands.LobbyCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class CubeTasticEssentials extends JavaPlugin {
    private static CubeTasticEssentials instance;

    @Override
    public void onEnable() {
        instance = this;

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        new LobbyCommand("lobby");
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public static CubeTasticEssentials getInstance() {
        return instance;
    }
}
