package com.sap.sepp.prefix.listener;
/*
# (C) Copyright 2020 Prefix (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Prefix-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ChatEvent: 15.04.2020 00:57 by sebip
*/

import com.sap.sepp.prefix.Prefix;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.PluginManager;

public class ChatEvent implements Listener {

    public ChatEvent(final Prefix instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if(e.getPlayer().hasPermission("cubetastic.inhaber") || e.getPlayer().isOp())
            e.setFormat("§cInhaber §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.leitung"))
            e.setFormat("§1Leitung §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.admin"))
            e.setFormat("§4Admin §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.srmod"))
            e.setFormat("§2Sr.Mod §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.mod"))
            e.setFormat("§aMod §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.supporter"))
            e.setFormat("§9Supporter §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.builder"))
            e.setFormat("§bBuilder §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.tsupp"))
            e.setFormat("§9T.Supporter §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else if(e.getPlayer().hasPermission("cubetastic.tbuilder"))
            e.setFormat("§bT.Builder §8| §7" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
        else
            e.setFormat("§7Spieler §8| §f" + e.getPlayer().getName() + "§8» §f" + e.getMessage());
    }
}
