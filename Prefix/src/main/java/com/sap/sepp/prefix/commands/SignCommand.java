package com.sap.sepp.prefix.commands;
/*
# (C) Copyright 2020 Prefix (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Prefix-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SignCommand: 15.04.2020 01:28 by sebip
*/

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SignCommand implements CommandExecutor {
    public SignCommand(final String command) {
        Bukkit.getPluginCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player) {
            final Player player = (Player) sender;

            if(player.hasPermission("sign.use")) {
                if(args.length > 0) {
                    String message = "";

                    for(int i = 0; i < args.length; i++) {
                        message = message + args[i] + " ";
                    }

                    if(player.getItemInHand() != null) {
                        ItemStack item = player.getItemInHand();
                        ItemMeta meta = item.getItemMeta();

                        Date now = new Date();
                        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);

                        ArrayList<String> lores = new ArrayList<>();

                        if(player.hasPermission("sign.user.color"))
                            message = ChatColor.translateAlternateColorCodes('&', message);

                        lores.add(message);
                        lores.add("§3Signiert von §e" + player.getName() + " §3am §e" + format.format(now));

                        meta.setLore(lores);
                        item.setItemMeta(meta);
                        player.getPlayer().getInventory().setItemInHand(item);

                        lores.remove("§3Signiert von §e" + player.getName() + " §3am §e" + format.format(now));
                        lores.remove(message);
                        player.sendMessage("§3Item wurde erfolgreich signiert!");
                    } else {
                        player.sendMessage("§c3Du musst ein Item in der Hand haben!");
                        return false;
                    }
                } else {
                    player.sendMessage("§cNutze /sign <Nachricht>");
                    return false;
                }
            }
        }
        return false;
    }
}
