package com.sap.sepp.prefix.utils;
/*
# (C) Copyright 2020 Prefix (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Prefix-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ScoreboardManager: 15.04.2020 01:10 by sebip
*/

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Iterator;

public class ScoreboardManager {


    public void setTab(Player PL) {
        try {
            Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

            for (Iterator iterator = Bukkit.getOnlinePlayers().iterator(); iterator.hasNext(); ) {
                Player all = (Player) iterator.next();

                Team inhaber = scoreboard.registerNewTeam("0001Inh");
                Team leitung = scoreboard.registerNewTeam("0002Lei");
                Team admin = scoreboard.registerNewTeam("0003Adm");
                Team srmod = scoreboard.registerNewTeam("0004SrM");
                Team mod = scoreboard.registerNewTeam("0005Mod");
                Team supp = scoreboard.registerNewTeam("0006Sup");
                Team builder = scoreboard.registerNewTeam("0007Bui");
                Team tsupp = scoreboard.registerNewTeam("0008TSu");
                Team tbuilder = scoreboard.registerNewTeam("0009TBu");
                Team def = scoreboard.registerNewTeam("0010Def");

                inhaber.setPrefix("§cInhaber §8| §7");
                inhaber.setSuffix("§c");

                leitung.setPrefix("§1Leitung §8| §7");
                leitung.setSuffix("§1");

                admin.setPrefix("§4Admin §8| §7");
                admin.setSuffix("§4");

                srmod.setPrefix("§2SrMod §8| §7");
                srmod.setSuffix("§2");

                mod.setPrefix("§aMod §8| §7");
                mod.setSuffix("§a");

                supp.setPrefix("§9Supp §8| §7");
                supp.setSuffix("§9");

                builder.setPrefix("§bBuilder §8| §7");
                builder.setSuffix("§b");

                tsupp.setPrefix("§9TSupp §8| §7");
                tsupp.setSuffix("§9");

                tbuilder.setPrefix("§bTBuilder §8| §7");
                tbuilder.setSuffix("§b");

                def.setPrefix("§7Spieler §8| §7");
                def.setSuffix("§7");

                Bukkit.getOnlinePlayers().forEach(p -> {
                    if (p.hasPermission("cubetastic.inhaber")) {
                        inhaber.addEntry(p.getName());
                        p.setDisplayName("§c" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.leitung")) {
                        leitung.addEntry(p.getName());
                        p.setDisplayName("§1" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.admin")) {
                        admin.addEntry(p.getName());
                        p.setDisplayName("§4" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.srmod")) {
                        srmod.addEntry(p.getName());
                        p.setDisplayName("§2" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.mod")) {
                        mod.addEntry(p.getName());
                        p.setDisplayName("§a" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.supporter")) {
                        supp.addEntry(p.getName());
                        p.setDisplayName("§9" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.builder")) {
                        builder.addEntry(p.getName());
                        p.setDisplayName("§b" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.tsupp")) {
                        tsupp.addEntry(p.getName());
                        p.setDisplayName("§9" + p.getDisplayName());
                    } else if (p.hasPermission("cubetastic.tbuilder")) {
                        tbuilder.addEntry(p.getName());
                        p.setDisplayName("§b" + p.getDisplayName());
                    } else {
                        def.addEntry(p.getName());
                        p.setDisplayName("§7" + p.getDisplayName());
                    }
                    p.setScoreboard(scoreboard);
                });
            }

        } catch (IllegalArgumentException ex) {
        }
    }
}
