package com.sap.sepp.prefix.listener;
/*
# (C) Copyright 2020 Prefix (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Prefix-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerJoin: 15.04.2020 01:26 by sebip
*/

import com.sap.sepp.prefix.Prefix;
import com.sap.sepp.prefix.utils.ScoreboardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;

public class PlayerJoin implements Listener {

    public PlayerJoin(final Prefix instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e) {
        try {
            final Player player = e.getPlayer();

            (new ScoreboardManager()).setTab(player);
        } catch (IllegalArgumentException ex) {}
    }
}
