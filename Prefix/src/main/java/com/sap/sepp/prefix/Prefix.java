package com.sap.sepp.prefix;
/*
# (C) Copyright 2020 Prefix (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Prefix-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Prefix: 15.04.2020 00:56 by sebip
*/

import com.sap.sepp.prefix.commands.SignCommand;
import com.sap.sepp.prefix.listener.ChatEvent;
import com.sap.sepp.prefix.listener.PlayerJoin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

public class Prefix extends JavaPlugin {
    private static Prefix instance;
    private Scoreboard scoreboard;

    @Override
    public void onEnable() {
        instance = this;

        new ChatEvent(getInstance());
        //new PlayerJoin(getInstance());
        new SignCommand("sign");

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        setPrefix(e.getPlayer());
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public static Prefix getInstance() {
        return instance;
    }

    private void setPrefix(final Player p) {
        String team = "";

        if (p.hasPermission("cubetastic.inhaber")) {
            team = "0001Inh";
        } else if (p.hasPermission("cubetastic.leitung")) {
            team = "0002Lei";
        } else if (p.hasPermission("cubetastic.admin")) {
            team = "0003Adm";
        } else if (p.hasPermission("cubetastic.srmod")) {
            team = "0004SrM";
        } else if (p.hasPermission("cubetastic.mod")) {
            team = "0005Mod";
        } else if (p.hasPermission("cubetastic.supporter")) {
            team = "0006Sup";
        } else if (p.hasPermission("cubetastic.builder")) {
           team = "0007Bui";
        } else if (p.hasPermission("cubetastic.tsupp")) {
            team = "0008TSu";
        } else if (p.hasPermission("cubetastic.tbuilder")) {
            team = "0009TBu";
        } else {
            team = "0010Def";
        }

        scoreboard.getTeam(team).addPlayer(p);

        p.setDisplayName(scoreboard.getTeam(team).getPrefix() + p.getName());

        for(Player all : Bukkit.getOnlinePlayers())
            all.setScoreboard(scoreboard);
    }
}
