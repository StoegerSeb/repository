package com.sap.sepp.ttt.utils;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RoundRestartDaemon: 14.04.2020 23:40 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.helper.gamemode.ArenaHelper;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class RoundRestartDaemon extends BukkitRunnable {
    private Arena arena;

    private final Set<UUID> players = new HashSet<>();

    private final boolean willCycle;

    private final boolean willRestart;

    public RoundRestartDaemon(Round round) {
        this.willRestart = (round.getChallengers().size() > 0);
        this.arena = round.getArena();
        if (TTTCore.config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED) {
            this.arena.getMetadata().set("roundTally", (Integer) this.arena.getMetadata().get("roundTally").get() + 1);
            this.willCycle = ArenaHelper.shouldArenaCycle(this.arena);
        } else {
            this.willCycle = false;
        }
        if (this.willRestart)
            for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                Challenger ch = unmodifiableIterator.next();
                this.players.add(ch.getUniqueId());
            }
    }

    public void run() {
        assert !this.arena.getRound().isPresent();
        if (this.willCycle)
            cycleArena();
        if (this.willRestart) {
            Round round = this.arena.createRound();
            round.getMetadata().set("restarting", Boolean.TRUE);
            for (UUID uuid : this.players)
                round.addChallenger(uuid);
            round.getMetadata().remove("restarting");
        }
    }

    private void cycleArena() {
        this.arena.getMetadata().remove("startTime");
        this.arena.getMetadata().remove("roundTally");
        ArenaHelper.applyNextArena();
        this.arena = TTTCore.getDedicatedArena();
    }
}