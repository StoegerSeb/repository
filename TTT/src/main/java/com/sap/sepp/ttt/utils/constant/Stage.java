package com.sap.sepp.ttt.utils.constant;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Stage: 14.04.2020 23:47 by sebip
*/

import com.sap.sepp.api.round.LifecycleStage;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.config.ConfigKey;

public class Stage {
    public static final LifecycleStage WAITING = new LifecycleStage("waiting", -1);

    public static LifecycleStage PREPARING;

    public static LifecycleStage PLAYING;

    public static LifecycleStage ROUND_OVER;

    static {
        initialize();
    }

    public static void initialize() {
        PREPARING = new LifecycleStage("preparing", TTTCore.config.get(ConfigKey.PREPTIME_SECONDS));
        PLAYING = new LifecycleStage("playing", TTTCore.config.get(ConfigKey.HASTE) ? TTTCore.config
                .get(ConfigKey.HASTE_STARTING_SECONDS) : TTTCore.config
                .get(ConfigKey.ROUNDTIME_SECONDS));
        ROUND_OVER = new LifecycleStage("round_over", TTTCore.config.get(ConfigKey.POSTTIME_SECONDS));
    }
}
