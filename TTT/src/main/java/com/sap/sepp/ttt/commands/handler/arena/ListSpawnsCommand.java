package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ListSpawnsCommand: 14.04.2020 23:29 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.utils.physical.Location3D;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Map;

public class ListSpawnsCommand extends CommandHandler {
    public ListSpawnsCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        Optional<Arena> arena = TTTCore.mg.getArena(this.args[1]);
        if (!arena.isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        ImmutableMap immutableMap = arena.get().getSpawnPoints();
        TTTCore.locale.getLocalizable("info.personal.arena.listspawns").withPrefix(Color.INFO)
                .withReplacements(Color.EM + arena.get().getDisplayName() + Color.INFO).sendTo(this.sender);
        for (Map.Entry<Integer, Location3D> spawn : (Iterable<Map.Entry<Integer, Location3D>>)immutableMap.entrySet()) {
            Location3D l = spawn.getValue();
            this.sender.sendMessage(Color.SECONDARY + "    " + spawn.getKey() + ": " + ChatColor.WHITE + "(" + l
                    .getX() + ", " + l.getY() + ", " + l.getZ() + ")");
        }
    }
}
