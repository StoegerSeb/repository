package com.sap.sepp.ttt.utils.constant;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TelemetryKey: 14.04.2020 23:47 by sebip
*/

public class TelemetryKey {
    public static final String UUID = "uuid";

    public static final String VERSION = "version";

    public static final String PLATFORM = "platform";

    public static final String FLINT_API = "flintApi";

    public static final String OPERATING_MODE = "opMode";

    public static final String ARENA_COUNT = "arenas";

    public static final String ROUND_COUNT = "rounds";

    public static final String ROUND_MEAN_PLAYERS = "players";

    public static final String ROUND_DURATION_MEAN = "roundDur";

    public static final String ROUND_DURATION_STD_DEV = "roundDurSD";

    public static final String ROUND_INNOCENT_WINS = "iWins";

    public static final String ROUND_TRAITOR_WINS = "tWins";

    public static final String ROUND_FORFEITS = "forfeits";
}
