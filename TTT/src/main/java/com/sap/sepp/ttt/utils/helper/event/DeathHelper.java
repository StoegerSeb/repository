package com.sap.sepp.ttt.utils.helper.event;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class DeathHelper: 14.04.2020 23:49 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.scoreboard.ScoreboardManager;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.helper.gamemode.KarmaHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.RoleHelper;
import com.sap.sepp.ttt.utils.helper.platform.LocationHelper;
import com.sap.sepp.ttt.utils.helper.platform.MaterialHelper;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;

public final class DeathHelper {
    private final PlayerDeathEvent event;

    private final Player player;

    public DeathHelper(PlayerDeathEvent event) {
        this.event = event;
        this.player = event.getEntity();
    }

    public DeathHelper(Player player) {
        this.event = null;
        this.player = player;
    }

    public void handleEvent() {
        Optional<Challenger> chOpt = TTTCore.mg.getChallenger(this.player.getUniqueId());
        if (!chOpt.isPresent())
            return;
        Challenger ch = chOpt.get();
        Location loc = this.player.getLocation();
        Optional<Challenger> killer = getKiller();
        cancelEvent(ch);
        if (killer.isPresent()) {
            KarmaHelper.applyKillKarma((Challenger)killer.get(), ch);
            if (killer.get().getRound().getLifecycleStage().getDuration() != -1 &&
                    RoleHelper.isTraitor(killer.get()) && !RoleHelper.isTraitor(chOpt.get())) {
                killer.get().getRound().getMetadata().set("hasteTime",
                        (Integer) killer.get().getRound().getMetadata().get("hasteTime").or(0) + TTTCore.config
                                .get(ConfigKey.HASTE_SECONDS_PER_DEATH).intValue());
                killer.get().getRound().setTime(killer.get().getRound().getTime() - TTTCore.config
                        .get(ConfigKey.HASTE_SECONDS_PER_DEATH).intValue());
            }
        }
        Block block = loc.getBlock();
        while (block.getType() != Material.AIR && !MaterialHelper.instance().isLiquid(block.getType()))
            block = loc.add(0.0D, 1.0D, 0.0D).getBlock();
        ch.getRound().getArena().markForRollback(LocationHelper.convert(block.getLocation()));
        createBody(block.getLocation(), ch, killer.orNull());
        ((ScoreboardManager)ch.getRound().getMetadata().get("scoreboardManager").get()).updateEntry(ch);
    }

    private void cancelEvent(Challenger ch) {
        Location loc = this.player.getLocation();
        if (this.event != null) {
            this.event.setDeathMessage("");
            this.event.getDrops().clear();
            NmsHelper.sendRespawnPacket(this.player);
            this.player.teleport(loc);
        }
        ch.setSpectating(true);
        PlayerHelper.watchPlayerGameMode(ch);
        this.player.setHealth(this.player.getMaxHealth());
    }

    private Optional<Challenger> getKiller() {
        if (this.event == null || this.player.getKiller() == null)
            return Optional.absent();
        UUID uuid = null;
        if (this.player.getKiller().getType() == EntityType.PLAYER) {
            uuid = this.player.getKiller().getUniqueId();
        } else if (this.player.getKiller() instanceof Projectile) {
            ProjectileSource shooter = ((Projectile)this.player).getShooter();
            if (shooter instanceof Player)
                uuid = ((Player)shooter).getUniqueId();
        }
        if (uuid != null)
            return TTTCore.mg.getChallenger(uuid);
        return Optional.absent();
    }

    private void createBody(Location loc, Challenger ch, Challenger killer) {
        Boundary bound = ch.getRound().getArena().getBoundary();
        if (!bound.contains(LocationHelper.convert(loc))) {
            double x = (loc.getX() > bound.getUpperBound().getX()) ? bound.getUpperBound().getX() : ((loc.getX() < bound.getLowerBound().getX()) ? bound.getLowerBound().getX() : loc.getX());
            double y = (loc.getY() > bound.getUpperBound().getY()) ? bound.getUpperBound().getY() : ((loc.getY() < bound.getLowerBound().getY()) ? bound.getLowerBound().getY() : loc.getY());
            double z = (loc.getZ() > bound.getUpperBound().getZ()) ? bound.getUpperBound().getZ() : ((loc.getZ() < bound.getLowerBound().getZ()) ? bound.getLowerBound().getZ() : loc.getZ());
            loc = new Location(loc.getWorld(), x, y, z);
        }
        loc.getBlock().setType(((loc.getBlockX() + loc.getBlockY()) % 2 == 0) ? Material.TRAPPED_CHEST : Material.CHEST);
        storeBody(loc, ch, killer);
    }

    private void storeBody(Location loc, Challenger ch, Challenger killer) {
        List<Body> bodies = (List<Body>)ch.getRound().getMetadata().get("bodies").orNull();
        if (bodies == null)
            bodies = new ArrayList<>();
        long expiry = -1L;
        if (killer != null) {
            double dist = this.player.getLocation().toVector().distance(Bukkit.getPlayer(killer.getUniqueId()).getLocation().toVector());
            if (dist <= TTTCore.config.get(ConfigKey.KILLER_DNA_RANGE).intValue()) {
                double a = 0.2268D;
                int decayTime = TTTCore.config.get(ConfigKey.KILLER_DNA_BASETIME).intValue() - (int)Math.floor(0.2268D * Math.pow(dist, 2.0D));
                if (decayTime > 0)
                    expiry = System.currentTimeMillis() + (decayTime * 1000);
            }
        }
        Body body;
        bodies.add(

                body = new Body(ch.getRound(), LocationHelper.convert(loc), ch.getUniqueId(), ch.getName(), (killer != null) ? killer.getUniqueId() : null, ch.getMetadata().containsKey("detective") ? "detective" : (ch.getTeam().isPresent() ? ((Team)ch.getTeam().get()).getId() : null), System.currentTimeMillis(), expiry));
        ch.getRound().getMetadata().set("bodies", bodies);
        ch.getMetadata().set("body", body);
    }
}