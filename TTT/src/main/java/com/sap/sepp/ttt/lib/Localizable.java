package com.sap.sepp.ttt.lib;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Localizable: 14.04.2020 23:56 by sebip
*/

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;

public class Localizable {
    private final LocaleManager parent;

    private final String key;

    private String[] replacements = new String[0];

    private Localizable[] locReplacements = new Localizable[0];

    private String prefix = "";

    private String suffix = "";

    Localizable(LocaleManager parent, String key) {
        this.parent = parent;
        this.key = key;
    }

    public LocaleManager getParent() {
        return this.parent;
    }

    public String getKey() {
        return this.key;
    }

    public Localizable withReplacements(String... replacements) {
        this.replacements = Arrays.copyOf(replacements, replacements.length);
        this.locReplacements = null;
        return this;
    }

    public Localizable withReplacements(Localizable... replacements) {
        this.locReplacements = Arrays.copyOf(replacements, replacements.length);
        this.replacements = null;
        return this;
    }

    public Localizable withPrefix(String prefix) {
        this.prefix = fromNullableString(prefix);
        return this;
    }

    public Localizable withSuffix(String suffix) {
        this.suffix = fromNullableString(suffix);
        return this;
    }

    public String localizeIn(String locale, String... fallbacks) {
        return localizeIn(locale, false, fallbacks);
    }

    private String localizeIn(String locale, boolean recursive, String... fallbacks) {
        if ((getParent()).configs.containsKey(locale)) {
            Properties props = (getParent()).configs.get(locale);
            if (props.containsKey(getKey())) {
                String message = (String)props.get(getKey());
                if (this.replacements != null) {
                    for (int i = 0; i < this.replacements.length; i++)
                        message = message.replaceAll("%" + (i + 1), Matcher.quoteReplacement(this.replacements[i]));
                } else if (this.locReplacements != null) {
                    for (int i = 0; i < this.locReplacements.length; i++) {
                        String strRepl = this.locReplacements[i].localizeIn(locale, false);
                        message = message.replaceAll("%" + (i + 1), Matcher.quoteReplacement(strRepl));
                    }
                }
                return this.prefix + message + this.suffix;
            }
        }
        if (!recursive) {
            ArrayList<Object> fbList = Lists.newArrayList((Object[])fallbacks);
            for (int i = 0; i < fbList.size(); i++) {
                String fb = (String) fbList.get(i);
                if (LocaleManager.ALTERNATIVES.containsKey(fb))
                    for (String alt : LocaleManager.ALTERNATIVES.get(fb)) {
                        if (!fbList.contains(alt)) {
                            fbList.add(i + 1, alt);
                            i++;
                        }
                    }
            }
            if (LocaleManager.ALTERNATIVES.containsKey(locale))
                for (String alt : LocaleManager.ALTERNATIVES.get(locale)) {
                    if (!fbList.contains(alt))
                        fbList.add(0, alt);
                }
            fallbacks = new String[fbList.size()];
            fbList.toArray(fallbacks);
        }
        if (fallbacks.length > 0) {
            String[] newFallbacks = new String[fallbacks.length - 1];
            System.arraycopy(fallbacks, 1, newFallbacks, 0, newFallbacks.length);
            return localizeIn(fallbacks[0], true, newFallbacks);
        }
        if (!locale.equals(getParent().getDefaultLocale()))
            return localizeIn(getParent().getDefaultLocale(), true);
        return this.prefix + getKey() + this.suffix;
    }

    public String localize() {
        return localizeIn(getParent().getDefaultLocale());
    }

    public String localizeFor(CommandSender sender) {
        return (sender instanceof Player) ? localizeIn(getParent().getLocale((Player)sender)) : localize();
    }

    public void sendTo(CommandSender sender) {
        sender.sendMessage(localizeFor(sender));
    }

    public void broadcast() {
        for (Player player : getParent().getOnlinePlayers())
            sendTo(player);
        Bukkit.getLogger().info(localize());
    }

    public void broadcast(World... worlds) {
        for (World w : worlds) {
            for (Player player : w.getPlayers())
                sendTo(player);
        }
        Bukkit.getLogger().info(localize());
    }

    private String fromNullableString(String nullable) {
        return (nullable != null) ? nullable : "";
    }
}
