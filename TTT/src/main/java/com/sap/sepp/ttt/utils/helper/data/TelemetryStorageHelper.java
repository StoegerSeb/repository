package com.sap.sepp.ttt.utils.helper.data;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TelemetryStorageHelper: 14.04.2020 23:49 by sebip
*/

import com.sap.sepp.api.round.Round;
import org.bukkit.Tag;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TelemetryStorageHelper {
    private static final String STORE_FILE_NAME = "telemetry/rounds.dat";

    private static final String KEY_ROUND_DURATION = "dur";

    private static final String KEY_ROUND_RESULT = "res";

    private static final String KEY_ROUND_PLAYERS = "plc";

    public static void pushRound(Round round) {
        int duration = (Integer) round.getMetadata().get("duration").get();
        byte result = (Byte) round.getMetadata().get("result").get();
        int players = (Integer) round.getMetadata().get("players").get();
        File store = getStoreFile();
        List<CompoundTag> tags = store.exists() ? loadStore() : new ArrayList<>();
        Map<String, Tag> tagMap = new HashMap<>();
        tagMap.put("dur", new IntTag("dur", duration));
        tagMap.put("res", new ByteTag("res", result));
        tagMap.put("plc", new IntTag("plc", players));
        CompoundTag newTag = new CompoundTag(null, tagMap);
        tags.add(newTag);
        List<Tag> list = new ArrayList<>();
        for (Tag tag : tags)
            list.add(tag);
        ListTag listTag = new ListTag("root", CompoundTag.class, list);
        try (NBTOutputStream os = new NBTOutputStream(new FileOutputStream(store))) {
            if (!store.exists())
                Files.createFile(store.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
            os.writeTag((Tag)listTag);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to save telemetry data store to disk", ex);
        }
    }

    public static RoundSummaryStats getSummaryStats() {
        return new RoundSummaryStats(readAndPopStore());
    }

    private static List<CompoundTag> loadStore() {
        File store = getStoreFile();
        if (!store.exists())
            return new ArrayList<>();
        try (NBTInputStream is = new NBTInputStream(new FileInputStream(store))) {
            Tag tag = is.readTag();
            if (!(tag instanceof ListTag)) {
                is.close();
                Files.delete(store.toPath());
                throw new IllegalStateException("Root tag of telemetry data store is not a list!");
            }
            ListTag list = (ListTag)tag;
            List<CompoundTag> tagList = new ArrayList<>();
            for (Tag element : list.getValue()) {
                if (!(element instanceof CompoundTag)) {
                    TTTCore.log.warning("Found non-compound root tag in telemetry data store! Ignoring...");
                    continue;
                }
                tagList.add((CompoundTag)element);
            }
            return tagList;
        } catch (IOException ex) {
            throw new RuntimeException("Failed to read telemetry data store", ex);
        }
    }

    private static List<RoundSummary> readAndPopStore() {
        List<RoundSummary> rounds = new ArrayList<>();
        for (CompoundTag tag : loadStore()) {
            int duration = ((IntTag)tag.getValue().get("dur")).getValue().intValue();
            byte result = ((ByteTag)tag.getValue().get("res")).getValue().byteValue();
            int players = ((IntTag)tag.getValue().get("plc")).getValue().intValue();
            rounds.add(new RoundSummary(duration, result, players));
        }
        File store = getStoreFile();
        if (store.exists())
            try {
                Files.delete(store.toPath());
            } catch (IOException ex) {
                ex.printStackTrace();
                TTTCore.log.severe("Failed to delete telemetry database! Attempting to manually erase data...");
                try (FileOutputStream os = new FileOutputStream(store)) {
                    os.write(new byte[0]);
                } catch (IOException exc) {
                    throw new RuntimeException("Failed to erase telemetry database!", exc);
                }
            }
        return rounds;
    }

    private static File getStoreFile() {
        return new File(TTTCore.getPlugin().getDataFolder(), "telemetry/rounds.dat");
    }

    static class RoundSummary {
        private final int duration;

        private final byte result;

        private final int playerCount;

        RoundSummary(int duration, byte result, int playerCount) {
            this.duration = duration;
            this.result = result;
            this.playerCount = playerCount;
        }

        private int getDuration() {
            return this.duration;
        }

        private byte getResult() {
            return this.result;
        }

        private int getPlayerCount() {
            return this.playerCount;
        }
    }

    public static class RoundSummaryStats {
        private final int roundCount;

        private final float playerCount;

        private final float durationMean;

        private final float durationStdDev;

        private final int innoWins;

        private final int traitorWins;

        private final int forfeits;

        RoundSummaryStats(List<TelemetryStorageHelper.RoundSummary> rounds) {
            this.roundCount = rounds.size();
            int durationSum = 0;
            int playerSum = 0;
            int[] results = new int[3];
            for (TelemetryStorageHelper.RoundSummary round : rounds) {
                durationSum += round.getDuration();
                playerSum += round.getPlayerCount();
                results[round.getResult()] = results[round.getResult()] + 1;
            }
            this.durationMean = (this.roundCount > 0) ? (durationSum / this.roundCount) : 0.0F;
            this.playerCount = (playerSum > 0) ? (playerSum / this.roundCount) : 0.0F;
            if (this.roundCount > 0) {
                float stdDevSum = 0.0F;
                for (TelemetryStorageHelper.RoundSummary round : rounds)
                    stdDevSum = (float)(stdDevSum + Math.pow((round.getDuration() - this.durationMean), 2.0D));
                this.durationStdDev = (float)Math.sqrt((stdDevSum / (this.roundCount - 1)));
            } else {
                this.durationStdDev = 0.0F;
            }
            this.innoWins = results[0];
            this.traitorWins = results[1];
            this.forfeits = results[2];
        }

        public int getRoundCount() {
            return this.roundCount;
        }

        public float getMeanPlayerCount() {
            return this.playerCount;
        }

        public float getDurationMean() {
            return this.durationMean;
        }

        public float getDurationStdDev() {
            return this.durationStdDev;
        }

        public int getInnoWins() {
            return this.innoWins;
        }

        public int getTraitorWins() {
            return this.traitorWins;
        }

        public int getForfeits() {
            return this.forfeits;
        }
    }
}
