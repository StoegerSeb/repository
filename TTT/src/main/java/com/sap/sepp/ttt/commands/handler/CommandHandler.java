package com.sap.sepp.ttt.commands.handler;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommandHandler: 14.04.2020 23:28 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.CommandSender;

public abstract class CommandHandler {
    protected CommandSender sender;

    protected String[] args;

    public CommandHandler(CommandSender sender, String[] args) {
        this.sender = sender;
        this.args = args;
    }

    public abstract void handle();

    protected void printInvalidArgsError() {
        TTTCore.locale.getLocalizable("error.command.invalid-args").withPrefix(Color.ALERT).sendTo(this.sender);
    }
}
