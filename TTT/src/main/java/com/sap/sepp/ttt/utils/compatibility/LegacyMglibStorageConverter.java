package com.sap.sepp.ttt.utils.compatibility;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LegacyMglibStorageConverter: 14.04.2020 23:41 by sebip
*/

public final class LegacyMglibStorageConverter {
    public static void convertArenaStore() {
        File arenaStore = new File(TTTCore.getPlugin().getDataFolder(), "arenas.yml");
        if (arenaStore.exists()) {
            TTTCore.log.info("Converting legacy arena store - please wait");
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(arenaStore);
                int count = 0;
                for (String arenaId : yaml.getKeys(false)) {
                    ConfigurationSection cs = yaml.getConfigurationSection(arenaId);
                    String displayName = cs.contains("displayname") ? cs.getString("displayname") : arenaId;
                    if (!cs.contains("world")) {
                        TTTCore.log.warning("Arena with ID \"" + arenaId + "\" is missing required world attribute - cannot convert");
                        continue;
                    }
                    String world = cs.getString("world");
                    ArrayList<Location3D> spawnPoints = new ArrayList<>();
                    if (!cs.contains("spawns")) {
                        TTTCore.log.warning("Arena with ID \"" + arenaId + "\" does not contain spawn point definitions - cannot convert");
                        continue;
                    }
                    ConfigurationSection spawns = cs.getConfigurationSection("spawns");
                    for (String spawnOrdinal : spawns.getKeys(false)) {
                        ConfigurationSection spawn = spawns.getConfigurationSection(spawnOrdinal);
                        if (!spawn.contains("x") || !spawn.contains("y") || !spawn.contains("z")) {
                            TTTCore.log.warning("Spawn " + spawnOrdinal + " for arena with ID \"" + arenaId + "\" does not contain position data - cannot convert");
                            continue;
                        }
                        double x = spawn.getDouble("x", Double.NaN);
                        double y = spawn.getDouble("y", Double.NaN);
                        double z = spawn.getDouble("z", Double.NaN);
                        if (x != x || y != y || z != z) {
                            TTTCore.log.warning("Spawn " + spawnOrdinal + " for arena with ID \"" + arenaId + "\" contains malformed location data - cannot convert");
                            continue;
                        }
                        spawnPoints.add(new Location3D(world, x, y, z));
                    }
                    if (spawnPoints.isEmpty()) {
                        TTTCore.log.warning("Arena with ID \"" + arenaId + "\" does not contain spawn point definitions - cannot convert");
                        continue;
                    }
                    int j = 1;
                    String finalId = arenaId;
                    while (TTTCore.mg.getArena(finalId).isPresent()) {
                        finalId = finalId + "-converted" + ((j > 1) ? (String)Integer.valueOf(j) : "");
                        j++;
                    }
                    if (arenaId.contains("-converted"))
                        TTTCore.log.warning("Converted arena \"" + finalId + "\" with new ID \"" + arenaId + "\"");
                    Location3D[] spawnArr = new Location3D[spawnPoints.size()];
                    spawnPoints.toArray(spawnArr);
                    Arena arena = ((Arena.Builder)TTTCore.mg.createBuilder(Arena.class)).id(finalId).displayName(displayName).spawnPoints(spawnArr).boundary(Boundary.INFINITE).build();
                    count++;
                }
                TTTCore.log.info("Successfully converted " + count + " legacy arenas");
                try {
                    Files.move(arenaStore, new File(TTTCore.getPlugin().getDataFolder(), "arenas.yml.old"));
                } catch (IOException ex) {
                    TTTCore.log.severe("Failed to rename old arenas.yml file - you may need to do this manually");
                    ex.printStackTrace();
                }
            } catch (InvalidConfigurationException|IOException ex) {
                ex.printStackTrace();
                TTTCore.log.severe("Failed to convert legacy arena store!");
            }
        }
    }

    public static void convertLobbyStore() {
        File arenaStore = new File(TTTCore.getPlugin().getDataFolder(), "lobbies.yml");
        if (arenaStore.exists()) {
            TTTCore.log.info("Converting legacy lobby sign store - please wait");
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(arenaStore);
                int count = 0;
                label54: for (String lobbyOrdinal : yaml.getKeys(false)) {
                    ConfigurationSection cs = yaml.getConfigurationSection(lobbyOrdinal);
                    String[] required = { "world", "x", "y", "z", "arena", "type", "number" };
                    for (String str : required) {
                        if (!cs.contains(str)) {
                            TTTCore.log.warning("Lobby sign at ordinal " + lobbyOrdinal + " is missing required attribute \"" + str + "\" - cannot convert");
                            continue label54;
                        }
                    }
                    String world = cs.getString("world");
                    int x = cs.getInt("x", -2147483648);
                    int y = cs.getInt("y", -2147483648);
                    int z = cs.getInt("z", -2147483648);
                    String arenaId = cs.getString("arena");
                    String type = cs.getString("type");
                    int number = cs.getInt("number", -2147483648);
                    if (x == Integer.MIN_VALUE || y == Integer.MIN_VALUE || z == Integer.MIN_VALUE || number == Integer.MIN_VALUE) {
                        TTTCore.log.warning("Malformed location data for lobby sign at ordinal " + lobbyOrdinal + " - cannot convert");
                        continue;
                    }
                    Optional<Arena> arena = TTTCore.mg.getArena(arenaId);
                    if (!arena.isPresent()) {
                        TTTCore.log.warning("Cannot get arena \"" + arenaId + "\" for lobby sign at ordinal " + lobbyOrdinal + " - cannot convert");
                        continue;
                    }
                    Location l = new Location(Bukkit.getWorld(world), x, y, z);
                    if (!MaterialHelper.instance().isWallSign(l.getBlock().getType()) &&
                            !MaterialHelper.instance().isStandingSign(l.getBlock().getType()))
                        l.getBlock().setType((MaterialHelper.instance()).OAK_WALL_SIGN);
                    Location3D loc = new Location3D(world, x, y, z);
                    switch (type) {
                        case "STATUS":
                            ((Arena)arena.get()).createStatusLobbySign(loc);
                            break;
                        case "PLAYERS":
                            ((Arena)arena.get()).createChallengerListingLobbySign(loc, number);
                            break;
                        default:
                            TTTCore.log.warning("Invalid type \"" + type + "\" for lobby sign at ordinal " + lobbyOrdinal + " - cannot convert");
                            break;
                    }
                    count++;
                }
                TTTCore.log.info("Successfully converted " + count + " legacy lobby signs");
                try {
                    Files.move(arenaStore, new File(TTTCore.getPlugin().getDataFolder(), "lobbies.yml.old"));
                } catch (IOException ex) {
                    TTTCore.log.severe("Failed to rename old lobbies.yml file - you may need to do this manually");
                    ex.printStackTrace();
                }
            } catch (InvalidConfigurationException|IOException ex) {
                ex.printStackTrace();
                TTTCore.log.severe("Failed to convert legacy lobby store!");
            }
        }
    }
}