package com.sap.sepp.ttt;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TTTBootstrap: 14.04.2020 23:41 by sebip
*/

import com.sap.sepp.ttt.commands.SpecialCommandManager;
import com.sap.sepp.ttt.lib.LocaleManager;
import com.sap.sepp.ttt.listener.ListenerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class TTTBootstrap extends JavaPlugin {
    public static TTTBootstrap INSTANCE;

    public static LocaleManager locale;

    public static boolean STEEL = true;

    public TTTBootstrap() {
        INSTANCE = this;
    }

    public void onEnable() {
        STEEL = Bukkit.getPluginManager().isPluginEnabled("Steel");
        locale = new LocaleManager(this);
        if (!STEEL) {
            fail();
            return;
        }
        (new TTTCore(this, locale)).initialize();
    }

    public void onDisable() {
        if (STEEL)
            TTTCore.getInstance().deinitialize();
    }

    public File getFile() {
        return super.getFile();
    }

    public void failMinor() {
        locale.setDefaultLocale(getConfig().getString("locale"));
        getLogger().warning(locale.getLocalizable("error.plugin.flint")
                .withReplacements("4").localize());
        fail();
    }

    public void failMajor() {
        locale.setDefaultLocale(getConfig().getString("locale"));
        getLogger().warning(locale.getLocalizable("error.plugin.flint.major")
                .withReplacements(Bukkit.getPluginManager().getPlugin("Steel").getDescription().getVersion(), "1").localize());
        fail();
    }

    private void fail() {
        ListenerManager.registerSpecialEventListeners();
        getCommand("ttt").setExecutor(new SpecialCommandManager());
    }
}