package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CreateArenaCommand: 14.04.2020 23:29 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.listener.wizard.WizardListener;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateArenaCommand extends CommandHandler {
    public CreateArenaCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (!WizardListener.wizards.containsKey(((Player)this.sender).getUniqueId())) {
            WizardListener.wizards.put(((Player)this.sender).getUniqueId(), 0);
            WizardListener.wizardInfo.put(((Player)this.sender).getUniqueId(), new Object[4]);
            TTTCore.locale.getLocalizable("info.personal.arena.create.welcome").withPrefix(Color.INFO).sendTo(this.sender);
            TTTCore.locale.getLocalizable("info.personal.arena.create.exit-note").withPrefix(Color.INFO)
                    .withReplacements(TTTCore.locale.getLocalizable("info.personal.arena.create.cancel-keyword")
                            .withPrefix(Color.EM).withSuffix(Color.INFO)).sendTo(this.sender);
        } else {
            TTTCore.locale.getLocalizable("error.arena.create.already").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }
}
