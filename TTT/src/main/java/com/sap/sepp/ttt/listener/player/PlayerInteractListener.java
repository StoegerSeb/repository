package com.sap.sepp.ttt.listener.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerInteractListener: 14.04.2020 23:37 by sebip
*/

public class PlayerInteractListener implements Listener {
    private static final long INTERACT_COOLDOWN = 100L;

    private static final Map<UUID, Long> LAST_INTERACT_MAP = new HashMap<>();

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (TTTCore.mg.getChallenger(event.getPlayer().getUniqueId()).isPresent()) {
            Challenger ch = (Challenger)TTTCore.mg.getChallenger(event.getPlayer().getUniqueId()).get();
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (event.getClickedBlock().getType() == Material.ENDER_CHEST ||
                        MaterialHelper.instance().isBed(event.getClickedBlock().getType())) {
                    event.setCancelled(true);
                    return;
                }
                if (LAST_INTERACT_MAP.containsKey(event.getPlayer().getUniqueId()) &&
                        System.currentTimeMillis() - ((Long)LAST_INTERACT_MAP.get(event.getPlayer().getUniqueId())).longValue() < 100L)
                    return;
                InteractHelper.handleEvent(event, ch);
                LAST_INTERACT_MAP.put(event.getPlayer().getUniqueId(), Long.valueOf(System.currentTimeMillis()));
            }
        }
        InteractHelper.handleGun(event);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        for (HumanEntity he : event.getViewers()) {
            Player p = (Player)he;
            Optional<Challenger> ch = TTTCore.mg.getChallenger(p.getUniqueId());
            if (ch.isPresent() && ((Challenger)ch.get()).getMetadata().containsKey("searchingBody"))
                event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onInventoryClose(InventoryCloseEvent event) {
        Optional<Challenger> ch = TTTCore.mg.getChallenger(event.getPlayer().getUniqueId());
        if (ch.isPresent() && ((Challenger)ch.get()).getMetadata().containsKey("searchingBody"))
            ((Challenger)ch.get()).getMetadata().remove("searchingBody");
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        if (TTTCore.mg.getChallenger(event.getPlayer().getUniqueId()).isPresent())
            event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        if (TTTCore.mg.getChallenger(event.getPlayer().getUniqueId()).isPresent()) {
            event.setCancelled(true);
            TTTCore.locale.getLocalizable("info.personal.status.no-drop").withPrefix(Color.ALERT)
                    .sendTo((CommandSender)event.getPlayer());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Optional<Challenger> ch = TTTCore.mg.getChallenger(event.getPlayer().getUniqueId());
        if (ch.isPresent() && (
                (Challenger)ch.get()).isSpectating()) {
            boolean spec = ((Challenger)ch.get()).getMetadata().containsKey("pureSpectator");
            Localizable prefixLabel = TTTCore.locale.getLocalizable(spec ? "fragment.spectator" : "fragment.dead");
            ChatColor color = spec ? ChatColor.GRAY : ChatColor.RED;
            String prefix = color + "[" + prefixLabel.localize().toUpperCase() + "]" + ChatColor.RESET;
            event.setFormat(prefix + event.getFormat());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
        Optional<Challenger> ch = TTTCore.mg.getChallenger(event.getPlayer().getUniqueId());
        if (ch.isPresent() && ((Challenger)ch.get()).getMetadata().containsKey("watchGm")) {
            event.setCancelled(true);
            ((Challenger)ch.get()).getMetadata().remove("watchGm");
        }
    }
}