package com.sap.sepp.ttt.commands.handler.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RespawnCommand: 14.04.2020 23:32 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.metadata.Metadata;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.scoreboard.ScoreboardManager;
import com.sap.sepp.ttt.utils.Body;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.gamemode.RoundHelper;
import com.sap.sepp.ttt.utils.helper.platform.LocationHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class RespawnCommand extends CommandHandler {
    public RespawnCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        String name = this.args[1];
        Player pl = Bukkit.getPlayer(name);
        if (pl == null) {
            TTTCore.locale.getLocalizable("error.round.player-offline").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        Optional<Challenger> ch = TTTCore.mg.getChallenger(pl.getUniqueId());
        if (!ch.isPresent()) {
            TTTCore.locale.getLocalizable("error.round.no-such-player").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        if (!ch.get().isSpectating() || ch
                .get().getMetadata().containsKey("pureSpectator") ||
                !ch.get().getMetadata().containsKey("body")) {
            TTTCore.locale.getLocalizable("error.round.not-dead").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        Location loc = LocationHelper.convert(((Body) ch.get().getMetadata().get("body").get()).getLocation());
        loc.getBlock().setType(Material.AIR);
        pl.teleport(loc);
        Metadata meta = ch.get().getMetadata();
        Body body = (Body)meta.get("body").orNull();
        meta.remove("body");
        meta.remove("bodyFound");
        if (body != null) {
            List<Body> bodies = (List<Body>) ch.get().getRound().getMetadata().get("bodies").get();
            bodies.remove(body);
            ch.get().getRound().getMetadata().set("bodies", bodies);
        }
        ch.get().setSpectating(false);
        RoundHelper.distributeItems(ch.get());
        pl.setHealth(pl.getMaxHealth());
        pl.setFoodLevel(20);
        ((ScoreboardManager) ch.get().getRound().getMetadata().get("scoreboardManager").get())
                .updateEntry((Challenger)ch.get());
        TTTCore.locale.getLocalizable("info.personal.respawn").withPrefix(Color.INFO).sendTo(pl);
        TTTCore.locale.getLocalizable("info.personal.respawn.other").withPrefix(Color.INFO)
                .withReplacements(ch.get().getName()).sendTo(this.sender);
    }
}
