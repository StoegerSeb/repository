package com.sap.sepp.ttt.commands;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommandManager: 14.04.2020 23:26 by sebip
*/

import com.google.common.collect.ImmutableMap;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.commands.handler.arena.*;
import com.sap.sepp.ttt.commands.handler.misc.DefaultCommand;
import com.sap.sepp.ttt.commands.handler.misc.XyzzyCommand;
import com.sap.sepp.ttt.commands.handler.player.*;
import com.sap.sepp.ttt.commands.handler.round.*;
import com.sap.sepp.ttt.commands.handler.use.JoinCommand;
import com.sap.sepp.ttt.commands.handler.use.LeaveCommand;
import com.sap.sepp.ttt.commands.handler.use.ListArenasCommand;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.HelpCommand;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommandManager implements CommandExecutor {
    public static final ImmutableMap<String, CommandRef> commands;

    static {
        Map<String, CommandRef> map = new LinkedHashMap<>();
        addRef(map, "join", (Class) JoinCommand.class, "use", "[arena name]", 1, false);
        addRef(map, "leave", (Class) LeaveCommand.class, "use", "", 1, false);
        addRef(map, "listarenas", (Class) ListArenasCommand.class, "use", "", 1, true);
        addRef(map, "arenainfo", (Class) ArenaInfoCommand.class, "admin", "[arena name]", 1, true);
        addRef(map, "createarena", (Class) CreateArenaCommand.class, "superadmin", "", 1, false, "carena");
        addRef(map, "import", (Class) ImportCommand.class, "superadmin", "[arena name]", 2, true);
        addRef(map, "removearena", (Class) RemoveArenaCommand.class, "superadmin", "[arena name]", 2, true, "rarena");
        addRef(map, "listspawns", (Class) ListSpawnsCommand.class, "superadmin", "[arena name]", 2, true);
        addRef(map, "addspawn", (Class)AddSpawnCommand.class, "superadmin", "[arena name] {[x] [y] [z]}", 2, true);
        addRef(map, "removespawn", (Class)RemoveSpawnCommand.class, "superadmin", "[arena name] [index]|[[x] [y] [z]]", 2, true);
        addRef(map, "role", (Class) RoleCommand.class, "admin", "[player name]", 2, true);
        addRef(map, "slay", (Class) SlayCommand.class, "admin", "[player name]", 2, true);
        addRef(map, "respawn", (Class) RespawnCommand.class, "admin", "[player name]", 2, true);
        addRef(map, "kick", (Class) KickCommand.class, "admin", "[player name]", 2, true);
        addRef(map, "ban", (Class) BanCommand.class, "admin", "[player name]", 2, true);
        addRef(map, "pardon", (Class)PardonCommand.class, "admin", "[player name] {minutes}", 2, true);
        addRef(map, "roles", (Class) RolesCommand.class, "admin", "[arena name]", 2, true);
        addRef(map, "prepare", (Class) PrepareCommand.class, "admin", "[arena name]", 2, true);
        addRef(map, "start", (Class) StartCommand.class, "admin", "[arena name]", 2, true);
        addRef(map, "end", (Class) EndCommand.class, "admin", "[arena name] {victor (t/i)}", 2, true);
        addRef(map, "forceend", (Class) ForceEndCommand.class, "admin", "[arena name] {victor (t/i)}", 2, true);
        addRef(map, "help", (Class) HelpCommand.class, null, "{command}", 1, true, "?");
        addRef(map, "xyzzy", (Class) XyzzyCommand.class, null, "", 1, true, true);
        commands = ImmutableMap.copyOf(map);
    }

    private static void addRef(Map<String, CommandRef> map, String cmd, Class<? extends CommandHandler> clazz, String perm, String usage, int minArgs, boolean consoleAllowed, boolean hidden, String... aliases) {
        cmd = cmd.toLowerCase();
        CommandRef cr = new CommandRef(cmd, clazz, TTTCore.locale.getLocalizable("info.command.desc." + cmd), (perm != null) ? ("ttt." + perm) : null, "/ttt " + cmd + " " + usage, minArgs, consoleAllowed, hidden, aliases);
        map.put(cmd, cr);
        for (String alias : aliases)
            map.put(alias, cr);
    }

    private static void addRef(Map<String, CommandRef> map, String cmd, Class<? extends CommandHandler> clazz, String perm, String usage, int minArgs, boolean consoleAllowed, String... aliases) {
        addRef(map, cmd, clazz, perm, usage, minArgs, consoleAllowed, false, aliases);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("ttt")) {
            if (args.length == 0) {
                (new DefaultCommand(sender, args)).handle();
                return true;
            }
            String subCmd = args[0].toLowerCase();
            if (commands.containsKey(subCmd)) {
                commands.get(subCmd).invoke(sender, args);
            } else {
                TTTCore.locale.getLocalizable("error.command.invalid-args")
                        .withPrefix(Color.ALERT).sendTo(sender);
            }
            return true;
        }
        return false;
    }
}
