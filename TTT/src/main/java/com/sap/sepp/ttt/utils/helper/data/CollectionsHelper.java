package com.sap.sepp.ttt.utils.helper.data;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CollectionsHelper: 14.04.2020 23:48 by sebip
*/

import java.util.ArrayList;
import java.util.List;

public final class CollectionsHelper {
    public static String prettyList(List<?> list) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).toString());
            if (i < list.size() - 2) {
                sb.append(", ");
            } else if (i == list.size() - 2) {
                if (list.size() > 2)
                    sb.append(",");
                sb.append(" and ");
            }
        }
        return sb.toString();
    }

    public static List<String> formatLore(String str) {
        int lineLength = 36;
        List<String> list = new ArrayList<>();
        StringBuilder currentBuilder = new StringBuilder();
        for (String s : str.split(" ")) {
            if (currentBuilder.length() + s.trim().length() + 1 > 36) {
                list.add(currentBuilder.toString());
                currentBuilder = new StringBuilder();
            } else if (!currentBuilder.toString().isEmpty()) {
                currentBuilder.append(" ");
            }
            currentBuilder.append(s.trim());
        }
        String current = currentBuilder.toString();
        if (!current.isEmpty())
            list.add(current);
        return list;
    }
}
