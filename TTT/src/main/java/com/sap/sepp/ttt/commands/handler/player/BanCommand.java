package com.sap.sepp.ttt.commands.handler.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class BanCommand: 14.04.2020 23:31 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.data.DataVerificationHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.BanHelper;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;

public class BanCommand extends CommandHandler {
    public BanCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        String name = this.args[1];
        int time = -1;
        if (this.args.length > 2)
            if (DataVerificationHelper.isInt(this.args[2])) {
                time = Integer.parseInt(this.args[2]);
            } else {
                TTTCore.locale.getLocalizable("error.admin.ban.invalid-time").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
        try {
            OfflinePlayer pl = Bukkit.getOfflinePlayer(name);
            if (pl == null) {
                TTTCore.locale.getLocalizable("error.admin.ban.invalid-player").withPrefix(Color.ALERT)
                        .withReplacements(name).sendTo(this.sender);
                return;
            }
            BanHelper.ban(pl.getUniqueId(), time);
            if (time == -1) {
                if (pl.isOnline())
                    TTTCore.locale.getLocalizable("info.personal.ban.perm").withPrefix(Color.ALERT)
                            .sendTo(pl.getPlayer());
                TTTCore.locale.getLocalizable("info.personal.ban.other.perm").withPrefix(Color.INFO)
                        .withReplacements(name).sendTo(this.sender);
            } else {
                if (pl.isOnline())
                    TTTCore.locale.getLocalizable("info.personal.ban.temp").withPrefix(Color.ALERT)
                            .withReplacements(TTTCore.locale.getLocalizable("fragment.minutes" + ((time == 1) ? ".singular" : ""))
                                    .withReplacements(time + "")).sendTo(pl.getPlayer());
                TTTCore.locale.getLocalizable("info.personal.ban.other.temp").withPrefix(Color.INFO)
                        .withReplacements(pl.getName(), TTTCore.locale
                                .getLocalizable("fragment.minutes" + ((time == 1) ? ".singular" : ""))
                                .withReplacements(time + "").localizeFor(this.sender)).sendTo(this.sender);
            }
        } catch (InvalidConfigurationException |java.io.IOException ex) {
            TTTCore.locale.getLocalizable("error.plugin.ban").withPrefix(Color.ALERT).sendTo(this.sender);
            ex.printStackTrace();
        }
    }
}