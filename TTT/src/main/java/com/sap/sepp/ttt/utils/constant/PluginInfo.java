package com.sap.sepp.ttt.utils.constant;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PluginInfo: 14.04.2020 23:47 by sebip
*/

public class PluginInfo {
    public static final int MIN_FLINT_VERSION = 4;

    public static final int FLINT_MAJOR_VERSION = 1;

    public static final int TTT_CURSEFORGE_PROJECT_ID = 52474;

    public static final int STEEL_CURSEFORGE_PROJECT_ID = 95203;
}
