package com.sap.sepp.ttt.utils.helper.platform;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class MaterialHelper: 14.04.2020 23:54 by sebip
*/

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.sap.sepp.ttt.TTTCore;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;

public abstract class MaterialHelper {
    private static final MaterialHelper INSTANCE;

    public Material CLOCK;

    public Material IRON_HORSE_ARMOR;

    public Material LEAD;

    public Material OAK_WALL_SIGN;

    static {
        if (TTTCore.getInstance().isLegacyMinecraftVersion()) {
            INSTANCE = new LegacyMaterialHelper();
        } else {
            INSTANCE = new ModernMaterialHelper();
        }
    }

    public abstract ItemStack createRoleWool(String paramString);

    public abstract boolean isLiquid(Material paramMaterial);

    public abstract boolean isWallSign(Material paramMaterial);

    public abstract boolean isStandingSign(Material paramMaterial);

    public abstract boolean isBed(Material paramMaterial);

    public static MaterialHelper instance() {
        return INSTANCE;
    }

    private static class LegacyMaterialHelper extends MaterialHelper {
        private static Material BED_BLOCK;
        private static Material IRON_BARDING;
        private static Material LEASH;
        private static Material SIGN_POST;
        private static Material STATIONARY_LAVA;
        private static Material STATIONARY_WATER;
        private static Material WALL_SIGN;
        private static Material WATCH;
        private static Material WOOL;

        static {
            for (Field field : LegacyMaterialHelper.class.getDeclaredFields()) {
                if (field.getType() == Material.class)
                    try {
                        field.set(null, Material.valueOf(field.getName()));
                    } catch (IllegalAccessException ex) {
                        throw new RuntimeException("Failed to set legacy material field " + field.getName() + ".");
                    }
            }
        }

        private static final ImmutableSet<Material> FLUIDS = ImmutableSet.of(Material.WATER, Material.LAVA, STATIONARY_WATER, STATIONARY_LAVA);

        private static final ImmutableMap<String, Integer> WOOL_DURABILITY_MAP = ImmutableMap.of("detective",
                11, "innocent",
                5, "traitor",
                14);

        public boolean isBed(Material material) {
            return (material == BED_BLOCK);
        }

        public boolean isStandingSign(Material material) {
            return (material == SIGN_POST);
        }

        public boolean isWallSign(Material material) {
            return (material == WALL_SIGN);
        }

        public boolean isLiquid(Material material) {
            return FLUIDS.contains(material);
        }

        public ItemStack createRoleWool(String role) {
            ItemStack roleId = new ItemStack(WOOL, 1);
            short durability = ((Integer)WOOL_DURABILITY_MAP.getOrDefault(role, -1)).shortValue();
            if (durability == -1)
                throw new AssertionError("Bad role \"" + role + "\"");
            roleId.setDurability(durability);
            return roleId;
        }

        private LegacyMaterialHelper() {}
    }

    private static class ModernMaterialHelper extends MaterialHelper {
        private static final ImmutableMap<String, Material> WOOL_MAP = ImmutableMap.of("detective", Material.BLUE_WOOL, "innocent", Material.GREEN_WOOL, "traitor", Material.RED_WOOL);

        public boolean isBed(Material material) {
            return material.createBlockData() instanceof org.bukkit.block.data.type.Bed;
        }

        public boolean isStandingSign(Material material) {
            return (material == Material.ACACIA_SIGN || material == Material.BIRCH_SIGN || material == Material.DARK_OAK_SIGN || material == Material.JUNGLE_SIGN || material == Material.OAK_SIGN || material == Material.SPRUCE_SIGN);
        }

        public boolean isWallSign(Material material) {
            return (material == Material.ACACIA_WALL_SIGN || material == Material.BIRCH_WALL_SIGN || material == Material.DARK_OAK_WALL_SIGN || material == Material.JUNGLE_WALL_SIGN || material == Material.OAK_WALL_SIGN || material == Material.SPRUCE_WALL_SIGN);
        }

        public boolean isLiquid(Material material) {
            return (material == Material.WATER || material == Material.LAVA);
        }

        public ItemStack createRoleWool(String role) {
            Material wool = (Material)WOOL_MAP.get(role);
            if (wool == null)
                throw new AssertionError("Bad role \"" + role + "\"");
            return new ItemStack(wool, 1);
        }

        private ModernMaterialHelper() {}
    }
}