package com.sap.sepp.ttt.listener.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class WizardListener: 14.04.2020 23:38 by sebip
*/

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Text;
import com.sap.sepp.ttt.utils.helper.platform.LocationHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class WizardListener implements Listener {
    private static final long INTERACT_COOLDOWN = 100L;

    private static final Map<UUID, Long> LAST_INTERACT_MAP = new HashMap<>();

    public static BiMap<UUID, Integer> wizards = HashBiMap.create();

    public static BiMap<UUID, Object[]> wizardInfo = HashBiMap.create();

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (wizards.containsKey(event.getPlayer().getUniqueId())) {
            int stage = wizards.get(event.getPlayer().getUniqueId());
            if (event.getMessage().equalsIgnoreCase(TTTCore.locale
                    .getLocalizable("info.personal.arena.create.cancel-keyword")
                    .localizeFor(event.getPlayer()))) {
                event.setCancelled(true);
                wizards.remove(event.getPlayer().getUniqueId());
                wizardInfo.remove(event.getPlayer().getUniqueId());
                event.getPlayer().sendMessage(Text.DIVIDER);
                TTTCore.locale.getLocalizable("info.personal.arena.create.cancelled").withPrefix(Color.ALERT)
                        .sendTo(event.getPlayer());
                return;
            }
            event.setCancelled(true);
            switch (stage) {
                case 0:
                    if (!TTTCore.mg.getArena(event.getMessage()).isPresent()) {
                        if (event.getMessage().contains(".") || event.getMessage().contains(" ")) {
                            TTTCore.locale.getLocalizable("error.arena.create.invalid-id")
                                    .withPrefix(Color.ALERT).sendTo(event.getPlayer());
                        } else {
                            increment(event.getPlayer());
                            wizardInfo.get(event.getPlayer().getUniqueId())[0] = event.getMessage();
                            event.getPlayer().sendMessage(Text.DIVIDER);
                            TTTCore.locale.getLocalizable("info.personal.arena.create.id")
                                    .withPrefix(Color.INFO)
                                    .withReplacements(Color.EM + event.getMessage().toLowerCase() + Color.INFO).sendTo(event.getPlayer());
                        }
                    } else {
                        TTTCore.locale.getLocalizable("error.arena.create.id-already-exists")
                                .withPrefix(Color.ALERT).sendTo(event.getPlayer());
                    }
                    return;
                case 3:
                    if (event.getMessage().equalsIgnoreCase(TTTCore.locale
                            .getLocalizable("info.personal.arena.create.ok-keyword")
                            .localizeFor(event.getPlayer()))) {
                        Object[] info = wizardInfo.get(event.getPlayer().getUniqueId());
                        Boundary boundary = new Boundary((Location3D)info[1], (Location3D)info[2]);
                        Location3D spawn = LocationHelper.convert(event.getPlayer().getLocation());
                        if (!event.getPlayer().getWorld().getName().equals((
                                (Location3D) wizardInfo.get(event.getPlayer().getUniqueId())[1])
                                .getWorld().get()) ||
                                !boundary.contains(spawn)) {
                            TTTCore.locale.getLocalizable("error.arena.create.bad-spawn").withPrefix(Color.ALERT)
                                    .sendTo(event.getPlayer());
                        } else {
                            ((Arena.Builder)TTTCore.mg.createBuilder(Arena.class)).id((String)info[0]).spawnPoints(new Location3D[] { spawn }).boundary(boundary).build();
                            ArenaHelper.updateShuffledArenas();
                            event.getPlayer().sendMessage(Text.DIVIDER);
                            TTTCore.locale.getLocalizable("info.personal.arena.create.success").withPrefix(Color.INFO)
                                    .withReplacements(Color.EM + "/ttt join " + ((String)info[0]).toLowerCase() + Color.INFO).sendTo(event.getPlayer());
                            wizards.remove(event.getPlayer().getUniqueId());
                            wizardInfo.remove(event.getPlayer().getUniqueId());
                        }
                        return;
                    }
                    break;
            }
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (LAST_INTERACT_MAP.containsKey(event.getPlayer().getUniqueId()) &&
                System.currentTimeMillis() - LAST_INTERACT_MAP.get(event.getPlayer().getUniqueId()).longValue() < 100L)
            return;
        LAST_INTERACT_MAP.put(event.getPlayer().getUniqueId(), Long.valueOf(System.currentTimeMillis()));
        if ((event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) &&
                wizards.containsKey(event.getPlayer().getUniqueId())) {
            int stage = wizards.get(event.getPlayer().getUniqueId()).intValue();
            event.setCancelled(true);
            Block c = event.getClickedBlock();
            switch (stage) {
                case 1:
                    increment(event.getPlayer());
                    wizardInfo.get(event.getPlayer().getUniqueId())[1] = new Location3D(c
                            .getWorld().getName(), c.getX(), 0.0D, c.getZ());
                    event.getPlayer().sendMessage(Text.DIVIDER);
                    TTTCore.locale.getLocalizable("info.personal.arena.create.bound-1")
                            .withPrefix(Color.INFO)
                            .withReplacements(Color.EM + "(x=" + c.getX() + ", z=" + c.getZ() + ")" + Color.INFO).sendTo((CommandSender)event.getPlayer());
                    return;
                case 2:
                    if (c.getWorld().getName().equals((
                            (Location3D) wizardInfo.get(event.getPlayer().getUniqueId())[1])
                            .getWorld().get())) {
                        increment(event.getPlayer());
                        wizardInfo.get(event.getPlayer().getUniqueId())[2] = new Location3D(c
                                .getWorld().getName(), c.getX(), c.getWorld().getMaxHeight(), c
                                .getZ());
                        event.getPlayer().sendMessage(Text.DIVIDER);
                        TTTCore.locale.getLocalizable("info.personal.arena.create.bound-2")
                                .withPrefix(Color.INFO)
                                .withReplacements(Color.EM + "(x=" + c.getX() + ", z=" + c.getZ() + ")" + Color.INFO, Color.EM + TTTCore.locale

                                        .getLocalizable("info.personal.arena.create.ok-keyword")
                                        .localizeFor((CommandSender)event.getPlayer()) + Color.INFO).sendTo((CommandSender)event.getPlayer());
                    } else {
                        TTTCore.locale.getLocalizable("error.arena.create.bad-bound")
                                .withPrefix(Color.ALERT).sendTo((CommandSender)event.getPlayer());
                    }
                    return;
            }
            event.setCancelled(false);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (wizards.containsKey(event.getPlayer().getUniqueId())) {
            wizards.remove(event.getPlayer().getUniqueId());
            wizardInfo.remove(event.getPlayer().getUniqueId());
        }
    }

    private void increment(Player player) {
        wizards.put(player.getUniqueId(), Integer.valueOf(wizards.get(player.getUniqueId()).intValue() + 1));
    }

    private class Stage {
        private static final int WIZARD_ID = 0;

        private static final int WIZARD_FIRST_BOUND = 1;

        private static final int WIZARD_SECOND_BOUND = 2;

        private static final int WIZARD_SPAWN_POINT = 3;
    }
}
