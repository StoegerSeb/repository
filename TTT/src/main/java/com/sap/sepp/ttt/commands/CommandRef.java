package com.sap.sepp.ttt.commands;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommandRef: 14.04.2020 23:27 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.CommandSender;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class CommandRef {
    private String cmd;

    private Constructor<? extends CommandHandler> constructor;

    private Localizable description;

    private String permission;

    private String usage;

    private int argCount;

    private boolean consoleAllowed;

    private boolean hidden;

    private String[] aliases;

    public CommandRef(String cmd, Class<? extends CommandHandler> handlerClass, Localizable desc, String perm, String usage, int argCount, boolean consoleAllowed, boolean hidden, String... aliases) {
        this.cmd = cmd;
        try {
            this.constructor = handlerClass.getConstructor(new Class[] { CommandSender.class, String[].class });
        } catch (NoSuchMethodException ex) {
            throw new AssertionError(ex);
        }
        this.description = desc;
        this.permission = perm;
        this.usage = usage;
        this.argCount = argCount;
        this.consoleAllowed = consoleAllowed;
        this.hidden = hidden;
        this.aliases = aliases;
    }

    public CommandRef(String cmd, Class<? extends CommandHandler> handlerClass, Localizable desc, String perm, String usage, int argCount, boolean consoleAllowed, String... aliases) {
        this(cmd, handlerClass, desc, perm, usage, argCount, consoleAllowed, false, aliases);
    }

    public String getLabel() {
        return this.cmd;
    }

    public Constructor<? extends CommandHandler> getHandlerConstructor() {
        return this.constructor;
    }

    public Localizable getDescription() {
        return this.description;
    }

    public String getPermission() {
        return this.permission;
    }

    public String getUsage() {
        return this.usage;
    }

    public int getArgCount() {
        return this.argCount;
    }

    public boolean isConsoleAllowed() {
        return this.consoleAllowed;
    }

    public boolean isHidden() {
        return this.hidden;
    }

    public String[] getAliases() {
        return this.aliases;
    }

    public void invoke(CommandSender sender, String[] args) {
        if (!doAssertions(sender, args))
            return;
        try {
            ((CommandHandler)this.constructor.newInstance(new Object[] { sender, args })).handle();
        } catch (InstantiationException|IllegalAccessException ex) {
            throw new AssertionError(ex);
        } catch (InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    private boolean doAssertions(CommandSender sender, String[] args) {
        return (assertPermission(sender) && assertPlayer(sender) && assertArgumentCount(sender, args));
    }

    private boolean assertPermission(CommandSender sender) {
        if (getPermission() != null && !sender.hasPermission(getPermission())) {
            TTTCore.locale.getLocalizable("error.perms.generic").withPrefix(Color.ALERT)
                    .sendTo(sender);
            return false;
        }
        return true;
    }

    private boolean assertPlayer(CommandSender sender) {
        if (!isConsoleAllowed() && !(sender instanceof org.bukkit.entity.Player)) {
            TTTCore.locale.getLocalizable("error.command.ingame").withPrefix(Color.ALERT).sendTo(sender);
            return false;
        }
        return true;
    }

    private boolean assertArgumentCount(CommandSender sender, String[] args) {
        if (args.length < getArgCount()) {
            TTTCore.locale.getLocalizable("error.command.too-few-args").withPrefix(Color.ALERT)
                    .sendTo(sender);
            return false;
        }
        return true;
    }
}
