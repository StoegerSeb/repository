package com.sap.sepp.ttt.lib;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TitleUtil: 14.04.2020 23:55 by sebip
*/

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class TitleUtil {
    private static Throwable throwable;

    private static final String VERSION_STRING;

    private static final boolean TITLE_SUPPORT;

    private static Field entityPlayer_playerConnection;

    private static Constructor<?> packetPlayOutTitle_init_LL;

    private static Constructor<?> packetPlayOutTitle_init_III;

    private static Method chatSerializer_a;

    private static Method craftPlayer_getHandle;

    private static Method playerConnection_sendPacket;

    private static Object enumTitleAction_subtitle;

    private static Object enumTitleAction_title;

    static {
        String[] array = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
        VERSION_STRING = (array.length == 4) ? (array[3] + ".") : "";
        boolean titleSupport = true;
        try {
            Class<? extends Enum> enumTitleAction;
            try {
                enumTitleAction = (Class)getNmsClass("PacketPlayOutTitle$EnumTitleAction");
            } catch (ClassNotFoundException ex) {
                enumTitleAction = (Class)getNmsClass("EnumTitleAction");
            }
            enumTitleAction_title = Enum.valueOf(enumTitleAction, "TITLE");
            enumTitleAction_subtitle = Enum.valueOf(enumTitleAction, "SUBTITLE");
            Class<?> packetPlayOutTitle = getNmsClass("PacketPlayOutTitle");
            packetPlayOutTitle_init_LL = packetPlayOutTitle.getConstructor(enumTitleAction, getNmsClass("IChatBaseComponent"));
            packetPlayOutTitle_init_III = packetPlayOutTitle.getConstructor(int.class, int.class, int.class);
            try {
                chatSerializer_a = getNmsClass("IChatBaseComponent$ChatSerializer").getDeclaredMethod("a", String.class);
            } catch (ClassNotFoundException ex) {
                chatSerializer_a = getNmsClass("ChatSerializer").getDeclaredMethod("a", String.class);
            }
            craftPlayer_getHandle = getCraftClass("entity.CraftPlayer").getMethod("getHandle");
            entityPlayer_playerConnection = getNmsClass("EntityPlayer").getDeclaredField("playerConnection");
            playerConnection_sendPacket = getNmsClass("PlayerConnection").getMethod("sendPacket", getNmsClass("Packet"));
        } catch (ClassNotFoundException|NoSuchFieldException|NoSuchMethodException ex) {
            throwable = ex;
            titleSupport = false;
        }
        TITLE_SUPPORT = titleSupport;
    }

    public static boolean areTitlesSupported() {
        return TITLE_SUPPORT;
    }

    public Throwable getException() {
        return throwable;
    }

    private static void sendTitle(Player player, String title, ChatColor color, boolean sub) {
        if (TITLE_SUPPORT) {
            String json = "{\"text\":\"" + title + "\"";
            if (color != null)
                json = json + ",\"color\":\"" + color.name().toLowerCase() + "\"";
            json = json + "}";
            try {
                Object packet = packetPlayOutTitle_init_LL.newInstance(sub ? enumTitleAction_subtitle : enumTitleAction_title, chatSerializer_a

                        .invoke(null, json));
                Object vanillaPlayer = craftPlayer_getHandle.invoke(player);
                Object playerConnection = entityPlayer_playerConnection.get(vanillaPlayer);
                playerConnection_sendPacket.invoke(playerConnection, packet);
            } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException|InstantiationException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void sendTitle(Player player, String title, ChatColor color) {
        sendTitle(player, title, color, false);
    }

    public static void sendSubtitle(Player player, String subtitle, ChatColor color) {
        sendTitle(player, subtitle, color, true);
    }

    public static void sendTimes(Player player, int fadeIn, int stay, int fadeOut) {
        if (TITLE_SUPPORT)
            try {
                Object packet = packetPlayOutTitle_init_III.newInstance(fadeIn, stay, fadeOut);
                Object vanillaPlayer = craftPlayer_getHandle.invoke(player);
                Object playerConnection = entityPlayer_playerConnection.get(vanillaPlayer);
                playerConnection_sendPacket.invoke(playerConnection, packet);
            } catch (IllegalAccessException|InstantiationException|java.lang.reflect.InvocationTargetException ex) {
                ex.printStackTrace();
            }
    }

    public static void sendTitle(Player player, String title, ChatColor titleColor, String subtitle, ChatColor subColor) {
        sendSubtitle(player, subtitle, subColor);
        sendTitle(player, title, titleColor);
    }

    public static void sendTitle(Player player, String title, ChatColor titleColor, String subtitle, ChatColor subColor, int fadeIn, int stay, int fadeOut) {
        sendTimes(player, fadeIn, stay, fadeOut);
        sendSubtitle(player, subtitle, subColor);
        sendTitle(player, title, titleColor);
    }

    public static void sendTitle(Player player, String title, ChatColor titleColor, int fadeIn, int stay, int fadeOut) {
        sendTimes(player, fadeIn, stay, fadeOut);
        sendTitle(player, title, titleColor);
    }

    private static Class<?> getNmsClass(String name) throws ClassNotFoundException {
        String className = "net.minecraft.server." + VERSION_STRING + name;
        return Class.forName(className);
    }

    private static Class<?> getCraftClass(String name) throws ClassNotFoundException {
        String className = "org.bukkit.craftbukkit." + VERSION_STRING + name;
        return Class.forName(className);
    }
}