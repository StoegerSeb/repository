package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RoundHelper: 14.04.2020 23:52 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.scoreboard.ScoreboardManager;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.helper.platform.TitleHelper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.NumberFormat;

public final class RoundHelper {
    private static final ItemStack ITEM_CROWBAR = new ItemStack((Material) TTTCore.config.get(ConfigKey.CROWBAR_ITEM), 1);

    static {
        ItemMeta cbMeta = ITEM_CROWBAR.getItemMeta();
        cbMeta.setDisplayName(Color.INFO + TTTCore.locale.getLocalizable("item.crowbar.name").localize());
        ITEM_CROWBAR.setItemMeta(cbMeta);
    }

    private static final ItemStack ITEM_GUN = new ItemStack((Material)TTTCore.config.get(ConfigKey.GUN_ITEM), 1);

    static {
        ItemMeta gunMeta = ITEM_GUN.getItemMeta();
        gunMeta.setDisplayName(Color.INFO + TTTCore.locale.getLocalizable("item.gun.name").localize());
        ITEM_GUN.setItemMeta(gunMeta);
    }

    private static final ItemStack ITEM_AMMO = new ItemStack(Material.ARROW, TTTCore.config.get(ConfigKey.INITIAL_AMMO));

    private static final ItemStack ITEM_DNA_SCANNER = new ItemStack(Material.COMPASS, 1);

    static {
        ItemMeta dnaMeta = ITEM_DNA_SCANNER.getItemMeta();
        dnaMeta.setDisplayName(Color.INFO + TTTCore.locale.getLocalizable("item.dna-scanner.name").localize());
        ITEM_DNA_SCANNER.setItemMeta(dnaMeta);
    }

    public static void startRound(Round round) {
        RoleHelper.assignRoles(round);
        ScoreboardManager sm = (ScoreboardManager)round.getMetadata().get("scoreboardManager").get();
        sm.updateAllEntries();
        UnmodifiableIterator<Challenger> unmodifiableIterator;
        for (unmodifiableIterator = round.getTeam("traitor").get().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            sm.applyScoreboard(ch);
        }
        distributeItems(round);
        for (unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            assert ch.getTeam().isPresent();
            Player pl = TTTCore.getPlugin().getServer().getPlayer(ch.getUniqueId());
            assert pl != null;
            pl.setHealth(pl.getMaxHealth());
            pl.setFoodLevel(20);
            if (ch.getTeam().get().getId().equals("innocent")) {
                if (ch.getMetadata().has("detective")) {
                    TTTCore.locale.getLocalizable("info.personal.status.role.detective")
                            .withPrefix(Color.DETECTIVE).sendTo(pl);
                    TitleHelper.sendStatusTitle(pl, "detective");
                } else {
                    TTTCore.locale.getLocalizable("info.personal.status.role.innocent")
                            .withPrefix(Color.INNOCENT).sendTo(pl);
                    TitleHelper.sendStatusTitle(pl, "innocent");
                }
            } else if (ch.getTeam().get().getId().equals("traitor")) {
                if (ch.getTeam().get().getChallengers().size() > 1) {
                    TTTCore.locale.getLocalizable("info.personal.status.role.traitor")
                            .withPrefix(Color.TRAITOR).sendTo(pl);
                    TTTCore.locale.getLocalizable("info.personal.status.role.traitor.allies")
                            .withPrefix(Color.TRAITOR).sendTo(pl);
                    for (UnmodifiableIterator<Challenger> unmodifiableIterator1 = ch.getTeam().get().getChallengers().iterator(); unmodifiableIterator1.hasNext(); ) {
                        Challenger traitor = unmodifiableIterator1.next();
                        if (traitor != ch)
                            pl.sendMessage(Color.TRAITOR + "- " + traitor.getName());
                    }
                } else {
                    TTTCore.locale.getLocalizable("info.personal.status.role.traitor.alone")
                            .withPrefix(Color.TRAITOR).sendTo(pl);
                }
                TitleHelper.sendStatusTitle(pl, "traitor");
            }
            if (TTTCore.config.get(ConfigKey.KARMA_DAMAGE_REDUCTION)) {
                KarmaHelper.applyDamageReduction(ch);
                double reduc = KarmaHelper.getDamageReduction(ch);
                String percentage = (reduc < 1.0D) ? ((int)(reduc * 100.0D) + "%") : TTTCore.locale.getLocalizable("fragment.full").localizeFor(pl);
                TTTCore.locale.getLocalizable("info.personal.status.karma-damage")
                        .withPrefix(Color.INFO).withReplacements(KarmaHelper.getKarma(ch) + "", percentage).sendTo(pl);
            }
        }
        ((ScoreboardManager)round.getMetadata().get("scoreboardManager").get()).updateAllEntries();
        broadcast(round, TTTCore.locale.getLocalizable("info.global.round.event.started")
                .withPrefix(Color.INFO));
    }

    public static void closeRound(Round round, boolean sendWinMessages) {
        KarmaHelper.allocateKarma(round);
        KarmaHelper.saveKarma(round);
        if (sendWinMessages) {
            boolean tVic = (Boolean) round.getMetadata().get("t-victory").or(Boolean.FALSE);
            String color = tVic ? Color.TRAITOR : Color.INNOCENT;
            Localizable msg = TTTCore.locale.getLocalizable("info.global.round.event.end." + (tVic ? "traitor" : "innocent")).withPrefix(color).withReplacements(Color.EM + round.getArena().getDisplayName() + color);
            if (TTTCore.config.get(ConfigKey.BROADCAST_WIN_MESSAGES_TO_SERVER)) {
                msg.broadcast();
            } else {
                for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                    Challenger ch = unmodifiableIterator.next();
                    msg.sendTo(Bukkit.getPlayer(ch.getUniqueId()));
                }
            }
        }
        TitleHelper.sendVictoryTitle(round, (Boolean) round
                .getMetadata().get("t-victory").or(Boolean.FALSE));
    }

    public static void distributeItems(Round round) {
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            distributeItems(ch);
        }
    }

    public static void distributeItems(Challenger chal) {
        Player pl = Bukkit.getPlayer(chal.getUniqueId());
        assert pl != null;
        pl.getInventory().clear();
        pl.getInventory().addItem(ITEM_CROWBAR, ITEM_GUN, ITEM_AMMO);
        if (chal.getMetadata().containsKey("detective"))
            pl.getInventory().addItem(ITEM_DNA_SCANNER);
    }

    public static void broadcast(Round round, Localizable localizable) {
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            Player pl = Bukkit.getPlayer(ch.getUniqueId());
            assert pl != null;
            localizable.sendTo(pl);
        }
    }

    public static String getTimeDisplay(Round round, boolean t, ChatColor defaultColor) {
        long dispTime;
        StringBuilder timeStr = new StringBuilder();
        boolean indefinite = (round.getLifecycleStage().getDuration() == -1);
        boolean hasteDisp = (round.getLifecycleStage() == Stage.PLAYING && !indefinite && TTTCore.config.get(ConfigKey.HASTE) && System.currentTimeMillis() % 8000L >= 4000L);
        if (indefinite) {
            dispTime = round.getTime();
        } else if (hasteDisp || round.getLifecycleStage() != Stage.PLAYING) {
            dispTime = round.getRemainingTime();
        } else {
            dispTime = round.getRemainingTime() - (Integer) round.getMetadata().get("hasteTime").or(0);
        }
        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setMinimumIntegerDigits(2);
        int secondsPerMinute = 60;
        String minutes = nf.format(dispTime / 60L);
        String seconds = nf.format(dispTime % 60L);
        if (hasteDisp) {
            timeStr.append(ChatColor.RED.toString());
            if (t) {
                timeStr.append(minutes).append(":").append(seconds);
            } else {
                timeStr.append(TTTCore.locale.getLocalizable("fragment.haste-mode").localize());
            }
        } else {
            if (defaultColor != null)
                timeStr.append(defaultColor.toString());
            if (round.getLifecycleStage() == Stage.PLAYING && dispTime <= 0L) {
                timeStr.append(TTTCore.locale.getLocalizable("fragment.overtime").localize());
            } else {
                timeStr.append(minutes).append(":").append(seconds);
            }
        }
        return timeStr.toString();
    }
}
