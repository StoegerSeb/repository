package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class BanHelper: 14.04.2020 23:51 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

public final class BanHelper {
    public static void ban(UUID player, int minutes) throws InvalidConfigurationException, IOException {
        File f = new File(TTTCore.getPlugin().getDataFolder(), "bans.yml");
        YamlConfiguration y = new YamlConfiguration();
        Player p = Bukkit.getPlayer(player);
        y.load(f);
        long unbanTime = (minutes < 0) ? -1L : (System.currentTimeMillis() / 1000L + (minutes * 60));
        y.set(player.toString(), unbanTime);
        y.save(f);
        if (p != null) {
            Optional<Challenger> ch = TTTCore.mg.getChallenger(p.getUniqueId());
            if (ch.isPresent())
                ch.get().removeFromRound();
        }
    }

    public static boolean pardon(UUID uuid) throws InvalidConfigurationException, IOException {
        File f = new File(TTTCore.getPlugin().getDataFolder(), "bans.yml");
        YamlConfiguration y = new YamlConfiguration();
        Player p = Bukkit.getPlayer(uuid);
        y.load(f);
        if (y.contains(uuid.toString())) {
            y.set(uuid.toString(), null);
            y.save(f);
            if (TTTCore.config.get(ConfigKey.VERBOSE_LOGGING))
                TTTCore.log.info(TTTCore.locale.getLocalizable("info.personal.pardon")
                        .withReplacements((p != null) ? p.getName() : uuid.toString()).localize());
            return true;
        }
        return false;
    }

    public static boolean checkBan(UUID uuid) {
        File f = new File(TTTCore.getPlugin().getDataFolder(), "bans.yml");
        YamlConfiguration y = new YamlConfiguration();
        try {
            y.load(f);
            if (y.isSet(uuid.toString())) {
                long unbanTime = y.getLong(uuid.toString());
                if (unbanTime != -1L && unbanTime <= System.currentTimeMillis() / 1000L) {
                    pardon(uuid);
                } else {
                    Localizable loc;
                    if (unbanTime == -1L) {
                        loc = TTTCore.locale.getLocalizable("info.personal.ban.perm");
                    } else {
                        Calendar cal = Calendar.getInstance();
                        cal.setTimeInMillis(unbanTime * 1000L);
                        String year = Integer.toString(cal.get(1));
                        String month = Integer.toString(cal.get(2) + 1);
                        String day = Integer.toString(cal.get(5));
                        String hour = Integer.toString(cal.get(11));
                        String min = Integer.toString(cal.get(12));
                        String sec = Integer.toString(cal.get(13));
                        min = (min.length() == 1) ? ("0" + min) : min;
                        sec = (sec.length() == 1) ? ("0" + sec) : sec;
                        loc = TTTCore.locale.getLocalizable("info.personal.ban.temp.until").withReplacements(hour + ":" + min + ":" + sec, month + "/" + day + "/" + year + ".");
                    }
                    loc.withPrefix(Color.ALERT);
                    loc.sendTo(Bukkit.getPlayer(uuid));
                    return true;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            TTTCore.log.warning("Failed to load bans from disk!");
        }
        return false;
    }
}
