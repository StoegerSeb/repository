package com.sap.sepp.ttt.utils.helper.platform;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TitleHelper: 14.04.2020 23:55 by sebip
*/

public final class TitleHelper {
    public static void sendStatusTitle(Player player, String role) {
        if (((Boolean)TTTCore.config.get(ConfigKey.SEND_TITLES)).booleanValue() && TitleUtil.areTitlesSupported()) {
            String color;
            if (player == null)
                throw new IllegalArgumentException("Player cannot be null!");
            role = role.toLowerCase();
            String title = TTTCore.locale.getLocalizable("info.personal.status.role." + role + ".title").localizeFor((CommandSender)player);
            switch (role) {
                case "innocent":
                    color = Color.INNOCENT;
                    break;
                case "detective":
                    color = Color.DETECTIVE;
                    break;
                default:
                    color = Color.TRAITOR;
                    break;
            }
            if (((Boolean)TTTCore.config.get(ConfigKey.LARGE_STATUS_TITLES)).booleanValue()) {
                TitleUtil.sendTitle(player, title, ChatColor.getByChar(color.charAt(1)));
            } else {
                TitleUtil.sendTitle(player, "", ChatColor.RESET, title, ChatColor.getByChar(color.charAt(1)));
            }
        }
    }

    public static void sendVictoryTitle(Round round, boolean traitorVictory) {
        if (((Boolean)TTTCore.config.get(ConfigKey.SEND_TITLES)).booleanValue() && TitleUtil.areTitlesSupported()) {
            Preconditions.checkNotNull(round, "Round cannot be null!");
            Localizable loc = TTTCore.locale.getLocalizable("info.global.round.event.end." + (traitorVictory ? "traitor" : "innocent") + ".min");
            ChatColor color = ChatColor.getByChar((traitorVictory ? Color.TRAITOR : Color.INNOCENT)
                    .charAt(1));
            for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                Challenger ch = unmodifiableIterator.next();
                Player pl = Bukkit.getPlayer(ch.getUniqueId());
                if (((Boolean)TTTCore.config.get(ConfigKey.LARGE_VICTORY_TITLES)).booleanValue()) {
                    TitleUtil.sendTitle(Bukkit.getPlayer(ch.getUniqueId()), loc.localizeFor((CommandSender)pl), color);
                    continue;
                }
                TitleUtil.sendTitle(Bukkit.getPlayer(ch.getUniqueId()), "", ChatColor.RESET, loc.localizeFor((CommandSender)pl), color);
            }
        }
    }
}