package com.sap.sepp.ttt.utils;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TelemetryRunner: 14.04.2020 23:40 by sebip
*/

public class TelemetryRunner implements Runnable {
    private static final int TICKS_PER_HOUR = 72000;

    private static final int SECONDS_PER_DAY = 86400;

    private static final String UUID_FILE_NAME = "telemetry/uuid.dat";

    private static final String TIMESTAMP_FILE_NAME = "telemetry/timestamp.dat";

    private static final String TELEMETRY_SERVER = "https://telemetry.caseif.net/ttt.php";

    private final JTelemetry jt = new JTelemetry("https://telemetry.caseif.net/ttt.php");

    public TelemetryRunner() {
        Bukkit.getScheduler().runTaskTimer((Plugin)TTTCore.getPlugin(), this, 0L, 72000L);
    }

    public void run() {
        if (shouldRun()) {
            JTelemetry.Payload payload = constructPayload();
            try {
                writeRunTime();
                JTelemetry.HttpResponse response = payload.submit();
                if (response.getStatusCode() / 100 != 2)
                    TTTCore.log.warning("Telemetry server responded with non-success status code (" + response
                            .getStatusCode() + " " + response.getMessage() + "). Please report this.");
            } catch (IOException ex) {
                throw new RuntimeException("Failed to submit telemetry data to remote server", ex);
            }
        }
    }

    private static boolean shouldRun() {
        try {
            File tsFile = new File(TTTCore.getPlugin().getDataFolder(), "telemetry/timestamp.dat");
            if (!tsFile.exists()) {
                writeRunTime();
                return false;
            }
            byte[] bytes = new byte[8];
            try (FileInputStream is = new FileInputStream(tsFile)) {
                int readBytes = is.read(bytes);
                if (readBytes < 8) {
                    TTTCore.log.warning("Telemetry timestamp file is malformed - regenerating");
                    Files.delete(tsFile.toPath());
                    writeRunTime();
                    return false;
                }
                long timestamp = ByteHelper.bytesToLong(bytes);
                return ((System.currentTimeMillis() - timestamp) / 1000L >= 86400L);
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to read timestamp file - not submitting telemetry data", ex);
        }
    }

    private static void writeRunTime() throws IOException {
        File tsFile = new File(TTTCore.getPlugin().getDataFolder(), "telemetry/timestamp.dat");
        if (tsFile.exists())
            Files.delete(tsFile.toPath());
        Files.createDirectories(tsFile.getParentFile().toPath(), (FileAttribute<?>[])new FileAttribute[0]);
        Files.createFile(tsFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
        try (FileOutputStream os = new FileOutputStream(tsFile)) {
            os.write(ByteHelper.longToBytes(System.currentTimeMillis()));
        }
    }

    private static UUID getUuid() throws IOException {
        File uuidFile = new File(TTTCore.getPlugin().getDataFolder(), "telemetry/uuid.dat");
        if (!uuidFile.exists()) {
            Files.createDirectories(uuidFile.getParentFile().toPath(), (FileAttribute<?>[])new FileAttribute[0]);
            Files.createFile(uuidFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
        }
        try (FileInputStream is = new FileInputStream(uuidFile)) {
            UUID uuid = null;
            byte[] most = new byte[8];
            byte[] least = new byte[8];
            int read1 = is.read(most);
            int read2 = is.read(least);
            if (read1 == 8 || read2 == 8) {
                uuid = new UUID(ByteHelper.bytesToLong(most), ByteHelper.bytesToLong(least));
            } else {
                TTTCore.log.warning("UUID file is missing or malformed - regenerating");
            }
        }
    }

    private JTelemetry.Payload constructPayload() {
        UUID uuid;
        JTelemetry.Payload payload = this.jt.createPayload();
        try {
            uuid = getUuid();
        } catch (IOException ex) {
            throw new RuntimeException("Failed to get telemetry UUID - not submitting data", ex);
        }
        payload.addData("uuid", uuid.toString());
        payload.addData("version", TTTCore.getPlugin().getDescription().getVersion());
        payload.addData("platform", Bukkit.getName());
        payload.addData("flintApi", FlintCore.getApiRevision());
        payload.addData("opMode", ((OperatingMode)TTTCore.config.get(ConfigKey.OPERATING_MODE)).name());
        payload.addData("arenas", TTTCore.mg.getArenas().size());
        TelemetryStorageHelper.RoundSummaryStats stats = TelemetryStorageHelper.getSummaryStats();
        payload.addData("rounds", stats.getRoundCount());
        payload.addData("players", stats.getMeanPlayerCount());
        payload.addData("roundDur", stats.getDurationMean());
        payload.addData("roundDurSD", stats.getDurationStdDev());
        payload.addData("iWins", stats.getInnoWins());
        payload.addData("tWins", stats.getTraitorWins());
        payload.addData("forfeits", stats.getForfeits());
        return payload;
    }
}
