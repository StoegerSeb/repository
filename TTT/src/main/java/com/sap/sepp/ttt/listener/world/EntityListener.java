package com.sap.sepp.ttt.listener.world;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class EntityListener: 14.04.2020 23:39 by sebip
*/

public class EntityListener implements Listener {
    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.PLAYER && TTTCore.mg
                .getChallenger(event.getEntity().getUniqueId()).isPresent()) {
            event.setCancelled(true);
        } else if (event.getEntity() instanceof Projectile && ((Projectile)event
                .getEntity()).getShooter() instanceof Player && TTTCore.mg
                .getChallenger(((Player)((Projectile)event.getEntity()).getShooter()).getUniqueId())
                .isPresent()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (TTTCore.mg.getChallenger(event.getPlayer().getUniqueId()).isPresent())
            event.setCancelled(true);
    }

    @EventHandler
    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
        if (event.getTarget() != null && event.getTarget().getType() == EntityType.PLAYER && TTTCore.mg
                .getChallenger(event.getTarget().getUniqueId()).isPresent())
            event.setCancelled(true);
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        for (UnmodifiableIterator<Arena> unmodifiableIterator = TTTCore.mg.getArenas().iterator(); unmodifiableIterator.hasNext(); ) {
            Arena arena = unmodifiableIterator.next();
            if (arena.getWorld().equals(event.getLocation().getWorld().getName()) && arena
                    .getBoundary().contains(LocationHelper.convert(event.getLocation())))
                event.setCancelled(true);
        }
    }
}