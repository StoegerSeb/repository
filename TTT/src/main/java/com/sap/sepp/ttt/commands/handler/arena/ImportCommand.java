package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ImportCommand: 14.04.2020 23:29 by sebip
*/

import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.gamemode.ArenaHelper;
import com.sap.sepp.ttt.utils.helper.io.FileHelper;
import com.sap.sepp.ttt.utils.helper.platform.LocationHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;

import java.io.File;

public class ImportCommand extends CommandHandler {
    public ImportCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (TTTCore.mg.getArena(this.args[1].toLowerCase()).isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.already-exists").withPrefix(Color.ALERT)
                    .sendTo(this.sender);
            return;
        }
        String worldName = null;
        assert Bukkit.getWorldContainer().listFiles() != null;
        for (File f : Bukkit.getWorldContainer().listFiles()) {
            if (f.getName().equalsIgnoreCase(this.args[1]))
                worldName = f.getName();
        }
        if (worldName != null &&
                FileHelper.isWorld(this.args[1])) {
            World w = Bukkit.createWorld(new WorldCreator(worldName));
            if (w != null) {
                Location l = w.getSpawnLocation();
                TTTCore.mg.createBuilder(Arena.class).id(worldName).spawnPoints(LocationHelper.convert(l)).boundary(Boundary.INFINITE).build();
                ArenaHelper.updateShuffledArenas();
                TTTCore.locale.getLocalizable("info.personal.arena.import.success").withPrefix(Color.INFO)
                        .sendTo(this.sender);
                return;
            }
        }
        TTTCore.locale.getLocalizable("error.plugin.world-load").withPrefix(Color.ALERT).sendTo(this.sender);
    }
}