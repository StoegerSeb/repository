package com.sap.sepp.ttt.listener.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerUpdateListener: 14.04.2020 23:37 by sebip
*/

public class PlayerUpdateListener implements Listener {
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        (new DeathHelper(event)).handleEvent();
    }

    @EventHandler
    public void onHealthRegenerate(EntityRegainHealthEvent event) {
        if (event.getEntity() instanceof Player) {
            Player p = (Player)event.getEntity();
            if (TTTCore.mg.getChallenger(p.getUniqueId()).isPresent())
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (TTTCore.mg.getChallenger(event.getEntity().getUniqueId()).isPresent())
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            Optional<Challenger> victim = TTTCore.mg.getChallenger(event.getEntity().getUniqueId());
            if (victim.isPresent() && (((Challenger)victim
                    .get()).getRound().getLifecycleStage() == Stage.WAITING || ((Challenger)victim
                    .get()).getRound().getLifecycleStage() == Stage.PREPARING)) {
                event.setCancelled(true);
                if (event.getCause() == EntityDamageEvent.DamageCause.VOID)
                    Bukkit.getPlayer(((Challenger)victim.get()).getUniqueId()).teleport(
                            LocationHelper.convert((Location3D)((Challenger)victim.get()).getRound().getArena().getSpawnPoints().get(Integer.valueOf(0))));
                return;
            }
            if (event instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent ed = (EntityDamageByEntityEvent)event;
                if (ed.getDamager().getType() == EntityType.PLAYER || (ed
                        .getDamager() instanceof Projectile && ((Projectile)ed
                        .getDamager()).getShooter() instanceof Player)) {
                    Player damager = (ed.getDamager().getType() == EntityType.PLAYER) ? (Player)ed.getDamager() : (Player)((Projectile)ed.getDamager()).getShooter();
                    Optional<Challenger> damagerCh = TTTCore.mg.getChallenger(damager.getUniqueId());
                    if (damagerCh.isPresent()) {
                        if (((Challenger)damagerCh.get()).getRound().getLifecycleStage() == Stage.WAITING || ((Challenger)damagerCh
                                .get()).getRound().getLifecycleStage() == Stage.PREPARING ||
                                !victim.isPresent() || ((Challenger)damagerCh
                                .get()).isSpectating()) {
                            event.setCancelled(true);
                            return;
                        }
                        if (damager.getItemInHand() != null && damager
                                .getItemInHand().getItemMeta() != null && damager
                                .getItemInHand().getItemMeta().getDisplayName() != null && damager
                                .getItemInHand().getItemMeta().getDisplayName()
                                .endsWith(TTTCore.locale.getLocalizable("item.crowbar.name")
                                        .localize()))
                            event.setDamage(((Integer)TTTCore.config.get(ConfigKey.CROWBAR_DAMAGE)).intValue());
                        Optional<Double> reduc = ((Challenger)damagerCh.get()).getMetadata().get("damageRed");
                        if (reduc.isPresent())
                            event.setDamage((int)(event.getDamage() * ((Double)reduc.get()).doubleValue()));
                        KarmaHelper.applyDamageKarma((Challenger)damagerCh.get(), (Challenger)victim.get(), event.getDamage());
                    }
                }
            }
        }
    }
}
