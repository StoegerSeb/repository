package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class AddSpawnCommand: 14.04.2020 23:28 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.utils.physical.Location3D;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.data.DataVerificationHelper;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddSpawnCommand extends CommandHandler {
    public AddSpawnCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        int x, y, z;
        World w = null;
        if (this.args.length == 2) {
            if (this.sender instanceof Player) {
                w = ((Player)this.sender).getWorld();
                x = ((Player)this.sender).getLocation().getBlockX();
                y = ((Player)this.sender).getLocation().getBlockY();
                z = ((Player)this.sender).getLocation().getBlockZ();
            } else {
                TTTCore.locale.getLocalizable("error.command.ingame").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
        } else if (this.args.length == 5) {
            if (DataVerificationHelper.isInt(this.args[2]) && DataVerificationHelper.isInt(this.args[3]) && DataVerificationHelper.isInt(this.args[4])) {
                x = Integer.parseInt(this.args[2]);
                y = Integer.parseInt(this.args[3]);
                z = Integer.parseInt(this.args[4]);
            } else {
                printInvalidArgsError();
                return;
            }
        } else {
            printInvalidArgsError();
            return;
        }
        Optional<Arena> arena = TTTCore.mg.getArena(this.args[1]);
        if (arena.isPresent()) {
            Location3D loc = new Location3D((w != null) ? w.getName() : null, x, y, z);
            if (w != null && !arena.get().getWorld().equals(w.getName())) {
                TTTCore.locale.getLocalizable("error.arena.invalid-location").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
            if (!arena.get().getBoundary().contains(loc)) {
                TTTCore.locale.getLocalizable("error.arena.create.bad-spawn").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
            arena.get().addSpawnPoint(loc);
            TTTCore.locale.getLocalizable("info.personal.arena.addspawn").withPrefix(Color.INFO)
                    .withReplacements("(" + x + ", " + y + ", " + z + ")", arena.get().getDisplayName()).sendTo(this.sender);
        } else {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }
}