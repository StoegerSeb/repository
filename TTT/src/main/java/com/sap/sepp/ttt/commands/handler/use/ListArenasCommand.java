package com.sap.sepp.ttt.commands.handler.use;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ListArenasCommand: 14.04.2020 23:34 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ListArenasCommand extends CommandHandler {
    public ListArenasCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        TTTCore.locale.getLocalizable("info.personal.arena.list").withPrefix(Color.INFO).sendTo(this.sender);
        for (UnmodifiableIterator<Arena> unmodifiableIterator = TTTCore.mg.getArenas().iterator(); unmodifiableIterator.hasNext(); ) {
            Arena arena = unmodifiableIterator.next();
            this.sender.sendMessage("    " + Color.SECONDARY + arena.getId() + ": " + ChatColor.WHITE + TTTCore.locale
                    .getLocalizable("fragment.stage." + (
                            arena.getRound().isPresent() ? arena
                                    .getRound().get().getLifecycleStage().getId() : Stage.WAITING
                                    .getId()))
                    .localizeFor(this.sender).toUpperCase());
        }
    }
}
