package com.sap.sepp.ttt.commands.handler.use;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LeaveCommand: 14.04.2020 23:34 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.platform.BungeeHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand extends CommandHandler {
    public LeaveCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (TTTCore.config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED && (
                !this.sender.hasPermission("ttt.admin") || this.args.length < 2 || !this.args[1].equalsIgnoreCase("force"))) {
            if (BungeeHelper.hasSupport() && !TTTCore.config.get(ConfigKey.RETURN_SERVER).isEmpty()) {
                TTTCore.locale.getLocalizable("info.personal.bungee.connecting").withPrefix(Color.INFO).sendTo(this.sender);
                trySendForceLeaveTip();
                BungeeHelper.sendPlayerToReturnServer((Player)this.sender);
                return;
            }
            TTTCore.locale.getLocalizable("error.round.leave-dedicated").withPrefix(Color.ALERT).sendTo(this.sender);
            trySendForceLeaveTip();
            return;
        }
        Optional<Challenger> ch = TTTCore.mg.getChallenger(((Player)this.sender).getUniqueId());
        if (ch.isPresent()) {
            String roundName = ch.get().getRound().getArena().getDisplayName();
            ch.get().removeFromRound();
            TTTCore.locale.getLocalizable("info.personal.arena.leave.success").withPrefix(Color.INFO)
                    .withReplacements(Color.EM + roundName + Color.INFO).sendTo(this.sender);
        } else {
            TTTCore.locale.getLocalizable("error.round.outside").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }

    private void trySendForceLeaveTip() {
        if (this.sender.hasPermission("ttt.admin"))
            TTTCore.locale.getLocalizable("error.round.leave-dedicated-force").withPrefix(Color.ALERT)
                    .withReplacements(Color.EM + "/ttt leave force").sendTo(this.sender);
    }
}