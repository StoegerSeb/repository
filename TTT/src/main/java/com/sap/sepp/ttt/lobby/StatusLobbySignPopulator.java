package com.sap.sepp.ttt.lobby;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sepp_xGMx
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class StatusLobbySignPopulator: 14.04.2020 23:25 by sebip
*/

import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.round.Round;
import org.bukkit.ChatColor;

public class StatusLobbySignPopulator implements LobbySignPopulator {
    public static final int SIGN_HASTE_SWITCH_PERIOD = 4;

    private LobbySignPopulator defaultPop;

    public StatusLobbySignPopulator(LobbySignPopulator defaultPop) {
        this.defaultPop = defaultPop;
    }

    public String first(LobbySign sign) {
        return this.defaultPop.first(sign);
    }

    public String second(LobbySign sign) {
        return this.defaultPop.second(sign);
    }

    public String third(LobbySign sign) {
        if (!sign.getArena().getRound().isPresent())
            return "";
        return RoundHelper.getTimeDisplay((Round)sign.getArena().getRound().get(), false, ChatColor.DARK_PURPLE);
    }

    public String fourth(LobbySign sign) {
        return this.defaultPop.fourth(sign);
    }
}
