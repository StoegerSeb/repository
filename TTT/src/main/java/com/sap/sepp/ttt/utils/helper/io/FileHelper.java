package com.sap.sepp.ttt.utils.helper.io;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class FileHelper: 14.04.2020 23:53 by sebip
*/

public final class FileHelper {
    public static boolean isWorld(String worldName) {
        File folder = new File(Bukkit.getWorldContainer(), worldName);
        if (folder.exists()) {
            File[] files = folder.listFiles(new FilenameFilter() {
                public boolean accept(File file, String name) {
                    return name.equalsIgnoreCase("level.dat");
                }
            });
            if (files != null && files.length > 0)
                return true;
        }
        return false;
    }

    public static void copyFile(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists())
                targetLocation.mkdir();
            String[] children = sourceLocation.list();
            for (String aChildren : children)
                copyFile(new File(sourceLocation, aChildren), new File(targetLocation, aChildren));
        } else {
            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0)
                out.write(buf, 0, len);
            in.close();
            out.close();
        }
    }
}