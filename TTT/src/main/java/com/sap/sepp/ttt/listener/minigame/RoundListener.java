package com.sap.sepp.ttt.listener.minigame;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RoundListener: 14.04.2020 23:36 by sebip
*/

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.UnmodifiableIterator;
import com.google.common.eventbus.Subscribe;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.event.round.RoundChangeLifecycleStageEvent;
import com.sap.sepp.api.event.round.RoundEndEvent;
import com.sap.sepp.api.event.round.RoundTimerTickEvent;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.scoreboard.ScoreboardManager;
import com.sap.sepp.ttt.utils.RoundRestartDaemon;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.CommandRegex;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.helper.data.FunctionalHelper;
import com.sap.sepp.ttt.utils.helper.data.TelemetryStorageHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.ArenaHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.RoleHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.RoundHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;
import java.util.UUID;

public class RoundListener {
    private static final Predicate<Challenger> PRED_WIN = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf(
                    (RoleHelper.isTraitor(chal) == ((Boolean)chal.getRound().getMetadata().get("t-victory").or(Boolean.valueOf(false))).booleanValue()));
        }
    });

    private static final Predicate<Challenger> PRED_LOSE = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf(!RoundListener.PRED_WIN.apply(chal));
        }
    });

    private static final Predicate<Challenger> PRED_I = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf(!RoleHelper.isTraitor(chal));
        }
    });

    private static final Predicate<Challenger> PRED_IND = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf((!RoleHelper.isTraitor(chal) && !chal.getMetadata().containsKey("detective")));
        }
    });

    private static final Predicate<Challenger> PRED_D = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf(chal.getMetadata().containsKey("detective"));
        }
    });

    private static final Predicate<Challenger> PRED_T = FunctionalHelper.createPredicate(new Function<Challenger, Boolean>() {
        public Boolean apply(Challenger chal) {
            return Boolean.valueOf(RoleHelper.isTraitor(chal));
        }
    });

    @Subscribe
    public void onRoundChangeLifecycleStage(RoundChangeLifecycleStageEvent event) {
        if (event.getStageAfter() == Stage.PREPARING) {
            RoundHelper.broadcast(event.getRound(), TTTCore.locale.getLocalizable("info.global.round.event.starting")
                    .withPrefix(Color.INFO));
            runCommands(TTTCore.config.get(ConfigKey.PREPARE_CMDS), event.getRound());
        } else if (event.getStageAfter() == Stage.PLAYING) {
            RoundHelper.startRound(event.getRound());
            if (TTTCore.config.get(ConfigKey.ENABLE_TELEMETRY))
                event.getRound().getMetadata().set("players",
                        event.getRound().getChallengers().size());
            runCommands(TTTCore.config.get(ConfigKey.START_CMDS), event.getRound());
        } else if (event.getStageAfter() == Stage.ROUND_OVER) {
            RoundHelper.closeRound(event.getRound(), (event.getStageBefore() == Stage.PLAYING));
            if (ArenaHelper.shouldArenaCycle(event.getRound().getArena()))
                RoundHelper.broadcast(event.getRound(), TTTCore.locale
                        .getLocalizable("info.global.arena.status.map-change")
                        .withPrefix(Color.INFO));
            if (TTTCore.config.get(ConfigKey.ENABLE_TELEMETRY)) {
                if (!event.getRound().getMetadata().containsKey("duration"))
                    event.getRound().getMetadata()
                            .set("duration", event.getStageBefore().getDuration());
                if (!event.getRound().getMetadata().containsKey("result"))
                    event.getRound().getMetadata().set("result", 2);
                TelemetryStorageHelper.pushRound(event.getRound());
            }
            runCommands(TTTCore.config.get(ConfigKey.COOLDOWN_CMDS), event.getRound());
            runWinLoseCmds(event.getRound());
        }
    }

    @Subscribe
    public void onRoundTick(RoundTimerTickEvent event) {
        Round r = event.getRound();
        Optional<ScoreboardManager> sbm = r.getMetadata().get("scoreboardManager");
        if (sbm.isPresent())
            sbm.get().updateTitle();
        if (r.getLifecycleStage() != Stage.WAITING &&
                event.getRound().getLifecycleStage() == Stage.PLAYING) {
            boolean iLeft = false;
            boolean tLeft = false;
            for (UnmodifiableIterator<Challenger> unmodifiableIterator = event.getRound().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                Challenger ch = unmodifiableIterator.next();
                if (!tLeft || !iLeft) {
                    if (!ch.isSpectating())
                        if (RoleHelper.isTraitor(ch)) {
                            tLeft = true;
                        } else {
                            iLeft = true;
                        }
                    Player tracker = TTTCore.getPlugin().getServer().getPlayer(ch.getName());
                    if (ch.getMetadata().has("detective") && ch.getMetadata().has("tracking")) {
                        if (ch.getRound().getTime() % TTTCore.config.get(ConfigKey.SCANNER_CHARGE_TIME) == 0L) {
                            Player killer = TTTCore.getPlugin().getServer().getPlayer((UUID)ch.getMetadata().get("tracking").get());
                            if (killer != null && TTTCore.mg
                                    .getChallenger(killer.getUniqueId()).isPresent()) {
                                tracker.setCompassTarget(killer.getLocation());
                                continue;
                            }
                            TTTCore.locale.getLocalizable("error.round.trackee-left")
                                    .withPrefix(Color.ALERT).sendTo(tracker);
                            ch.getMetadata().remove("tracking");
                            tracker.setCompassTarget(Bukkit.getWorlds().get(1).getSpawnLocation());
                        }
                        continue;
                    }
                    tracker.setCompassTarget(new Location(tracker

                            .getWorld(), tracker
                            .getLocation().getX() + Math.random() * 100.0D - 50.0D, tracker
                            .getLocation().getY(), tracker
                            .getLocation().getZ() + Math.random() * 100.0D - 50.0D));
                }
            }
            if (!tLeft || !iLeft) {
                if (tLeft)
                    event.getRound().getMetadata().set("t-victory", Boolean.TRUE);
                if (TTTCore.config.get(ConfigKey.ENABLE_TELEMETRY)) {
                    event.getRound().getMetadata().set("result", (byte) (tLeft ? 1 : 0));
                    event.getRound().getMetadata()
                            .set("duration", (int) event.getRound().getTime());
                }
                event.getRound().setLifecycleStage(Stage.ROUND_OVER, true);
            }
        }
    }

    @Subscribe
    public void onRoundEnd(RoundEndEvent event) {
        if (event.getRound().getLifecycleStage() != Stage.ROUND_OVER)
            RoundHelper.closeRound(event.getRound(), (event.getRound().getLifecycleStage() == Stage.PLAYING));
        for (Entity ent : Bukkit.getWorld(event.getRound().getArena().getWorld()).getEntities()) {
            if (ent.getType() == EntityType.ARROW)
                ent.remove();
        }
        ((ScoreboardManager)event.getRound().getMetadata().get("scoreboardManager").get())
                .uninitialize();
        runCommands(TTTCore.config.get(ConfigKey.END_CMDS), event.getRound());
        if (event.isNatural() && (TTTCore.config
                .get(ConfigKey.OPERATING_MODE) == OperatingMode.CONTINUOUS || TTTCore.config
                .get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED))
            (new RoundRestartDaemon(event.getRound())).runTask((Plugin)TTTCore.getPlugin());
    }

    @Subscribe
    public void onStageChange(RoundChangeLifecycleStageEvent event) {
        if (event.getStageBefore() == Stage.PREPARING && event.getStageAfter() == Stage.WAITING) {
            UnmodifiableIterator<Challenger> unmodifiableIterator;
            for (unmodifiableIterator = event.getRound().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                Challenger ch = unmodifiableIterator.next();
                Bukkit.getPlayer(ch.getUniqueId())
                        .setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
            }
            for (unmodifiableIterator = event.getRound().getTeams().iterator(); unmodifiableIterator.hasNext(); ) {
                Team team = (Team)unmodifiableIterator.next();
                event.getRound().removeTeam(team);
            }
            ((ScoreboardManager)event.getRound().getMetadata().get("scoreboardManager").get())
                    .updateAllEntries();
        } else if (event.getStageAfter() == Stage.ROUND_OVER) {
            event.getRound().setConfigValue(ConfigNode.WITHHOLD_SPECTATOR_CHAT, Boolean.FALSE);
        }
    }

    private void runCommands(List<String> commands, Round round) {
        runCommands(commands, round, false);
    }

    @SafeVarargs
    private final void runCommands(List<String> commands, Round round, boolean requireParam, Predicate<Challenger>... preds) {
        for (String cmd : commands) {
            cmd = CommandRegex.ARENA_WILDCARD.matcher(cmd).replaceAll(round.getArena().getId());
            if (CommandRegex.PLAYER_WILDCARD.matcher(cmd).find()) {
                UnmodifiableIterator<Challenger> unmodifiableIterator;
                label23: for (unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                    Challenger ch = unmodifiableIterator.next();
                    for (Predicate<Challenger> pred : preds) {
                        if (!pred.apply(ch))
                            continue label23;
                    }
                    String chCmd = CommandRegex.PLAYER_WILDCARD.matcher(cmd).replaceAll(ch.getName());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), chCmd);
                }
                continue;
            }
            if (!requireParam)
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
        }
    }

    private void runWinLoseCmds(Round round) {
        runCommands(TTTCore.config.get(ConfigKey.WIN_CMDS), round, true, PRED_WIN);
        runCommands(TTTCore.config.get(ConfigKey.WIN_I_CMDS), round, true, PRED_WIN, PRED_I);
        runCommands(TTTCore.config.get(ConfigKey.WIN_IND_CMDS), round, true, PRED_WIN, PRED_IND);
        runCommands(TTTCore.config.get(ConfigKey.WIN_D_CMDS), round, true, PRED_WIN, PRED_D);
        runCommands(TTTCore.config.get(ConfigKey.WIN_T_CMDS), round, true, PRED_WIN, PRED_T);
        runCommands(TTTCore.config.get(ConfigKey.LOSE_CMDS), round, true, PRED_LOSE);
        runCommands(TTTCore.config.get(ConfigKey.LOSE_I_CMDS), round, true, PRED_LOSE, PRED_I);
        runCommands(TTTCore.config.get(ConfigKey.LOSE_IND_CMDS), round, true, PRED_LOSE, PRED_IND);
        runCommands(TTTCore.config.get(ConfigKey.LOSE_D_CMDS), round, true, PRED_LOSE, PRED_D);
        runCommands(TTTCore.config.get(ConfigKey.LOSE_T_CMDS), round, true, PRED_LOSE, PRED_T);
    }
}
