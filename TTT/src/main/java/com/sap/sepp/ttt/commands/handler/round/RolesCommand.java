package com.sap.sepp.ttt.commands.handler.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RolesCommand: 14.04.2020 23:33 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.gamemode.RoleHelper;
import org.bukkit.command.CommandSender;

public class RolesCommand extends CommandHandler {
    public RolesCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        String arenaName = this.args[1];
        Optional<Arena> arena = TTTCore.mg.getArena(this.args[1]);
        if (!arena.isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT)
                    .withReplacements(Color.EM + arenaName + Color.ALERT).sendTo(this.sender);
            return;
        }
        if (!arena.get().getRound().isPresent()) {
            TTTCore.locale.getLocalizable("error.round.dne").withPrefix(Color.ALERT)
                    .withReplacements(Color.EM + arenaName + Color.ALERT).sendTo(this.sender);
            return;
        }
        TTTCore.locale.getLocalizable("fragment.in-arena-all").withPrefix(Color.INFO)
                .withReplacements(Color.EM + arena.get().getDisplayName() + Color.INFO).sendTo(this.sender);
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = arena.get().getRound().get().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            this.sender.sendMessage("    " + Color.SECONDARY + ch.getName() + ": " +
                    RoleHelper.genRoleMessage(this.sender, ch));
        }
    }
}