package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class KarmaHelper: 14.04.2020 23:52 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

public final class KarmaHelper {
    private static final int BASE_KARMA = 1000;

    private static HashMap<UUID, Integer> playerKarma = new HashMap<>();

    public static void saveKarma(Round round) {
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            saveKarma(ch);
        }
    }

    public static void saveKarma(Challenger challenger) {
        if (challenger.getMetadata().containsKey("pureSpectator"))
            return;
        playerKarma.put(challenger.getUniqueId(), getKarma(challenger));
        File karmaFile = new File(TTTCore.getPlugin().getDataFolder(), "karma.yml");
        try {
            if (karmaFile.exists()) {
                YamlConfiguration karmaYaml = new YamlConfiguration();
                karmaYaml.load(karmaFile);
                karmaYaml.set(challenger.getUniqueId().toString(), getKarma(challenger));
                karmaYaml.save(karmaFile);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void loadKarma(UUID uuid) throws InvalidConfigurationException, IOException {
        File karmaFile = new File(TTTCore.getPlugin().getDataFolder(), "karma.yml");
        if (!karmaFile.exists())
            TTTCore.getInstance().createFile("karma.yml");
        YamlConfiguration karmaYaml = new YamlConfiguration();
        karmaYaml.load(karmaFile);
        if (karmaYaml.isSet(uuid.toString())) {
            if (karmaYaml.getInt(uuid.toString()) > TTTCore.config.get(ConfigKey.KARMA_MAX)) {
                playerKarma.put(uuid, TTTCore.config.get(ConfigKey.KARMA_MAX));
            } else {
                playerKarma.put(uuid, karmaYaml.getInt(uuid.toString()));
            }
        } else {
            playerKarma.put(uuid, TTTCore.config.get(ConfigKey.KARMA_STARTING));
        }
    }

    public static int getKarma(UUID uuid) {
        if (!playerKarma.containsKey(uuid))
            try {
                loadKarma(uuid);
            } catch (InvalidConfigurationException|IOException ex) {
                throw new RuntimeException(ex);
            }
        return playerKarma.get(uuid);
    }

    public static void allocateKarma(Round round) {
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger challenger = unmodifiableIterator.next();
            addKarma(challenger, TTTCore.config.get(ConfigKey.KARMA_ROUND_INCREMENT), true);
            if (!challenger.getMetadata().containsKey("hasTeamKilled")) {
                int karmaHeal = TTTCore.config.get(ConfigKey.KARMA_CLEAN_BONUS);
                if (getKarma(challenger) > 1000 && TTTCore.config.get(ConfigKey.KARMA_MAX) - 1000 > 0)
                    karmaHeal = (int)Math.round(TTTCore.config
                            .get(ConfigKey.KARMA_CLEAN_BONUS) * Math.pow(0.0D, (
                            getKarma(challenger) - 1000) / (TTTCore.config
                            .get(ConfigKey.KARMA_MAX) - 1000) * TTTCore.config
                            .get(ConfigKey.KARMA_CLEAN_HALF)));
                addKarma(challenger, karmaHeal, true);
            }
        }
    }

    private static int getDamagePenalty(double damage, int victimKarma) {
        return tryRound(victimKarma * damage * TTTCore.config.get(ConfigKey.KARMA_RATIO));
    }

    private static int getKillPenalty(int victimKarma) {
        return getDamagePenalty(TTTCore.config.get(ConfigKey.KARMA_KILL_PENALTY), victimKarma);
    }

    private static int getDamageReward(double damage) {
        return tryRound(TTTCore.config.get(ConfigKey.KARMA_MAX) * damage * TTTCore.config
                .get(ConfigKey.KARMA_TRAITORDMG_RATIO));
    }

    private static int getKillReward() {
        return getDamageReward(TTTCore.config.get(ConfigKey.KARMA_TRAITORKILL_BONUS));
    }

    public static void applyDamageKarma(Challenger damager, Challenger victim, double damage) {
        if (RoleHelper.isTraitor(damager) == RoleHelper.isTraitor(victim)) {
            subtractKarma(damager, getDamagePenalty(damage, getKarma(victim)));
        } else if (!RoleHelper.isTraitor(damager)) {
            addKarma(damager, getDamageReward(damage));
        }
    }

    public static void applyKillKarma(Challenger killer, Challenger victim) {
        if (RoleHelper.isTraitor(killer) == RoleHelper.isTraitor(victim)) {
            subtractKarma(killer, getKillPenalty(getKarma(victim)));
        } else if (!RoleHelper.isTraitor(killer)) {
            addKarma(killer, getKillReward());
        }
    }

    private static void handleKick(Challenger player) {
        Player p = TTTCore.getPlugin().getServer().getPlayer(player.getName());
        assert p != null;
        player.removeFromRound();
        if (TTTCore.config.get(ConfigKey.KARMA_LOW_BAN)) {
            try {
                BanHelper.ban(p.getUniqueId(), TTTCore.config.get(ConfigKey.KARMA_LOW_BAN_MINUTES));
                if (TTTCore.config.get(ConfigKey.KARMA_LOW_BAN_MINUTES) < 0) {
                    TTTCore.locale.getLocalizable("info.personal.ban.perm.karma")
                            .withPrefix(Color.INFO)
                            .withReplacements(TTTCore.config.get(ConfigKey.KARMA_LOW_AUTOKICK) + "").sendTo(p);
                } else {
                    TTTCore.locale.getLocalizable("info.personal.ban.temp.karma")
                            .withPrefix(Color.INFO)
                            .withReplacements(TTTCore.config.get(ConfigKey.KARMA_LOW_BAN_MINUTES) + "", TTTCore.config
                                    .get(ConfigKey.KARMA_LOW_AUTOKICK) + "").sendTo(p);
                }
            } catch (InvalidConfigurationException|IOException ex) {
                ex.printStackTrace();
                TTTCore.log.severe(TTTCore.locale.getLocalizable("error.plugin.ban")
                        .withPrefix(Color.ALERT + "").withReplacements(player.getName()).localize());
            }
        } else {
            TTTCore.locale.getLocalizable("info.personal.kick.karma").withPrefix(Color.INFO)
                    .withReplacements(TTTCore.config.get(ConfigKey.KARMA_LOW_AUTOKICK) + "").sendTo(p);
        }
    }

    public static int getKarma(Challenger mp) {
        return mp.getMetadata().containsKey("karma") ? (Integer) mp
                .getMetadata().get("karma").get() : TTTCore.config
                .get(ConfigKey.KARMA_STARTING);
    }

    public static void applyDamageReduction(Challenger challenger) {
        double damageRed;
        int baseKarma = getKarma(challenger) - 1000;
        double a = -2.0E-6D;
        double b = 7.0E-4D;
        double strictA = -2.5E-6D;
        double minDamageRed = 0.01D;
        if (TTTCore.config.get(ConfigKey.KARMA_STRICT)) {
            damageRed = -2.0E-6D * Math.pow(baseKarma, 2.0D) + 7.0E-4D * baseKarma + 1.0D;
        } else {
            damageRed = -2.5E-6D * Math.pow(baseKarma, 2.0D) + 1.0D;
        }
        if (damageRed <= 0.0D)
            damageRed = 0.01D;
        challenger.getMetadata().set("damageRed", damageRed);
    }

    public static double getDamageReduction(Challenger challenger) {
        return challenger.getMetadata().containsKey("damageRed") ? (Double) challenger
                .getMetadata().get("damageRed").get() : 1.0D;
    }

    public static void applyKarma(Challenger challenger) {
        int karma = Math.max(getKarma(challenger.getUniqueId()), TTTCore.config.get(ConfigKey.KARMA_LOW_AUTOKICK));
        challenger.getMetadata().set("karma", karma);
        challenger.getMetadata().set("displayKarma", karma);
    }

    public static void resetKarma(UUID uuid) {
        playerKarma.remove(uuid);
    }

    private static void addKarma(Challenger challenger, int amount) {
        addKarma(challenger, amount, false);
    }

    private static void addKarma(Challenger challenger, int amount, boolean force) {
        if (!force && challenger.getRound().getLifecycleStage() == Stage.ROUND_OVER)
            return;
        int karma = getKarma(challenger);
        if (amount == 0 && TTTCore.config.get(ConfigKey.KARMA_ROUND_TO_ONE))
            amount = 1;
        if (karma + amount < TTTCore.config.get(ConfigKey.KARMA_MAX)) {
            karma += amount;
        } else if (karma < TTTCore.config.get(ConfigKey.KARMA_MAX)) {
            karma = TTTCore.config.get(ConfigKey.KARMA_MAX);
        }
        challenger.getMetadata().set("karma", karma);
        if (karma < TTTCore.config.get(ConfigKey.KARMA_LOW_AUTOKICK))
            handleKick(challenger);
        if (TTTCore.config.get(ConfigKey.KARMA_DEBUG))
            TTTCore.kLog.info("[TTT Karma Debug] " + challenger.getName() + ": " + ((amount > 0) ? "+" : "") + amount + ". New value: " + karma);
    }

    private static void subtractKarma(Challenger challenger, int amount) {
        addKarma(challenger, -amount);
        challenger.getMetadata().set("hasTeamKilled", Boolean.TRUE);
    }

    private static int tryRound(double penetration) {
        int val = (int)penetration;
        if (val == 0 && TTTCore.config.get(ConfigKey.KARMA_ROUND_TO_ONE))
            return 1;
        return val;
    }
}