package com.sap.sepp.ttt.utils.constant;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class MetadataKey: 14.04.2020 23:47 by sebip
*/

public class MetadataKey {
    public static class Arena {
        public static final String ARENA_START_TIME = "startTime";

        public static final String ARENA_ROUND_TALLY = "roundTally";
    }

    public static class Player {
        public static final String BODY = "body";

        public static final String BODY_FOUND = "bodyFound";

        public static final String KARMA = "karma";

        public static final String DISPLAY_KARMA = "displayKarma";

        public static final String DAMAGE_REDUCTION = "damageRed";

        public static final String TEAM_KILLED = "hasTeamKilled";

        public static final String WATCH_GAME_MODE = "watchGm";

        public static final String PURE_SPECTATOR = "pureSpectator";

        public static final String TEAM_NAME = "teamName";

        public static final String SEARCHING_BODY = "searchingBody";
    }

    public static class Round {
        public static final String BODY_LIST = "bodies";

        public static final String SCOREBOARD_MANAGER = "scoreboardManager";

        public static final String TRAITOR_VICTORY = "t-victory";

        public static final String ROUND_RESTARTING = "restarting";

        public static final String ROUND_DURATION = "duration";

        public static final String ROUND_RESULT = "result";

        public static final String ROUND_PLAYER_COUNT = "players";

        public static final String HASTE_TIME = "hasteTime";
    }
}