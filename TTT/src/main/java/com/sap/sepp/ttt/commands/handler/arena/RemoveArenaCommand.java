package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RemoveArenaCommand: 14.04.2020 23:29 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.gamemode.ArenaHelper;
import org.bukkit.command.CommandSender;

public class RemoveArenaCommand extends CommandHandler {
    public RemoveArenaCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        String name = this.args[1];
        try {
            TTTCore.mg.removeArena(name);
            ArenaHelper.updateShuffledArenas();
            TTTCore.locale.getLocalizable("info.personal.arena.remove.success").withPrefix(Color.INFO)
                    .withReplacements(Color.EM + name + Color.INFO).sendTo(this.sender);
        } catch (IllegalArgumentException ex) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }
}
