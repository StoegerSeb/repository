package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RemoveSpawnCommand: 14.04.2020 23:30 by sebip
*/

import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.utils.physical.Location3D;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.data.DataVerificationHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveSpawnCommand extends CommandHandler {
    public RemoveSpawnCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (!TTTCore.mg.getArena(this.args[1]).isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        Arena arena = TTTCore.mg.getArena(this.args[1]).get();
        int x = 0;
        int y = 0;
        int z = 0;
        int index = Integer.MAX_VALUE;
        if (this.args.length == 2) {
            if (this.sender instanceof Player) {
                x = ((Player)this.sender).getLocation().getBlockX();
                y = ((Player)this.sender).getLocation().getBlockY();
                z = ((Player)this.sender).getLocation().getBlockZ();
            } else {
                TTTCore.locale.getLocalizable("error.command.ingame").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
        } else if (this.args.length == 3) {
            if (DataVerificationHelper.isInt(this.args[2])) {
                index = Integer.parseInt(this.args[2]);
            } else {
                printInvalidArgsError();
                return;
            }
        } else if (this.args.length == 5) {
            if (DataVerificationHelper.isInt(this.args[2]) && DataVerificationHelper.isInt(this.args[3]) && DataVerificationHelper.isInt(this.args[4])) {
                x = Integer.parseInt(this.args[2]);
                y = Integer.parseInt(this.args[3]);
                z = Integer.parseInt(this.args[4]);
            } else {
                printInvalidArgsError();
                return;
            }
        }
        try {
            if (index != Integer.MAX_VALUE) {
                arena.removeSpawnPoint(index);
                TTTCore.locale.getLocalizable("info.personal.arena.removespawn.index").withPrefix(Color.INFO)
                        .withReplacements(Color.EM + index + Color.INFO, Color.EM + arena.getDisplayName() + Color.INFO).sendTo(this.sender);
            } else {
                arena.removeSpawnPoint(new Location3D(arena.getWorld(), x, y, z));
                TTTCore.locale.getLocalizable("info.personal.arena.removespawn.coords").withPrefix(Color.INFO)
                        .withReplacements(Color.EM + "(" + x + ", " + y + ", " + z + ")" + Color.INFO, Color.EM + arena.getDisplayName() + Color.INFO).sendTo(this.sender);
            }
        } catch (IllegalArgumentException ex) {
            TTTCore.locale.getLocalizable("error.arena.removespawn.missing").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }
}
