package com.sap.sepp.ttt.commands;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SpecialCommandManager: 14.04.2020 23:27 by sebip
*/

import com.sap.sepp.ttt.TTTBootstrap;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SpecialCommandManager implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("ttt")) {
            if (sender.hasPermission("ttt.build.warn")) {
                TTTBootstrap.locale.getLocalizable("error.plugin.flint").withPrefix(Color.ALERT)
                        .withReplacements(new String[] { "4" }).sendTo(sender);
            } else {
                TTTBootstrap.locale.getLocalizable("error.plugin.disabled").withPrefix(Color.ALERT)
                        .sendTo(sender);
            }
            return true;
        }
        return false;
    }
}
