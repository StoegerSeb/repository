package com.sap.sepp.ttt.commands.handler.use;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class JoinCommand: 14.04.2020 23:34 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.round.JoinResult;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.helper.gamemode.BanHelper;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinCommand extends CommandHandler {
    public JoinCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (BanHelper.checkBan(((Player)this.sender).getUniqueId()))
            return;
        if (this.args.length < 2 && TTTCore.config.get(ConfigKey.OPERATING_MODE) != OperatingMode.DEDICATED) {
            TTTCore.locale.getLocalizable("error.command.too-few-args").sendTo(this.sender);
            return;
        }
        Optional<Arena> arena = (TTTCore.config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED) ? Optional.of(TTTCore.getDedicatedArena()) : TTTCore.mg.getArena(this.args[1]);
        if (!arena.isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        Round round = arena.get().getOrCreateRound();
        if ((round.getLifecycleStage() == Stage.PLAYING || round.getLifecycleStage() == Stage.ROUND_OVER) &&
                !TTTCore.config.get(ConfigKey.ALLOW_JOIN_AS_SPECTATOR))
            TTTCore.locale.getLocalizable("error.round.in-progress").withPrefix(Color.ALERT).sendTo(this.sender);
        JoinResult result = round.addChallenger(((Player)this.sender).getUniqueId());
        if (result.getStatus() != JoinResult.Status.SUCCESS) {
            switch (result.getStatus()) {
                case ALREADY_IN_ROUND:
                    TTTCore.locale.getLocalizable("error.round.inside").withPrefix(Color.ALERT).sendTo(this.sender);
                    return;
                case INTERNAL_ERROR:
                    throw new RuntimeException(result.getThrowable());
                case PLAYER_OFFLINE:
                    TTTCore.locale.getLocalizable("error.round.player-offline").withPrefix(Color.ALERT).sendTo(this.sender);
                    return;
                case ROUND_FULL:
                    TTTCore.locale.getLocalizable("error.round.full").withPrefix(Color.ALERT).sendTo(this.sender);
                    return;
            }
            throw new AssertionError("Failed to determine reaosn for RoundJoinException. Report this immediately.");
        }
    }
}