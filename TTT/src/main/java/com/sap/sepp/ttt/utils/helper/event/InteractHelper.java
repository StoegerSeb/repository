package com.sap.sepp.ttt.utils.helper.event;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class InteractHelper: 14.04.2020 23:50 by sebip
*/

public final class InteractHelper {
    public static void handleEvent(PlayerInteractEvent event, Challenger opener) {
        Location3D clicked = new Location3D(event.getClickedBlock().getWorld().getName(), event.getClickedBlock().getX(), event.getClickedBlock().getY(), event.getClickedBlock().getZ());
        if (event.getClickedBlock().getType() != Material.CHEST && event
                .getClickedBlock().getType() != Material.TRAPPED_CHEST)
            return;
        List<Body> bodies = (List<Body>)opener.getRound().getMetadata().get("bodies").orNull();
        if (bodies == null)
            return;
        for (Body body : bodies) {
            if (!body.getLocation().equals(clicked))
                continue;
            if (opener.getMetadata().containsKey("detective") && !opener.isSpectating() &&
                    event.getPlayer().getItemInHand() != null && event
                    .getPlayer().getItemInHand().getType() == Material.COMPASS && event
                    .getPlayer().getItemInHand().getItemMeta() != null && event
                    .getPlayer().getItemInHand().getItemMeta().getDisplayName() != null && event
                    .getPlayer().getItemInHand().getItemMeta().getDisplayName().endsWith(TTTCore.locale
                            .getLocalizable("item.dna-scanner.name").localize())) {
                event.setCancelled(true);
                doDnaCheck(body, opener, event.getPlayer());
                return;
            }
            event.setCancelled(true);
            searchBody(body, event.getPlayer(), ((Chest)event.getClickedBlock().getState()).getInventory().getSize());
            if (opener.isSpectating() || event.getPlayer().isSneaking()) {
                TTTCore.locale.getLocalizable("info.personal.status.discreet-search").withPrefix(Color.INFO)
                        .sendTo((CommandSender)event.getPlayer());
                return;
            }
            if (!body.isFound()) {
                String color;
                Optional<Challenger> bodyPlayer = TTTCore.mg.getChallenger(body.getPlayer());
                switch (body.getRole()) {
                    case "innocent":
                        color = Color.INNOCENT;
                        break;
                    case "traitor":
                        color = Color.TRAITOR;
                        break;
                    case "detective":
                        color = Color.DETECTIVE;
                        break;
                    default:
                        event.getPlayer().sendMessage("Something's gone terribly wrong inside the TTT plugin. Please notify an admin.");
                        throw new AssertionError("Failed to determine role of found body. Report this immediately.");
                }
                body.setFound();
                if (bodyPlayer.isPresent() && ((Challenger)bodyPlayer.get()).getRound() == body.getRound()) {
                    ((Challenger)bodyPlayer.get()).getMetadata().set("bodyFound", Boolean.valueOf(true));
                    ScoreboardManager sm = (ScoreboardManager)body.getRound().getMetadata().get("scoreboardManager").get();
                    sm.updateEntry((Challenger)bodyPlayer.get());
                }
                Localizable loc = TTTCore.locale.getLocalizable("info.global.round.event.body-find").withPrefix(color);
                Localizable roleMsg = TTTCore.locale.getLocalizable("info.global.round.event.body-find." + body.getRole());
                for (UnmodifiableIterator<Challenger> unmodifiableIterator = body.getRound().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                    Challenger c = unmodifiableIterator.next();
                    Player pl = Bukkit.getPlayer(c.getUniqueId());
                    loc.withReplacements(new String[] { event.getPlayer().getName(), body.getName() }).withSuffix(" " + roleMsg.localizeFor((CommandSender)pl)).sendTo((CommandSender)pl);
                }
            }
        }
    }

    public static void doDnaCheck(Body body, Challenger ch, Player pl) {
        if (!body.isFound()) {
            TTTCore.locale.getLocalizable("info.personal.status.dna-id")
                    .withPrefix(Color.ALERT).sendTo((CommandSender)pl);
            return;
        }
        if (!body.getKiller().isPresent() || body.getExpiry() == -1L) {
            TTTCore.locale.getLocalizable("info.personal.status.no-dna")
                    .withPrefix(Color.ALERT).sendTo((CommandSender)pl);
            return;
        }
        if (System.currentTimeMillis() > body.getExpiry()) {
            TTTCore.locale.getLocalizable("info.personal.status.dna-decayed")
                    .withPrefix(Color.ALERT).sendTo((CommandSender)pl);
            return;
        }
        Player killer = Bukkit.getPlayer((UUID)body.getKiller().get());
        if (killer != null && TTTCore.mg
                .getChallenger(killer.getUniqueId()).isPresent() &&
                !((Challenger)TTTCore.mg.getChallenger(killer.getUniqueId()).get()).isSpectating()) {
            ch.getMetadata().set("tracking", body.getKiller().get());
            TTTCore.locale.getLocalizable("info.personal.status.collect-dna")
                    .withPrefix(Color.INFO)
                    .withReplacements(new String[] { body.getName() }).sendTo((CommandSender)pl);
        } else {
            TTTCore.locale.getLocalizable("error.round.killer-left")
                    .withPrefix(Color.ALERT).sendTo((CommandSender)pl);
        }
    }

    public static void handleGun(PlayerInteractEvent event) {
        if ((event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.RIGHT_CLICK_AIR) || event
                .getPlayer().getItemInHand() == null || event
                .getPlayer().getItemInHand().getItemMeta() == null || event
                .getPlayer().getItemInHand().getItemMeta().getDisplayName() == null)
            return;
        if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName()
                .endsWith(TTTCore.locale.getLocalizable("item.gun.name").localize())) {
            Optional<Challenger> ch = TTTCore.mg.getChallenger(event.getPlayer().getUniqueId());
            if (!ch.isPresent() || ((Challenger)ch.get()).isSpectating() || ((Challenger)ch
                    .get()).getRound().getLifecycleStage() == Stage.WAITING || ((Challenger)ch
                    .get()).getRound().getLifecycleStage() == Stage.PREPARING)
                return;
            event.setCancelled(true);
            if (event.getPlayer().getInventory().contains(Material.ARROW) ||
                    !((Boolean)TTTCore.config.get(ConfigKey.REQUIRE_AMMO_FOR_GUNS)).booleanValue()) {
                if (((Boolean)TTTCore.config.get(ConfigKey.REQUIRE_AMMO_FOR_GUNS)).booleanValue()) {
                    InventoryHelper.removeArrow((Inventory)event.getPlayer().getInventory());
                    event.getPlayer().updateInventory();
                }
                event.getPlayer().launchProjectile(Arrow.class);
            } else {
                TTTCore.locale.getLocalizable("info.personal.status.no-ammo")
                        .withPrefix(Color.ALERT).sendTo((CommandSender)event.getPlayer());
            }
        }
    }

    private static void searchBody(Body body, Player player, int size) {
        String prefix;
        Inventory inv = Bukkit.createInventory((InventoryHolder)player, size);
        ItemStack id = new ItemStack(TTTCore.HALLOWEEN ? Material.JACK_O_LANTERN : Material.PAPER, 1);
        ItemMeta idMeta = id.getItemMeta();
        idMeta.setDisplayName(TTTCore.locale.getLocalizable("item.id.name").localizeFor((CommandSender)player));
        List<String> idLore = new ArrayList<>();
        idLore.add(TTTCore.locale.getLocalizable("item.id.desc").withReplacements(new String[] { body.getName() }).localizeFor((CommandSender)player));
        idMeta.setLore(idLore);
        id.setItemMeta(idMeta);
        inv.addItem(new ItemStack[] { id });
        String roleStr = body.getRole();
        switch (roleStr) {
            case "detective":
                prefix = Color.DETECTIVE;
                break;
            case "innocent":
                prefix = Color.INNOCENT;
                break;
            case "traitor":
                prefix = Color.TRAITOR;
                break;
            default:
                throw new AssertionError();
        }
        ItemStack roleId = MaterialHelper.instance().createRoleWool(roleStr);
        ItemMeta roleIdMeta = roleId.getItemMeta();
        roleIdMeta.setDisplayName(TTTCore.locale.getLocalizable("fragment." + roleStr).withPrefix(prefix)
                .localizeFor((CommandSender)player));
        roleIdMeta.setLore(Collections.singletonList(TTTCore.locale
                .getLocalizable("item.role." + roleStr).localizeFor((CommandSender)player)));
        roleId.setItemMeta(roleIdMeta);
        inv.addItem(new ItemStack[] { roleId });
        ItemStack clock = new ItemStack((MaterialHelper.instance()).CLOCK, 1);
        ItemMeta clockMeta = clock.getItemMeta();
        long deathSeconds = (System.currentTimeMillis() - body.getDeathTime()) / 1000L;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(2);
        String deathTime = nf.format(deathSeconds / 60L) + ":" + nf.format(deathSeconds % 60L);
        clockMeta.setDisplayName(deathTime);
        clockMeta.setLore(CollectionsHelper.formatLore(TTTCore.locale
                .getLocalizable("item.deathclock.desc").withReplacements(new String[] { deathTime }).withReplacements(new String[] { deathTime }).localizeFor((CommandSender)player)));
        clock.setItemMeta(clockMeta);
        inv.addItem(new ItemStack[] { clock });
        if (body.getExpiry() > System.currentTimeMillis()) {
            long decaySeconds = (body.getExpiry() - System.currentTimeMillis()) / 1000L;
            NumberFormat numberFormat = NumberFormat.getInstance();
            numberFormat.setMinimumIntegerDigits(2);
            String decayTime = numberFormat.format(decaySeconds / 60L) + ":" + numberFormat.format(decaySeconds % 60L);
            ItemStack dna = new ItemStack((MaterialHelper.instance()).LEAD, 1);
            ItemMeta dnaMeta = dna.getItemMeta();
            dnaMeta.setDisplayName(TTTCore.locale.getLocalizable("item.dna.name").localizeFor((CommandSender)player));
            dnaMeta.setLore(CollectionsHelper.formatLore(TTTCore.locale
                    .getLocalizable("item.dna.desc").withReplacements(new String[] { decayTime }).localizeFor((CommandSender)player)));
            dna.setItemMeta(dnaMeta);
            inv.addItem(new ItemStack[] { dna });
        }
        player.openInventory(inv);
        ((Challenger)TTTCore.mg.getChallenger(player.getUniqueId()).get()).getMetadata().set("searchingBody", Boolean.valueOf(true));
    }
}
