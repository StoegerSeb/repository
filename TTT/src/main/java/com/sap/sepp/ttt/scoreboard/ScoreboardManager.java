package com.sap.sepp.ttt.scoreboard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ScoreboardManager: 14.04.2020 23:39 by sebip
*/


import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.round.Round;
import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardManager {
    private static final String OBJECTIVE_ID = "ttt";

    private static final boolean FULL_ENTRY_SUPPORT;

    private static final ImmutableMap<String, String> LIFE_STATUS_PREFIXES = ImmutableMap.builder()
            .put("alive", "")
            .put("mia", ")
                    .put("dead", ")
                            .build();

    private Round round;

    private Scoreboard iBoard = createBoard(false);

    private Scoreboard tBoard = createBoard(true);

    static {
        boolean support = false;
        try {
            Scoreboard.class.getMethod("getEntryTeam", String.class);
            support = true;
        } catch (NoSuchMethodException noSuchMethodException) {}
        FULL_ENTRY_SUPPORT = support;
    }

    public ScoreboardManager(Round round) {
        this.round = round;
    }

    public Round getRound() {
        return this.round;
    }

    private Scoreboard getInnocentBoard() {
        return this.iBoard;
    }

    private Scoreboard getTraitorBoard() {
        return this.tBoard;
    }

    private Scoreboard createBoard(boolean isTBoard) {
        Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective obj = sb.registerNewObjective("ttt", "dummy");
        obj.setDisplayName("Players");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        registerTeams(sb, isTBoard);
        return sb;
    }

    private void registerTeams(Scoreboard sb, boolean isTBoard) {
        String[] roles = { "innocent", "traitor", "detective" };
        String[] aliveStatuses = { "alive", "mia", "dead" };
        for (String role : roles) {
            for (String alive : aliveStatuses) {
                String teamId = role.charAt(0) + "" + alive.charAt(0);
                if (sb.getTeam(teamId) != null)
                    return;
                Team team = sb.registerNewTeam(teamId);
                String rolePrefix = role.equals("detective") ? Color.DETECTIVE : ((role.equals("traitor") && isTBoard) ? Color.TRAITOR : "");
                String alivePrefix = DataVerificationHelper.fromNullableString((String)LIFE_STATUS_PREFIXES.get(alive));
                team.setPrefix(rolePrefix + alivePrefix);
            }
        }
    }

    private void applyTeam(Challenger ch) {
        String role = RoleHelper.isTraitor(ch) ? "traitor" : (ch.getMetadata().containsKey("detective") ? "detective" : "innocent");
        String alive = !ch.isSpectating() ? "alive" : (ch.getMetadata().containsKey("bodyFound") ? "dead" : "mia");
        String teamName = role.charAt(0) + "" + alive.charAt(0);
        ch.getMetadata().set("teamName", teamName);
    }

    public void applyScoreboard(Challenger ch) {
        Bukkit.getPlayer(ch.getUniqueId()).setScoreboard(RoleHelper.isTraitor(ch) ? this.tBoard : this.iBoard);
    }

    public void updateAllEntries() {
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = getRound().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            updateEntry(ch);
        }
    }

    private void updateEntry(Challenger ch, Scoreboard sb) {
        assert ch.getRound() == getRound();
        if (ch.getMetadata().has("pureSpectator"))
            return;
        String teamName = (String)ch.getMetadata().get("teamName").get();
        if (sb.getTeam(teamName) == null)
            registerTeams(sb, (sb == this.tBoard));
        for (Team team : sb.getTeams()) {
            if (FULL_ENTRY_SUPPORT) {
                if (team.getName().equals(teamName) && !team.hasEntry(ch.getName())) {
                    team.addEntry(ch.getName());
                    continue;
                }
                if (!team.getName().equals(teamName) && team.hasEntry(ch.getName()))
                    team.removeEntry(ch.getName());
                continue;
            }
            Player pl = Bukkit.getPlayer(ch.getUniqueId());
            if (team.getName().equals(teamName) && !team.hasPlayer((OfflinePlayer)pl)) {
                team.addPlayer((OfflinePlayer)pl);
                continue;
            }
            if (!team.getName().equals(teamName) && team.hasPlayer((OfflinePlayer)pl))
                team.removePlayer((OfflinePlayer)pl);
        }
        sb.getObjective("ttt").getScore(ch.getName())
                .setScore(((Integer)ch.getMetadata().get("displayKarma").or(Integer.valueOf(1000))).intValue());
    }

    public void updateEntry(Challenger ch) {
        applyTeam(ch);
        updateEntry(ch, getInnocentBoard());
        updateEntry(ch, getTraitorBoard());
    }

    private void remove(Challenger ch, Scoreboard sm) {
        sm.resetScores(ch.getName());
    }

    public void remove(Challenger ch) {
        remove(ch, getInnocentBoard());
        remove(ch, getTraitorBoard());
    }

    public void updateTitle() {
        updateTitle(this.iBoard.getObjective("ttt"), false);
        updateTitle(this.tBoard.getObjective("ttt"), true);
    }

    private void updateTitle(Objective obj, boolean t) {
        obj.setDisplayName(getTitle(t));
    }

    private String getTitle(boolean t) {
        StringBuilder title = new StringBuilder();
        title.append(Color.SECONDARY);
        title.append(TTTCore.locale.getLocalizable("fragment.stage." + this.round.getLifecycleStage().getId())
                .localize().toUpperCase());
        if (this.round.getLifecycleStage() != Stage.WAITING) {
            title.append(" - ");
            title.append(RoundHelper.getTimeDisplay(this.round, t, null));
        }
        return title.toString();
    }

    public void uninitialize() {
        this.iBoard.getObjective("ttt").unregister();
        this.tBoard.getObjective("ttt").unregister();
    }
}
