package com.sap.sepp.ttt;

import com.google.common.base.StandardSystemProperty;
import com.google.common.collect.ImmutableSet;
import com.sap.sepp.api.FlintCore;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.arena.SpawningMode;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.steel.utils.TelemetryRunner;
import com.sap.sepp.ttt.commands.CommandManager;
import com.sap.sepp.ttt.lib.LocaleManager;
import com.sap.sepp.ttt.lib.TitleUtil;
import com.sap.sepp.ttt.listener.ListenerManager;
import com.sap.sepp.ttt.lobby.StatusLobbySignPopulator;
import com.sap.sepp.ttt.utils.compatibility.LegacyConfigFolderRenamer;
import com.sap.sepp.ttt.utils.compatibility.LegacyMglibStorageConverter;
import com.sap.sepp.ttt.utils.compatibility.LegacyMglibStorageDeleter;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.config.TTTConfig;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.helper.gamemode.ArenaHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.ContributorListHelper;
import com.sap.sepp.ttt.utils.helper.platform.BungeeHelper;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.logging.Logger;

public class TTTCore {
    private static TTTCore INSTANCE;

    public static Minigame mg;

    public static Logger log;

    public static Logger kLog;

    private static JavaPlugin plugin;

    public static LocaleManager locale;

    public static TTTConfig config;

    public static ContributorListHelper clh;

    private static TelemetryRunner telRunner;

    public static final boolean HALLOWEEN;

    private static Arena dedicatedArena;

    private boolean legacyMinecraftVersion;

    private static final int MC_1_8_TRANSFORMED = 1008000;

    private static final int MC_1_8_8_TRANSFORMED = 1008008;

    private static final int MC_1_13_TRANSFORMED = 1013000;

    static {
        Calendar cal = Calendar.getInstance();
        HALLOWEEN = (cal.get(2) == 9 && cal.get(5) == 31);
    }

    TTTCore(JavaPlugin plugin, LocaleManager localeManager) {
        if (INSTANCE != null)
            throw new IllegalStateException("Cannot initialize singleton class TTTCore more than once");
        INSTANCE = this;
        TTTCore.plugin = plugin;
        locale = localeManager;
    }

    public void initialize() {
        log = plugin.getLogger();
        kLog = Logger.getLogger("TTT Karma Debug");
        kLog.setParent(log);
        checkJavaVersion();
        checkBukkitVersion();
        checkIfLegacyMinecraft();
        mg = FlintCore.registerPlugin(plugin.getName());
        config = new TTTConfig(plugin.getConfig());
        if (FlintCore.getMajorVersion() != 1)
            TTTBootstrap.INSTANCE.failMajor();
        if (FlintCore.getApiRevision() < 4) {
            TTTBootstrap.INSTANCE.failMinor();
            return;
        }
        if (config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED) {
            ArenaHelper.applyNextArena();
            if (Bukkit.getOnlinePlayers().size() > 0)
                Bukkit.getScheduler().runTask(getPlugin(), new Runnable() {
                    public void run() {
                        BungeeHelper.initialize();
                    }
                });
        }
        clh = new ContributorListHelper(TTTCore.class.getResourceAsStream("/contributors.txt"));
        if (config.get(ConfigKey.ENABLE_TELEMETRY))
            telRunner = new TelemetryRunner();
        mg.setConfigValue((ConfigNode)ConfigNode.FORBIDDEN_COMMANDS, ImmutableSet.of("kit", "msg", "pm", "r", "me", "back", (Object[])new String[0]));
        mg.setConfigValue(ConfigNode.STATUS_LOBBY_SIGN_POPULATOR, new StatusLobbySignPopulator((LobbySignPopulator)mg
                .getConfigValue((ConfigNode)ConfigNode.STATUS_LOBBY_SIGN_POPULATOR)));
        applyConfigOptions();
        doCompatibilityActions();
        mg.setConfigValue(ConfigNode.SPAWNING_MODE, SpawningMode.RANDOM);
        ListenerManager.registerEventListeners();
        plugin.getCommand("ttt").setExecutor((CommandExecutor)new CommandManager());
        if (!(new File(plugin.getDataFolder(), "config.yml")).exists()) {
            plugin.saveDefaultConfig();
        } else {
            try {
                config.addMissingKeys();
            } catch (Exception ex) {
                ex.printStackTrace();
                log.severe("Failed to write new config keys!");
            }
        }
        createFile("karma.yml");
        createFile("bans.yml");
        File invDir = new File(plugin.getDataFolder() + File.separator + "inventories");
        invDir.mkdir();
        if (config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED)
            for (Player pl : Bukkit.getOnlinePlayers())
                getDedicatedArena().getOrCreateRound().addChallenger(pl.getUniqueId());
    }

    public boolean isLegacyMinecraftVersion() {
        return this.legacyMinecraftVersion;
    }

    private int getTransformedMcVersion() {
        String[] mcVersions = Bukkit.getBukkitVersion().split("-")[0].split("\\.");
        return Integer.parseInt(mcVersions[0]) * 1000000 +
                Integer.parseInt(mcVersions[1]) * 1000 + ((mcVersions.length > 2) ?
                Integer.parseInt(mcVersions[2]) : 0);
    }

    private void checkIfLegacyMinecraft() {
        this.legacyMinecraftVersion = (getTransformedMcVersion() < 1013000);
    }

    public void applyConfigOptions() {
        locale.setDefaultLocale(config.get(ConfigKey.LOCALE));
        mg.setConfigValue(ConfigNode.MAX_PLAYERS, config.get(ConfigKey.MAXIMUM_PLAYERS));
        Stage.initialize();
        mg.setConfigValue(ConfigNode.DEFAULT_LIFECYCLE_STAGES,
                ImmutableSet.of(Stage.WAITING, Stage.PREPARING, Stage.PLAYING, Stage.ROUND_OVER));
        if (config.get(ConfigKey.SEND_TITLES).booleanValue() && !TitleUtil.areTitlesSupported())
            logWarning("error.plugin.title-support");
    }

    public void deinitialize() {
        if (config.get(ConfigKey.VERBOSE_LOGGING).booleanValue())
            logInfo("info.plugin.disable", plugin.toString());
        INSTANCE = null;
        mg = null;
        log = null;
        kLog = null;
        plugin = null;
        locale = null;
        config = null;
        clh = null;
        dedicatedArena = null;
    }

    public static TTTCore getInstance() {
        return INSTANCE;
    }

    public static JavaPlugin getPlugin() {
        return plugin;
    }

    public static Arena getDedicatedArena() {
        return dedicatedArena;
    }

    public static void setDedicatedArena(Arena arena) {
        dedicatedArena = arena;
    }

    public void createFile(String s) {
        File f = new File(plugin.getDataFolder(), s);
        if (!f.exists()) {
            if (config.get(ConfigKey.VERBOSE_LOGGING).booleanValue())
                logInfo("info.plugin.compatibility.creating-file", s);
            try {
                f.createNewFile();
            } catch (Exception ex) {
                ex.printStackTrace();
                logWarning("error.plugin.file-write", s);
            }
        }
    }

    public void createLocale(String s) {
        File exLocale = new File(plugin.getDataFolder() + File.separator + "locales", s);
        if (!exLocale.exists()) {
            InputStream is = null;
            OutputStream os = null;
            try {
                File dir = new File(plugin.getDataFolder(), "locales");
                dir.mkdir();
                exLocale.createNewFile();
                is = TTTCore.class.getClassLoader().getResourceAsStream("locales/" + s);
                os = new FileOutputStream(exLocale);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = is.read(buffer)) != -1)
                    os.write(buffer, 0, len);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (is != null)
                        is.close();
                    if (os != null)
                        os.close();
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }
    }

    public void logInfo(String localizationKey, String... replacements) {
        log.info(localize(localizationKey, replacements));
    }

    public void logWarning(String localizationKey, String... replacements) {
        log.warning(localize(localizationKey, replacements));
    }

    public void logSevere(String localizationKey, String... replacements) {
        log.severe(localize(localizationKey, replacements));
    }

    private String localize(String localizationKey, String... replacements) {
        return locale.getLocalizable(localizationKey).withReplacements(replacements).localize();
    }

    private void doCompatibilityActions() {
        LegacyConfigFolderRenamer.renameLegacyFolder();
        LegacyMglibStorageConverter.convertArenaStore();
        LegacyMglibStorageConverter.convertLobbyStore();
        LegacyMglibStorageDeleter.deleteObsoleteStorage();
    }

    private void checkJavaVersion() {
        try {
            if (Float.parseFloat(StandardSystemProperty.JAVA_CLASS_VERSION.value()) < 52.0D)
                logWarning("error.plugin.old-java");
        } catch (NumberFormatException ignored) {
            logWarning("error.plugin.unknown-java");
        }
    }

    private void checkBukkitVersion() {
        int mcVer = getTransformedMcVersion();
        if (mcVer >= 1008000 && mcVer < 1008008) {
            logWarning("error.plugin.old-bukkit-1.8");
        } else if (mcVer >= 1013000) {
            logWarning("This server is running Minecraft version 1.13 or later.");
            logWarning("TTT's support for this version may be incomplete or unstable.");
        }
    }
}