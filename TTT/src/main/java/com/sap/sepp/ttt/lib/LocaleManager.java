package com.sap.sepp.ttt.lib;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LocaleManager: 14.04.2020 23:56 by sebip
*/

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.CodeSource;
import java.util.*;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LocaleManager {
    private static final String DEFAULT_LOCALE = "en_US";

    private static final String LOCALE_FOLDER = "lang";

    static final HashMap<String, List<String>> ALTERNATIVES = new HashMap<>();

    static final Logger LOGGER = Logger.getLogger("Rosetta");

    private final Plugin owner;

    private String defaultLocale = "en_US";

    HashMap<String, Properties> configs = new HashMap<>();

    static {
        ALTERNATIVES.put("en_AU", Arrays.asList("en_GB", "en_CA", "en_US"));
        ALTERNATIVES.put("en_CA", Arrays.asList("en_GB", "en_AU", "en_US"));
        ALTERNATIVES.put("en_GB", Arrays.asList("en_GB", "en_AU", "en_US"));
        ALTERNATIVES.put("en_PT", Arrays.asList("en_US", "en_CA", "en_GB", "en_AU"));
        ALTERNATIVES.put("en_US", Arrays.asList("en_CA", "en_GB", "en_AU"));
        ALTERNATIVES.put("es_AR", Arrays.asList("es_UY", "es_VE", "es_MX", "es_ES"));
        ALTERNATIVES.put("es_ES", Arrays.asList("es_MX", "es_AR", "es_UY", "es_VE"));
        ALTERNATIVES.put("es_MX", Arrays.asList("es_ES", "es_AR", "es_UY", "es_VE"));
        ALTERNATIVES.put("es_UY", Arrays.asList("es_AR", "es_VE", "es_MX", "es_ES"));
        ALTERNATIVES.put("es_VE", Arrays.asList("es_AR", "es_UY", "es_MX", "es_ES"));
        ALTERNATIVES.put("fr_CA", Arrays.asList("fr_FR"));
        ALTERNATIVES.put("fr_FR", Arrays.asList("fr_CA"));
        ALTERNATIVES.put("nb_NO", Arrays.asList("no_NO", "nn_NO"));
        ALTERNATIVES.put("nn_NO", Arrays.asList("no_NO", "nb_NO"));
        ALTERNATIVES.put("no_NO", Arrays.asList("nb_NO", "nn_NO"));
        ALTERNATIVES.put("pt_BR", Arrays.asList("pt_PT"));
        ALTERNATIVES.put("pt_PT", Arrays.asList("pt_BR"));
    }

    public LocaleManager(Plugin plugin) {
        this.owner = plugin;
        loadShippedLocales();
        loadCustomLocales();
    }

    private void loadCustomLocales() {
        File dataFolder = getOwningPlugin().getDataFolder();
        if (dataFolder.isDirectory()) {
            File localeFolder = new File(dataFolder, "lang");
            if (localeFolder.exists())
                if (localeFolder.isDirectory()) {
                    File[] contents = localeFolder.listFiles();
                    if (contents != null)
                        for (File locale : contents) {
                            if (!locale.isDirectory()) {
                                if (locale.getName().endsWith(".properties"))
                                    try {
                                        loadLocale(locale.getName().replace(".properties", ""), new FileInputStream(locale), false);
                                    } catch (IOException ex) {
                                        LOGGER.warning("Failed to load custom locale \"" + locale.getName() + "\" for plugin " +
                                                getOwningPlugin() + " (" + ex.getClass().getName() + ")");
                                    }
                            } else {
                                LOGGER.warning("Found subfolder \"" + locale.getName() + "\" within locale folder \"" + "lang" + "\" in data folder for plugin " +
                                        getOwningPlugin() + " - not loading");
                            }
                        }
                } else {
                    LOGGER.warning("Locale folder \"lang\" in data folder for plugin " +
                            getOwningPlugin() + " is not a directory - not loading custom locales");
                }
        }
    }

    private void loadShippedLocales() {
        CodeSource cs = getOwningPlugin().getClass().getProtectionDomain().getCodeSource();
        if (cs != null) {
            try {
                URL jar = cs.getLocation();
                ZipInputStream zip = new ZipInputStream(jar.openStream());
                ZipEntry entry;
                while ((entry = zip.getNextEntry()) != null) {
                    String entryName = entry.getName();
                    if (entryName.startsWith("lang/") && entryName.endsWith(".properties")) {
                        String[] arr = entryName.split("/");
                        String localeName = arr[arr.length - 1].replace(".properties", "");
                        loadLocale(localeName, zip, true);
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException("Failed to initialize LocaleManager for plugin " + getOwningPlugin() + " - Rosetta cannot continue!", ex);
            }
        } else {
            throw new RuntimeException("Failed to load code source for plugin " + getOwningPlugin() + " - Rosetta cannot continue!");
        }
    }

    private void loadLocale(String name, InputStream is, boolean printStackTrace) {
        try {
            Properties config, temp = new Properties();
            temp.load(is);
            if (this.configs.containsKey(name)) {
                config = this.configs.get(name);
                for (Map.Entry<Object, Object> e : temp.entrySet())
                    config.put(e.getKey(), e.getValue());
            } else {
                config = temp;
            }
            this.configs.put(name, config);
        } catch (IOException ex) {
            if (printStackTrace)
                ex.printStackTrace();
            LOGGER.warning("Failed to load locale " + name + " for plugin " + getOwningPlugin() + " - skipping");
        }
    }

    public Plugin getOwningPlugin() {
        return this.owner;
    }

    public String getDefaultLocale() {
        return this.defaultLocale;
    }

    public void setDefaultLocale(String locale) {
        this.defaultLocale = locale;
    }

    public Localizable getLocalizable(String key) {
        return new Localizable(this, key);
    }

    String getLocale(Player player) {
        if (NmsHelper.hasSupport())
            try {
                return NmsHelper.getLocale(player);
            } catch (IllegalAccessException| InvocationTargetException ex) {
                ex.printStackTrace();
                LOGGER.warning("Could not get locale of player " + player.getName());
            }
        return getDefaultLocale();
    }

    Collection<? extends Player> getOnlinePlayers() {
        try {
            Object obj = NmsHelper.BUKKIT_GETONLINEPLAYERS.invoke(null);
            if (obj instanceof Collection)
                return (Collection<? extends Player>)obj;
            return Lists.newArrayList((Object[])obj);
        } catch (IllegalAccessException|InvocationTargetException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private static class NmsHelper {
        private static final boolean SUPPORT;

        private static final String PACKAGE_VERSION;

        private static final Method PLAYER_SPIGOT;

        private static final Method PLAYER$SPIGOT_GETLOCALE;

        private static final Method CRAFTPLAYER_GETHANDLE;

        private static final Method BUKKIT_GETONLINEPLAYERS;

        private static final Field ENTITY_PLAYER_LOCALE;

        private static final Field LOCALE_LANGUAGE_WRAPPED_STRING;

        static {
            String[] array = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
            PACKAGE_VERSION = (array.length == 4) ? (array[3] + ".") : "";
            Method player_spigot = null;
            Method player$spigot_getLocale = null;
            Method craftPlayer_getHandle = null;
            Method bukkit_getOnlinePlayers = null;
            Field entityPlayer_locale = null;
            Field localeLanguage_wrappedString = null;
            try {
                bukkit_getOnlinePlayers = Bukkit.class.getMethod("getOnlinePlayers");
                Class<?> craftPlayer = getCraftClass("entity.CraftPlayer");
                try {
                    player_spigot = Player.class.getMethod("spigot");
                    Class<?> player$spigot = Class.forName("org.bukkit.entity.Player$Spigot");
                    player$spigot_getLocale = player$spigot.getMethod("getLocale");
                } catch (NoSuchMethodException noSuchMethodException) {}
                if (player$spigot_getLocale == null) {
                    craftPlayer_getHandle = craftPlayer.getMethod("getHandle");
                    entityPlayer_locale = getNmsClass("EntityPlayer").getDeclaredField("locale");
                    entityPlayer_locale.setAccessible(true);
                    if (entityPlayer_locale.getType().getSimpleName().equals("LocaleLanguage"))
                        try {
                            localeLanguage_wrappedString = entityPlayer_locale.getType().getDeclaredField("e");
                        } catch (NoSuchFieldException ex) {
                            localeLanguage_wrappedString = entityPlayer_locale.getType().getDeclaredField("d");
                        }
                }
            } catch (ClassNotFoundException|NoSuchFieldException|NoSuchMethodException ex) {
                ex.printStackTrace();
                LocaleManager.LOGGER.severe("Cannot initialize NMS components - per-player localization disabled");
            }
            PLAYER_SPIGOT = player_spigot;
            PLAYER$SPIGOT_GETLOCALE = player$spigot_getLocale;
            CRAFTPLAYER_GETHANDLE = craftPlayer_getHandle;
            BUKKIT_GETONLINEPLAYERS = bukkit_getOnlinePlayers;
            ENTITY_PLAYER_LOCALE = entityPlayer_locale;
            LOCALE_LANGUAGE_WRAPPED_STRING = localeLanguage_wrappedString;
            SUPPORT = (CRAFTPLAYER_GETHANDLE != null);
        }

        private static boolean hasSupport() {
            return SUPPORT;
        }

        private static String getLocale(Player player) throws IllegalAccessException, InvocationTargetException {
            if (PLAYER$SPIGOT_GETLOCALE != null)
                return (String)PLAYER$SPIGOT_GETLOCALE.invoke(PLAYER_SPIGOT.invoke(player), new Object[0]);
            Object entityPlayer = CRAFTPLAYER_GETHANDLE.invoke(player);
            Object locale = ENTITY_PLAYER_LOCALE.get(entityPlayer);
            if (LOCALE_LANGUAGE_WRAPPED_STRING != null)
                return (String)LOCALE_LANGUAGE_WRAPPED_STRING.get(locale);
            return (String)locale;
        }

        private static Class<?> getCraftClass(String className) throws ClassNotFoundException {
            return Class.forName("org.bukkit.craftbukkit." + PACKAGE_VERSION + className);
        }

        private static Class<?> getNmsClass(String className) throws ClassNotFoundException {
            return Class.forName("net.minecraft.server." + PACKAGE_VERSION + className);
        }
    }
}