package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ArenaHelper: 14.04.2020 23:50 by sebip
*/

import com.google.common.collect.Lists;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.CycleMode;

import java.util.Collections;
import java.util.List;

public final class ArenaHelper {
    public static List<Arena> shuffledArenas;

    public static int arenaIndex = 0;

    static {
        updateShuffledArenas();
    }

    public static void updateShuffledArenas() {
        shuffledArenas = Lists.newArrayList((Iterable)TTTCore.mg.getArenas());
        Collections.shuffle(shuffledArenas);
    }

    public static void applyNextArena() {
        TTTCore.setDedicatedArena(getNextArena());
        TTTCore.getDedicatedArena().getMetadata().set("startTime", System.currentTimeMillis());
        TTTCore.getDedicatedArena().getMetadata().set("roundTally", 1);
    }

    private static Arena getNextArena() {
        int randIndex;
        assert TTTCore.mg.getArenas().size() > 0;
        incrementIndex();
        switch (TTTCore.config.get(ConfigKey.MAP_CYCLE_MODE)) {
            case SEQUENTIAL:
                return TTTCore.mg.getArenas().get(arenaIndex);
            case SHUFFLE:
                return shuffledArenas.get(arenaIndex);
            case RANDOM:
                randIndex = (int)Math.floor(Math.random() * TTTCore.mg.getArenas().size());
                return TTTCore.mg.getArenas().get(randIndex);
        }
        throw new AssertionError();
    }

    private static void incrementIndex() {
        arenaIndex++;
        if (arenaIndex >= TTTCore.mg.getArenas().size())
            arenaIndex = 0;
    }

    public static boolean shouldArenaCycle(Arena arena) {
        int timeLimit = TTTCore.config.get(ConfigKey.MAP_CYCLE_TIME_LIMIT);
        int roundLimit = TTTCore.config.get(ConfigKey.MAP_CYCLE_ROUND_LIMIT);
        if (!arena.getMetadata().containsKey("startTime"))
            arena.getMetadata().set("startTime", System.currentTimeMillis());
        if (!arena.getMetadata().containsKey("roundTally"))
            arena.getMetadata().set("roundTally", 1);
        if (timeLimit >= 0 && TTTCore.config.get(ConfigKey.MAP_CYCLE_TIME_LIMIT) >= 0 &&
                System.currentTimeMillis() - (Long) arena.getMetadata().get("startTime").get() >= (timeLimit * 60 * 1000))
            return true;
        return roundLimit >= 0 && (Integer) arena.getMetadata().get("roundTally").get() - 1 >= roundLimit;
    }
}
