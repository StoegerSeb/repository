package com.sap.sepp.ttt.listener.minigame;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ChallengerListener: 14.04.2020 23:36 by sebip
*/

import com.google.common.eventbus.Subscribe;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.event.lobby.PlayerClickLobbySignEvent;
import com.sap.sepp.api.event.round.challenger.ChallengerJoinRoundEvent;
import com.sap.sepp.api.event.round.challenger.ChallengerLeaveRoundEvent;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.use.JoinCommand;
import com.sap.sepp.ttt.scoreboard.ScoreboardManager;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.CommandRegex;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.helper.gamemode.KarmaHelper;
import com.sap.sepp.ttt.utils.helper.gamemode.RoundHelper;
import com.sap.sepp.ttt.utils.helper.platform.BungeeHelper;
import com.sap.sepp.ttt.utils.helper.platform.LocationHelper;
import com.sap.sepp.ttt.utils.helper.platform.PlayerHelper;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ChallengerListener {
    @Subscribe
    public void onChallengerJoinRound(ChallengerJoinRoundEvent event) {
        if (event.getRound().getLifecycleStage() == Stage.PLAYING || event
                .getRound().getLifecycleStage() == Stage.ROUND_OVER) {
            event.getChallenger().setSpectating(true);
            PlayerHelper.watchPlayerGameMode(event.getChallenger());
            event.getChallenger().getMetadata().set("pureSpectator", Boolean.TRUE);
        }
        Player pl = Bukkit.getPlayer(event.getChallenger().getUniqueId());
        pl.setHealth(pl.getMaxHealth());
        pl.setCompassTarget(Bukkit.getWorlds().get(1).getSpawnLocation());
        if (!event.getChallenger().getMetadata().containsKey("pureSpectator")) {
            pl.setGameMode(GameMode.SURVIVAL);
            KarmaHelper.applyKarma(event.getChallenger());
            if (!event.getRound().getMetadata().containsKey("restarting")) {
                RoundHelper.broadcast(event.getRound(), TTTCore.locale
                        .getLocalizable("info.global.arena.event.join").withPrefix(Color.INFO)
                        .withReplacements(event.getChallenger().getName() + TTTCore.clh
                                .getContributorString(pl)));
                if (TTTCore.config.get(ConfigKey.OPERATING_MODE) != OperatingMode.DEDICATED ||
                        BungeeHelper.hasSupport())
                    TTTCore.locale.getLocalizable("info.personal.arena.join.leave-tip").withPrefix(Color.INFO)
                            .withReplacements(Color.EM + "/ttt leave" + Color.INFO).sendTo(pl);
            }
            if (event.getRound().getLifecycleStage() == Stage.WAITING && event
                    .getRound().getChallengers().size() >= TTTCore.config.get(ConfigKey.MINIMUM_PLAYERS))
                event.getRound().nextLifecycleStage();
        }
        if (!event.getRound().getMetadata().containsKey("scoreboardManager"))
            event.getRound().getMetadata()
                    .set("scoreboardManager", new ScoreboardManager(event.getRound()));
        ScoreboardManager sm = (ScoreboardManager)event.getRound().getMetadata().get("scoreboardManager").get();
        sm.applyScoreboard(event.getChallenger());
        sm.updateEntry(event.getChallenger());
        runCommands(TTTCore.config.get(ConfigKey.JOIN_CMDS), event.getChallenger());
    }

    @Subscribe
    public void onChallengerLeaveRound(ChallengerLeaveRoundEvent event) {
        try {
            Player pl = Bukkit.getPlayer(event.getChallenger().getUniqueId());
            pl.setScoreboard(TTTCore.getPlugin().getServer().getScoreboardManager().getNewScoreboard());
            if (event.getChallenger().getMetadata().containsKey("searchingBody"))
                pl.closeInventory();
            pl.setDisplayName(event.getChallenger().getName());
            pl.setCompassTarget(LocationHelper.convert(event.getReturnLocation()).getWorld().getSpawnLocation());
            pl.setHealth(pl.getMaxHealth());
        } catch (IllegalStateException illegalStateException) {}
        if (!event.getRound().isEnding() &&
                !event.getChallenger().getMetadata().containsKey("pureSpectator")) {
            KarmaHelper.saveKarma(event.getChallenger());
            RoundHelper.broadcast(event.getRound(), TTTCore.locale.getLocalizable("info.global.arena.event.leave")
                    .withPrefix(Color.INFO).withReplacements(event.getChallenger().getName(), Color.EM + event
                            .getChallenger().getRound().getArena().getDisplayName() + Color.INFO));
            if (event.getRound().getLifecycleStage() == Stage.PREPARING && event
                    .getRound().getChallengers().size() <= 1) {
                event.getRound().setLifecycleStage(Stage.WAITING, true);
                RoundHelper.broadcast(event.getRound(), TTTCore.locale
                        .getLocalizable("info.global.round.status.starting.stopped")
                        .withPrefix(Color.ALERT));
            }
        }
        ((ScoreboardManager)event.getRound().getMetadata().get("scoreboardManager").get())
                .remove(event.getChallenger());
        if (event.getRound().getChallengers().isEmpty())
            event.getRound().end();
        runCommands(TTTCore.config.get(ConfigKey.LEAVE_CMDS), event.getChallenger());
    }

    @Subscribe
    public void onPlayerClickLobbySign(PlayerClickLobbySignEvent event) {
        Player player = Bukkit.getPlayer(event.getPlayer());
        if (player.hasPermission("ttt.lobby.use")) {
            (new JoinCommand(player, new String[] { "join", event.getLobbySign().getArena().getId() })).handle();
        } else {
            TTTCore.locale.getLocalizable("error.perms.generic").withPrefix(Color.ALERT).sendTo(player);
        }
    }

    private void runCommands(List<String> commands, Challenger challenger) {
        for (String cmd : commands) {
            cmd = CommandRegex.PLAYER_WILDCARD.matcher(cmd).replaceAll(challenger.getName());
            cmd = CommandRegex.ARENA_WILDCARD.matcher(cmd).replaceAll(challenger.getRound().getArena().getId());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
        }
    }
}