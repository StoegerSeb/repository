package com.sap.sepp.ttt.listener.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerConnectionListener: 14.04.2020 23:37 by sebip
*/

public class PlayerConnectionListener implements Listener {
    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        if (TTTCore.config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED)
            Bukkit.getScheduler().runTask((Plugin)TTTCore.getPlugin(), new Runnable() {
                public void run() {
                    if (!BungeeHelper.wasInitializationCalled())
                        BungeeHelper.initialize();
                    TTTCore.getDedicatedArena().getOrCreateRound().addChallenger(event.getPlayer().getUniqueId());
                }
            });
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (!((Boolean)TTTCore.config.get(ConfigKey.KARMA_PERSIST)).booleanValue())
            KarmaHelper.resetKarma(event.getPlayer().getUniqueId());
    }
}
