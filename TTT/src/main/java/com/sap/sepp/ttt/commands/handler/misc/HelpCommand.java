package com.sap.sepp.ttt.commands.handler.misc;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class HelpCommand: 14.04.2020 23:30 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.CommandManager;
import com.sap.sepp.ttt.commands.CommandRef;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.data.DataVerificationHelper;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class HelpCommand extends CommandHandler {
    private static final int COMMANDS_PER_PAGE = 4;

    private static final String DIVIDER = "------------";

    public HelpCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        boolean isInt = false;
        if (this.args.length == 1 || (isInt = DataVerificationHelper.isInt(this.args[1]))) {
            int pageIndex = isInt ? Integer.parseInt(this.args[1]) : 1;
            List<CommandRef> availableCommands = getAvailableCommands();
            int pageCount = (int)Math.ceil((availableCommands.size() / 4.0F));
            if (pageIndex > pageCount || pageIndex <= 0) {
                TTTCore.locale.getLocalizable("error.command.help.bad-page").withPrefix(Color.ALERT).sendTo(this.sender);
                return;
            }
            printHeader(pageIndex, pageCount);
            for (int i = 0; i < 4; i++) {
                int index = 4 * (pageIndex - 1) + i;
                if (index >= availableCommands.size())
                    break;
                printDescription(availableCommands.get(index));
            }
            printFooter(pageIndex, pageCount);
        } else {
            if (!CommandManager.commands.containsKey(this.args[1]) || CommandManager.commands.get(this.args[1]).isHidden()) {
                printInvalidArgsError();
                return;
            }
            CommandRef cmdRef = CommandManager.commands.get(this.args[1]);
            if (cmdRef.getPermission() == null || this.sender.hasPermission(cmdRef.getPermission())) {
                printDescription(cmdRef);
            } else {
                TTTCore.locale.getLocalizable("error.perms.generic").withPrefix(Color.ALERT).sendTo(this.sender);
            }
        }
    }

    private void printDescription(CommandRef cmdRef) {
        assert cmdRef != null;
        cmdRef.getDescription().withPrefix(Color.INFO + "/ttt " + cmdRef.getLabel() + " " + ChatColor.WHITE)
                .sendTo(this.sender);
        TTTCore.locale.getLocalizable("fragment.usage")
                .withPrefix("    " + Color.SECONDARY + ChatColor.ITALIC)
                .withReplacements("" + ChatColor.WHITE + ChatColor.ITALIC + cmdRef.getUsage()).sendTo(this.sender);
        if ((cmdRef.getAliases()).length > 0) {
            StringBuilder aliasStr = new StringBuilder();
            for (String alias : cmdRef.getAliases())
                aliasStr.append(alias).append(", ");
            aliasStr.delete(aliasStr.length() - 2, aliasStr.length());
            TTTCore.locale.getLocalizable("fragment.aliases").withPrefix("    " + Color.SECONDARY + ChatColor.ITALIC)
                    .withReplacements("" + ChatColor.WHITE + ChatColor.ITALIC + aliasStr.toString()).sendTo(this.sender);
        }
    }

    private List<CommandRef> getAvailableCommands() {
        Set<CommandRef> cmds = new LinkedHashSet<>();
        for (UnmodifiableIterator<CommandRef> unmodifiableIterator = CommandManager.commands.values().iterator(); unmodifiableIterator.hasNext(); ) {
            CommandRef ref = unmodifiableIterator.next();
            if (ref.isHidden())
                continue;
            if (ref.getPermission() == null || this.sender.hasPermission(ref.getPermission()))
                cmds.add(ref);
        }
        return new ArrayList<>(cmds);
    }

    private void printHeader(int pageIndex, int pageCount) {
        this.sender.sendMessage("");
        TTTCore.locale.getLocalizable("info.help.available-cmds").withPrefix(Color.INFO).sendTo(this.sender);
        TTTCore.locale.getLocalizable("info.help.page").withPrefix("------------ " + Color.EM)
                .withSuffix(ChatColor.WHITE + " " + "------------").withReplacements(pageIndex + " / " + pageCount).sendTo(this.sender);
    }

    private void printFooter(int pageIndex, int pageCount) {
        if (pageIndex < pageCount)
            TTTCore.locale.getLocalizable("info.help.next-page").withPrefix(Color.INFO)
                    .withReplacements(Color.EM + "/ttt help " + (pageIndex + 1) + Color.INFO).sendTo(this.sender);
    }
}