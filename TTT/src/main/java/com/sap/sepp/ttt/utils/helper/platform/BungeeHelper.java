package com.sap.sepp.ttt.utils.helper.platform;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class BungeeHelper: 14.04.2020 23:54 by sebip
*/

import org.bukkit.entity.Player;

public class BungeeHelper implements PluginMessageListener {
    private static final BungeeHelper INSTANCE;

    private static boolean startedInitializing = false;

    private static boolean support = false;

    static {
        INSTANCE = new BungeeHelper();
    }

    public static void initialize() {
        Preconditions.checkState(!startedInitializing, "BungeeHelper initialization cannot be called more than once");
        startedInitializing = true;
        if (((String)TTTCore.config.get(ConfigKey.RETURN_SERVER)).isEmpty())
            return;
        registerBungeeChannel();
        sendPluginMessage("GetServers", null, (Player)Iterables.getFirst(Bukkit.getOnlinePlayers(), null));
    }

    public static boolean wasInitializationCalled() {
        return startedInitializing;
    }

    public static boolean hasSupport() {
        return support;
    }

    public static void sendPlayerToReturnServer(Player player) {
        sendPluginMessage("GetServers", null, player);
    }

    private static void sendPluginMessage(String subchannel, String content, Player player) {
        assert player != null;
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(subchannel);
        if (content != null)
            out.writeUTF(content);
        player.sendPluginMessage((Plugin)TTTCore.getPlugin(), "BungeeCord", out.toByteArray());
    }

    private static void registerBungeeChannel() {
        TTTCore.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel((Plugin)TTTCore.getPlugin(), "BungeeCord");
        TTTCore.getPlugin().getServer().getMessenger().registerIncomingPluginChannel((Plugin)TTTCore.getPlugin(), "BungeeCord", INSTANCE);
    }

    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord"))
            return;
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();
        if (subchannel.equals("GetServers")) {
            List<String> servers = Arrays.asList(in.readUTF().split(","));
            if (!support) {
                if (!servers.contains(TTTCore.config.get(ConfigKey.RETURN_SERVER)))
                    TTTCore.log.warning(TTTCore.locale.getLocalizable("error.bungee.configuration").localize());
                support = true;
            } else if (servers.contains(TTTCore.config.get(ConfigKey.RETURN_SERVER))) {
                sendPluginMessage("Connect", (String)TTTCore.config.get(ConfigKey.RETURN_SERVER), player);
            } else {
                TTTCore.locale.getLocalizable("error.bungee.configuration").withPrefix(Color.ALERT)
                        .sendTo((CommandSender)player);
                TTTCore.locale.getLocalizable("error.report").withPrefix(Color.ALERT).sendTo((CommandSender)player);
            }
        }
    }
}
