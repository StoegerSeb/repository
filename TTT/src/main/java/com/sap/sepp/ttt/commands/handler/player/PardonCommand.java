package com.sap.sepp.ttt.commands.handler.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PardonCommand: 14.04.2020 23:31 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.gamemode.BanHelper;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;

public class PardonCommand extends CommandHandler {
    public PardonCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        String name = this.args[1];
        OfflinePlayer pl = Bukkit.getOfflinePlayer(name);
        if (pl == null) {
            TTTCore.locale.getLocalizable("error.admin.ban.invalid-player").withPrefix(Color.ALERT)
                    .withReplacements(name).sendTo(this.sender);
            return;
        }
        try {
            if (BanHelper.pardon(pl.getUniqueId())) {
                if (pl.isOnline())
                    TTTCore.locale.getLocalizable("info.personal.pardon").withPrefix(Color.INFO).sendTo(pl.getPlayer());
                TTTCore.locale.getLocalizable("info.personal.pardon.other").withPrefix(Color.INFO)
                        .withReplacements(name).sendTo(this.sender);
            } else {
                TTTCore.locale.getLocalizable("error.plugin.pardon.absent").withPrefix(Color.ALERT)
                        .withReplacements(name).sendTo(this.sender);
            }
        } catch (InvalidConfigurationException |java.io.IOException ex) {
            ex.printStackTrace();
            TTTCore.locale.getLocalizable("error.plugin.pardon").withPrefix(Color.ALERT).sendTo(this.sender);
        }
    }
}
