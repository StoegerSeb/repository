package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ContributorListHelper: 14.04.2020 23:51 by sebip
*/

import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.constant.Color;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public final class ContributorListHelper {
    private Map<String, Set<UUID>> contributors = new HashMap<>();

    private InputStream stream;

    public ContributorListHelper(InputStream in) {
        this.stream = in;
    }

    private String readIn() {
        char[] buffer = new char[1024];
        StringBuilder builder = new StringBuilder();
        Reader in = new InputStreamReader(this.stream, StandardCharsets.UTF_8);
        try {
            int piece;
            while ((piece = in.read(buffer, 0, buffer.length)) >= 0)
                builder.append(buffer, 0, piece);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return builder.toString();
    }

    private void read() {
        String[] lines = readIn().split("\n");
        for (String line : lines) {
            int commentIndex = line.indexOf('#');
            if (commentIndex != -1)
                line = line.substring(0, commentIndex);
            String[] entry = line.split(" ");
            if (entry.length == 2) {
                String key = entry[0];
                try {
                    UUID value = UUID.fromString(entry[1]);
                    if (this.contributors.containsKey(key)) {
                        this.contributors.get(key).add(value);
                    } else {
                        Set<UUID> set = new HashSet<>();
                        set.add(value);
                        this.contributors.put(key, set);
                    }
                } catch (IllegalArgumentException illegalArgumentException) {}
            }
        }
    }

    private boolean hasRole(UUID uuid, String role) {
        return (this.contributors.containsKey(role) && this.contributors.get(role).contains(uuid));
    }

    public String getContributorString(Player player) {
        UUID uuid = player.getUniqueId();
        String str = "";
        if (hasRole(uuid, "dev")) {
            str = str + ", " + TTTCore.locale.getLocalizable("fragment.special.dev").withPrefix(Color.TRAITOR).localizeFor(player) + "," + Color.INFO;
        } else if (hasRole(uuid, "alpha")) {
            str = str + ", " + TTTCore.locale.getLocalizable("fragment.special.tester.alpha").withPrefix(Color.TRAITOR).localizeFor(player) + "," + Color.INFO;
        }
        return str;
    }
}
