package com.sap.sepp.ttt.utils.helper.gamemode;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RoleHelper: 14.04.2020 23:52 by sebip
*/

import com.google.common.collect.Lists;
import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.helper.data.DataVerificationHelper;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Collections;
import java.util.List;

public final class RoleHelper {
    public static void assignRoles(Round round) {
        int players = round.getChallengers().size();
        Team iTeam = round.getOrCreateTeam("innocent");
        Team tTeam = round.getOrCreateTeam("traitor");
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = round.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger ch = unmodifiableIterator.next();
            ch.setTeam(iTeam);
        }
        int tLimit = DataVerificationHelper.clamp((int)(players * TTTCore.config.get(ConfigKey.TRAITOR_PCT)), 1, players - 1);
        tLimit = DataVerificationHelper.clamp(tLimit, 1, players - 1);
        List<Challenger> tList = Lists.newArrayList((Iterable)round.getChallengers());
        Collections.shuffle(tList);
        for (int i = 0; i < tLimit; i++)
            tList.get(i).setTeam(tTeam);
        int dLimit = (int)(players * TTTCore.config.get(ConfigKey.DETECTIVE_PCT));
        dLimit = DataVerificationHelper.clamp(dLimit, 0, iTeam.getChallengers().size());
        if (players >= TTTCore.config.get(ConfigKey.DETECTIVE_MIN_PLAYERS) && dLimit == 0)
            dLimit = 1;
        List<Challenger> dList = Lists.newArrayList((Iterable)iTeam.getChallengers());
        Collections.shuffle(dList);
        for (int j = 0; j < dLimit; j++)
            dList.get(j).getMetadata().set("detective", Boolean.TRUE);
    }

    public static String genRoleMessage(CommandSender sender, Challenger ch) {
        String color, roleFrag;
        if (!ch.getTeam().isPresent()) {
            color = ChatColor.GRAY.toString();
            roleFrag = "unassigned";
        } else if (ch.getTeam().get().getId().equals("traitor")) {
            color = Color.TRAITOR;
            roleFrag = "traitor";
        } else if (ch.getMetadata().containsKey("detective")) {
            color = Color.DETECTIVE;
            roleFrag = "detective";
        } else {
            color = Color.INNOCENT;
            roleFrag = "innocent";
        }
        String roleMsg = TTTCore.locale.getLocalizable("fragment." + roleFrag).withPrefix(color).localizeFor(sender).toUpperCase();
        if (ch.isSpectating() && !ch.getMetadata().containsKey("pureSpectator"))
            roleMsg = roleMsg + TTTCore.locale.getLocalizable("fragment.deceased").withPrefix(" " + ChatColor.GRAY + "(").localizeFor(sender) + ")";
        return roleMsg;
    }

    public static boolean isTraitor(Challenger player) {
        return (player.getTeam().isPresent() && player.getTeam().get().getId().equals("traitor"));
    }
}
