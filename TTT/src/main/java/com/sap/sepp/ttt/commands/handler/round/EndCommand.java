package com.sap.sepp.ttt.commands.handler.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class EndCommand: 14.04.2020 23:32 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import org.bukkit.command.CommandSender;

public class EndCommand extends CommandHandler {
    public EndCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        handle(false);
    }

    protected void handle(boolean force) {
        String arenaName = this.args[1];
        Optional<Arena> arena = TTTCore.mg.getArena(arenaName);
        if (!arena.isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT)
                    .withReplacements(Color.EM + arenaName + Color.ALERT).sendTo(this.sender);
            return;
        }
        if (!arena.get().getRound().isPresent() || arena
                .get().getRound().get().getLifecycleStage() == Stage.WAITING) {
            TTTCore.locale.getLocalizable("error.arena.no-round").withPrefix(Color.ALERT)
                    .withReplacements(Color.EM + arenaName + Color.ALERT).sendTo(this.sender);
            return;
        }
        if (this.args.length > 2)
            if (this.args[2].equalsIgnoreCase("t")) {
                arena.get().getRound().get().getMetadata().set("t-victory", Boolean.TRUE);
            } else if (!this.args[2].equalsIgnoreCase("i")) {
                printInvalidArgsError();
                return;
            }
        arena.get().getRound().get().setLifecycleStage(Stage.ROUND_OVER, true);
        if (force)
            arena.get().getRound().get().setTime((Stage.ROUND_OVER.getDuration() + 1));
    }
}
