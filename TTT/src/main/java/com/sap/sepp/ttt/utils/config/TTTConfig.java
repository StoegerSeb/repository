package com.sap.sepp.ttt.utils.config;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TTTConfig: 14.04.2020 23:43 by sebip
*/

import com.google.common.collect.ImmutableMap;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;

public final class TTTConfig {
    private final Map<ConfigKey<?>, Object> map = new HashMap<>();

    private final FileConfiguration config;

    private static final ImmutableMap<Object, Object> LEGACY_NODES = ImmutableMap.builder()
            .put("setup-time", "preptime-seconds")
            .put("time-limit", "roundtime-seconds")

            .put("traitor-ratio", "traitor-pct")
            .put("detective-ratio", "detective-pct")
            .put("minimum-players-for-detective", "detective-min-players")

            .put("default-karma", "karma-starting")
            .put("max-karma", "karma-max")
            .put("damage-penalty", "karma-ratio")
            .put("kill-penalty", "karma-kill-penalty")
            .put("karma-heal", "karma-round-increment")
            .put("t-damage-reward", "karma-traitordmg-ratio")
            .put("tbonus", "karma-traitorkill-bonus")
            .put("karma-kick", "karma-low-autokick")
            .put("karma-ban", "karma-low-ban")
            .put("karma-ban-time", "karma-low-ban-minutes")
            .put("karma-persistance", "karma-persist")
            .put("damage-reduction", "karma-damage-reduction").build();

    public TTTConfig(FileConfiguration config) {
        this.config = config;
        for (ConfigKey<?> key : ConfigKey.getAllKeys())
            set(key);
    }

    public <T> T get(ConfigKey<T> key) {
        return (T)this.map.get(key);
    }

    private void set(ConfigKey<?> key) {
        if (key.getType().getRawType() == Integer.class) {
            this.map.put(key, Integer.valueOf(getInt(key.getConfigKey())));
        } else if (key.getType().getRawType() == Double.class) {
            this.map.put(key, Double.valueOf(getDouble(key.getConfigKey())));
        } else if (key.getType().getRawType() == Boolean.class) {
            this.map.put(key, Boolean.valueOf(getBoolean(key.getConfigKey())));
        } else if (key.getType().getRawType() == String.class) {
            this.map.put(key, getString(key.getConfigKey()));
        } else if (key.getType().getRawType() == List.class) {
            this.map.put(key, getList(key.getConfigKey()));
        } else if (key.getType().getRawType() == Material.class) {
            this.map.put(key, getMaterial(key.getConfigKey(), (Material)key.getDefault()));
        } else if (key.getType().getRawType() == OperatingMode.class) {
            this.map.put(key, getOperatingMode(key.getConfigKey(), (OperatingMode)key.getDefault()));
        } else if (key.getType().getRawType() == CycleMode.class) {
            this.map.put(key, getCycleMode(key.getConfigKey(), (CycleMode)key.getDefault()));
        }
    }

    private String getString(String key) {
        String value = this.config.getString(key);
        if (value != null) {
            if (value.contains("))
                    value = value.replace(", ");
            return value;
        }
        return "";
    }

    private boolean getBoolean(String key) {
        return this.config.getBoolean(key);
    }

    private int getInt(String key) {
        return this.config.getInt(key);
    }

    private double getDouble(String key) {
        return this.config.getDouble(key);
    }

    private List<?> getList(String key) {
        return this.config.getList(key);
    }

    private Material getMaterial(String key, Material fallback) {
        Material m = Material.getMaterial(this.config.getString(key));
        return (m != null) ? m : fallback;
    }

    private OperatingMode getOperatingMode(String key, OperatingMode fallback) {
        OperatingMode mode;
        try {
            mode = OperatingMode.valueOf(getString(key).toUpperCase());
            if (mode == OperatingMode.DEDICATED && TTTCore.mg.getArenas().size() == 0) {
                mode = OperatingMode.STANDARD;
                TTTCore.log.warning(TTTCore.locale.getLocalizable("error.plugin.dedicated-no-arenas").localize());
                TTTCore.log.warning(TTTCore.locale.getLocalizable("error.plugin.dedicated-fallback").localize());
            }
        } catch (IllegalArgumentException ex) {
            TTTCore.getPlugin().getLogger().warning(TTTCore.locale.getLocalizable("error.plugin.config.fallback")
                    .withReplacements(new String[] { key, fallback.toString() }).localize());
            mode = OperatingMode.STANDARD;
        }
        return mode;
    }

    private CycleMode getCycleMode(String key, CycleMode fallback) {
        CycleMode mode;
        try {
            mode = CycleMode.valueOf(getString(key).toUpperCase());
        } catch (IllegalArgumentException ex) {
            TTTCore.getPlugin().getLogger().warning(TTTCore.locale.getLocalizable("error.plugin.config.fallback")
                    .withReplacements(new String[] { key, fallback.toString() }).localize());
            mode = CycleMode.SHUFFLE;
        }
        return mode;
    }

    private Set<String> getLegacyKeys(final String modernKey) {
        return new HashSet<>(Collections2.filter((Collection)LEGACY_NODES.keySet(), FunctionalHelper.createPredicate(new Function<String, Boolean>() {
            public Boolean apply(String key) {
                return Boolean.valueOf(((String)TTTConfig.LEGACY_NODES.get(key)).equals(modernKey));
            }
        })));
    }

    public void addMissingKeys() throws InvalidConfigurationException, IOException {
        BufferedReader stockConfig = new BufferedReader(new InputStreamReader(TTTBootstrap.class.getResourceAsStream("/config.yml")));
        File userConfigFile = new File(TTTBootstrap.INSTANCE.getDataFolder(), "config.yml");
        YamlConfiguration userConfig = new YamlConfiguration();
        userConfig.load(userConfigFile);
        StringBuilder sb = new StringBuilder();
        char newlineChar = '\n';
        String currentList = null;
        StringBuilder buffer = null;
        String line;
        while ((line = stockConfig.readLine()) != null) {
            line = line.trim();
            if (!line.startsWith("#") && !line.isEmpty()) {
                if (currentList != null && !line.startsWith("-")) {
                    if (buffer != null) {
                        for (String item : userConfig.getStringList(currentList))
                            sb.append("- ").append(item).append('\n');
                        sb.append(buffer);
                    }
                    currentList = null;
                    buffer = null;
                }
                if (buffer != null)
                    continue;
                if (currentList == null && line.contains(":")) {
                    boolean equal;
                    String key = line.split(":")[0];
                    if (TTTCore.getPlugin().getConfig().isList(key)) {
                        currentList = key;
                        if (userConfig.isList(key))
                            buffer = new StringBuilder();
                        if (TTTCore.getPlugin().getConfig().getList(key).isEmpty()) {
                            sb.append(line).append('\n');
                            continue;
                        }
                        sb.append(key).append(":\n");
                        continue;
                    }
                    String internalValue = line.substring(key.length() + 1, line.length()).trim();
                    String userValue = null;
                    if (userConfig.contains(key.trim())) {
                        userValue = userConfig.getString(key.trim());
                    } else {
                        Set<String> legacyKeys = getLegacyKeys(key.trim());
                        for (String lgcy : legacyKeys) {
                            if (userConfig.contains(lgcy)) {
                                userValue = userConfig.getString(lgcy);
                                break;
                            }
                        }
                        if (userValue == null)
                            userValue = internalValue;
                    }
                    if (key.equals("gun-item") && userValue.equals("IRON_HORSE_ARMOR"))
                        userValue = "IRON_BARDING";
                    try {
                        equal = NumberFormat.getInstance().parse(internalValue).equals(NumberFormat.getInstance().parse(userValue));
                    } catch (ParseException ex) {
                        equal = internalValue.equals(userValue);
                    }
                    if (!equal) {
                        if (DataVerificationHelper.isDouble(userValue))
                            userValue = BigDecimal.valueOf(Double.parseDouble(userValue)).stripTrailingZeros().toPlainString();
                        sb.append(key).append(": ").append(userValue).append('\n');
                        continue;
                    }
                }
            }
            ((buffer != null) ? buffer : sb).append(line).append('\n');
        }
        FileHelper.copyFile(userConfigFile, new File(userConfigFile.getParentFile(), "config.yml.old"));
        FileWriter w = new FileWriter(userConfigFile);
        w.append(sb.toString());
        w.flush();
    }
}