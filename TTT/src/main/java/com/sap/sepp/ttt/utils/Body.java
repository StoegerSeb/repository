package com.sap.sepp.ttt.utils;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Body: 07.04.2020 15:39 by sebip
*/

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Location3D;

import java.util.UUID;

public class Body {
    private final Round round;

    private final Location3D location;

    private final UUID player;

    private final String name;

    private final UUID killer;

    private final String role;

    private final long deathTime;

    private final long expiry;

    private boolean found;

    public Body(Round round, Location3D location, UUID player, String name, UUID killer, String role, long deathTime, long expireTime) {
        this.round = round;
        this.location = location;
        this.player = player;
        this.name = name;
        this.killer = killer;
        this.role = (role != null) ? role : "innocent";
        this.deathTime = deathTime;
        this.expiry = expireTime;
    }

    public Round getRound() {
        return this.round;
    }

    public Location3D getLocation() {
        return this.location;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public String getName() {
        return this.name;
    }

    public Optional<UUID> getKiller() {
        return Optional.fromNullable(this.killer);
    }

    public String getRole() {
        return this.role;
    }

    public long getDeathTime() {
        return this.deathTime;
    }

    public long getExpiry() {
        return this.expiry;
    }

    public boolean isFound() {
        return this.found;
    }

    public void setFound() {
        this.found = true;
    }

    public int hashCode() {
        return Objects.hashCode(this.round, this.location, this.player, this.name, this.killer, this.role, this.expiry);
    }
}
