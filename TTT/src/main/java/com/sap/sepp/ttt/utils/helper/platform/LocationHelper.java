package com.sap.sepp.ttt.utils.helper.platform;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LocationHelper: 14.04.2020 23:54 by sebip
*/

import com.sap.sepp.api.utils.physical.Location3D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;

public final class LocationHelper {
    public static Location3D convert(Location location) {
        return new Location3D(location.getWorld().getName(), location.getX(), location.getY(), location.getZ());
    }

    public static Location convert(Location3D location) {
        return new Location(location.getWorld().isPresent() ?
                Bukkit.createWorld(new WorldCreator(location.getWorld().get())) : null, location

                .getX(), location.getY(), location.getZ());
    }
}
