package com.sap.sepp.ttt.utils.compatibility;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LegacyConfigFolderRenamer: 14.04.2020 23:41 by sebip
*/

public final class LegacyConfigFolderRenamer {
    public static void renameLegacyFolder() {
        File old = new File(Bukkit.getWorldContainer() + File.separator + "plugins", "Trouble In Terrorist Town");
        if (old.exists() && !TTTCore.getPlugin().getDataFolder().exists()) {
            TTTCore.getInstance().logWarning("info.plugin.compatibility.rename", new String[0]);
            try {
                old.renameTo(TTTCore.getPlugin().getDataFolder());
            } catch (Exception ex) {
                TTTCore.getInstance().logWarning("error.plugin.folder-rename", new String[0]);
                ex.printStackTrace();
            }
        }
    }
}
