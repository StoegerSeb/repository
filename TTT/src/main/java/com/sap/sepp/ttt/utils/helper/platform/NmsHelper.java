package com.sap.sepp.ttt.utils.helper.platform;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class NmsHelper: 14.04.2020 23:54 by sebip
*/

public final class NmsHelper {
    private static final boolean NMS_SUPPORT;

    public static final String VERSION_STRING;

    public static final Method CRAFT_PLAYER_GET_HANDLE;

    public static final Field ENTITY_PLAYER_PLAYER_CONNECTION;

    public static final Method PLAYER_CONNECTION_A_PACKET_PLAY_IN_CLIENT_COMMAND;

    public static final Object CLIENT_COMMAND_PACKET_INSTANCE;

    static {
        boolean nmsException = false;
        String[] array = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
        VERSION_STRING = (array.length == 4) ? (array[3] + ".") : "";
        Method craftPlayer_getHandle = null;
        Field entityPlayer_playerConection = null;
        Method playerConnection_a_packetPlayInClientCommand = null;
        Object clientCommandPacketInstance = null;
        try {
            craftPlayer_getHandle = getCraftClass("entity.CraftPlayer").getMethod("getHandle", new Class[0]);
            entityPlayer_playerConection = getNmsClass("EntityPlayer").getDeclaredField("playerConnection");
            playerConnection_a_packetPlayInClientCommand = getNmsClass("PlayerConnection").getMethod("a", new Class[] { getNmsClass("PacketPlayInClientCommand") });
            try {
                try {
                    Class<? extends Enum> enumClass;
                    try {
                        String className = "PacketPlayInClientCommand$EnumClientCommand";
                        Class<? extends Enum> temp = (Class)getNmsClass(className);
                        enumClass = temp;
                    } catch (ClassNotFoundException ex) {
                        Class<? extends Enum> temp = (Class)getNmsClass("EnumClientCommand");
                        enumClass = temp;
                    }
                    Object performRespawn = Enum.valueOf(enumClass, "PERFORM_RESPAWN");
                    clientCommandPacketInstance = getNmsClass("PacketPlayInClientCommand").getConstructor(new Class[] { performRespawn.getClass() }).newInstance(new Object[] { performRespawn });
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                    clientCommandPacketInstance = getNmsClass("Packet205ClientCommand").getConstructor(new Class[0]).newInstance(new Object[0]);
                    clientCommandPacketInstance.getClass().getDeclaredField("a").set(clientCommandPacketInstance, Integer.valueOf(1));
                }
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
                TTTCore.getInstance()
                        .logSevere(TTTCore.locale.getLocalizable("plugin.alert.nms.client-command").localize(), new String[0]);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            TTTCore.getInstance().logSevere(TTTCore.locale.getLocalizable("plugin.alert.nms.fail").localize(), new String[0]);
            nmsException = true;
        }
        CRAFT_PLAYER_GET_HANDLE = craftPlayer_getHandle;
        ENTITY_PLAYER_PLAYER_CONNECTION = entityPlayer_playerConection;
        PLAYER_CONNECTION_A_PACKET_PLAY_IN_CLIENT_COMMAND = playerConnection_a_packetPlayInClientCommand;
        CLIENT_COMMAND_PACKET_INSTANCE = clientCommandPacketInstance;
        NMS_SUPPORT = !nmsException;
    }

    private static Class<?> getNmsClass(String name) throws ClassNotFoundException {
        String className = "net.minecraft.server." + VERSION_STRING + name;
        return Class.forName(className);
    }

    private static Class<?> getCraftClass(String name) throws ClassNotFoundException {
        String className = "org.bukkit.craftbukkit." + VERSION_STRING + name;
        return Class.forName(className);
    }

    public static void sendRespawnPacket(Player player) {
        if (NMS_SUPPORT)
            try {
                Object nmsPlayer = CRAFT_PLAYER_GET_HANDLE.invoke(player, new Object[0]);
                Object conn = ENTITY_PLAYER_PLAYER_CONNECTION.get(nmsPlayer);
                PLAYER_CONNECTION_A_PACKET_PLAY_IN_CLIENT_COMMAND.invoke(conn, new Object[] { CLIENT_COMMAND_PACKET_INSTANCE });
            } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException ex) {
                TTTCore.log.severe("Failed to force-respawn player " + player.getName());
                ex.printStackTrace();
            }
    }
}