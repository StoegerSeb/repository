package com.sap.sepp.ttt.commands.handler.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ArenaInfoCommand: 14.04.2020 23:29 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.ttt.TTTCore;
import com.sap.sepp.ttt.commands.handler.CommandHandler;
import com.sap.sepp.ttt.lib.Localizable;
import com.sap.sepp.ttt.utils.config.ConfigKey;
import com.sap.sepp.ttt.utils.config.OperatingMode;
import com.sap.sepp.ttt.utils.constant.Color;
import com.sap.sepp.ttt.utils.constant.Stage;
import com.sap.sepp.ttt.utils.constant.Text;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ArenaInfoCommand extends CommandHandler {
    public ArenaInfoCommand(CommandSender sender, String[] args) {
        super(sender, args);
    }

    public void handle() {
        if (this.args.length == 1 && TTTCore.config.get(ConfigKey.OPERATING_MODE) != OperatingMode.DEDICATED) {
            TTTCore.locale.getLocalizable("error.command.too-few-args").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        String arenaName = (this.args.length > 1) ? this.args[1] : TTTCore.getDedicatedArena().getDisplayName();
        Optional<Arena> arenaOpt = TTTCore.mg.getArena(arenaName);
        if (!arenaOpt.isPresent()) {
            TTTCore.locale.getLocalizable("error.arena.dne").withPrefix(Color.ALERT).sendTo(this.sender);
            return;
        }
        Arena arena = arenaOpt.get();
        this.sender.sendMessage(Text.DIVIDER);
        TTTCore.locale.getLocalizable("info.personal.arena-info.header").withPrefix(Color.INFO)
                .withReplacements(Color.EM + arena.getDisplayName()).sendTo(this.sender);
        TTTCore.locale.getLocalizable("info.personal.arena-info.has-round").withPrefix(Color.INFO)
                .withReplacements(TTTCore.locale.getLocalizable(arena.getRound().isPresent() ? "fragment.yes" : "fragment.no")
                        .withPrefix(Color.EM)).sendTo(this.sender);
        if (arena.getRound().isPresent()) {
            Round round = arena.getRound().get();
            TTTCore.locale.getLocalizable("info.personal.arena-info.player-count").withPrefix(Color.INFO)
                    .withReplacements(Color.EM + round.getChallengers().size()).sendTo(this.sender);
            TTTCore.locale.getLocalizable("info.personal.arena-info.stage").withPrefix(Color.INFO)
                    .withReplacements(TTTCore.locale.getLocalizable("fragment.stage." + round.getLifecycleStage().getId())
                            .withPrefix(Color.EM).localizeFor(this.sender).toUpperCase()).sendTo(this.sender);
            if (round.getLifecycleStage() != Stage.WAITING)
                TTTCore.locale.getLocalizable("info.personal.arena-info.time").withPrefix(Color.INFO)
                        .withReplacements(TTTCore.locale.getLocalizable("fragment.seconds" + (
                                (round.getRemainingTime() == 1L) ? ".singular" : ""))
                                .withPrefix(Color.EM).withReplacements(round.getRemainingTime() + "")).sendTo(this.sender);
        }
        if (TTTCore.config.get(ConfigKey.OPERATING_MODE) == OperatingMode.DEDICATED) {
            long elapsed = System.currentTimeMillis() - (Long) arena.getMetadata().get("startTime").get();
            long remainingTime = Math.max(0L, TTTCore.config.get(ConfigKey.MAP_CYCLE_TIME_LIMIT) - elapsed / 1000L / 60L);
            TTTCore.locale.getLocalizable("info.personal.arena-info.map-change-time").withPrefix(Color.INFO)
                    .withReplacements(TTTCore.locale.getLocalizable("fragment.minutes" + ((remainingTime == 1L) ? ".singular" : ""))
                            .withPrefix(Color.EM).withReplacements(remainingTime + "")).sendTo(this.sender);
            int remainingRounds = TTTCore.config.get(ConfigKey.MAP_CYCLE_ROUND_LIMIT) - (Integer) arena.getMetadata().get("roundTally").get();
            TTTCore.locale.getLocalizable("info.personal.arena-info.map-change-rounds").withPrefix(Color.INFO)
                    .withReplacements(Color.EM + remainingRounds).sendTo(this.sender);
            if (remainingTime == 0L || remainingRounds == 0) {
                TTTCore.locale.getLocalizable("info.personal.arena-info.map-change-after-current")
                        .withPrefix(Color.INFO + ChatColor.ITALIC).sendTo(this.sender);
            } else if (remainingRounds == 1) {
                TTTCore.locale.getLocalizable("info.personal.arena-info.map-change-after-next")
                        .withPrefix(Color.INFO + ChatColor.ITALIC).sendTo(this.sender);
            }
        }
        this.sender.sendMessage(Text.DIVIDER);
    }
}
