package com.sap.sepp.api.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Arena: 07.04.2020 17:05 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.sap.sepp.api.component.Component;
import com.sap.sepp.api.component.ComponentOwner;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.exceptions.rollback.RollbackException;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.type.ChallengerListingLobbySign;
import com.sap.sepp.api.lobby.type.StatusLobbySign;
import com.sap.sepp.api.metadata.persist.PersistentMetadataHolder;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.round.LifecycleStage;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.builder.Buildable;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;

public interface Arena extends PersistentMetadataHolder, ComponentOwner, Component<Minigame>, Buildable<Arena.Builder> {
    Minigame getMinigame() throws OrphanedComponentException;

    String getId() throws OrphanedComponentException;

    @Deprecated
    String getName() throws OrphanedComponentException;

    String getDisplayName() throws OrphanedComponentException;

    void setDisplayName(String paramString);

    String getWorld() throws OrphanedComponentException;

    Boundary getBoundary() throws OrphanedComponentException;

    void setBoundary(Boundary paramBoundary) throws OrphanedComponentException;

    ImmutableMap<Integer, Location3D> getSpawnPoints() throws OrphanedComponentException;

    int addSpawnPoint(Location3D paramLocation3D) throws IllegalArgumentException, OrphanedComponentException;

    void removeSpawnPoint(int paramInt) throws OrphanedComponentException;

    void removeSpawnPoint(Location3D paramLocation3D) throws OrphanedComponentException;

    Optional<Round> getRound() throws OrphanedComponentException;

    Round createRound(ImmutableSet<LifecycleStage> paramImmutableSet) throws IllegalArgumentException, IllegalStateException, OrphanedComponentException;

    Round createRound() throws IllegalStateException, OrphanedComponentException;

    Round getOrCreateRound(ImmutableSet<LifecycleStage> paramImmutableSet) throws IllegalArgumentException, OrphanedComponentException;

    Round getOrCreateRound() throws OrphanedComponentException;

    ImmutableList<LobbySign> getLobbySigns() throws OrphanedComponentException;

    Optional<LobbySign> getLobbySignAt(Location3D paramLocation3D) throws IllegalArgumentException, OrphanedComponentException;

    Optional<StatusLobbySign> createStatusLobbySign(Location3D paramLocation3D) throws IllegalArgumentException, OrphanedComponentException;

    Optional<ChallengerListingLobbySign> createChallengerListingLobbySign(Location3D paramLocation3D, int paramInt) throws IllegalArgumentException, OrphanedComponentException;

    void markForRollback(Location3D paramLocation3D) throws IllegalArgumentException, RollbackException, OrphanedComponentException;

    void rollback() throws IllegalStateException, OrphanedComponentException;

    public static interface Builder extends com.sap.sepp.api.utils.builder.Builder<Arena> {
        Builder id(String param1String);

        Builder displayName(String param1String);

        Builder spawnPoints(Location3D... param1VarArgs);

        Builder boundary(Boundary param1Boundary);

        Arena build() throws IllegalStateException;
    }
}