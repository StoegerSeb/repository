package com.sap.sepp.api.lobby.popular;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LobbySignPopular: 07.04.2020 16:03 by sebip
*/

import com.google.common.base.Function;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.utils.builder.Buildable;

public interface LobbySignPopulator extends Buildable<LobbySignPopulator.Builder> {
    String first(LobbySign paramLobbySign);

    String second(LobbySign paramLobbySign);

    String third(LobbySign paramLobbySign);

    String fourth(LobbySign paramLobbySign);

    public static interface Builder extends com.sap.sepp.api.utils.builder.Builder<LobbySignPopulator> {
        Builder first(Function<LobbySign, String> param1Function);

        Builder second(Function<LobbySign, String> param1Function);

        Builder third(Function<LobbySign, String> param1Function);

        Builder fourth(Function<LobbySign, String> param1Function);

        LobbySignPopulator build();
    }
}
