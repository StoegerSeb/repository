package com.sap.sepp.api.challenger;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Team: 07.04.2020 16:00 by sebip
*/

import com.google.common.collect.ImmutableList;
import com.sap.sepp.api.component.Component;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.metadata.MetadataHolder;
import com.sap.sepp.api.round.Round;

public interface Team extends MetadataHolder, Component<Round> {
    Round getRound() throws OrphanedComponentException;

    String getId() throws OrphanedComponentException;

    String getName() throws OrphanedComponentException;

    void setName(String paramString) throws OrphanedComponentException;

    ImmutableList<Challenger> getChallengers() throws OrphanedComponentException;

    void addChallenger(Challenger paramChallenger) throws IllegalArgumentException, OrphanedComponentException;

    void removeChallenger(Challenger paramChallenger) throws IllegalArgumentException, OrphanedComponentException;
}
