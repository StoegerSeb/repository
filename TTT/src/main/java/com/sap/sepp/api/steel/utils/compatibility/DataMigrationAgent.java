package com.sap.sepp.api.steel.utils.compatibility;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class DataMigrationAgent: 07.04.2020 16:17 by sebip
*/

import com.sap.sepp.api.steel.SteelCore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;

abstract class DataMigrationAgent {
    public abstract void migrateData();

    protected abstract File getOldDir();

    protected void relocateOldFile(Path oldFile) {
        try {
            Path oldDir = getOldDir().toPath();
            if (!Files.exists(oldDir, new java.nio.file.LinkOption[0]))
                Files.createDirectory(oldDir, (FileAttribute<?>[])new FileAttribute[0]);
            Path copyPath = oldDir.resolve(oldFile.getFileName());
            Files.deleteIfExists(copyPath);
            Files.move(oldFile, copyPath, new java.nio.file.CopyOption[0]);
            SteelCore.logInfo("Old file has been relocated to " + copyPath.toString() + ".");
        } catch (IOException ex) {
            SteelCore.logSevere("Failed to relocate " + oldFile.getFileName().toString() + "! This is very bad.");
            throw new RuntimeException(ex);
        }
    }
}
