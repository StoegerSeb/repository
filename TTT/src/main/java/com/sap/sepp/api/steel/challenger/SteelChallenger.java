package com.sap.sepp.api.steel.challenger;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelChallenger: 07.04.2020 16:29 by sebip
*/

import org.bukkit.GameMode;

public class SteelChallenger extends CommonChallenger {
    private GameMode prevGameMode;

    private boolean hadFlight;

    private List<UUID> alreadyInvisibleTo = new ArrayList<>();

    public SteelChallenger(UUID uuid, SteelRound round) {
        super(uuid, Bukkit.getPlayer(uuid).getName(), (CommonRound)round);
    }

    public void setSpectating(boolean spectating) {
        super.setSpectating(spectating);
        Player pl = Bukkit.getPlayer(getUniqueId());
        assert pl != null;
        if (spectating) {
            this.prevGameMode = pl.getGameMode();
            if (SteelCore.SPECTATOR_SUPPORT) {
                pl.setGameMode(GameMode.SPECTATOR);
            } else {
                pl.setGameMode(GameMode.ADVENTURE);
                for (Player p : PlayerHelper.getOnlinePlayers())
                    tryHide(pl, p);
                this.hadFlight = pl.getAllowFlight();
                pl.setAllowFlight(true);
            }
        } else {
            if (this.prevGameMode != null) {
                pl.setGameMode(this.prevGameMode);
                this.prevGameMode = null;
            }
            if (!SteelCore.SPECTATOR_SUPPORT) {
                for (Player p : PlayerHelper.getOnlinePlayers()) {
                    if (!this.alreadyInvisibleTo.contains(p.getUniqueId()))
                        p.showPlayer(pl);
                    this.alreadyInvisibleTo.clear();
                }
                pl.setAllowFlight(this.hadFlight);
                this.hadFlight = false;
            }
        }
    }

    public void tryHide(Player hidden, Player viewer) {
        if (viewer.canSee(hidden)) {
            viewer.hidePlayer(hidden);
        } else {
            this.alreadyInvisibleTo.add(viewer.getUniqueId());
        }
    }
}