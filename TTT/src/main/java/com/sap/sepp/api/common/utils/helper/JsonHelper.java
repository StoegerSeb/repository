package com.sap.sepp.api.common.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class JsonHelper: 07.04.2020 16:45 by sebip
*/

import com.google.common.base.Optional;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;

public class JsonHelper {
    public static JsonObject readOrCreateJson(File file) throws IOException {
        if (file.exists())
            try (FileReader reader = new FileReader(file)) {
                JsonElement el = (new JsonParser()).parse(reader);
                return el.isJsonObject() ? el.getAsJsonObject() : new JsonObject();
            }
        Files.createFile(file.toPath(), new FileAttribute[0]);
        return new JsonObject();
    }

    public static Optional<JsonObject> readJson(File file) throws IllegalArgumentException, IOException {
        if (file.exists())
            try (FileReader reader = new FileReader(file)) {
                JsonElement el = (new JsonParser()).parse(reader);
                if (el.isJsonObject())
                    return Optional.of(el.getAsJsonObject());
            }
        return Optional.absent();
    }
}
