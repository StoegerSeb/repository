package com.sap.sepp.api.common;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonCore: 07.04.2020 16:38 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.FlintCore;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.lobby.populator.FunctionalLobbySignPopulator;
import com.sap.sepp.api.common.utils.PlatformUtils;
import com.sap.sepp.api.common.utils.agent.chat.IChatAgent;
import com.sap.sepp.api.common.utils.builder.BuilderRegistry;
import com.sap.sepp.api.common.utils.factory.FactoryRegistry;
import com.sap.sepp.api.common.utils.factory.IMinigameFactory;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.minigame.Minigame;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public abstract class CommonCore extends FlintCore {
    private static Map<String, Minigame> minigames = new HashMap<>();

    public static PlatformUtils PLATFORM_UTILS;

    protected static void initializeCommon() {
        registerBuilders();
    }

    private static void registerBuilders() {
        BuilderRegistry.instance().registerBuilder(Arena.class, CommonArena.Builder.class);
        BuilderRegistry.instance().registerBuilder(LobbySignPopulator.class, FunctionalLobbySignPopulator.Builder.class);
    }

    protected Minigame registerPlugin0(String pluginId) throws IllegalStateException {
        if (getMinigames().containsKey(pluginId))
            throw new IllegalStateException(pluginId + " attempted to register itself more than once");
        Minigame minigame = ((IMinigameFactory) FactoryRegistry.getFactory(Minigame.class)).createMinigame(pluginId);
        getMinigames().put(pluginId, minigame);
        return minigame;
    }

    public static Map<String, Minigame> getMinigames() {
        return minigames;
    }

    public static Optional<Challenger> getChallenger(UUID uuid) {
        for (Minigame mg : getMinigames().values()) {
            if (mg.getChallenger(uuid).isPresent())
                return mg.getChallenger(uuid);
        }
        return Optional.absent();
    }

    public static void logInfo(String message) {
        ((CommonCore)INSTANCE).logInfo0(message);
    }

    protected abstract void logInfo0(String paramString);

    public static void logWarning(String message) {
        ((CommonCore)INSTANCE).logWarning0(message);
    }

    protected abstract void logWarning0(String paramString);

    public static void logSevere(String message) {
        ((CommonCore)INSTANCE).logSevere0(message);
    }

    protected abstract void logSevere0(String paramString);

    public static void logVerbose(String message) {
        ((CommonCore)INSTANCE).logVerbose0(message);
    }

    protected abstract void logVerbose0(String paramString);

    public static void orphan(CommonComponent<?> component) {
        ((CommonCore)INSTANCE).orphan0(component);
    }

    protected abstract void orphan0(CommonComponent<?> paramCommonComponent);

    public static IChatAgent getChatAgent() {
        return ((CommonCore)INSTANCE).getChatAgent0();
    }

    protected abstract IChatAgent getChatAgent0();
}
