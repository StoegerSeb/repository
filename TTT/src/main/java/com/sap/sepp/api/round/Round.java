package com.sap.sepp.api.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Round: 07.04.2020 16:06 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.component.Component;
import com.sap.sepp.api.component.ComponentOwner;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.config.RoundConfigNode;
import com.sap.sepp.api.metadata.MetadataHolder;
import com.sap.sepp.api.utils.physical.Location3D;

import java.util.UUID;

public interface Round extends MetadataHolder, ComponentOwner, Component<Arena> {
    Arena getArena() throws OrphanedComponentException;

    ImmutableList<Challenger> getChallengers() throws OrphanedComponentException;

    Optional<Challenger> getChallenger(UUID paramUUID) throws OrphanedComponentException;

    JoinResult addChallenger(UUID paramUUID) throws OrphanedComponentException;

    void removeChallenger(UUID paramUUID) throws IllegalArgumentException, OrphanedComponentException;

    void removeChallenger(Challenger paramChallenger) throws IllegalArgumentException, OrphanedComponentException;

    Location3D nextSpawnPoint();

    ImmutableList<Team> getTeams() throws OrphanedComponentException;

    Optional<Team> getTeam(String paramString) throws OrphanedComponentException;

    Team createTeam(String paramString) throws IllegalArgumentException, OrphanedComponentException;

    Team getOrCreateTeam(String paramString) throws OrphanedComponentException;

    void removeTeam(String paramString) throws IllegalArgumentException, OrphanedComponentException;

    void removeTeam(Team paramTeam) throws IllegalArgumentException, OrphanedComponentException;

    ImmutableList<Challenger> getSpectators() throws OrphanedComponentException;

    void broadcast(String paramString) throws OrphanedComponentException;

    ImmutableSet<LifecycleStage> getLifecycleStages() throws OrphanedComponentException;

    LifecycleStage getLifecycleStage() throws OrphanedComponentException;

    void setLifecycleStage(LifecycleStage paramLifecycleStage, boolean paramBoolean) throws IllegalArgumentException, OrphanedComponentException;

    void setLifecycleStage(LifecycleStage paramLifecycleStage) throws IllegalArgumentException, OrphanedComponentException;

    Optional<LifecycleStage> getLifecycleStage(String paramString) throws OrphanedComponentException;

    LifecycleStage getLifecycleStage(int paramInt) throws IndexOutOfBoundsException, OrphanedComponentException;

    Optional<LifecycleStage> getNextLifecycleStage() throws OrphanedComponentException;

    void nextLifecycleStage() throws IllegalStateException, OrphanedComponentException;

    long getTime() throws OrphanedComponentException;

    void setTime(long paramLong) throws OrphanedComponentException;

    long getRemainingTime() throws OrphanedComponentException;

    boolean isTimerTicking() throws OrphanedComponentException;

    void setTimerTicking(boolean paramBoolean) throws OrphanedComponentException;

    void resetTimer() throws OrphanedComponentException;

    void end() throws IllegalStateException, OrphanedComponentException;

    @Deprecated
    void end(EndParameter... paramVarArgs) throws IllegalStateException, OrphanedComponentException;

    @Deprecated
    void end(boolean paramBoolean) throws IllegalStateException, OrphanedComponentException;

    boolean isEnding() throws OrphanedComponentException;

    <T> T getConfigValue(RoundConfigNode<T> paramRoundConfigNode) throws OrphanedComponentException;

    <T> void setConfigValue(RoundConfigNode<T> paramRoundConfigNode, T paramT) throws OrphanedComponentException;

    public static interface EndParameter {
        public enum RollbackBehavior implements EndParameter {
            DO_ROLLBACK, SKIP_ROLLBACK;
        }
    }
}