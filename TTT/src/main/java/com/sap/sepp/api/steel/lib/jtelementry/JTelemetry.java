package com.sap.sepp.api.steel.lib.jtelementry;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class JTelementry: 07.04.2020 16:23 by sebip
*/

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class JTelemetry {
    private static final int PROTOCOL_REVISION = 1;

    private final String recipient;

    public JTelemetry(String recipient) throws IllegalArgumentException {
        this.recipient = recipient;
    }

    public String getRecipient() {
        return this.recipient;
    }

    public Payload createPayload() {
        return new Payload(this);
    }

    public class Payload {
        private JTelemetry parent;

        private Map<String, DataEntry> dataMap = new HashMap<>();

        private Payload(JTelemetry parent) {
            this.parent = parent;
        }

        public JTelemetry getParent() {
            return this.parent;
        }

        public JTelemetry.HttpResponse submit() throws IOException {
            URL url = new URL(getParent().getRecipient());
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/octet-stream");
            byte[] serial = serializeData();
            conn.setRequestProperty("Content-Length", Integer.toString(serial.length));
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(serial);
            }
            return new JTelemetry.HttpResponse(conn.getResponseCode(), conn.getResponseMessage());
        }

        private byte[] serializeData() {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] magic = { -80, 0, -79, -27 };
                out.write(magic);
                out.write(JTelemetry.ByteUtils.toBytes(1));
                for (DataEntry entry : this.dataMap.values()) {
                    byte[] serial = entry.serialize();
                    out.write(serial);
                }
                return out.toByteArray();
            } catch (IOException ex) {
                throw new AssertionError(ex);
            }
        }

        public void addData(String key, boolean data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.BOOLEAN, key, Boolean.valueOf(data));
        }

        public void addData(String key, byte data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.BYTE, key, Byte.valueOf(data));
        }

        public void addData(String key, short data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.SHORT, key, Short.valueOf(data));
        }

        public void addData(String key, int data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.INT, key, Integer.valueOf(data));
        }

        public void addData(String key, long data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.LONG, key, Long.valueOf(data));
        }

        public void addData(String key, float data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.FLOAT, key, Float.valueOf(data));
        }

        public void addData(String key, double data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.DOUBLE, key, Double.valueOf(data));
        }

        public void addData(String key, String data) throws IllegalArgumentException {
            addData(JTelemetry.DataType.STRING, key, data);
        }

        public void addData(String key, byte... data) throws IllegalArgumentException {
            DataEntry[] array = new DataEntry[data.length];
            for (int i = 0; i < data.length; i++)
                array[i] = new DataEntry(JTelemetry.DataType.BYTE, null, Byte.valueOf(data[i]));
            addData(JTelemetry.DataType.ARRAY, key, array);
        }

        public void addData(String key, int[] data) throws IllegalArgumentException {
            DataEntry[] array = new DataEntry[data.length];
            for (int i = 0; i < data.length; i++)
                array[i] = new DataEntry(JTelemetry.DataType.INT, null, Integer.valueOf(data[i]));
            addData(JTelemetry.DataType.ARRAY, key, array);
        }

        public void addData(String key, String[] data) throws IllegalArgumentException {
            DataEntry[] array = new DataEntry[data.length];
            for (int i = 0; i < data.length; i++)
                array[i] = new DataEntry(JTelemetry.DataType.STRING, null, data[i]);
            addData(JTelemetry.DataType.ARRAY, key, array);
        }

        private void addData(JTelemetry.DataType type, String key, Object data) throws IllegalArgumentException {
            assert type != null;
            if (key == null)
                throw new IllegalArgumentException("Key must not be null");
            if (data == null)
                throw new IllegalArgumentException("Content must not be null");
            if (this.dataMap.containsKey(key))
                throw new IllegalArgumentException("Cannot redefine key \"" + key + "\" within the payload");
            this.dataMap.put(key, new DataEntry(type, key, data));
        }

        private class DataEntry {
            private JTelemetry.DataType type;

            private String key;

            private Object content;

            private DataEntry(JTelemetry.DataType type, String key, Object content) throws IllegalArgumentException {
                this.type = type;
                this.key = key;
                this.content = content;
            }

            private byte[] serialize() throws IOException {
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                out.write((byte)this.type.ordinal());
                if (this.key != null) {
                    byte[] keyBytes = this.key.getBytes(StandardCharsets.UTF_8);
                    out.write(JTelemetry.ByteUtils.toBytes(keyBytes.length));
                    out.write(keyBytes);
                }
                if (this.type == JTelemetry.DataType.ARRAY) {
                    DataEntry[] entries = (DataEntry[])this.content;
                    ByteArrayOutputStream temp = new ByteArrayOutputStream();
                    for (DataEntry entry : entries) {
                        byte[] arrayOfByte = entry.serialize();
                        temp.write(arrayOfByte);
                    }
                    byte[] bytes = temp.toByteArray();
                    out.write(JTelemetry.ByteUtils.toBytes(bytes.length));
                    out.write(JTelemetry.ByteUtils.toBytes(entries.length));
                    out.write(bytes);
                } else if (this.type == JTelemetry.DataType.STRING) {
                    byte[] strBytes = ((String)this.content).getBytes(StandardCharsets.UTF_8);
                    out.write(JTelemetry.ByteUtils.toBytes(strBytes.length));
                    out.write(strBytes);
                } else {
                    assert this.type.getLength() != -1;
                    switch (this.type) {
                        case BOOLEAN:
                            out.write(((Boolean)this.content).booleanValue() ? 1 : 0);
                            return out.toByteArray();
                        case BYTE:
                            out.write(((Byte)this.content).byteValue());
                            return out.toByteArray();
                        case SHORT:
                            out.write(JTelemetry.ByteUtils.toBytes(((Short)this.content).shortValue()));
                            return out.toByteArray();
                        case INT:
                            out.write(JTelemetry.ByteUtils.toBytes(((Integer)this.content).intValue()));
                            return out.toByteArray();
                        case LONG:
                            out.write(JTelemetry.ByteUtils.toBytes(((Long)this.content).longValue()));
                            return out.toByteArray();
                        case FLOAT:
                            out.write(JTelemetry.ByteUtils.toBytes(((Float)this.content).floatValue()));
                            return out.toByteArray();
                        case DOUBLE:
                            out.write(JTelemetry.ByteUtils.toBytes(((Double)this.content).doubleValue()));
                            return out.toByteArray();
                    }
                    throw new AssertionError();
                }
                return out.toByteArray();
            }
        }
    }

    private enum DataType {
        BOOLEAN(1),
        BYTE(1),
        SHORT(2),
        INT(4),
        LONG(8),
        FLOAT(4),
        DOUBLE(8),
        STRING(-1),
        ARRAY(-1);

        private final int length;

        DataType(int length) {
            this.length = length;
        }

        private int getLength() {
            return this.length;
        }
    }

    private static class ByteUtils {
        private static final ByteBuffer SHORT_BUFFER = ByteBuffer.allocate(2);

        private static final ByteBuffer MID_BUFFER = ByteBuffer.allocate(4);

        private static final ByteBuffer LONG_BUFFER = ByteBuffer.allocate(8);

        public static byte[] toBytes(short x) {
            SHORT_BUFFER.putShort(0, x);
            return SHORT_BUFFER.array();
        }

        public static byte[] toBytes(int x) {
            MID_BUFFER.putInt(0, x);
            return MID_BUFFER.array();
        }

        public static byte[] toBytes(long x) {
            LONG_BUFFER.putLong(0, x);
            return LONG_BUFFER.array();
        }

        public static byte[] toBytes(float x) {
            MID_BUFFER.putFloat(0, x);
            return MID_BUFFER.array();
        }

        public static byte[] toBytes(double x) {
            LONG_BUFFER.putDouble(0, x);
            return LONG_BUFFER.array();
        }
    }

    public class HttpResponse {
        private final int code;

        private final String message;

        private HttpResponse(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getStatusCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }
}
