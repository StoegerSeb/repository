package com.sap.sepp.api.utils.physical;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Location3D: 07.04.2020 15:47 by sebip
*/

import com.google.common.base.Objects;
import com.google.common.base.Optional;

public class Location3D {
    private static final char SEPARATOR = ';';

    private final String world;

    private final double x;
    private final double y;
    private final double z;

    public Location3D(final String world, final double x, final double y, final double z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Location3D(final double x, final double y, final double z) {
        this.world = null;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Optional<String> getWorld() {
        return Optional.fromNullable(this.world);
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public String serialize() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        if (getWorld().isPresent())
            sb.append("\"").append(getWorld().get()).append("\"").append(';');
        sb.append(toCommaDecimal(getX())).append(';');
        sb.append(toCommaDecimal(getY())).append(';');
        sb.append(toCommaDecimal(getZ()));
        sb.append(")");
        return sb.toString();
    }

    public static Location3D deserialize(String serial) throws IllegalArgumentException {
        if (serial.startsWith("(") && serial.endsWith(")")) {
            serial = serial.substring(1, serial.length() - 1);
            String[] parts = serial.split(Character.toString(';'));
            try {
                switch (parts.length) {
                    case 3:
                        return new Location3D(
                                fromCommaDecimal(parts[0]),
                                fromCommaDecimal(parts[1]),
                                fromCommaDecimal(parts[2]));
                    case 4:
                        if (parts[0].startsWith("\"") && parts[0].endsWith("\""))
                            return new Location3D(parts[0]
                                    .substring(1, parts[0].length() - 1),
                                    fromCommaDecimal(parts[1]),
                                    fromCommaDecimal(parts[2]),
                                    fromCommaDecimal(parts[3]));
                        break;
                }
            } catch (NullPointerException | NumberFormatException ex) {
                throw new IllegalArgumentException("Invalid serial", ex);
            }
        }
        throw new IllegalArgumentException("Invalid serial");
    }

    private static String toCommaDecimal(final double d) {
        return Double.toString(d).replace(".", ",");
    }

    private static double fromCommaDecimal(final String d) throws NumberFormatException {
        return Double.parseDouble(d.replace(",", "."));
    }

    public boolean equals(Object obj) {
        if (obj instanceof Location3D) {
            final Location3D l = (Location3D) obj;
            return ((l.getWorld().isPresent() ? l.getWorld().get().equals(this.world) : (this.world == null)) && l
                    .getX() == getX() && l.getY() == getY() && l.getZ() == getZ());
        }
        return false;
    }

    public int hashCode() {
        return Objects.hashCode(this.world, this.x, this.y, this.z);
    }
}
