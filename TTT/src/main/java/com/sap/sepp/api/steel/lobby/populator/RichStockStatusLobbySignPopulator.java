package com.sap.sepp.api.steel.lobby.populator;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RichStockStatusLobbySignPopulator: 07.04.2020 16:34 by sebip
*/

public class RichStockStatusLobbySignPopulator extends StockStatusLobbySignPopulator {
    private static final ChatColor[] STATUS_COLORS = new ChatColor[] { ChatColor.DARK_AQUA, ChatColor.DARK_PURPLE, ChatColor.DARK_PURPLE, ChatColor.DARK_BLUE };

    public String first(LobbySign sign) {
        return STATUS_COLORS[0] + super.first(sign);
    }

    public String second(LobbySign sign) {
        return STATUS_COLORS[1] + super.second(sign);
    }

    public String third(LobbySign sign) {
        return STATUS_COLORS[2] + super.third(sign);
    }

    public String fourth(LobbySign sign) {
        return STATUS_COLORS[3] + super.fourth(sign);
    }
}
