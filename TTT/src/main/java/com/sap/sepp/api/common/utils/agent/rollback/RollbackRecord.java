package com.sap.sepp.api.common.utils.agent.rollback;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RollbackRecord: 07.04.2020 16:41 by sebip
*/

import com.sap.sepp.api.utils.physical.Location3D;

import java.util.UUID;

public class RollbackRecord {
    private final int id;

    private final UUID uuid;

    private final Location3D location;

    private final String type;

    private final int data;

    private final String stateSerial;

    private final Type recordType;

    protected RollbackRecord(int id, UUID uuid, Location3D location, String type, int data, String stateSerial, Type recordType) {
        this.id = id;
        this.uuid = uuid;
        this.location = location;
        this.type = type;
        this.data = data;
        this.stateSerial = stateSerial;
        this.recordType = recordType;
    }

    public static RollbackRecord createBlockRecord(int id, Location3D loc, String type, int data, String stateSerial) {
        return new RollbackRecord(id, null, loc, type, data, stateSerial, Type.BLOCK_CHANGE);
    }

    public static RollbackRecord createEntityCreationRecord(int id, UUID uuid, String world) {
        return new RollbackRecord(id, uuid, new Location3D(world, 0.0D, 0.0D, 0.0D), null, 0, null, Type.ENTITY_CREATION);
    }

    public static RollbackRecord createEntityChangeRecord(int id, UUID uuid, Location3D loc, String type, String stateSerial) {
        return new RollbackRecord(id, uuid, loc, type, 0, stateSerial, Type.BLOCK_CHANGE);
    }

    public int getId() {
        return this.id;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public Location3D getLocation() {
        return this.location;
    }

    public String getTypeData() {
        return this.type;
    }

    public int getData() {
        return this.data;
    }

    public String getStateSerial() {
        return this.stateSerial;
    }

    public Type getType() {
        return this.recordType;
    }

    public enum Type {
        BLOCK_CHANGE, ENTITY_CREATION, ENTITY_CHANGE;
    }
}
