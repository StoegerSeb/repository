package com.sap.sepp.api.common.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class WizardCollectedData: 07.04.2020 16:54 by sebip
*/

public class WizardCollectedData implements IWizardCollectedData {
    private String arena;

    private LobbySign.Type type;

    private int index;

    public String getArena() {
        return this.arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }

    public LobbySign.Type getSignType() {
        return this.type;
    }

    public void setSignType(LobbySign.Type type) {
        this.type = type;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
