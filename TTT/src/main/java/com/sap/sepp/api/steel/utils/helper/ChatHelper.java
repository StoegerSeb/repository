package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ChatHelper: 07.04.2020 16:20 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.steel.SteelCore;
import org.bukkit.entity.Player;

public class ChatHelper {
    public static boolean isBarrierPresent(Player sender, Player recipient) {
        return (isRoundBarrierPresent(sender, recipient) ||
                isTeamBarrierPresent(sender, recipient) ||
                isSpectatorBarrierPresent(sender, recipient));
    }

    public static boolean isRoundBarrierPresent(Player sender, Player recipient) {
        Optional<Challenger> senderCh = SteelCore.getChallenger(sender.getUniqueId());
        Optional<Challenger> recipCh = SteelCore.getChallenger(recipient.getUniqueId());
        if ((checkRoundBarrier(senderCh) || checkRoundBarrier(recipCh)) && (
                senderCh.isPresent() != recipCh.isPresent() || ((Challenger)senderCh.get()).getRound() != ((Challenger)recipCh.get()).getRound()))
            return true;
        return false;
    }

    private static boolean checkRoundBarrier(Optional<Challenger> ch) {
        return (ch.isPresent() && ((Boolean)((Challenger)ch.get()).getRound().getConfigValue(ConfigNode.SEPARATE_ROUND_CHATS)).booleanValue());
    }

    public static boolean isTeamBarrierPresent(Player sender, Player recipient) {
        Optional<Challenger> senderCh = SteelCore.getChallenger(sender.getUniqueId());
        Optional<Challenger> recipCh = SteelCore.getChallenger(recipient.getUniqueId());
        return (senderCh.isPresent() && recipCh.isPresent() && ((Challenger)senderCh
                .get()).getRound() == ((Challenger)recipCh.get()).getRound() && ((Boolean)((Challenger)senderCh
                .get()).getRound().getConfigValue(ConfigNode.SEPARATE_TEAM_CHATS)).booleanValue() &&
                !((Challenger)senderCh.get()).getTeam().equals(((Challenger)recipCh.get()).getTeam()));
    }

    public static boolean isSpectatorBarrierPresent(Player sender, Player recipient) {
        Optional<Challenger> senderCh = SteelCore.getChallenger(sender.getUniqueId());
        Optional<Challenger> recipCh = SteelCore.getChallenger(recipient.getUniqueId());
        if (senderCh.isPresent() && (
                (Challenger)senderCh.get()).isSpectating() && ((Boolean)((Challenger)senderCh
                .get()).getRound().getConfigValue(ConfigNode.WITHHOLD_SPECTATOR_CHAT)).booleanValue())
            return (!recipCh.isPresent() || ((Challenger)recipCh.get()).getRound() != ((Challenger)senderCh.get()).getRound() ||
                    !((Challenger)recipCh.get()).isSpectating());
        return false;
    }
}