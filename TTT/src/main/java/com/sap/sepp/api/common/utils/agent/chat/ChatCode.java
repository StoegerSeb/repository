package com.sap.sepp.api.common.utils.agent.chat;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ChatCode: 07.04.2020 16:39 by sebip
*/

public class ChatCode {
    public static final String BLACK = "§0";

    public static final String BLUE = "§1";

    public static final String GREEN = "§2";

    public static final String TEAL = "§3";

    public static final String RED = "§4";

    public static final String PURPLE = "§5";

    public static final String GOLD = "§6";

    public static final String LIGHT_GRAY = "§7";

    public static final String DARK_GRAY = "§8";

    public static final String LIGHT_BLUE = "§9";

    public static final String LIGHT_GREEN = "§a";

    public static final String AQUA = "§b";

    public static final String LIGHT_RED = "§c";

    public static final String PINK = "§d";

    public static final String YELLOW = "§e";

    public static final String WHITE = "§f";

    public static final String OBFUSCATED = "§k";

    public static final String BOLD = "§l";

    public static final String STRIKETHROUGH = "§m";

    public static final String UNDERLINE = "§n";

    public static final String ITALIC = "§o";

    public static final String RESET = "§r";
}