package com.sap.sepp.api.common.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonWizardPlayer: 07.04.2020 16:53 by sebip
*/

public abstract class CommonWizardPlayer implements IWizardPlayer {
    protected final UUID uuid;

    protected final Location3D location;

    protected final IWizardManager manager;

    protected WizardStage stage;

    protected final IWizardCollectedData wizardData;

    protected final List<String[]> withheldMessages = (List)new ArrayList<>();

    protected CommonWizardPlayer(UUID uuid, Location3D location, IWizardManager manager) {
        this.uuid = uuid;
        this.location = location;
        this.manager = manager;
        this.stage = WizardStage.GET_ARENA;
        this.wizardData = new WizardCollectedData();
        recordTargetBlockState();
    }

    public UUID getUniqueId() {
        return this.uuid;
    }

    public Location3D getLocation() {
        return this.location;
    }

    public IWizardManager getParent() {
        return this.manager;
    }

    public String[] accept(String input) {
        Optional<Arena> arena;
        if (input.equalsIgnoreCase("cancel")) {
            getParent().removePlayer(getUniqueId());
            playbackWithheldMessages();
            return new String[] { "sign creation cancelled." };
        }
        switch (this.stage) {
            case GET_ARENA:
                arena = getParent().getOwner().getArena(input);
                if (arena.isPresent()) {
                    this.wizardData.setArena(input);
                    this.stage = WizardStage.GET_TYPE;
                    return new String[] { WizardMessages.DIVIDER, "please select the type of lobby sign you would like to create from the list below (type a number):", "- Status signs display basic information about the round contained by an arena such as timer info and player count.", "Listing - Player listing signs display a portion of the players in a round. If this is selected, you will be asked next to define which portion of players to display." };
                }
                return new String[] { "arena by that ID exists! Please enter another arena ID." };
            case GET_TYPE:
                try {
                    int i = Integer.parseInt(input);
                    switch (i) {
                        case 1:
                            this.wizardData.setSignType(LobbySign.Type.STATUS);
                            this.stage = WizardStage.CONFIRMATION;
                            return constructConfirmation();
                        case 2:
                            this.wizardData.setSignType(LobbySign.Type.CHALLENGER_LISTING);
                            this.stage = WizardStage.GET_INDEX;
                            return new String[] { WizardMessages.DIVIDER, "Next, please select which portion of players you would like the player listing sign to display. (will display players will display players and so on.)" };
                    }
                } catch (NumberFormatException numberFormatException) {}
                return new String[] { "sign type! Please select a valid option." };
            case GET_INDEX:
                try {
                    int i = Integer.parseInt(input);
                    if (i > 0) {
                        this.wizardData.setIndex(i - 1);
                        this.stage = WizardStage.CONFIRMATION;
                        return constructConfirmation();
                    }
                } catch (NumberFormatException numberFormatException) {}
                return new String[] { "sign index! Please enter a number greater than or equal to 1." };
            case CONFIRMATION:
                if (input.equalsIgnoreCase("yes")) {
                    arena = getParent().getOwner().getArena(this.wizardData.getArena());
                    if (arena.isPresent()) {
                        Optional<ChallengerListingLobbySign> sign;
                        restoreTargetBlock();
                        switch (this.wizardData.getSignType()) {
                            case GET_ARENA:
                                try {
                                    Optional<StatusLobbySign> optional = ((Arena)arena.get()).createStatusLobbySign(getLocation());
                                    if (optional.isPresent()) {
                                        getParent().removePlayer(getUniqueId());
                                        playbackWithheldMessages();
                                        return new String[] { WizardMessages.DIVIDER, "lobby sign was successfully created! The wizard will now exit." };
                                    }
                                    CommonCore.logSevere("Failed to register lobby sign via wizard");
                                    return new String[] { WizardMessages.DIVIDER, "internal exception occurred while creating the lobby sign. The wizard will now exit." };
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                    return new String[] { WizardMessages.DIVIDER, "internal exception occurred while creating the lobby sign. The wizard will now exit.", "+ ex
                                            .getMessage() };
                                }
                            case GET_TYPE:
                                sign = ((Arena)arena.get()).createChallengerListingLobbySign(getLocation(), this.wizardData.getIndex());
                                if (sign.isPresent()) {
                                    getParent().removePlayer(getUniqueId());
                                    playbackWithheldMessages();
                                    return new String[] { WizardMessages.DIVIDER, "lobby sign was successfully created! The wizard will now exit." };
                                }
                                return new String[] { WizardMessages.DIVIDER, "internal exception occurred while creating the lobby sign. The wizard will now exit." };
                        }
                        throw new AssertionError("Invalid sign type in wizard data. Report this immediately.");
                    }
                    getParent().removePlayer(getUniqueId());
                    playbackWithheldMessages();
                    return new String[] { WizardMessages.DIVIDER, "selected arena has been removed. The wizard will now exit." };
                }
                if (input.equalsIgnoreCase("no")) {
                    this.stage = WizardStage.GET_ARENA;
                    return new String[] { WizardMessages.DIVIDER, "wizard will now reset.", WizardMessages.DIVIDER, "start, please type the name of the arena you would like to create a lobby sign for. (You may type cancel at any time to exit the wizard." };
                }
                return new String[] { "type or };
                }
            throw new AssertionError("Cannot process input for wizard player. Report this immediately.");
        }

        public void withholdMessage(String sender, String message) {
            this.withheldMessages.add(new String[] { sender, message });
        }

        private String[] constructConfirmation() {
            ArrayList<String> msgs = new ArrayList<>();
            msgs.add(WizardMessages.DIVIDER);
            msgs.add("Your lobby sign will be created with the following info:");
            msgs.add("ID: + this.wizardData.getArena());
                    msgs.add("type: + this.wizardData.getSignType().toString());
            if (this.wizardData.getSignType() == LobbySign.Type.CHALLENGER_LISTING)
                msgs.add("index: + (this.wizardData.getIndex() + 1));
                        msgs.add("this okay? (Type or );
                                String[] arr = new String[msgs.size()];
            msgs.toArray(arr);
            return arr;
        }

        protected abstract void recordTargetBlockState();

        protected abstract void restoreTargetBlock();
    }
