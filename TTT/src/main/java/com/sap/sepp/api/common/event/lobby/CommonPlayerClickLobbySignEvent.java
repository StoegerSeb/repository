package com.sap.sepp.api.common.event.lobby;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonPlayerClickLobbySignEvent: 07.04.2020 16:48 by sebip
*/

import com.sap.sepp.api.event.lobby.PlayerClickLobbySignEvent;
import com.sap.sepp.api.lobby.LobbySign;

import java.util.UUID;

public class CommonPlayerClickLobbySignEvent implements PlayerClickLobbySignEvent {
    private final UUID player;

    private final LobbySign sign;

    private final PlayerClickLobbySignEvent.ClickType type;

    public CommonPlayerClickLobbySignEvent(UUID player, LobbySign sign, PlayerClickLobbySignEvent.ClickType type) {
        this.player = player;
        this.sign = sign;
        this.type = type;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public LobbySign getLobbySign() {
        return this.sign;
    }

    public PlayerClickLobbySignEvent.ClickType getClickType() {
        return this.type;
    }
}
