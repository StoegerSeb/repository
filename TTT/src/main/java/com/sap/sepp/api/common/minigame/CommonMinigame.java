package com.sap.sepp.api.common.minigame;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonMinigame: 07.04.2020 16:56 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.UnmodifiableIterator;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.event.FlintSubscriberExceptionHandler;
import com.sap.sepp.api.common.utils.builder.BuilderRegistry;
import com.sap.sepp.api.common.utils.factory.FactoryRegistry;
import com.sap.sepp.api.common.utils.factory.IArenaFactory;
import com.sap.sepp.api.common.utils.factory.ILobbySignFactory;
import com.sap.sepp.api.common.utils.file.CommonDataFiles;
import com.sap.sepp.api.common.utils.helper.JsonHelper;
import com.sap.sepp.api.common.utils.helper.JsonSerializer;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public abstract class CommonMinigame implements Minigame {
    private EventBus eventBus;

    private final Map<ConfigNode<?>, Object> config = new HashMap<>();

    private final BiMap<String, Arena> arenas = HashBiMap.create();

    private final BiMap<Arena, Round> rounds = HashBiMap.create();

    protected CommonMinigame() {
        boolean exceptionHandlerSupport = false;
        try {
            Class.forName("com.google.common.eventbus.SubscriberExceptionHandler");
            exceptionHandlerSupport = true;
        } catch (ClassNotFoundException ex) {
            CommonCore.logWarning("Guava version is < 16.0 - SubscriberExceptionHandler is not supported. Exceptions occurring in Flint event handlers may not be logged correctly.");
        }
        this.eventBus = exceptionHandlerSupport ? BreakingEventBusFactory.getBreakingEventBus() : new EventBus();
    }

    public <T> T getConfigValue(ConfigNode<T> node) {
        return this.config.containsKey(node) ? (T)this.config.get(node) : node.getDefaultValue();
    }

    public EventBus getEventBus() {
        return this.eventBus;
    }

    public <T> void setConfigValue(ConfigNode<T> node, T value) {
        Preconditions.checkNotNull(node, "node");
        Preconditions.checkNotNull(value, "value");
        this.config.put(node, value);
    }

    public ImmutableList<Arena> getArenas() {
        return ImmutableList.copyOf(this.arenas.values());
    }

    public Optional<Arena> getArena(String arenaName) {
        return Optional.fromNullable(this.arenas.get(arenaName.toLowerCase()));
    }

    public Arena createArena(String id, String name, Location3D spawnPoint, Boundary boundary) throws IllegalArgumentException {
        CommonArena arena = ((IArenaFactory) FactoryRegistry.getFactory(Arena.class)).createArena(this, id, name, new Location3D[] { spawnPoint }, boundary);
        try {
            arena.store();
        } catch (IOException ex) {
            throw new RuntimeException("Failed to store arena with ID " + id + ".", ex);
        }
        return arena;
    }

    public Arena createArena(String id, Location3D spawnPoint, Boundary boundary) throws IllegalArgumentException {
        return createArena(id, id, spawnPoint, boundary);
    }

    public void removeArena(String id) throws IllegalArgumentException {
        id = id.toLowerCase();
        Arena arena = getArenaMap().get(id);
        if (arena != null) {
            removeArena(arena);
        } else {
            throw new IllegalArgumentException("Cannot find arena with ID " + id + " in minigame " + getPlugin());
        }
    }

    public void removeArena(Arena arena) throws IllegalArgumentException {
        if (arena.getMinigame() != this)
            throw new IllegalArgumentException("Cannot remove arena with different parent minigame");
        if (arena.getRound().isPresent()) {
            arena.getRound().get().end();
            CommonCore.logVerbose("Minigame " + getPlugin() + " requested to remove arena " + arena.getId() + " while it still contained a round. The engine will end it automatically, but typically this behavior is not ideal and the round should be ended before the arena is requested for removal.");
        }
        getArenaMap().remove(arena.getId());
        try {
            ((CommonArena)arena).removeFromStore();
        } catch (IOException ex) {
            CommonCore.logSevere("Failed to remove arena with ID " + arena.getId() + " from persistent store");
            ex.printStackTrace();
        }
        ((CommonArena)arena).orphan();
    }

    protected void loadArenas() {
        File arenaStore = CommonDataFiles.ARENA_STORE.getFile(this);
        if (!arenaStore.exists())
            return;
        try {
            Optional<JsonObject> jsonOpt = JsonHelper.readJson(arenaStore);
            if (!jsonOpt.isPresent()) {
                CommonCore.logWarning("Arena store does not exist or contains malformed data. Not reading.");
                return;
            }
            JsonObject json = jsonOpt.get();
            for (Map.Entry<String, JsonElement> entry : (Iterable<Map.Entry<String, JsonElement>>)json.entrySet()) {
                if (json.get(entry.getKey()).isJsonObject()) {
                    JsonObject arenaJson = json.getAsJsonObject(entry.getKey());
                    if (arenaJson.has("name") && arenaJson
                            .has("world")) {
                        Location3D upperBound = JsonSerializer.deserializeLocation(arenaJson
                                .getAsJsonObject("bound.upper"));
                        Location3D lowerBound = JsonSerializer.deserializeLocation(arenaJson
                                .getAsJsonObject("bound.lower"));
                        CommonArena arena = ((IArenaFactory)FactoryRegistry.getFactory(Arena.class)).createArena(this, entry

                                .getKey().toLowerCase(), arenaJson
                                .get("name").getAsString(), new Location3D[] { new Location3D(arenaJson

                                .get("world").getAsString(), lowerBound
                                .getX(), lowerBound.getY(), lowerBound.getZ()) }new Boundary(upperBound, lowerBound))
                        arena.getSpawnPointMap().remove(0);
                        arena.configure(arenaJson);
                        continue;
                    }
                    CommonCore.logWarning("Invalid object \"" + entry.getKey() + "\"in arena store");
                    continue;
                }
                CommonCore.logWarning("Found non-object for key \"" + entry.getKey() + "\" - not loading");
            }
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load existing arenas from disk", ex);
        }
    }

    public void loadLobbySigns() {
        try {
            File store = CommonDataFiles.LOBBY_STORE.getFile(this);
            Optional<JsonObject> jsonOpt = JsonHelper.readJson(store);
            if (!jsonOpt.isPresent())
                return;
            JsonObject json = jsonOpt.get();
            for (Map.Entry<String, JsonElement> entry : (Iterable<Map.Entry<String, JsonElement>>)json.entrySet()) {
                if (json.get(entry.getKey()).isJsonObject()) {
                    Optional<Arena> arena = getArena(entry.getKey());
                    if (arena.isPresent()) {
                        JsonObject arenaJson = json.getAsJsonObject(entry.getKey());
                        List<String> toRemove = new ArrayList<>();
                        for (Map.Entry<String, JsonElement> arenaEntry : (Iterable<Map.Entry<String, JsonElement>>)arenaJson.entrySet()) {
                            if (arenaJson.get(arenaEntry.getKey()).isJsonObject())
                                try {
                                    Location3D loc = Location3D.deserialize(arenaEntry.getKey());
                                    switch (checkPhysicalLobbySign(loc)) {
                                        case 0:
                                            break;
                                        case 1:
                                            continue;
                                        case 2:
                                            toRemove.add(arenaEntry.getKey());
                                            continue;
                                        default:
                                            throw new AssertionError("The platform implementation did something super-wrong. Report this immediately.");
                                    }
                                    try {
                                        LobbySign sign = ((ILobbySignF<actory)FactoryRegistry.getFactory(LobbySign.class)).createLobbySign(loc, (Arena)arena.get(), arenaJson
                                                .getAsJsonObject(arenaEntry.getKey()));
                                        ((CommonArena)arena.get()).getLobbySignMap().put(loc, sign);
                                    } catch (IllegalArgumentException ex) {
                                        CommonCore.logWarning("Found lobby sign in store with invalid configuration. Removing...");
                                        json.remove(arenaEntry.getKey());
                                    }
                                } catch (IllegalArgumentException ignored) {
                                    CommonCore.logWarning("Found lobby sign in store with invalid location serial. Removing...");
                                }
                        }
                        for (String key : toRemove)
                            arenaJson.remove(key);
                        continue;
                    }
                    CommonCore.logVerbose("Found orphaned lobby sign group (arena \"" + entry.getKey() + "\") - not loading");
                }
            }
            try (FileWriter writer = new FileWriter(store)) {
                writer.write(json.toString());
            }
        } catch (IOException ex) {
            CommonCore.logSevere("Failed to load lobby signs for minigame " + getPlugin());
            ex.printStackTrace();
        }
    }

    protected abstract int checkPhysicalLobbySign(Location3D paramLocation3D);

    public ImmutableList<Round> getRounds() {
        return ImmutableList.copyOf(this.rounds.values());
    }

    public ImmutableList<Challenger> getChallengers() {
        ImmutableList.Builder<Challenger> builder = ImmutableList.builder();
        for (UnmodifiableIterator<Round> unmodifiableIterator = getRounds().iterator(); unmodifiableIterator.hasNext(); ) {
            Round r = unmodifiableIterator.next();
            builder.addAll((Iterable)r.getChallengers());
        }
        return builder.build();
    }

    public Optional<Challenger> getChallenger(UUID uuid) {
        for (UnmodifiableIterator<Round> unmodifiableIterator = getRounds().iterator(); unmodifiableIterator.hasNext(); ) {
            Round r = unmodifiableIterator.next();
            if (r.getChallenger(uuid).isPresent())
                return r.getChallenger(uuid);
        }
        return Optional.absent();
    }

    public <T extends com.sap.sepp.api.utils.builder.Buildable<U>, U extends com.sap.sepp.api.utils.builder.Builder<T>> U createBuilder(Class<T> type) {
        return (U) BuilderRegistry.instance().createBuilder(type, this);
    }

    public Map<ConfigNode<?>, Object> getConfigMap() {
        return this.config;
    }

    public Map<String, Arena> getArenaMap() {
        return this.arenas;
    }

    public Map<Arena, Round> getRoundMap() {
        return this.rounds;
    }

    private static class BreakingEventBusFactory {
        private static EventBus getBreakingEventBus() {
            return new EventBus((SubscriberExceptionHandler) FlintSubscriberExceptionHandler.getInstance());
        }
    }
}
