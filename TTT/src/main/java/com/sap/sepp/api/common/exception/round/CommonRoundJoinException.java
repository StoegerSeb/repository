package com.sap.sepp.api.common.exception.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonRoundJoinException: 07.04.2020 16:51 by sebip
*/

import com.sap.sepp.api.exceptions.round.RoundJoinException;
import com.sap.sepp.api.round.Round;

import java.util.UUID;

public class CommonRoundJoinException extends RoundJoinException {
    public CommonRoundJoinException(UUID player, Round round, Throwable cause, String message) {
        super(player, round, cause, message);
    }

    public CommonRoundJoinException(UUID player, Round round, Throwable cause) {
        super(player, round, cause);
    }

    public CommonRoundJoinException(UUID player, Round round, RoundJoinException.Reason reason) {
        super(player, round, reason, reason.getMessage());
    }
}
