package com.sap.sepp.api.common.metadata;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonMetadata: 07.04.2020 16:55 by sebip
*/

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.eventbus.EventBus;
import com.sap.sepp.api.metadata.Metadata;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class CommonMetadata implements Metadata {
    private static final EventBus EVENT_BUS = new EventBus();

    protected final Map<String, Object> data = new HashMap<>();

    public boolean containsKey(String key) {
        return this.data.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return this.data.containsValue(value);
    }

    public boolean has(String key) {
        return containsKey(key);
    }

    public <T> Optional<T> get(String key) throws ClassCastException {
        return Optional.fromNullable(this.data.get(key));
    }

    public <T> void set(String key, T value) {
        this.data.put(key, value);
    }

    public Metadata createStructure(String key) throws IllegalArgumentException {
        Preconditions.checkArgument(!this.data.containsKey(key), "Metadata key " + key + " is already set");
        Metadata structure = new CommonMetadata();
        this.data.put(key, structure);
        return structure;
    }

    public boolean remove(String key) {
        Object result = this.data.remove(key);
        return (result != null);
    }

    public ImmutableSet<String> getAllKeys() {
        return ImmutableSet.copyOf(this.data.keySet());
    }

    public ImmutableSet<String> keySet() {
        return ImmutableSet.copyOf(this.data.keySet());
    }

    public ImmutableCollection<?> values() {
        return ImmutableList.copyOf(this.data.values());
    }

    public ImmutableSet<? extends Map.Entry<String, ?>> entrySet() {
        return ImmutableSet.copyOf(Collections2.transform(this.data.entrySet(), new Function<Map.Entry<String, ?>, AbstractMap.SimpleImmutableEntry<String, ?>>() {
            public AbstractMap.SimpleImmutableEntry<String, ?> apply(Map.Entry<String, ?> input) {
                return new AbstractMap.SimpleImmutableEntry<>(input);
            }
        }));
    }

    public void clear() {
        this.data.clear();
    }

    public static EventBus getEventBus() {
        return EVENT_BUS;
    }
}
