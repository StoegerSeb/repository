package com.sap.sepp.api.steel.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class WizardPlayer: 07.04.2020 16:35 by sebip
*/

class WizardPlayer extends CommonWizardPlayer {
    private Material origMaterial;

    private byte origLegacyData;

    private Object origBlockData;

    WizardPlayer(UUID uuid, Location3D location, IWizardManager manager) {
        super(uuid, location, manager);
    }

    public void playbackWithheldMessages() {
        Bukkit.getScheduler().runTask((Plugin)SteelMain.getInstance(), new Runnable() {
            public void run() {
                Player player = Bukkit.getPlayer(WizardPlayer.this.uuid);
                player.sendMessage("back withheld chat messages...");
                for (String[] msg : WizardPlayer.this.withheldMessages)
                    player.sendMessage("<" + msg[0] + "> " + msg[1]);
            }
        });
    }

    protected void recordTargetBlockState() {
        assert LocationHelper.convertLocation(this.location).getBlock().getState() instanceof org.bukkit.block.Sign;
        this.origMaterial = LocationHelper.convertLocation(this.location).getBlock().getType();
        if (SteelCore.isLegacy()) {
            this.origLegacyData = LocationHelper.convertLocation(this.location).getBlock().getState().getRawData();
        } else {
            this.origBlockData = LocationHelper.convertLocation(this.location).getBlock().getBlockData();
        }
    }

    protected void restoreTargetBlock() {
        Bukkit.getScheduler().runTask((Plugin)SteelMain.getInstance(), new Runnable() {
            public void run() {
                Block b = LocationHelper.convertLocation(WizardPlayer.this.getLocation()).getBlock();
                b.setType(WizardPlayer.this.origMaterial);
                if (SteelCore.isLegacy()) {
                    SteelCore.getLegacyHelper().updateData(b, WizardPlayer.this.origLegacyData);
                } else {
                    b.setBlockData((BlockData)WizardPlayer.this.origBlockData);
                }
            }
        });
    }
}
