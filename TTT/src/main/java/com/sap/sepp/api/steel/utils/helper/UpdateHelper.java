package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class UpdateHelper: 07.04.2020 16:21 by sebip
*/

public class UpdateHelper {
    private static final int CURSEFORGE_PROJECT_ID = 95203;

    private static final Field UPDATER_DELIMETER;

    private static final Field UPDATER_TYPE;

    private static final Method UPDATER_RUN_UPDATER;

    static {
        try {
            UPDATER_DELIMETER = Updater.class.getDeclaredField("DELIMETER");
            UPDATER_DELIMETER.setAccessible(true);
            UPDATER_TYPE = Updater.class.getDeclaredField("type");
            UPDATER_TYPE.setAccessible(true);
            UPDATER_RUN_UPDATER = Updater.class.getDeclaredMethod("runUpdater", new Class[0]);
            UPDATER_RUN_UPDATER.setAccessible(true);
        } catch (NoSuchFieldException|NoSuchMethodException ex) {
            throw new RuntimeException("Failed to initialize updater!");
        }
    }

    public static void run() {
        try {
            if (SteelMain.getInstance().getConfig().getBoolean("enable-updater")) {
                Updater checker = new Updater((Plugin)SteelMain.getInstance(), 95203, SteelMain.getInstance().getFile(), Updater.UpdateType.NO_DOWNLOAD, true);
                if (checker.getResult() == Updater.UpdateResult.UPDATE_AVAILABLE) {
                    String remoteVersion = checker.getLatestName().split((String)UPDATER_DELIMETER.get((Object)null))[1];
                    int remoteMajor = Integer.parseInt(remoteVersion.split("\\.")[0]);
                    int localMajor = Integer.parseInt(SteelMain.getInstance().getDescription().getVersion().split("\\.")[0]);
                    if (remoteMajor == localMajor) {
                        UPDATER_TYPE.set(checker, Updater.UpdateType.DEFAULT);
                        UPDATER_RUN_UPDATER.invoke(checker, new Object[0]);
                    } else if (remoteMajor > localMajor) {
                        SteelCore.logInfo("A new major version (" + remoteVersion + ") of Steel is available! Keep in mind this may contain breaking changes, so ensure that your minigame plugins are compatible with the new major version before manually upgrading.");
                    } else {
                        SteelCore.logWarning("Local major version is greater than remote - something is not right");
                    }
                }
            }
        } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException|NumberFormatException ex) {
            throw new RuntimeException("Failed to run updater", ex);
        }
    }
}