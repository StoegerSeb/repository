package com.sap.sepp.api.common.utils.agent.rollback;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonRollbackAgent: 07.04.2020 16:41 by sebip
*/

import com.google.common.base.Preconditions;
import com.google.common.collect.UnmodifiableIterator;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.utils.file.CommonDataFiles;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.utils.physical.Location3D;

import java.io.*;
import java.sql.*;
import java.util.*;

public abstract class CommonRollbackAgent implements IRollbackAgent {
    private static final String SQLITE_PROTOCOL = "jdbc:sqlite:";

    private static final Properties SQL_QUERIES = new Properties();

    private final CommonArena arena;

    private final File rollbackStore;

    private final File stateStore;

    protected CommonRollbackAgent(CommonArena arena) {
        this.arena = arena;
        this.rollbackStore = CommonDataFiles.ROLLBACK_STORE.getFile(getArena().getMinigame());
        this.stateStore = CommonDataFiles.ROLLBACK_STATE_STORE.getFile(getArena().getMinigame());
        initializeStateStore();
    }

    static {
        try (InputStream is = CommonRollbackAgent.class.getResourceAsStream("/sql-queries.properties")) {
            SQL_QUERIES.load(is);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load SQL query strings", ex);
        }
    }

    public CommonArena getArena() {
        return this.arena;
    }

    public void createRollbackDatabase() throws IOException, SQLException {
        if (!this.rollbackStore.exists())
            this.rollbackStore.createNewFile();
        if (!this.stateStore.exists())
            this.stateStore.createNewFile();
        try(Connection conn = DriverManager.getConnection("jdbc:sqlite:" + this.rollbackStore.getAbsolutePath());
            PreparedStatement st = conn.prepareStatement(SQL_QUERIES.getProperty("create-rollback-table")
                    .replace("{table}", getArena().getId()))) {
            st.executeUpdate();
        }
    }

    public Map<Integer, String> loadStateMap() throws IOException {
        Map<Integer, String> stateMap = new HashMap<>();
        JsonObject json = (new JsonParser()).parse(new FileReader(this.stateStore)).getAsJsonObject();
        if (!json.has(getArena().getId()) || !json.get(getArena().getId()).isJsonObject())
            throw new IOException("Cannot load rollback states for arena " + getArena().getId());
        JsonObject arena = json.getAsJsonObject(getArena().getId());
        for (Map.Entry<String, JsonElement> entry : arena.entrySet()) {
            int id = -1;
            try {
                id = Integer.parseInt(entry.getKey());
            } catch (NumberFormatException ex) {
                CommonCore.logWarning("Cannot load rollback state with ID " + entry.getKey() + " - key is not an int");
            }
            if (!entry.getValue().isJsonPrimitive() || !entry.getValue().getAsJsonPrimitive().isString()) {
                CommonCore.logWarning("Cannot load rollback state with ID " + id + " - not a string");
                continue;
            }
            stateMap.put(id, entry.getValue().getAsString());
        }
        return stateMap;
    }

    public void logChange(RollbackRecord record) throws IOException, SQLException {
        String world = record.getLocation().getWorld().isPresent() ? (String)record.getLocation().getWorld().get() : this.arena.getWorld();
        Preconditions.checkNotNull(record.getLocation(), "Location required for all record types");
        switch (record.getType()) {
            case BLOCK_CHANGE:
                Preconditions.checkNotNull(record.getTypeData(), "Type required for BLOCK_CHANGED record type");
                break;
            case ENTITY_CREATION:
                Preconditions.checkNotNull(record.getUuid(), "UUID required for ENTITY_CREATED record type");
                break;
            case ENTITY_CHANGE:
                Preconditions.checkNotNull(record.getTypeData(), "Type required for ENTITY_CHANGED record type");
                Preconditions.checkNotNull(record.getStateSerial(), "State required for ENTITY_CHANGED record type");
                break;
            default:
                throw new AssertionError("Undefined record type");
        }
        if (!this.rollbackStore.exists())
            this.rollbackStore.createNewFile();
        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:" + this.rollbackStore.getPath())) {
            String querySql;
            String updateSql;
            int id;
            switch (record.getType()) {
                case BLOCK_CHANGE:
                    querySql = SQL_QUERIES.getProperty("query-by-location").replace("{world}", world).replace("{x}", "" + record.getLocation().getX()).replace("{y}", "" + record.getLocation().getY()).replace("{z}", "" + record.getLocation().getZ());
                    break;
                case ENTITY_CHANGE:
                    querySql = SQL_QUERIES.getProperty("query-by-uuid").replace("{uuid}", record.getUuid().toString());
                    break;
                default:
                    querySql = null;
                    break;
            }
            if (querySql != null) {
                querySql = querySql.replace("{table}", getArena().getId());
                try(PreparedStatement query = conn.prepareStatement(querySql);
                    ResultSet queryResults = query.executeQuery()) {
                    if (queryResults.next())
                        return;
                }
            }
            switch (record.getType()) {
                case BLOCK_CHANGE:
                    updateSql = SQL_QUERIES.getProperty("insert-block-rollback-record").replace("{world}", world).replace("{x}", "" + record.getLocation().getX()).replace("{y}", "" + record.getLocation().getY()).replace("{z}", "" + record.getLocation().getZ()).replace("{type}", record.getTypeData()).replace("{data}", "" + record.getData());
                    break;
                case ENTITY_CREATION:
                    updateSql = SQL_QUERIES.getProperty("insert-entity-created-rollback-record").replace("{world}", world).replace("{uuid}", record.getUuid().toString());
                    break;
                case ENTITY_CHANGE:
                    updateSql = SQL_QUERIES.getProperty("insert-entity-changed-rollback-record").replace("{world}", world).replace("{x}", "" + record.getLocation().getX()).replace("{y}", "" + record.getLocation().getY()).replace("{z}", "" + record.getLocation().getZ()).replace("{uuid}", record.getUuid().toString()).replace("{type}", record.getTypeData());
                    break;
                default:
                    throw new AssertionError("Inconsistency detected in method: recordType is in an illegal state. Report this immediately.");
            }
            if (updateSql != null)
                updateSql = updateSql.replace("{table}", getArena().getId()).replace("{state}", "" + ((record.getStateSerial() != null) ? 1 : 0)).replace("{record_type}", "" + record.getType().ordinal());
            try (PreparedStatement ps = conn.prepareStatement(updateSql, 1)) {
                ps.executeUpdate();
                try (ResultSet gen = ps.getGeneratedKeys()) {
                    if (gen.next()) {
                        id = gen.getInt(1);
                    } else {
                        throw new SQLException("Failed to get generated key from update query");
                    }
                }
            }
            if (record.getStateSerial() != null)
                saveStateSerial(id, record.getStateSerial());
        }
    }

    public void popRollbacks() throws IOException, SQLException {
        Set<RollbackRecord> blockChangeRecords = new HashSet<>();
        Set<RollbackRecord> entityCreateRecords = new HashSet<>();
        final Set<RollbackRecord> entityChangeRecords = new HashSet<>();
        if (this.rollbackStore.exists()) {
            Map<Integer, String> stateMap = loadStateMap();
            try(Connection conn = DriverManager.getConnection("jdbc:sqlite:" + this.rollbackStore.getAbsolutePath());
                PreparedStatement query = conn.prepareStatement(SQL_QUERIES.getProperty("get-all-records")
                        .replace("{table}", getArena().getId()));
                PreparedStatement drop = conn.prepareStatement(SQL_QUERIES.getProperty("drop-table")
                        .replace("{table}", getArena().getId()));
                ResultSet rs = query.executeQuery()) {
                cacheEntities();
                while (rs.next()) {
                    try {
                        int id = rs.getInt("id");
                        String world = rs.getString("world");
                        int x = rs.getInt("x");
                        int y = rs.getInt("y");
                        int z = rs.getInt("z");
                        UUID uuid = (rs.getString("uuid") != null) ? UUID.fromString(rs.getString("uuid")) : null;
                        String type = rs.getString("type");
                        int data = rs.getInt("data");
                        boolean state = rs.getBoolean("state");
                        RollbackRecord.Type recordType = RollbackRecord.Type.values()[rs.getInt("record_type")];
                        if (world.equals(getArena().getWorld())) {
                            String stateSerial = stateMap.get(id);
                            if (state && stateSerial == null)
                                CommonCore.logVerbose("Rollback record with ID " + id + " was marked as having state, but no corresponding serial was found");
                            switch (recordType) {
                                case BLOCK_CHANGE:
                                    blockChangeRecords.add(RollbackRecord.createBlockRecord(id, new Location3D(world, x, y, z), type, data, stateSerial));
                                    continue;
                                case ENTITY_CREATION:
                                    entityCreateRecords.add(RollbackRecord.createEntityCreationRecord(id, uuid, world));
                                    continue;
                                case ENTITY_CHANGE:
                                    entityChangeRecords.add(RollbackRecord.createEntityChangeRecord(id, uuid, new Location3D(world, x, y, z), type, stateSerial));
                                    continue;
                            }
                            CommonCore.logWarning("Invalid rollback record type at ID " + id);
                            continue;
                        }
                        CommonCore.logVerbose("Rollback record with ID " + id + " in arena " + getArena().getId() + " has a mismtching world name - refusing to roll back");
                    } catch (SQLException ex) {
                        CommonCore.logSevere("Failed to read rollback record in arena " + getArena().getId());
                        ex.printStackTrace();
                    }
                }
                for (RollbackRecord record : blockChangeRecords) {
                    assert record.getType() == RollbackRecord.Type.BLOCK_CHANGE;
                    rollbackBlock(record);
                }
                for (RollbackRecord record : entityCreateRecords) {
                    assert record.getType() == RollbackRecord.Type.BLOCK_CHANGE;
                    rollbackEntityCreation(record);
                }
                delay(new Runnable() {
                    public void run() {
                        try {
                            for (RollbackRecord record : entityChangeRecords) {
                                assert record.getType() == RollbackRecord.Type.BLOCK_CHANGE;
                                CommonRollbackAgent.this.rollbackEntityChange(record);
                            }
                        } catch (IOException iOException) {}
                    }
                });
                drop.executeUpdate();
            }
            clearStateStore();
        } else {
            throw new IllegalArgumentException("Rollback store does not exist");
        }
    }

    public void clearStateStore() throws IOException {
        JsonObject json = (new JsonParser()).parse(new FileReader(this.stateStore)).getAsJsonObject();
        if (!json.has(getArena().getId()) || !json.get(getArena().getId()).isJsonObject()) {
            CommonCore.logWarning("State store clear requested, but arena was not present");
            return;
        }
        json.remove(getArena().getId());
        json.add(getArena().getId(), new JsonObject());
        saveState(json);
    }

    public void initializeStateStore() {
        try {
            JsonObject jsonObject;
            if (!this.stateStore.exists())
                this.stateStore.createNewFile();
            JsonElement json = (new JsonParser()).parse(new FileReader(this.stateStore));
            if (json.isJsonNull())
                jsonObject = new JsonObject();
            jsonObject.getAsJsonObject().add(getArena().getId(), new JsonObject());
            saveState(jsonObject.getAsJsonObject());
        } catch (IOException ex) {
            throw new RuntimeException("Failed to intialize state store for arena " + this.arena.getId(), ex);
        }
    }

    public void saveStateSerial(int id, String serial) throws IOException {
        JsonObject json = (new JsonParser()).parse(new FileReader(this.stateStore)).getAsJsonObject();
        if (!json.has(getArena().getId())) {
            initializeStateStore();
            saveStateSerial(id, serial);
            return;
        }
        json.get(getArena().getId()).getAsJsonObject().addProperty(id + "", serial);
        saveState(json);
    }

    private void saveState(JsonObject json) throws IOException {
        try (FileWriter writer = new FileWriter(this.stateStore)) {
            writer.write((new Gson()).toJson((JsonElement)json));
        }
    }

    protected static List<Arena> checkChangeAtLocation(Location3D location) {
        List<Arena> arenas = new ArrayList<>();
        for (Minigame mg : CommonCore.getMinigames().values()) {
            for (UnmodifiableIterator<Arena> unmodifiableIterator = mg.getArenas().iterator(); unmodifiableIterator.hasNext(); ) {
                Arena arena = unmodifiableIterator.next();
                if (arena.getRound().isPresent() && arena.getBoundary().contains(location))
                    arenas.add(arena);
            }
        }
        return arenas;
    }

    protected abstract void delay(Runnable paramRunnable);
}
