package com.sap.sepp.api.common.lobby.populator;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class StockChallengerListingLobbySignPopulator: 07.04.2020 16:52 by sebip
*/

import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.lobby.type.ChallengerListingLobbySign;
import com.sap.sepp.api.round.Round;

public class StockChallengerListingLobbySignPopulator implements LobbySignPopulator {
    private static final String EMPTY_STRING = "";

    private static final int SIGN_SIZE = 4;

    public String first(LobbySign sign) {
        return getPlayer(sign, 0);
    }

    public String second(LobbySign sign) {
        return getPlayer(sign, 1);
    }

    public String third(LobbySign sign) {
        return getPlayer(sign, 2);
    }

    public String fourth(LobbySign sign) {
        return getPlayer(sign, 3);
    }

    public String getPlayer(LobbySign sign, int lineIndex) {
        int startIndex = ((ChallengerListingLobbySign)sign).getIndex() * 4;
        if (sign.getArena().getRound().isPresent() && startIndex + lineIndex < sign
                .getArena().getRound().get().getChallengers().size())
            return sign.getArena().getRound().get().getChallengers().get(startIndex + lineIndex).getName();
        return "";
    }
}
