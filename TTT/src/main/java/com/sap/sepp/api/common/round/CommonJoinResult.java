package com.sap.sepp.api.common.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonJoinResult: 07.04.2020 16:57 by sebip
*/

import com.google.common.base.Preconditions;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.round.JoinResult;

public class CommonJoinResult implements JoinResult {
    private JoinResult.Status status;

    private Challenger challenger;

    private Throwable throwable;

    public CommonJoinResult(Challenger challenger) {
        this.challenger = challenger;
        this.status = JoinResult.Status.SUCCESS;
    }

    public CommonJoinResult(Throwable throwable) {
        this.throwable = throwable;
        this.status = JoinResult.Status.INTERNAL_ERROR;
    }

    public CommonJoinResult(JoinResult.Status status) {
        assert status != JoinResult.Status.SUCCESS;
        assert status != JoinResult.Status.INTERNAL_ERROR;
        this.status = status;
    }

    public Challenger getChallenger() throws IllegalStateException {
        Preconditions.checkState((this.status == JoinResult.Status.SUCCESS), "Cannot get Challenger if JoinResult status is not SUCCESS");
        return this.challenger;
    }

    public JoinResult.Status getStatus() {
        return this.status;
    }

    public Throwable getThrowable() throws IllegalStateException {
        Preconditions.checkState((this.status == JoinResult.Status.INTERNAL_ERROR), "Cannot get Throwable if JoinResult status is not INTERNAL_ERROR");
        return this.throwable;
    }
}
