package com.sap.sepp.api.common.utils.unsafe;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonUnsafeUtil: 07.04.2020 16:45 by sebip
*/

import com.sap.sepp.api.utils.unsafe.UnsafeUtil;

import java.util.regex.Pattern;

public abstract class CommonUnsafeUtil extends UnsafeUtil {
    private static final Pattern FLINT_PACKAGE_PATTERN = Pattern.compile("^com\\.sap\\.sepp\\.api");

    private static final Pattern UNSAFE_PACKAGE_PATTERN = Pattern.compile("^com\\.sepp\\.api(.*)\\.utils\\.unsafe");

    protected static void testInternalUse() {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        for (int i = 1; i < stack.length; i++) {
            if (!UNSAFE_PACKAGE_PATTERN.matcher(stack[i].getClassName()).find()) {
                if (FLINT_PACKAGE_PATTERN.matcher(stack[i].getClassName()).find())
                    break;
                throw new IllegalStateException("UnsafeUtil may not be used externally");
            }
        }
    }
}
