package com.sap.sepp.api.steel.listener.rollback;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RollbackBlockListener: 07.04.2020 16:31 by sebip
*/

public class RollbackBlockListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBurn(BlockBurnEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockFade(BlockFadeEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockFromTo(BlockFromToEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockSpread(BlockSpreadEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
        for (Block b : event.getBlocks())
            RollbackAgent.checkBlockChange(b, (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
        for (Block b : event.getBlocks())
            RollbackAgent.checkBlockChange(b, (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockForm(BlockFormEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockGrow(BlockGrowEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockMultiPlace(BlockMultiPlaceEvent event) {
        for (BlockState state : event.getReplacedBlockStates())
            RollbackAgent.checkBlockChange(state.getBlock(), (Event)event);
        RollbackAgent.checkBlockChange(event.getBlock(), (Event)event);
    }
}
