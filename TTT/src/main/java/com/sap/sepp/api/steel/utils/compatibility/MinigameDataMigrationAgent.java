package com.sap.sepp.api.steel.utils.compatibility;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class MinigameDataMigrationAgent: 07.04.2020 16:17 by sebip
*/

public class MinigameDataMigrationAgent extends DataMigrationAgent {
    private final Minigame mg;

    public MinigameDataMigrationAgent(Minigame mg) {
        this.mg = mg;
    }

    public void migrateData() {
        migrateArenaStore();
        migrateLobbyStore();
    }

    private void migrateArenaStore() {
        File oldFile = SteelDataFiles.OLD_ARENA_STORE.getFile(this.mg);
        if (oldFile.exists()) {
            SteelCore.logInfo("Detected old arena store for minigame " + this.mg.getPlugin() + ". Attempting to migrate...");
            File newFile = SteelDataFiles.ARENA_STORE.getFile(this.mg);
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(oldFile);
                JsonObject json = new JsonObject();
                for (String key : yaml.getKeys(false)) {
                    if (!yaml.isConfigurationSection(key)) {
                        SteelCore.logWarning("Found invalid key \"" + key + "\" in old arena store. Not migrating...");
                        continue;
                    }
                    ConfigurationSection arenaSec = yaml.getConfigurationSection(key);
                    String name = arenaSec.getString("name");
                    String world = arenaSec.getString("world");
                    ConfigurationSection spawns = arenaSec.getConfigurationSection("spawns");
                    Location3D lowBound = Location3D.deserialize(arenaSec.getString("bound.lower"));
                    Location3D highBound = Location3D.deserialize(arenaSec.getString("bound.upper"));
                    JsonObject spawnsJson = new JsonObject();
                    for (String spawnKey : spawns.getKeys(false))
                        spawnsJson.add(spawnKey, (JsonElement)JsonSerializer.serializeLocation(Location3D.deserialize(spawns.getString(spawnKey))));
                    JsonObject arenaJson = new JsonObject();
                    arenaJson.addProperty("name", name);
                    arenaJson.addProperty("world", world);
                    arenaJson.add("spawns", (JsonElement)spawnsJson);
                    arenaJson.add("bound.lower", (JsonElement)JsonSerializer.serializeLocation(lowBound));
                    arenaJson.add("bound.upper", (JsonElement)JsonSerializer.serializeLocation(highBound));
                    ConfigurationSection metadataSec = arenaSec.getConfigurationSection("metadata");
                    PersistentMetadata metadata = (metadataSec != null) ? buildMetadata(metadataSec) : null;
                    if (metadata != null) {
                        JsonObject metaJson = new JsonObject();
                        JsonSerializer.serializeMetadata(metaJson, metadata);
                        arenaJson.add("metadata", (JsonElement)metaJson);
                    }
                    json.add(key, (JsonElement)arenaJson);
                }
                Files.deleteIfExists(newFile.toPath());
                Files.createFile(newFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
                try (FileWriter writer = new FileWriter(newFile)) {
                    writer.write(json.toString());
                }
            } catch (InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to migrate arena store!");
                ex.printStackTrace();
            }
            relocateOldFile(oldFile.toPath());
        }
    }

    private void migrateLobbyStore() {
        File oldFile = SteelDataFiles.OLD_LOBBY_STORE.getFile(this.mg);
        if (oldFile.exists()) {
            SteelCore.logInfo("Detected old lobby store for minigame " + this.mg.getPlugin() + ". Attempting to migrate...");
            File newFile = SteelDataFiles.LOBBY_STORE.getFile(this.mg);
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(oldFile);
                JsonObject json = new JsonObject();
                for (String key : yaml.getKeys(false)) {
                    if (!yaml.isConfigurationSection(key)) {
                        SteelCore.logWarning("Found invalid arena key \"" + key + "\" in old lobby store. Not migrating...");
                        continue;
                    }
                    JsonObject arenaJson = new JsonObject();
                    json.add(key, (JsonElement)arenaJson);
                    ConfigurationSection arenaSec = yaml.getConfigurationSection(key);
                    for (String coordKey : arenaSec.getKeys(false)) {
                        if (!arenaSec.isConfigurationSection(coordKey))
                            continue;
                        try {
                            ConfigurationSection signSec = arenaSec.getConfigurationSection(coordKey);
                            JsonObject signJson = new JsonObject();
                            arenaJson.add(coordKey, (JsonElement)signJson);
                            signJson.addProperty("type", signSec.getString("type"));
                            if (signJson.has("index"))
                                signJson.addProperty("index", signSec.getString("index"));
                        } catch (IllegalArgumentException illegalArgumentException) {}
                    }
                }
                Files.deleteIfExists(newFile.toPath());
                Files.createFile(newFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
                try (FileWriter writer = new FileWriter(newFile)) {
                    writer.write(json.toString());
                }
            } catch (InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to migrate lobby store!");
                ex.printStackTrace();
            }
            relocateOldFile(oldFile.toPath());
        }
    }

    protected File getOldDir() {
        return SteelDataFiles.MG_OLD_DATA_DIR.getFile(this.mg);
    }

    private PersistentMetadata buildMetadata(ConfigurationSection cs) {
        CommonPersistentMetadata commonPersistentMetadata = new CommonPersistentMetadata();
        for (String key : cs.getKeys(false)) {
            if (cs.isConfigurationSection(key)) {
                commonPersistentMetadata.set(key, buildMetadata(cs.getConfigurationSection(key)));
                continue;
            }
            commonPersistentMetadata.set(key, cs.getString(key));
        }
        return (PersistentMetadata)commonPersistentMetadata;
    }
}