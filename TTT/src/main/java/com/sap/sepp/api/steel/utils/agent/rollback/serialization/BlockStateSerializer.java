package com.sap.sepp.api.steel.utils.agent.rollback.serialization;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class BlockStateSerializer: 07.04.2020 16:12 by sebip
*/


import com.google.common.base.Optional;
import com.sap.sepp.api.steel.SteelCore;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.block.*;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.block.data.type.NoteBlock;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.material.FlowerPot;
import org.bukkit.material.MaterialData;

import java.util.Arrays;
import java.util.List;

public class BlockStateSerializer {
    private static final String INVENTORY_KEY = "inventory";

    private static final String SIGN_LINES_KEY = "lines";

    @Deprecated
    private static final String BANNER_BASE_COLOR_KEY = "base-color";

    private static final String BANNER_PATTERNS_KEY = "patterns";

    private static final String BANNER_PATTERN_COLOR_KEY = "color";

    private static final String BANNER_PATTERN_TYPE_KEY = "type";

    private static final String SPAWNER_TYPE_KEY = "spawner-type";

    private static final String SPAWNER_DELAY_KEY = "spawner-delay";

    @Deprecated
    private static final String NOTE_OCTAVE_KEY = "octave";

    @Deprecated
    private static final String NOTE_TONE_KEY = "tone";

    @Deprecated
    private static final String NOTE_SHARPED_KEY = "sharped";

    private static final String JUKEBOX_DISC_KEY = "disc";

    private static final String SKULL_OWNER_KEY = "owner";

    @Deprecated
    private static final String SKULL_ROTATION_KEY = "rotation";

    private static final String COMMAND_NAME_KEY = "cmd-name";

    private static final String COMMAND_CMD_KEY = "command";

    @Deprecated
    private static final String FLOWER_TYPE_KEY = "flower-type";

    @Deprecated
    private static final String FLOWER_DATA_KEY = "flower-data";

    private static final String BLOCK_DATA_KEY = "block-data";

    public static Optional<String> serializeState(Block block) {
        YamlConfiguration yaml = new YamlConfiguration();
        BlockState state = block.getState();
        if (state instanceof InventoryHolder)
            yaml.set("inventory", InventoryHelper.serializeInventory(((InventoryHolder) state).getInventory()));
        if (state instanceof Sign) {
            yaml.set("lines", Arrays.asList(((Sign) state).getLines()));
        } else if (state instanceof Banner) {
            if (SteelCore.isLegacy())
                yaml.set("base-color", ((Banner) state).getBaseColor().name());
            ConfigurationSection patternSection = yaml.createSection("patterns");
            List<Pattern> patterns = ((Banner) state).getPatterns();
            for (int i = 0; i < patterns.size(); i++) {
                ConfigurationSection subSection = patternSection.createSection("" + i);
                subSection.set("color", ((Pattern) patterns.get(i)).getColor().name());
                subSection.set("type", ((Pattern) patterns.get(i)).getPattern().name());
            }
        } else if (state instanceof CreatureSpawner) {
            yaml.set("spawner-type", ((CreatureSpawner) state).getSpawnedType().name());
            yaml.set("spawner-delay", Integer.valueOf(((CreatureSpawner) state).getDelay()));
        } else if (state instanceof NoteBlock) {
            if (SteelCore.isLegacy()) {
                yaml.set("octave", Integer.valueOf(((NoteBlock) state).getNote().getOctave()));
                yaml.set("tone", ((NoteBlock) state).getNote().getTone().name());
                yaml.set("sharped", Boolean.valueOf(((NoteBlock) state).getNote().isSharped()));
            }
        } else if (state instanceof Jukebox) {
            if (((Jukebox) state).isPlaying())
                yaml.set("disc", ((Jukebox) state).getPlaying());
        } else if (state instanceof Skull) {
            yaml.set("owner", ((Skull) state).getOwner());
            if (SteelCore.isLegacy())
                yaml.set("rotation", ((Skull) state).getRotation().name());
        } else if (state instanceof CommandBlock) {
            yaml.set("cmd-name", ((CommandBlock) state).getName());
            yaml.set("command", ((CommandBlock) state).getCommand());
        } else if (state instanceof FlowerPot &&
                SteelCore.isLegacy()) {
            yaml.set("flower-type", ((FlowerPot) state).getContents().getItemType().name());
            yaml.set("flower-data", Byte.valueOf(((FlowerPot) state).getContents().getData()));
        }
        if (!SteelCore.isLegacy())
            yaml.set("block-data", block.getBlockData().getAsString());
        if (yaml.getKeys(false).size() > 0)
            return Optional.of(yaml.saveToString());
        return Optional.absent();
    }

    public static void deserializeState(Block block, String serial) throws InvalidConfigurationException {
        YamlConfiguration yaml = new YamlConfiguration();
        yaml.loadFromString(serial);
        boolean hasBlockData = false;
        if (!SteelCore.isLegacy() &&
                yaml.isString("block-data")) {
            hasBlockData = true;
            block.setBlockData(Bukkit.createBlockData(yaml.getString("block-data")));
        }
        BlockState state = block.getState();
        boolean missingData = false;
        boolean malformedData = false;
        if (state instanceof InventoryHolder &&
                yaml.isConfigurationSection("inventory"))
            ((InventoryHolder) state).getInventory().setContents(
                    InventoryHelper.deserializeInventory(yaml.getConfigurationSection("inventory")));
        boolean recognizedState = true;
        if (state instanceof Sign) {
            if (yaml.isList("lines")) {
                List<String> lines = yaml.getStringList("lines");
                for (int i = 0; i < lines.size(); i++)
                    ((Sign) state).setLine(i, lines.get(i));
            } else {
                missingData = true;
            }
        } else if (state instanceof Banner) {
            if (yaml.isSet("base-color")) {
                try {
                    DyeColor color = DyeColor.valueOf(yaml.getString("base-color"));
                    ((Banner) state).setBaseColor(color);
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
            if (yaml.isConfigurationSection("patterns")) {
                ConfigurationSection patterns = yaml.getConfigurationSection("patterns");
                for (String key : patterns.getKeys(false)) {
                    ConfigurationSection subSection = patterns.getConfigurationSection(key);
                    try {
                        DyeColor color = DyeColor.valueOf(subSection.getString("color"));
                        PatternType type = PatternType.valueOf(subSection.getString("type"));
                        ((Banner) state).addPattern(new Pattern(color, type));
                    } catch (IllegalArgumentException ex) {
                        malformedData = true;
                    }
                }
            } else {
                missingData = true;
            }
        } else if (state instanceof CreatureSpawner) {
            if (yaml.isSet("spawner-type")) {
                try {
                    EntityType type = EntityType.valueOf(yaml.getString("spawner-type"));
                    ((CreatureSpawner) state).setSpawnedType(type);
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
        } else if (state instanceof NoteBlock) {
            if (yaml.isInt("octave") && yaml.isSet("tone")) {
                try {
                    Note.Tone tone = Note.Tone.valueOf(yaml.getString("tone"));
                    ((NoteBlock) state).setNote(new Note(yaml
                            .getInt("octave"), tone, yaml.getBoolean("sharped")));
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
        } else if (state instanceof Jukebox) {
            if (yaml.isSet("disc")) {
                try {
                    Material disc = Material.valueOf(yaml.getString("disc"));
                    ((Jukebox) state).setPlaying(disc);
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
        } else if (state instanceof Skull) {
            if (yaml.isSet("owner"))
                ((Skull) state).setOwner(yaml.getString("owner"));
            if (yaml.isSet("rotation")) {
                try {
                    BlockFace face = BlockFace.valueOf(yaml.getString("rotation"));
                    ((Skull) state).setRotation(face);
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
        } else if (state instanceof CommandBlock) {
            if (yaml.isSet("command")) {
                ((CommandBlock) state).setCommand(yaml.getString("command"));
            } else {
                missingData = true;
            }
            if (yaml.isSet("cmd-name")) {
                ((CommandBlock) state).setName(yaml.getString("cmd-name"));
            } else {
                missingData = true;
            }
        } else if (state instanceof FlowerPot) {
            if (yaml.isSet("flower-type")) {
                try {
                    byte data = yaml.isSet("flower-data") ? (byte) yaml.getInt("flower-data") : 0;
                    Material type = Material.valueOf(yaml.getString("flower-type"));
                    ((FlowerPot) state).setContents(new MaterialData(type, data));
                } catch (IllegalArgumentException ex) {
                    malformedData = true;
                }
            } else {
                missingData = true;
            }
        } else if (!(state instanceof InventoryHolder)) {
            recognizedState = false;
            if (!hasBlockData)
                SteelCore.logWarning("Failed to deserialize state data for rollback record for block at {" + block
                        .getX() + ", " + block.getY() + ", " + block.getZ() + "}");
        }
        if (recognizedState)
            state.update(true);
        if (missingData)
            SteelCore.logVerbose("Block with type " + block.getType().name() + " at {" + block.getX() + ", " + block
                    .getY() + ", " + block.getZ() + "} is missing important state data");
        if (malformedData)
            SteelCore.logVerbose("Block with type " + block.getType().name() + " at {" + block.getX() + ", " + block
                    .getY() + ", " + block.getZ() + "} has malformed state data");
    }
}