package com.sap.sepp.api.steel.utils.agent.chat;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ChatAgent: 07.04.2020 16:10 by sebip
*/

import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class ChatAgent implements IChatAgent {
    public void processAndSend(UUID recipient, String... messages) {
        getPlayer(recipient).sendMessage(messages);
    }

    private Player getPlayer(UUID uuid) {
        Player pl = Bukkit.getPlayer(uuid);
        Preconditions.checkArgument((pl != null), "Cannot find player with given UUID");
        return pl;
    }
}
