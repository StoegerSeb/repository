package com.sap.sepp.api.common.utils.file;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonDataFiles: 07.04.2020 16:44 by sebip
*/

import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.minigame.Minigame;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommonDataFiles {
    private static final List<DataFile> FILES = new ArrayList<>();

    static final String ROOT_DATA_DIR = "flint_data";

    public static final CoreDataFile OFFLINE_PLAYER_STORE = new CoreDataFile("offline_players.json");

    public static final CoreDataFile PLAYER_INVENTORY_DIR = new CoreDataFile("inventories", true);

    public static final CoreDataFile PLAYER_LOCATION_STORE = new CoreDataFile("locs.json");

    public static final MinigameDataFile ARENA_STORE = new MinigameDataFile("arenas.json");

    public static final MinigameDataFile LOBBY_STORE = new MinigameDataFile("lobbies.json");

    public static final MinigameDataFile ROLLBACK_STORE = new MinigameDataFile("rollback.db");

    public static final MinigameDataFile ROLLBACK_STATE_STORE = new MinigameDataFile("rollback_state.json");

    static void register(DataFile dataFile) {
        FILES.add(dataFile);
    }

    public static void createCoreDataFiles() {
        createMinigameDataFiles(null);
    }

    public static void createMinigameDataFiles(Minigame minigame) {
        for (DataFile df : FILES) {
            if ((minigame != null && df instanceof MinigameDataFile) || (minigame == null && df instanceof CoreDataFile)) {
                File file = (minigame != null) ? ((MinigameDataFile)df).getFile(minigame) : ((CoreDataFile)df).getFile();
                if (file.isDirectory() != df.isDirectory())
                    file.delete();
                if (!file.exists()) {
                    boolean result = false;
                    try {
                        boolean parent = true;
                        if (!file.getParentFile().exists())
                            parent = file.getParentFile().mkdirs();
                        if (parent)
                            if (df.isDirectory()) {
                                result = file.mkdir();
                            } else {
                                result = file.createNewFile();
                            }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        result = false;
                    }
                    if (!result)
                        CommonCore.logSevere("Failed to create " + ((minigame == null) ? "core" : "minigame") + " data file " + "flint_data" + File.separatorChar + df
                                .getFileName());
                }
            }
        }
    }
}