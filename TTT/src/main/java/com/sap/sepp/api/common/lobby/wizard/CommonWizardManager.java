package com.sap.sepp.api.common.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonWizardManager: 07.04.2020 16:52 by sebip
*/

public abstract class CommonWizardManager implements IWizardManager {
    private CommonMinigame minigame;

    protected final HashMap<UUID, IWizardPlayer> wizardPlayers = new HashMap<>();

    protected CommonWizardManager(Minigame minigame) {
        this.minigame = (CommonMinigame)minigame;
    }

    public CommonMinigame getOwner() {
        return this.minigame;
    }

    public boolean hasPlayer(UUID uuid) {
        return this.wizardPlayers.containsKey(uuid);
    }

    public void removePlayer(UUID uuid) {
        this.wizardPlayers.remove(uuid);
    }

    public String[] accept(UUID uuid, String input) {
        if (this.wizardPlayers.containsKey(uuid))
            return ((IWizardPlayer)this.wizardPlayers.get(uuid)).accept(input);
        throw new IllegalArgumentException("Player with UUID " + uuid.toString() + " is not engaged in a wizard");
    }

    public void withholdMessage(UUID uuid, String sender, String message) {
        if (this.wizardPlayers.containsKey(uuid)) {
            ((IWizardPlayer)this.wizardPlayers.get(uuid)).withholdMessage(sender, message);
        } else {
            throw new IllegalArgumentException("Player with UUID " + uuid.toString() + " is not engaged in a wizard");
        }
    }
}
