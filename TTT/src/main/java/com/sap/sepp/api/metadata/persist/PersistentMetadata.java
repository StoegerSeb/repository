package com.sap.sepp.api.metadata.persist;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PersistentMetadata: 07.04.2020 15:58 by sebip
*/

import com.google.common.base.Function;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.sap.sepp.api.metadata.Metadata;
import com.sap.sepp.api.serialization.Serializer;

import java.util.List;
import java.util.Map;

public interface PersistentMetadata extends Metadata {
    <T> T get(String paramString, Serializer<T> paramSerializer) throws ClassCastException, IllegalArgumentException;

    void set(String paramString, Object paramObject) throws UnsupportedOperationException;

    void set(String paramString1, String paramString2);

    void set(String paramString, boolean paramBoolean);

    void set(String paramString, byte paramByte);

    void set(String paramString, short paramShort);

    void set(String paramString, char paramChar);

    void set(String paramString, int paramInt);

    void set(String paramString, long paramLong);

    void set(String paramString, float paramFloat);

    void set(String paramString, double paramDouble);

    <T> void set(String paramString, T paramT, Serializer<T> paramSerializer);

    void set(String paramString, List<String> paramList);

    <T> void set(String paramString, List<T> paramList, Serializer<T> paramSerializer);

    PersistentMetadata createStructure(String paramString);

    ImmutableCollection<String> values();

    ImmutableCollection<Object> values(Function<String, Object> paramFunction);

    ImmutableSet<? extends Map.Entry<String, String>> entrySet();

    ImmutableSet<? extends Map.Entry<String, Object>> entrySet(Function<String, Object> paramFunction);
}
