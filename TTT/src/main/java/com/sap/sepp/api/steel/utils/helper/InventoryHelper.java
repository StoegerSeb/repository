package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class InventoryHelper: 07.04.2020 16:20 by sebip
*/

public class InventoryHelper {
    public static ConfigurationSection serializeInventory(Inventory inventory) {
        return serializeInventory(inventory.getContents());
    }

    public static ConfigurationSection serializeInventory(ItemStack[] contents) {
        ConfigurationSection cs = (new YamlConfiguration()).createSection("doot doot");
        cs.set("capacity", Integer.valueOf(contents.length));
        for (int i = 0; i < contents.length; i++)
            cs.set(Integer.toString(i), contents[i]);
        return cs;
    }

    public static ItemStack[] deserializeInventory(ConfigurationSection serial) throws IllegalArgumentException {
        if (!serial.contains("capacity"))
            throw new IllegalArgumentException("Serialized inventory is missing required element \"capacity\"");
        int capacity = serial.getInt("capacity");
        ItemStack[] contents = new ItemStack[capacity];
        for (int i = 0; i < capacity; i++) {
            if (serial.contains(Integer.toString(i)))
                contents[i] = serial.getItemStack(Integer.toString(i));
        }
        return contents;
    }

    private static JsonObject csToJson(ConfigurationSection cs) {
        JsonObject json = new JsonObject();
        for (String key : cs.getKeys(false)) {
            if (cs.isConfigurationSection(key)) {
                json.add(key, (JsonElement)csToJson(cs.getConfigurationSection(key)));
                continue;
            }
            if (cs.isList(key)) {
                JsonArray arr = new JsonArray();
                for (Object obj : cs.getList(key))
                    arr.add((JsonElement)objToJsonPrim(obj));
                continue;
            }
            json.add(key, (JsonElement)objToJsonPrim(cs.get(key)));
        }
        return json;
    }

    private static JsonPrimitive objToJsonPrim(Object obj) {
        if (obj instanceof Boolean)
            return new JsonPrimitive((Boolean)obj);
        if (obj instanceof Character)
            return new JsonPrimitive((Character)obj);
        if (obj instanceof Number)
            return new JsonPrimitive((Number)obj);
        if (obj instanceof String)
            return new JsonPrimitive((String)obj);
        throw new UnsupportedOperationException("BLEH");
    }
}