package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerHelper: 07.04.2020 16:21 by sebip
*/

public class PlayerHelper {
    private static final String PLAYER_INVENTORY_PRIMARY_KEY = "primary";

    private static final String PLAYER_INVENTORY_ARMOR_KEY = "armor";

    private static Method getOnlinePlayers;

    public static boolean newOnlinePlayersMethod = false;

    static {
        try {
            getOnlinePlayers = Bukkit.class.getMethod("getOnlinePlayers", new Class[0]);
            if (getOnlinePlayers.getReturnType() == Collection.class)
                newOnlinePlayersMethod = true;
        } catch (NoSuchMethodException ex) {
            SteelCore.logSevere("Failed to get getOnlinePlayers method!");
            ex.printStackTrace();
        }
    }

    public static void pushInventory(Player player) throws IOException {
        PlayerInventory inv = player.getInventory();
        File storage = new File(SteelDataFiles.PLAYER_INVENTORY_DIR.getFile(), player.getUniqueId() + ".yml");
        if (storage.exists()) {
            SteelCore.logVerbose("Inventory push requested for player " + player.getName() + ", but inventory was already present in persistent storage. Popping stored inventory first.");
            try {
                popInventory(player);
            } catch (InvalidConfigurationException ex) {
                throw new IOException(ex);
            }
        }
        YamlConfiguration yaml = new YamlConfiguration();
        yaml.set("primary", InventoryHelper.serializeInventory((Inventory)inv));
        yaml.set("armor", InventoryHelper.serializeInventory(inv.getArmorContents()));
        yaml.save(storage);
        inv.clear();
        inv.setArmorContents(new org.bukkit.inventory.ItemStack[(inv.getArmorContents()).length]);
        player.updateInventory();
    }

    public static void popInventory(Player player) throws IllegalArgumentException, IOException, InvalidConfigurationException {
        File storage = new File(SteelDataFiles.PLAYER_INVENTORY_DIR.getFile(), player.getUniqueId() + ".yml");
        if (!storage.exists())
            throw new IllegalArgumentException("Inventory pop requested for player " + player.getName() + ", but inventory was not present in persistent storage!");
        YamlConfiguration yaml = new YamlConfiguration();
        yaml.load(storage);
        if (!yaml.contains("primary"))
            throw new InvalidConfigurationException("Stored inventory is missing required section \"primary\"");
        player.getInventory().clear();
        player.getInventory().setContents(
                InventoryHelper.deserializeInventory(yaml.getConfigurationSection("primary")));
        if (yaml.contains("armor"))
            player.getInventory().setArmorContents(
                    InventoryHelper.deserializeInventory(yaml.getConfigurationSection("armor")));
        player.updateInventory();
        storage.delete();
    }

    public static void storeLocation(Player player) throws InvalidConfigurationException, IOException {
        CommonPlayerHelper.storeLocation(player.getUniqueId(),
                LocationHelper.convertLocation(player.getLocation()));
    }

    public static void popLocation(Player player) throws IllegalArgumentException, InvalidConfigurationException, IOException {
        Optional<Location3D> retLoc = CommonPlayerHelper.getReturnLocation(player.getUniqueId());
        if (!retLoc.isPresent())
            throw new IllegalArgumentException("Location of player " + player.getName() + " not present in persistent store");
        player.teleport(LocationHelper.convertLocation((Location3D)retLoc.get()));
    }

    public static Collection<? extends Player> getOnlinePlayers() {
        try {
            if (newOnlinePlayersMethod)
                return (Collection<? extends Player>)getOnlinePlayers.invoke(null, new Object[0]);
            return Arrays.asList((Player[])getOnlinePlayers.invoke(null, new Object[0]));
        } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException ex) {
            throw new RuntimeException("Failed to invoke getOnlinePlayers method!", ex);
        }
    }
}