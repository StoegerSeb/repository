package com.sap.sepp.api.steel.utils.compatibility;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CoreDataMigrationAgent: 07.04.2020 16:16 by sebip
*/

import com.sap.sepp.api.steel.SteelCore;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.List;

public class CoreDataMigrationAgent extends DataMigrationAgent {
    private static final String OFFLINE_PLAYER_LIST_KEY = "offline";

    public void migrateData() {
        migrateOfflinePlayerStore();
        migrateLocationStore();
    }

    protected File getOldDir() {
        return SteelDataFiles.CORE_OLD_DATA_DIR.getFile();
    }

    private void migrateOfflinePlayerStore() {
        File oldFile = SteelDataFiles.OLD_OFFLINE_PLAYER_STORE.getFile();
        if (oldFile.exists()) {
            SteelCore.logInfo("Detected old offline player store. Attempting to migrate...");
            File newFile = SteelDataFiles.OFFLINE_PLAYER_STORE.getFile();
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(oldFile);
                List<String> uuidList = yaml.getStringList("offline");
                JsonArray json = new JsonArray();
                for (String uuid : uuidList)
                    json.add((JsonElement)new JsonPrimitive(uuid));
                Files.deleteIfExists(newFile.toPath());
                Files.createFile(newFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
                try (FileWriter writer = new FileWriter(newFile)) {
                    writer.write(json.toString());
                }
            } catch (InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to migrate offline player store!");
                ex.printStackTrace();
            }
            relocateOldFile(oldFile.toPath());
        }
    }

    private void migrateLocationStore() {
        File oldFile = SteelDataFiles.OLD_PLAYER_LOCATION_STORE.getFile();
        if (oldFile.exists()) {
            SteelCore.logInfo("Detected old location store. Attempting to migrate...");
            File newFile = SteelDataFiles.PLAYER_LOCATION_STORE.getFile();
            try {
                YamlConfiguration yaml = new YamlConfiguration();
                yaml.load(oldFile);
                JsonObject json = new JsonObject();
                for (String key : yaml.getKeys(false))
                    json.add(key, (JsonElement)JsonSerializer.serializeLocation(Location3D.deserialize(yaml.getString(key))));
                Files.deleteIfExists(newFile.toPath());
                Files.createFile(newFile.toPath(), (FileAttribute<?>[])new FileAttribute[0]);
                try (FileWriter writer = new FileWriter(newFile)) {
                    writer.write(json.toString());
                }
            } catch (InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to migrate location store!");
                ex.printStackTrace();
            }
            relocateOldFile(oldFile.toPath());
        }
    }
}