package com.sap.sepp.api.minigame;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class MiniGame: 07.04.2020 17:11 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.eventbus.EventBus;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.component.ComponentOwner;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;

import java.util.UUID;

public interface Minigame extends ComponentOwner {
    String getPlugin();

    EventBus getEventBus();

    <T> T getConfigValue(ConfigNode<T> paramConfigNode);

    <T> void setConfigValue(ConfigNode<T> paramConfigNode, T paramT);

    ImmutableList<Arena> getArenas();

    Optional<Arena> getArena(String paramString);

    @Deprecated
    Arena createArena(String paramString1, String paramString2, Location3D paramLocation3D, Boundary paramBoundary) throws IllegalArgumentException;

    @Deprecated
    Arena createArena(String paramString, Location3D paramLocation3D, Boundary paramBoundary) throws IllegalArgumentException;

    void removeArena(String paramString) throws IllegalArgumentException;

    void removeArena(Arena paramArena) throws IllegalArgumentException;

    ImmutableList<Round> getRounds();

    ImmutableList<Challenger> getChallengers();

    Optional<Challenger> getChallenger(UUID paramUUID);

    <T extends com.sap.sepp.api.utils.builder.Buildable<U>, U extends com.sap.sepp.api.utils.builder.Builder<T>> U createBuilder(Class<T> paramClass);
}