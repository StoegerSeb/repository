package com.sap.sepp.api.steel.listener.rollback;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RollbackEntityListener: 07.04.2020 16:31 by sebip
*/

import com.sap.sepp.api.steel.SteelCore;
import com.sap.sepp.api.steel.utils.agent.rollback.RollbackAgent;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.ArrayList;
import java.util.List;

public class RollbackEntityListener implements Listener {
    private static final List<EntityType> SUPPORTED_TYPES = new ArrayList<>();

    static {
        SUPPORTED_TYPES.add(EntityType.ITEM_FRAME);
        SUPPORTED_TYPES.add(EntityType.LEASH_HITCH);
        SUPPORTED_TYPES.add(EntityType.PAINTING);
        try {
            SUPPORTED_TYPES.add(EntityType.ARMOR_STAND);
        } catch (NoSuchFieldError ex) {
            SteelCore.logVerbose("Server does not support 1.8 entities - not registering");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityExplode(EntityExplodeEvent event) {
        for (Block b : event.blockList())
            RollbackAgent.checkBlockChange(b, event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
        RollbackAgent.checkBlockChange(event.getBlock(), event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        handleEntityEvent(event.getEntity(), true, event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        handleEntityEvent(event.getEntity(), false, event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        handleEntityEvent(event.getRightClicked(), false, event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onHangingPlace(HangingPlaceEvent event) {
        handleEntityEvent(event.getEntity(), true, event);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onHangingBreak(HangingBreakEvent event) {
        handleEntityEvent(event.getEntity(), false, event);
    }

    public static void handleEntityEvent(Entity entity, boolean newlyCreated, Event event) {
        if (SUPPORTED_TYPES.contains(entity.getType()))
            RollbackAgent.checkEntityChange(entity, newlyCreated, event);
    }
}
