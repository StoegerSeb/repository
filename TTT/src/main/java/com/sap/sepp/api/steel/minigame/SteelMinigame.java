package com.sap.sepp.api.steel.minigame;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelMinigame: 07.04.2020 16:36 by sebip
*/

public class SteelMinigame extends CommonMinigame {
    private final Plugin plugin;

    private final IWizardManager wizardManager;

    public SteelMinigame(Plugin plugin) {
        assert plugin != null;
        this.plugin = plugin;
        if (!Bukkit.getPluginManager().isPluginEnabled(plugin))
            throw new IllegalArgumentException("Plugin \"" + plugin + "\" is not loaded!");
        SteelCore.logInfo(this.plugin + " has successfully hooked Steel");
        (new MinigameDataMigrationAgent((Minigame)this)).migrateData();
        this.wizardManager = (IWizardManager)new WizardManager((Minigame)this);
        SteelDataFiles.createMinigameDataFiles((Minigame)this);
        loadArenas();
        loadLobbySigns();
    }

    public String getPlugin() {
        return this.plugin.getName();
    }

    public Plugin getBukkitPlugin() {
        return this.plugin;
    }

    public IWizardManager getLobbyWizardManager() {
        return this.wizardManager;
    }

    protected int checkPhysicalLobbySign(Location3D loc) {
        if (loc.getWorld().isPresent()) {
            World w = Bukkit.getWorld((String)loc.getWorld().get());
            if (w != null) {
                Block block = w.getBlockAt(
                        (int)Math.floor(loc.getX()),
                        (int)Math.floor(loc.getY()),
                        (int)Math.floor(loc.getZ()));
                if (block.getState() instanceof org.bukkit.block.Sign)
                    return 0;
                SteelCore.logWarning("Found lobby sign with location not containing a sign block. Removing...");
                return 2;
            }
            SteelCore.logVerbose("Cannot load world \"" + (String)loc.getWorld().get() + "\" - not loading contained lobby sign");
            return 1;
        }
        SteelCore.logWarning("Found lobby sign in store with invalid location serial. Removing...");
        return 2;
    }
}
