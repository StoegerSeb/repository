package com.sap.sepp.api.common.event;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class FlintSubscriberExceptionHandler: 07.04.2020 16:48 by sebip
*/

import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.sap.sepp.api.common.CommonCore;

public class FlintSubscriberExceptionHandler implements SubscriberExceptionHandler {
    private static FlintSubscriberExceptionHandler INSTANCE;

    public static FlintSubscriberExceptionHandler getInstance() {
        if (INSTANCE == null)
            INSTANCE = new FlintSubscriberExceptionHandler();
        return INSTANCE;
    }

    public static void deinitialize() {
        INSTANCE = null;
    }

    public void handleException(Throwable exception, SubscriberExceptionContext context) {
        CommonCore.logSevere("Failed to dispatch event " + context.getEvent().getClass() + " to " + context.getSubscriberMethod());
        exception.printStackTrace();
    }
}
