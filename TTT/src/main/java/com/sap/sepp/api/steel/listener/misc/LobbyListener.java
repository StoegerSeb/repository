package com.sap.sepp.api.steel.listener.misc;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LobbyListener: 07.04.2020 16:29 by sebip
*/

import com.sap.sepp.api.utils.physical.Location3D;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class LobbyListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (event.getBlock().getState() instanceof org.bukkit.block.Sign) {
            Location3D loc = LocationHelper.convertLocation(event.getBlock().getLocation());
            for (Minigame mg : SteelCore.getMinigames().values()) {
                for (UnmodifiableIterator<Arena> unmodifiableIterator = mg.getArenas().iterator(); unmodifiableIterator.hasNext(); ) {
                    Arena arena = unmodifiableIterator.next();
                    if (arena.getLobbySignAt(loc).isPresent())
                        event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if ((event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK) &&
                event.getClickedBlock().getState() instanceof org.bukkit.block.Sign) {
            Location3D loc = LocationHelper.convertLocation(event.getClickedBlock().getLocation());
            for (Minigame mg : SteelCore.getMinigames().values()) {
                for (UnmodifiableIterator<Arena> unmodifiableIterator = mg.getArenas().iterator(); unmodifiableIterator.hasNext(); ) {
                    Arena arena = unmodifiableIterator.next();
                    if (arena.getLobbySignAt(loc).isPresent()) {
                        if (event.getAction() == Action.LEFT_CLICK_BLOCK && (event
                                .getPlayer().isSneaking() ||
                                !((Boolean)mg.getConfigValue(ConfigNode.REQUIRE_SNEAK_TO_DESTROY_LOBBY)).booleanValue()) && (
                                event.getPlayer().hasPermission(mg.getPlugin() + ".lobby.destroy") || event
                                        .getPlayer().hasPermission(mg.getPlugin() + ".lobby.*"))) {
                            ((LobbySign)arena.getLobbySignAt(loc).get()).unregister();
                            return;
                        }
                        mg.getEventBus().post(new CommonPlayerClickLobbySignEvent(event
                                .getPlayer().getUniqueId(), (LobbySign)arena
                                .getLobbySignAt(loc).get(),
                                (event.getAction() == Action.LEFT_CLICK_BLOCK) ? PlayerClickLobbySignEvent.ClickType.LEFT : PlayerClickLobbySignEvent.ClickType.RIGHT));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onSignChange(SignChangeEvent event) {
        for (Map.Entry<String, Minigame> e : (Iterable<Map.Entry<String, Minigame>>)SteelCore.getMinigames().entrySet()) {
            if (event.getLine(0).equalsIgnoreCase("[" + (String)e.getKey() + "]")) {
                if (((Boolean)((Minigame)e.getValue()).getConfigValue(ConfigNode.ENABLE_LOBBY_WIZARD)).booleanValue())
                    if (event.getPlayer().hasPermission((String)e.getKey() + ".lobby.create") || event
                            .getPlayer().hasPermission((String)e.getKey() + ".lobby.*")) {
                        IWizardManager wm = ((SteelMinigame)e.getValue()).getLobbyWizardManager();
                        if (!wm.hasPlayer(event.getPlayer().getUniqueId())) {
                            wm.addPlayer(event.getPlayer().getUniqueId(),
                                    LocationHelper.convertLocation(event.getBlock().getLocation()));
                        } else {
                            event.getPlayer().sendMessage(ChatColor.RED + "You are already in a lobby sign wizard");
                        }
                    } else {
                        event.getPlayer().sendMessage(ChatColor.RED + "You do not have permission to do this");
                    }
                return;
            }
        }
    }
}