package com.sap.sepp.api.steel.utils.agent.rollback.serialization;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class EntityStateSerializer: 07.04.2020 16:13 by sebip
*/

import com.sap.sepp.api.steel.SteelCore;
import org.bukkit.Art;
import org.bukkit.Rotation;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;

public class EntityStateSerializer {
    private static final String PITCH = "pitch";

    private static final String YAW = "yaw";

    private static final String ARMOR_STAND_HELMET = "stand.helmet";

    private static final String ARMOR_STAND_CHESTPLATE = "stand.chestplate";

    private static final String ARMOR_STAND_LEGGINGS = "stand.leggings";

    private static final String ARMOR_STAND_BOOTS = "stand.boots";

    private static final String ARMOR_STAND_HAND = "stand.hand";

    private static final String ARMOR_STAND_POSE_HEAD = "stand.pose.head";

    private static final String ARMOR_STAND_POSE_BODY = "stand.pose.body";

    private static final String ARMOR_STAND_POSE_ARM_LEFT = "stand.pose.arm_left";

    private static final String ARMOR_STAND_POSE_ARM_RIGHT = "stand.pose.arm.right";

    private static final String ARMOR_STAND_POSE_LEG_LEFT = "stand.pose.leg.left";

    private static final String ARMOR_STAND_POSE_LEG_RIGHT = "stand.pose.leg.right";

    private static final String ARMOR_STAND_ARMS = "stand.arms";

    private static final String ARMOR_STAND_BASE_PLATE = "stand.base_plate";

    private static final String ARMOR_STAND_GRAVITY = "stand.gravity";

    private static final String ARMOR_STAND_SMALL = "stand.small";

    private static final String ARMOR_STAND_VISIBLE = "stand.visible";

    private static final String HANGING_FACING = "facing";

    private static final String ITEM_FRAME_ITEM = "item";

    private static final String ITEM_FRAME_ROTATION = "rotation";

    private static final String PAINTING_ART = "art";

    public static String serializeState(Entity entity) {
        YamlConfiguration yaml = new YamlConfiguration();
        if (entity instanceof ArmorStand) {
            EulerAngleSerializer eas = EulerAngleSerializer.getInstance();
            ArmorStand stand = (ArmorStand)entity;
            yaml.set("pitch", Float.valueOf(stand.getLocation().getPitch()));
            yaml.set("yaw", Float.valueOf(stand.getLocation().getYaw()));
            yaml.set("stand.helmet", stand.getHelmet());
            yaml.set("stand.chestplate", stand.getChestplate());
            yaml.set("stand.leggings", stand.getLeggings());
            yaml.set("stand.boots", stand.getBoots());
            yaml.set("stand.hand", stand.getItemInHand());
            yaml.set("stand.pose.head", eas.serialize(stand.getHeadPose()));
            yaml.set("stand.pose.body", eas.serialize(stand.getBodyPose()));
            yaml.set("stand.pose.arm_left", eas.serialize(stand.getLeftArmPose()));
            yaml.set("stand.pose.arm.right", eas.serialize(stand.getRightArmPose()));
            yaml.set("stand.pose.leg.left", eas.serialize(stand.getLeftLegPose()));
            yaml.set("stand.pose.leg.right", eas.serialize(stand.getRightLegPose()));
            yaml.set("stand.arms", Boolean.valueOf(stand.hasArms()));
            yaml.set("stand.base_plate", Boolean.valueOf(stand.hasBasePlate()));
            yaml.set("stand.gravity", Boolean.valueOf(stand.hasGravity()));
            yaml.set("stand.small", Boolean.valueOf(stand.isSmall()));
            yaml.set("stand.visible", Boolean.valueOf(stand.isVisible()));
        } else if (entity instanceof Hanging) {
            yaml.set("facing", ((Hanging)entity).getFacing().name());
            if (entity instanceof ItemFrame) {
                yaml.set("item", ((ItemFrame)entity).getItem());
                yaml.set("rotation", ((ItemFrame)entity).getRotation().name());
            } else if (entity instanceof Painting) {
                yaml.set("art", ((Painting)entity).getArt().name());
            }
        }
        return yaml.saveToString();
    }

    public static void deserializeState(Entity entity, String serial) throws InvalidConfigurationException {
        YamlConfiguration yaml = new YamlConfiguration();
        yaml.loadFromString(serial);
        if (entity instanceof ArmorStand) {
            EulerAngleSerializer eas = EulerAngleSerializer.getInstance();
            ArmorStand stand = (ArmorStand)entity;
            stand.setHelmet(yaml.getItemStack("stand.helmet"));
            stand.setChestplate(yaml.getItemStack("stand.chestplate"));
            stand.setLeggings(yaml.getItemStack("stand.leggings"));
            stand.setBoots(yaml.getItemStack("stand.boots"));
            stand.setItemInHand(yaml.getItemStack("stand.hand"));
            stand.setHeadPose(eas.deserialize(yaml.getString("stand.pose.head")));
            stand.setBodyPose(eas.deserialize(yaml.getString("stand.pose.body")));
            stand.setLeftArmPose(eas.deserialize(yaml.getString("stand.pose.arm_left")));
            stand.setRightArmPose(eas.deserialize(yaml.getString("stand.pose.arm.right")));
            stand.setLeftLegPose(eas.deserialize(yaml.getString("stand.pose.leg.left")));
            stand.setRightLegPose(eas.deserialize(yaml.getString("stand.pose.leg.right")));
            stand.setArms(yaml.getBoolean("stand.arms"));
            stand.setBasePlate(yaml.getBoolean("stand.base_plate"));
            stand.setGravity(yaml.getBoolean("stand.gravity"));
            stand.setSmall(yaml.getBoolean("stand.small"));
            stand.setVisible(yaml.getBoolean("stand.visible"));
        } else if (entity instanceof Hanging) {
            try {
                BlockFace facing = BlockFace.valueOf(yaml.getString("facing"));
                ((Hanging)entity).setFacingDirection(facing, true);
            } catch (IllegalArgumentException ex) {
                SteelCore.logVerbose("Invalid serialized BlockFace value for hanging entity with UUID " + entity
                        .getUniqueId().toString());
            }
            if (entity instanceof ItemFrame) {
                ((ItemFrame)entity).setItem(yaml.getItemStack("item"));
                try {
                    Rotation rotation = Rotation.valueOf(yaml.getString("rotation"));
                    ((ItemFrame)entity).setRotation(rotation);
                } catch (IllegalArgumentException ex) {
                    SteelCore.logVerbose("Invalid serialized Rotation value for item frame with UUID " + entity
                            .getUniqueId().toString());
                }
            } else if (entity instanceof Painting) {
                try {
                    Art art = Art.valueOf(yaml.getString("art"));
                    ((Painting)entity).setArt(art, true);
                } catch (IllegalArgumentException ex) {
                    SteelCore.logVerbose("Invalid serialized Art value for item frame with UUID " + entity
                            .getUniqueId().toString());
                }
            }
        }
    }
}
