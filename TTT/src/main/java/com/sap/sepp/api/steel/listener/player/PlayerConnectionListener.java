package com.sap.sepp.api.steel.listener.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerConnectionListener: 07.04.2020 16:30 by sebip
*/

public class PlayerConnectionListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Optional<Challenger> ch = CommonCore.getChallenger(event.getPlayer().getUniqueId());
        if (ch.isPresent()) {
            ((SteelRound)((Challenger)ch.get()).getRound()).removeChallenger((Challenger)ch.get(), true, true);
            CommonPlayerHelper.setOfflineFlag(event.getPlayer().getUniqueId());
        }
        for (Minigame mg : CommonCore.getMinigames().values()) {
            if (((SteelMinigame)mg).getLobbyWizardManager().hasPlayer(event.getPlayer().getUniqueId())) {
                ((SteelMinigame)mg).getLobbyWizardManager().removePlayer(event.getPlayer().getUniqueId());
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!SteelCore.SPECTATOR_SUPPORT)
            for (Minigame mg : SteelCore.getMinigames().values()) {
                for (UnmodifiableIterator<Challenger> unmodifiableIterator = mg.getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
                    Challenger ch = unmodifiableIterator.next();
                    if (ch.isSpectating())
                        ((SteelChallenger)ch).tryHide(Bukkit.getPlayer(ch.getUniqueId()), event.getPlayer());
                }
            }
        tryReset(event.getPlayer());
        try {
            PlayerHelper.popInventory(event.getPlayer());
        } catch (IllegalArgumentException illegalArgumentException) {

        } catch (InvalidConfigurationException|java.io.IOException ex) {
            SteelCore.logSevere("Failed to pop inventory for player " + event.getPlayer().getName());
            ex.printStackTrace();
        }
        try {
            PlayerHelper.popLocation(event.getPlayer());
        } catch (IllegalArgumentException illegalArgumentException) {

        } catch (InvalidConfigurationException|java.io.IOException ex) {
            SteelCore.logSevere("Failed to pop location for player " + event.getPlayer().getName());
            ex.printStackTrace();
        }
    }

    private void tryReset(Player player) {
        if (CommonPlayerHelper.checkOfflineFlag(player.getUniqueId())) {
            try {
                PlayerHelper.popInventory(player);
            } catch (IllegalArgumentException|InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to pop inventory for player " + player.getName());
                ex.printStackTrace();
            }
            try {
                PlayerHelper.popLocation(player);
            } catch (IllegalArgumentException|InvalidConfigurationException|java.io.IOException ex) {
                SteelCore.logSevere("Failed to pop location for player " + player.getName());
                ex.printStackTrace();
            }
        }
    }
}