package com.sap.sepp.api.steel.utils.unsafe;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelUnsafeUtil: 07.04.2020 16:22 by sebip
*/

public class SteelUnsafeUtil extends CommonUnsafeUtil {
    private static final LobbySignPopulator STATUS_POPULATOR = (LobbySignPopulator)new RichStockStatusLobbySignPopulator();

    private static final LobbySignPopulator LISTING_POPULATOR = (LobbySignPopulator)new StockChallengerListingLobbySignPopulator();

    public static void initialize() {
        INSTANCE = (UnsafeUtil)new SteelUnsafeUtil();
    }

    public LobbySignPopulator getDefaultStatusLobbySignPopulator() {
        testInternalUse();
        return STATUS_POPULATOR;
    }

    public LobbySignPopulator getDefaultChallengerListingLobbySignPopulator() {
        testInternalUse();
        return LISTING_POPULATOR;
    }
}
