package com.sap.sepp.api.common.lobby.populator;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class FunctionalLobbySignPopulator: 07.04.2020 16:52 by sebip
*/

import com.google.common.base.Function;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.minigame.Minigame;

public class FunctionalLobbySignPopulator implements LobbySignPopulator {
    private final Function<LobbySign, String> first;

    private final Function<LobbySign, String> second;

    private final Function<LobbySign, String> third;

    private final Function<LobbySign, String> fourth;

    private FunctionalLobbySignPopulator(Function<LobbySign, String> first, Function<LobbySign, String> second, Function<LobbySign, String> third, Function<LobbySign, String> fourth) {
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }

    public String first(LobbySign sign) {
        return (String)this.first.apply(sign);
    }

    public String second(LobbySign sign) {
        return (String)this.second.apply(sign);
    }

    public String third(LobbySign sign) {
        return (String)this.third.apply(sign);
    }

    public String fourth(LobbySign sign) {
        return (String)this.fourth.apply(sign);
    }

    public static class Builder implements LobbySignPopulator.Builder {
        private static final Function<LobbySign, String> NOOP = new Function<LobbySign, String>() {
            public String apply(LobbySign lobbySign) {
                return "";
            }
        };

        private Function<LobbySign, String> first;

        private Function<LobbySign, String> second;

        private Function<LobbySign, String> third;

        private Function<LobbySign, String> fourth;

        public Builder(Minigame mg) {}

        public LobbySignPopulator.Builder first(Function<LobbySign, String> function) {
            this.first = function;
            return this;
        }

        public LobbySignPopulator.Builder second(Function<LobbySign, String> function) {
            this.second = function;
            return this;
        }

        public LobbySignPopulator.Builder third(Function<LobbySign, String> function) {
            this.third = function;
            return this;
        }

        public LobbySignPopulator.Builder fourth(Function<LobbySign, String> function) {
            this.fourth = function;
            return this;
        }

        public LobbySignPopulator build() {
            return new FunctionalLobbySignPopulator((this.first != null) ? this.first : NOOP, (this.second != null) ? this.second : NOOP, (this.third != null) ? this.third : NOOP, (this.fourth != null) ? this.fourth : NOOP);
        }
    }
}
