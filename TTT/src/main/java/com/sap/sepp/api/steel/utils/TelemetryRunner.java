package com.sap.sepp.api.steel.utils;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class TelemetryRunner: 07.04.2020 16:09 by sebip
*/

import com.sap.sepp.api.steel.SteelCore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.UUID;

public class TelemetryRunner implements Runnable {
    private static final String KEY_UUID = "uuid";

    private static final String KEY_VERSION = "version";

    private static final String KEY_API_LEVEL = "api";

    private static final String KEY_JAVA_VERSION = "java";

    private static final String KEY_MINIGAME_COUNT = "mgCount";

    private static final String KEY_MINIGAMES = "minigames";

    private static final String TELEMETRY_SERVER = "https://telemetry.caseif.net/steel.php";

    private final JTelemetry jt = new JTelemetry("https://telemetry.caseif.net/steel.php");

    public void run() {
        UUID uuid;
        JTelemetry.Payload payload = this.jt.createPayload();
        try {
            uuid = getUuid();
        } catch (IOException ex) {
            SteelCore.logSevere("Encountered IOException while getting telemetry UUID - not submitting data");
            ex.printStackTrace();
            return;
        }
        payload.addData("uuid", uuid.toString());
        payload.addData("version", SteelMain.getInstance().getDescription().getVersion());
        payload.addData("api", SteelCore.getApiRevision());
        payload.addData("java", System.getProperty("java.version"));
        payload.addData("mgCount", SteelCore.getMinigames().size());
        String[] plugins = new String[SteelCore.getMinigames().size()];
        int i = 0;
        for (String plugin : SteelCore.getMinigames().keySet()) {
            plugins[i] = plugin;
            i++;
        }
        payload.addData("minigames", plugins);
        try {
            JTelemetry.HttpResponse response = payload.submit();
            if (response.getStatusCode() / 100 != 2)
                SteelCore.logWarning("Telemetry server responded with non-success status code (" + response
                        .getStatusCode() + " " + response.getMessage() + "). Please report this.");
        } catch (IOException ex) {
            SteelCore.logSevere("Encountered IOException while submitting telemetry data to remote server");
            ex.printStackTrace();
        }
    }

    private static UUID getUuid() throws IOException {
        File uuidFile = SteelDataFiles.TELEMETRY_UUID_STORE.getFile();
        if (!uuidFile.exists())
            uuidFile.createNewFile();
        try (BufferedReader reader = new BufferedReader(new FileReader(uuidFile))) {
            String uuid = reader.readLine();
        }
    }
}