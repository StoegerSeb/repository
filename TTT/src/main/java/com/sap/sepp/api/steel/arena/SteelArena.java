package com.sap.sepp.api.steel.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelArena: 07.04.2020 16:28 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.minigame.CommonMinigame;
import com.sap.sepp.api.exceptions.rollback.RollbackException;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.steel.SteelCore;
import com.sap.sepp.api.steel.lobby.SteelLobbySign;
import com.sap.sepp.api.steel.lobby.type.SteelChallengerListingLobbySign;
import com.sap.sepp.api.steel.lobby.type.SteelStatusLobbySign;
import com.sap.sepp.api.steel.utils.agent.rollback.RollbackAgent;
import com.sap.sepp.api.steel.utils.helper.LocationHelper;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.io.IOException;

public class SteelArena extends CommonArena {
    public SteelArena(CommonMinigame parent, String id, String name, Location3D[] spawnPoints, Boundary boundary) {
        super(parent, id.toLowerCase(), name, spawnPoints, boundary);
    }

    public Optional createStatusLobbySign(Location3D location) throws IllegalArgumentException {
        if (checkLocationForLobbySign(location))
            return storeAndWrap(new SteelStatusLobbySign(location, this));
        return Optional.absent();
    }

    public Optional createChallengerListingLobbySign(Location3D location, int index) {
        if (checkLocationForLobbySign(location))
            return (Optional)storeAndWrap(new SteelChallengerListingLobbySign(location, this, index));
        return Optional.absent();
    }

    private Object storeAndWrap(SteelChallengerListingLobbySign steelChallengerListingLobbySign) {
        return null;
    }

    public void markForRollback(Location3D location) throws IllegalArgumentException, RollbackException {
        Preconditions.checkArgument(getBoundary().contains(location), "Cannot mark block for rollback in arena " +
                getId() + " - not within boundary");
        try {
            Location loc = LocationHelper.convertLocation(location);
            getRollbackAgent().logBlockChange(loc.getBlock());
        } catch (IOException |java.sql.SQLException ex) {
            throw new RollbackException(ex);
        }
    }

    public RollbackAgent getRollbackAgent() {
        return (RollbackAgent)super.getRollbackAgent();
    }

    private boolean checkLocationForLobbySign(Location3D location) throws IllegalArgumentException {
        Preconditions.checkArgument(location.getWorld().isPresent(), "Location for lobby sign must contain world");
        World world = Bukkit.getWorld(location.getWorld().get());
        if (world == null)
            throw new IllegalArgumentException("Invalid world for lobby sign location");
        Block block = LocationHelper.convertLocation(location).getBlock();
        if (!getLobbySignMap().containsKey(location)) {
            if (!(block.getState() instanceof org.bukkit.block.Sign))
                if (SteelCore.getMcVersion() >= 1014000) {
                    block.setType(Material.OAK_SIGN);
                } else {
                    block.setType(Material.valueOf("SIGN"));
                }
            return true;
        }
        return false;
    }

    private <T extends LobbySign> Optional<T> storeAndWrap(SteelStatusLobbySign sign) {
        sign.store();
        getLobbySignMap().put(sign.getLocation(), sign);
        return (Optional<T>) Optional.of(sign);
    }
}
