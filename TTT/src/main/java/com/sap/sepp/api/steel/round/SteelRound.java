package com.sap.sepp.api.steel.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelRound: 07.04.2020 16:36 by sebip
*/

public class SteelRound extends CommonRound {
    private final int schedulerHandle;

    public SteelRound(CommonArena arena, ImmutableSet<LifecycleStage> stages) {
        super(arena, stages);
        this.schedulerHandle = Bukkit.getScheduler().scheduleSyncRepeatingTask(((SteelMinigame)
                getArena().getMinigame()).getBukkitPlugin(), (Runnable)new SteelRoundWorker(this), 0L, 20L);
        try {
            arena.getRollbackAgent().createRollbackDatabase();
        } catch (IOException|java.sql.SQLException ex) {
            throw new RuntimeException("Failed to create rollback store", ex);
        }
    }

    public JoinResult addChallenger(UUID uuid) throws IllegalStateException, OrphanedComponentException {
        checkState();
        try {
            Player bukkitPlayer = Bukkit.getPlayer(uuid);
            if (bukkitPlayer == null)
                return (JoinResult)new CommonJoinResult(JoinResult.Status.PLAYER_OFFLINE);
            if (getChallengers().size() >= ((Integer)getConfigValue(ConfigNode.MAX_PLAYERS)).intValue())
                return (JoinResult)new CommonJoinResult(JoinResult.Status.ROUND_FULL);
            if (CommonCore.getChallenger(uuid).isPresent())
                return (JoinResult)new CommonJoinResult(JoinResult.Status.ALREADY_IN_ROUND);
            Location spawn = LocationHelper.convertLocation(nextSpawnPoint());
            SteelChallenger challenger = new SteelChallenger(uuid, this);
            try {
                PlayerHelper.storeLocation(bukkitPlayer);
            } catch (IllegalArgumentException|InvalidConfigurationException|IOException ex) {
                return (JoinResult)new CommonJoinResult(ex);
            }
            bukkitPlayer.teleport(spawn);
            getChallengerMap().put(uuid, challenger);
            for (UnmodifiableIterator<LobbySign> unmodifiableIterator = getArena().getLobbySigns().iterator(); unmodifiableIterator.hasNext(); ) {
                LobbySign sign = unmodifiableIterator.next();
                sign.update();
            }
            try {
                PlayerHelper.pushInventory(bukkitPlayer);
            } catch (IOException ex) {
                return (JoinResult)new CommonJoinResult(ex);
            }
            getArena().getMinigame().getEventBus().post(new CommonChallengerJoinRoundEvent((Challenger)challenger));
            return (JoinResult)new CommonJoinResult((Challenger)challenger);
        } catch (Throwable ex) {
            return (JoinResult)new CommonJoinResult(ex);
        }
    }

    public void removeChallenger(Challenger challenger, boolean isDisconnecting, boolean updateSigns) throws OrphanedComponentException {
        super.removeChallenger(challenger, isDisconnecting, updateSigns);
        Player bukkitPlayer = Bukkit.getPlayer(challenger.getUniqueId());
        Optional<Location3D> returnPoint = null;
        try {
            returnPoint = CommonPlayerHelper.getReturnLocation(bukkitPlayer.getUniqueId());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (returnPoint == null)
            returnPoint = Optional.of(LocationHelper.convertLocation(((World)Bukkit.getWorlds().get(0)).getSpawnLocation()));
        if (!isDisconnecting)
            try {
                PlayerHelper.popInventory(bukkitPlayer);
            } catch (InvalidConfigurationException|IOException ex) {
                (new RuntimeException("Could not pop inventory for player " + bukkitPlayer.getName() + " from persistent storage", ex))
                        .printStackTrace();
            }
        CommonChallengerLeaveRoundEvent event = new CommonChallengerLeaveRoundEvent(challenger, (Location3D)returnPoint.get());
        getArena().getMinigame().getEventBus().post(event);
        if (!challenger.getRound().isEnding())
            ((CommonChallenger)challenger).orphan();
        if (!event.getReturnLocation().equals(returnPoint))
            try {
                CommonPlayerHelper.storeLocation(bukkitPlayer.getUniqueId(), event.getReturnLocation());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        if (!isDisconnecting)
            try {
                PlayerHelper.popLocation(bukkitPlayer);
            } catch (IllegalArgumentException|InvalidConfigurationException|IOException ex) {
                SteelCore.logSevere("Could not pop location for player " + bukkitPlayer.getName() + " from persistent storage - defaulting to world spawn");
                ex.printStackTrace();
                bukkitPlayer.teleport(((World)Bukkit.getWorlds().get(0)).getSpawnLocation());
            }
    }

    public Location3D nextSpawnPoint() {
        if (getConfigValue(ConfigNode.SPAWNING_MODE) == SpawningMode.PROXIMITY_HIGH) {
            List<Location3D> candidates = new ArrayList<>();
            double greatestMean = 0.0D;
            if (getChallengers().size() == 0)
                return (Location3D)getArena().getSpawnPoints()
                        .get(Integer.valueOf((int)Math.floor(Math.random() * getArena().getSpawnPoints().size())));
            for (UnmodifiableIterator<Location3D> unmodifiableIterator = getArena().getSpawnPoints().values().iterator(); unmodifiableIterator.hasNext(); ) {
                Location3D loc = unmodifiableIterator.next();
                Location bukkitLoc = LocationHelper.convertLocation(loc);
                int sum = 0;
                for (UnmodifiableIterator<Challenger> unmodifiableIterator1 = getChallengers().iterator(); unmodifiableIterator1.hasNext(); ) {
                    Challenger ch = unmodifiableIterator1.next();
                    sum = (int)(sum + Bukkit.getPlayer(ch.getUniqueId()).getLocation().distance(bukkitLoc));
                }
                double mean = (sum / getChallengers().size());
                if (mean > greatestMean) {
                    candidates = Collections.singletonList(loc);
                    greatestMean = mean;
                    continue;
                }
                if (mean == greatestMean)
                    candidates.add(loc);
            }
            return candidates.get((int)Math.floor(Math.random() * candidates.size()));
        }
        return super.nextSpawnPoint();
    }

    public void broadcast(String message) {
        checkState();
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger c = unmodifiableIterator.next();
            Bukkit.getPlayer(c.getUniqueId()).sendMessage(message);
        }
    }

    public void cancelTimerTask() {
        Bukkit.getScheduler().cancelTask(this.schedulerHandle);
    }

    public boolean isOrphaned() {
        return this.orphan;
    }
}