package com.sap.sepp.api.common.arena;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonArena: 07.04.2020 16:46 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import com.google.common.eventbus.Subscribe;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.event.internal.metadata.PersistableMetadataMutateEvent;
import com.sap.sepp.api.common.lobby.CommonLobbySign;
import com.sap.sepp.api.common.metadata.CommonMetadata;
import com.sap.sepp.api.common.metadata.persist.CommonPersistentMetadataHolder;
import com.sap.sepp.api.common.minigame.CommonMinigame;
import com.sap.sepp.api.common.utils.agent.rollback.IRollbackAgent;
import com.sap.sepp.api.common.utils.factory.FactoryRegistry;
import com.sap.sepp.api.common.utils.factory.IArenaFactory;
import com.sap.sepp.api.common.utils.factory.IRollbackAgentFactory;
import com.sap.sepp.api.common.utils.factory.IRoundFactory;
import com.sap.sepp.api.common.utils.file.CommonDataFiles;
import com.sap.sepp.api.common.utils.helper.JsonHelper;
import com.sap.sepp.api.common.utils.helper.JsonSerializer;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.round.LifecycleStage;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Boundary;
import com.sap.sepp.api.utils.physical.Location3D;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommonArena extends CommonPersistentMetadataHolder implements Arena, CommonComponent<Minigame> {
    public static final String PERSISTENCE_NAME_KEY = "name";

    public static final String PERSISTENCE_WORLD_KEY = "world";

    public static final String PERSISTENCE_SPAWNS_KEY = "spawns";

    public static final String PERSISTENCE_BOUNDS_UPPER_KEY = "bound.upper";

    public static final String PERSISTENCE_BOUNDS_LOWER_KEY = "bound.lower";

    public static final String PERSISTENCE_METADATA_KEY = "metadata";

    private IRollbackAgent rbHelper;

    private final CommonMinigame parent;

    private final String id;

    private String name;

    private final String world;

    private final HashMap<Integer, Location3D> spawns = new HashMap<>();

    private final List<Location3D> shuffledSpawns;

    private final HashMap<Location3D, LobbySign> lobbies = new HashMap<>();

    private Boundary boundary;

    private boolean orphan = false;

    protected CommonArena(CommonMinigame parent, String id, String name, Location3D[] spawnPoints, Boundary boundary) throws IllegalArgumentException {
        assert parent != null;
        assert id != null;
        assert name != null;
        Preconditions.checkArgument((spawnPoints != null && spawnPoints.length > 0), "Initial spawn for arena \"" + id + "\" must not be null or empty");
        Preconditions.checkArgument(!parent.getArenaMap().containsKey(id), "Cannot create arena: arena with ID \"" + id + "\" already exists");
        String world = null;
        for (Location3D spawn : spawnPoints) {
            if (spawn.getWorld().isPresent())
                if (world != null) {
                    Preconditions.checkArgument(spawn.getWorld().get().equals(world), "Spawn points must not have different worlds");
                } else {
                    world = spawn.getWorld().get();
                }
        }
        Preconditions.checkArgument(spawnPoints[0].getWorld().isPresent(), "At least one spawn point for arena \"" + id + "\" must define a world");
        for (Location3D spawn : spawnPoints)
            Preconditions.checkArgument(boundary.contains(spawn), "Spawn points must be within arena boundary");
        if (!boundary.getLowerBound().getWorld().isPresent() && !boundary.getUpperBound().getWorld().isPresent()) {
            Location3D newLower = new Location3D(world, boundary.getLowerBound().getX(), boundary.getLowerBound().getY(), boundary.getLowerBound().getZ());
            boundary = new Boundary(newLower, boundary.getUpperBound());
        }
        this.parent = parent;
        this.id = id;
        this.name = name;
        this.world = world;
        for (int i = 0; i < spawnPoints.length; i++)
            this.spawns.put(i, spawnPoints[i]);
        this.shuffledSpawns = Lists.newArrayList((Object[])spawnPoints);
        Collections.shuffle(this.shuffledSpawns);
        this.boundary = boundary;
        this
                .rbHelper = ((IRollbackAgentFactory) FactoryRegistry.getFactory(IRollbackAgent.class)).createRollbackAgent(this);
        CommonMetadata.getEventBus().register(this);
        parent.getArenaMap().put(id.toLowerCase(), this);
    }

    public Minigame getOwner() throws OrphanedComponentException {
        checkState();
        return (Minigame)this.parent;
    }

    public Minigame getMinigame() throws OrphanedComponentException {
        return getOwner();
    }

    public String getId() throws OrphanedComponentException {
        checkState();
        return this.id;
    }

    public String getName() throws OrphanedComponentException {
        return getDisplayName();
    }

    public String getDisplayName() throws OrphanedComponentException {
        checkState();
        return this.name;
    }

    public void setDisplayName(String displayName) throws OrphanedComponentException {
        checkState();
        this.name = displayName;
    }

    public String getWorld() throws OrphanedComponentException {
        checkState();
        return this.world;
    }

    public Boundary getBoundary() throws OrphanedComponentException {
        checkState();
        return this.boundary;
    }

    public void setBoundary(Boundary bound) throws OrphanedComponentException {
        checkState();
        this.boundary = bound;
        try {
            store();
        } catch (Exception ex) {
            ex.printStackTrace();
            CommonCore.logSevere("Failed to save arena with ID " + getId() + " to persistent storage");
        }
    }

    public ImmutableMap<Integer, Location3D> getSpawnPoints() throws OrphanedComponentException {
        checkState();
        return ImmutableMap.copyOf(this.spawns);
    }

    public int addSpawnPoint(Location3D spawn) throws OrphanedComponentException {
        checkState();
        Preconditions.checkArgument(getBoundary().contains(spawn), "Spawn point must be within arena boundary");
        for (int id = 0; id <= this.spawns.size(); id++) {
            if (!this.spawns.containsKey(id)) {
                Location3D spawnLoc = new Location3D(this.world, spawn.getX(), spawn.getY(), spawn.getZ());
                this.spawns.put(id, spawnLoc);
                this.shuffledSpawns.add(spawnLoc);
                Collections.shuffle(this.shuffledSpawns);
                try {
                    store();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    CommonCore.logSevere("Failed to save arena with ID " + getId() + " to persistent storage");
                }
                return id;
            }
        }
        throw new AssertionError("Logic error: could not get next available spawn. Report this immediately.");
    }

    public void removeSpawnPoint(int index) throws OrphanedComponentException {
        checkState();
        Preconditions.checkArgument(this.spawns.containsKey(index), "Cannot remove spawn: none exists with given index");
        Location3D removedSpawn = this.spawns.remove(index);
        this.shuffledSpawns.remove(removedSpawn);
        Collections.shuffle(this.shuffledSpawns);
        try {
            store();
        } catch (Exception ex) {
            ex.printStackTrace();
            CommonCore.logSevere("Failed to save arena with ID " + getId() + " to persistent storage");
        }
    }

    public void removeSpawnPoint(Location3D location) throws OrphanedComponentException {
        checkState();
        Preconditions.checkArgument((!location.getWorld().isPresent() || location.getWorld().get().equals(this.world)), "Cannot remove spawn: world mismatch in provided location");
        Location3D loc = new Location3D(this.world, location.getX(), location.getY(), location.getZ());
        for (Map.Entry<Integer, Location3D> e : this.spawns.entrySet()) {
            if (e.getValue().equals(loc)) {
                removeSpawnPoint(e.getKey());
                return;
            }
        }
        throw new IllegalArgumentException("Cannot remove spawn: none exists at given location");
    }

    public Optional<Round> getRound() throws OrphanedComponentException {
        checkState();
        return Optional.fromNullable(this.parent.getRoundMap().get(this));
    }

    public Round createRound() throws IllegalStateException {
        checkState();
        Preconditions.checkState(!getRound().isPresent(), "Cannot create a round in an arena already hosting one");
        ImmutableSet<LifecycleStage> stages = getMinigame().getConfigValue(ConfigNode.DEFAULT_LIFECYCLE_STAGES);
        Preconditions.checkState((stages != null && !stages.isEmpty()), "Illegal call to nullary createRound method: default lifecycle stages are not set");
        return createRound((ImmutableSet<LifecycleStage>)getMinigame().getConfigValue(ConfigNode.DEFAULT_LIFECYCLE_STAGES));
    }

    public Round createRound(ImmutableSet<LifecycleStage> stages) throws IllegalArgumentException, IllegalStateException {
        checkState();
        Preconditions.checkState(!getRound().isPresent(), "Cannot create a round in an arena already hosting one");
        Preconditions.checkArgument((stages != null && !stages.isEmpty()), "LifecycleStage set must not be null or empty");
        ((CommonMinigame)getMinigame()).getRoundMap()
                .put(this, ((IRoundFactory)FactoryRegistry.getFactory(Round.class)).createRound(this, stages));
        Preconditions.checkState(getRound().isPresent(), "Cannot get created round from arena! This is a bug.");
        return getRound().get();
    }

    public Round getOrCreateRound() {
        return getRound().isPresent() ? getRound().get() : createRound();
    }

    public Round getOrCreateRound(ImmutableSet<LifecycleStage> stages) {
        return getRound().isPresent() ? getRound().get() : createRound(stages);
    }

    @Subscribe
    public void onMetadataMutate(PersistableMetadataMutateEvent event) {
        if (event.getMetadata() == getMetadata())
            try {
                store();
            } catch (Exception ex) {
                ex.printStackTrace();
                CommonCore.logSevere("Failed to save arena with ID " + getId() + " to persistent storage");
            }
    }

    public ImmutableList<LobbySign> getLobbySigns() {
        return ImmutableList.copyOf(this.lobbies.values());
    }

    public Optional<LobbySign> getLobbySignAt(Location3D location) throws IllegalArgumentException {
        return Optional.fromNullable(this.lobbies.get(location));
    }

    public void rollback() throws IllegalStateException {
        checkState();
        getRollbackAgent().popRollbacks();
    }

    public IRollbackAgent getRollbackAgent() {
        return this.rbHelper;
    }

    public HashMap<Location3D, LobbySign> getLobbySignMap() {
        return this.lobbies;
    }

    public HashMap<Integer, Location3D> getSpawnPointMap() {
        return this.spawns;
    }

    public ImmutableList<Location3D> getShuffledSpawnPoints() throws OrphanedComponentException {
        checkState();
        return ImmutableList.copyOf(this.shuffledSpawns);
    }

    public void unregisterLobbySign(Location3D location) {
        CommonLobbySign sign = (CommonLobbySign)this.lobbies.get(location);
        this.lobbies.remove(location);
        sign.unstore();
    }

    public void checkState() throws OrphanedComponentException {
        if (this.orphan)
            throw new OrphanedComponentException(this);
    }

    public void orphan() {
        CommonCore.orphan(this);
    }

    public void setOrphanFlag() {
        this.orphan = true;
    }

    public void configure(JsonObject json) {
        JsonObject spawnSection = json.getAsJsonObject("spawns");
        for (Map.Entry<String, JsonElement> entry : spawnSection.entrySet()) {
            try {
                int index = Integer.parseInt(entry.getKey());
                getSpawnPointMap()
                        .put(index, JsonSerializer.deserializeLocation(spawnSection.getAsJsonObject(entry.getKey())));
            } catch (IllegalArgumentException ignored) {
                CommonCore.logWarning("Invalid spawn at index " + entry.getKey() + " for arena \"" + getId() + "\"");
            }
        }
        if (json.has("metadata") && json.get("metadata").isJsonObject())
            JsonSerializer.deserializeMetadata(json.getAsJsonObject("metadata"), getPersistentMetadata());
    }

    public void store() throws IOException {
        CommonCore.logInfo("Storing");
        File arenaStore = CommonDataFiles.ARENA_STORE.getFile(getMinigame());
        JsonObject json = JsonHelper.readOrCreateJson(arenaStore);
        JsonObject jsonArena = new JsonObject();
        jsonArena.addProperty("name", getDisplayName());
        jsonArena.addProperty("world", getWorld());
        JsonObject spawns = new JsonObject();
        for (UnmodifiableIterator<Map.Entry<Integer, Location3D>> unmodifiableIterator = getSpawnPoints().entrySet().iterator(); unmodifiableIterator.hasNext(); ) {
            Map.Entry<Integer, Location3D> entry = unmodifiableIterator.next();
            spawns.add(entry.getKey().toString(), (JsonElement)JsonSerializer.serializeLocation(entry.getValue()));
        }
        jsonArena.add("spawns", spawns);
        jsonArena.add("bound.upper", (JsonElement)JsonSerializer.serializeLocation(getBoundary().getUpperBound()));
        jsonArena.add("bound.lower", (JsonElement)JsonSerializer.serializeLocation(getBoundary().getLowerBound()));
        JsonObject metadata = new JsonObject();
        JsonSerializer.serializeMetadata(metadata, getPersistentMetadata());
        jsonArena.add("metadata", metadata);
        json.add(getId(), jsonArena);
        try (FileWriter writer = new FileWriter(arenaStore)) {
            writer.write(json.toString());
        }
    }

    public void removeFromStore() throws IOException {
        File arenaStore = CommonDataFiles.ARENA_STORE.getFile(getMinigame());
        if (!arenaStore.exists())
            throw new IllegalStateException("Arena store does not exist!");
        JsonObject json = new JsonObject();
        try (FileReader reader = new FileReader(arenaStore)) {
            JsonElement el = (new JsonParser()).parse(reader);
            if (el.isJsonObject()) {
                json = el.getAsJsonObject();
            } else {
                CommonCore.logWarning("Root element of arena store is not object - overwriting");
                Files.delete(arenaStore.toPath());
            }
        }
        json.remove(getId());
        try (FileWriter writer = new FileWriter(arenaStore)) {
            writer.write(json.toString());
        }
    }

    public static class Builder implements Arena.Builder {
        private final Minigame mg;

        private String id;

        private String dispName;

        private Location3D[] spawnPoints;

        private Boundary boundary;

        public Builder(Minigame mg) {
            this.mg = mg;
        }

        public Arena.Builder id(String id) {
            this.id = id;
            return this;
        }

        public Arena.Builder displayName(String displayName) {
            this.dispName = displayName;
            return this;
        }

        public Arena.Builder spawnPoints(Location3D... spawnPoints) {
            this.spawnPoints = spawnPoints;
            return this;
        }

        public Arena.Builder boundary(Boundary boundary) {
            this.boundary = boundary;
            return this;
        }

        public Arena build() throws IllegalStateException {
            Preconditions.checkState((this.id != null), "ID must be set before building");
            Preconditions.checkState((this.spawnPoints != null && this.spawnPoints.length > 0), "Spawn points must be set before building");
            Preconditions.checkState((this.boundary != null), "Boundary must be set before building");
            CommonArena arena = ((IArenaFactory)FactoryRegistry.getFactory(Arena.class)).createArena((CommonMinigame)this.mg, this.id, (this.dispName != null) ? this.dispName : this.id, this.spawnPoints, this.boundary);
            try {
                arena.store();
            } catch (IOException ex) {
                throw new RuntimeException("Failed to store arena with ID " + this.id + ".", ex);
            }
            return arena;
        }
    }
}u