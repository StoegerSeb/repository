package com.sap.sepp.api.common.metadata.persist;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonPersistentMetadata: 07.04.2020 16:56 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.sap.sepp.api.common.metadata.CommonMetadata;
import com.sap.sepp.api.common.serialization.SimpleMetadataSerializer;
import com.sap.sepp.api.metadata.persist.PersistentMetadata;
import com.sap.sepp.api.serialization.Serializer;

public class CommonPersistentMetadata extends CommonMetadata implements PersistentMetadata {
    public static final String PRIMITIVE_PREFIX = "PRIM_";

    private static final SimpleMetadataSerializer SIMPLE_SERIALIZER = new SimpleMetadataSerializer();

    public <T> Optional<T> get(String key) throws ClassCastException {
        Optional<Object> value = super.get(key);
        if (value.isPresent() && value.get() instanceof String)
            return Optional.of((new SimpleMetadataSerializer()).deserialize(value.toString()));
        return (Optional)value;
    }

    public <T> T get(String key, Serializer<T> serializer) throws ClassCastException, IllegalArgumentException {
        Preconditions.checkArgument(this.data.get(key) instanceof String, "Metadata key " + key + " is not associated with a string");
        return (T)serializer.deserialize((String)this.data.get(key));
    }

    public void set(String key, Object value) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Generic set operation not permitted for PersistableMetadata objects");
    }

    public void set(String key, String value) {
        this.data.put(key, value);
        postEvent();
    }

    public void set(String key, boolean value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, byte value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, short value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, char value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, int value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, long value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, float value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public void set(String key, double value) {
        set(key, SIMPLE_SERIALIZER);
    }

    public <T> void set(String key, T value, Serializer<T> serializer) {
        set(key, serializer.serialize(value));
    }

    public void set(String key, List<String> value) {
        this.data.put(key, value);
        postEvent();
    }

    public <T> void set(String key, List<T> value, final Serializer<T> serializer) {
        List<String> transformed = Lists.newArrayList(Collections2.transform(value, new Function<T, String>() {
            public String apply(T input) {
                return serializer.serialize(input);
            }
        }));
        set(key, transformed);
    }

    public PersistentMetadata createStructure(String key) {
        Preconditions.checkArgument(!this.data.containsKey(key), "Metadata key " + key + " is already set");
        PersistentMetadata structure = new CommonPersistentMetadata();
        this.data.put(key, structure);
        postEvent();
        return structure;
    }

    public ImmutableCollection<String> values() {
        return (ImmutableCollection<String>)ImmutableList.copyOf(Collections2.transform(this.data.values(), new Function<Object, String>() {
            public String apply(Object input) {
                return (String)input;
            }
        }));
    }

    public ImmutableCollection<Object> values(final Function<String, Object> transformer) {
        return (ImmutableCollection<Object>)ImmutableList.copyOf(Collections2.transform(this.data.values(), new Function<Object, Object>() {
            public Object apply(Object input) {
                return transformer.apply(input);
            }
        }));
    }

    public ImmutableSet<? extends Map.Entry<String, String>> entrySet() {
        return ImmutableSet.copyOf(Collections2.transform(this.data.entrySet(), new Function<Map.Entry<String, ?>, AbstractMap.SimpleImmutableEntry<String, String>>() {
            public AbstractMap.SimpleImmutableEntry<String, String> apply(Map.Entry<String, ?> input) {
                return new AbstractMap.SimpleImmutableEntry<>(input.getKey(), (String)input.getValue());
            }
        }));
    }

    public ImmutableSet<? extends Map.Entry<String, Object>> entrySet(final Function<String, Object> transformer) {
        return ImmutableSet.copyOf(Collections2.transform(this.data.entrySet(), new Function<Map.Entry<String, Object>, AbstractMap.SimpleImmutableEntry<String, Object>>() {
            public AbstractMap.SimpleImmutableEntry<String, Object> apply(Map.Entry<String, Object> input) {
                Object attempt = CommonPersistentMetadata.SIMPLE_SERIALIZER.deserialize(input.getValue().toString());
                if (attempt instanceof String)
                    attempt = transformer.apply(attempt);
                return new AbstractMap.SimpleImmutableEntry<>(input.getKey(), attempt);
            }
        }));
    }

    public boolean remove(String key) {
        boolean result = super.remove(key);
        if (result)
            postEvent();
        return result;
    }

    public void clear() {
        super.clear();
        postEvent();
    }

    private void postEvent() {
        getEventBus().post(new PersistableMetadataMutateEvent(this));
    }
}