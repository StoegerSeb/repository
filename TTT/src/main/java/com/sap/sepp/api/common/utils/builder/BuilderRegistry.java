package com.sap.sepp.api.common.utils.builder;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class BuilderRegistry: 07.04.2020 16:42 by sebip
*/

import com.google.common.base.Preconditions;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.utils.builder.Buildable;
import com.sap.sepp.api.utils.builder.Builder;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class BuilderRegistry {
    private static final BuilderRegistry INSTANCE = new BuilderRegistry();

    private final Map<Class<? extends Buildable<?>>, Constructor<? extends Builder<?>>> ctorMap = new HashMap<>();

    public static BuilderRegistry instance() {
        return INSTANCE;
    }

    public <T extends Buildable<? super U>, U extends Builder<? extends T>> void registerBuilder(Class<T> target, Class<U> builder) {
        Preconditions.checkState(!this.ctorMap.containsKey(target), "Builder for class " + target
                .getName() + " has already been registered");
        if (!Modifier.isStatic(builder.getModifiers())) {
            CommonCore.logSevere("Cannot register non-static class " + builder.getName() + " as Builder");
            return;
        }
        try {
            Constructor<U> ctor = builder.getConstructor(Minigame.class);
            this.ctorMap.put(target, ctor);
        } catch (NoSuchMethodException ex) {
            CommonCore.logSevere("Failed to find applicable constructor for Builder class " + builder.getName() + " - skipping registration");
            ex.printStackTrace();
        }
    }

    public <T extends Buildable<? super U>, U extends Builder<? extends T>> U createBuilder(Class<T> clazz, Minigame mg) throws IllegalStateException {
        Preconditions.checkState(this.ctorMap.containsKey(clazz), "No Builder registration available for class " + clazz.getName());
        try {
            return (U)((Constructor<Builder>)this.ctorMap.get(clazz)).newInstance(new Object[] { mg });
        } catch (InstantiationException|IllegalAccessException|java.lang.reflect.InvocationTargetException ex) {
            throw new RuntimeException("Failed to instantiate Builder for class " + clazz.getName());
        }
    }
}