package com.sap.sepp.api.steel.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class WizardManager: 07.04.2020 16:35 by sebip
*/

public class WizardManager extends CommonWizardManager {
    public WizardManager(Minigame minigame) {
        super(minigame);
    }

    public void addPlayer(UUID uuid, Location3D location) {
        assert !this.wizardPlayers.containsKey(uuid);
        this.wizardPlayers.put(uuid, new WizardPlayer(uuid, location, (IWizardManager)this));
        CommonCore.getChatAgent().processAndSend(uuid, new String[] { "to the lobby sign wizard!", "messages will be withheld until the wizard is complete.", WizardMessages.DIVIDER, "start, please type the name of the arena you would like to create a lobby sign for. (You may type cancel at any time to exit the wizard." });
    }
}
