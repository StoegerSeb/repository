package com.sap.sepp.api.common.utils.factory;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class FactoryRegistry: 07.04.2020 16:42 by sebip
*/

import com.google.common.base.Preconditions;

import java.util.HashMap;
import java.util.Map;

public class FactoryRegistry {
    private static final Map<Class<?>, Factory<?>> factoryMap = new HashMap<>();

    public static <T, F extends Factory<? extends T>> void registerFactory(Class<T> target, F factory) {
        Preconditions.checkState(!factoryMap.containsKey(target), "Builder for class " + target
                .getName() + " has already been registered");
        factoryMap.put(target, (Factory<?>)factory);
    }

    public static <T, F extends Factory<T>> F getFactory(Class<T> clazz) throws IllegalStateException {
        Preconditions.checkState(factoryMap.containsKey(clazz), "No Factory registration available for class " + clazz.getName());
        return (F)factoryMap.get(clazz);
    }
}
