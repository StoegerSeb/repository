package com.sap.sepp.api.common.serialization;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SimpleMetadataSerializer: 07.04.2020 16:58 by sebip
*/

import com.sap.sepp.api.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

public class SimpleMetadataSerializer<T> implements Serializer<T> {
    private static final Map<Class<?>, Character> PRIM_PREFIX_MAP = new HashMap<>();

    static {
        PRIM_PREFIX_MAP.put(Boolean.class, 'Z');
        PRIM_PREFIX_MAP.put(Byte.class, 'B');
        PRIM_PREFIX_MAP.put(Short.class, 'S');
        PRIM_PREFIX_MAP.put(Character.class, 'C');
        PRIM_PREFIX_MAP.put(Integer.class, 'I');
        PRIM_PREFIX_MAP.put(Long.class, 'J');
        PRIM_PREFIX_MAP.put(Float.class, 'F');
        PRIM_PREFIX_MAP.put(Double.class, 'D');
    }

    public String serialize(Object object) {
        StringBuilder sb = new StringBuilder();
        sb.append("PRIM_");
        if (PRIM_PREFIX_MAP.containsKey(object.getClass()))
            sb.append(PRIM_PREFIX_MAP.get(object.getClass())).append('_');
        sb.append(object);
        return sb.toString();
    }

    public T deserialize(String str) {
        if (str.startsWith("PRIM_"))
            switch (str.charAt("PRIM_".length())) {
                case 'Z':
                    return (T)Boolean.valueOf(str.substring("PRIM_".length() + 2));
                case 'B':
                    return (T)Byte.valueOf(str.substring("PRIM_".length() + 2));
                case 'S':
                    return (T)Short.valueOf(str.substring("PRIM_".length() + 2));
                case 'C':
                    return (T)Character.valueOf(str.substring("PRIM_".length() + 2).charAt(0));
                case 'I':
                    return (T)Integer.valueOf(str.substring("PRIM_".length() + 2));
                case 'J':
                    return (T)Long.valueOf(str.substring("PRIM_".length() + 2));
                case 'F':
                    return (T)Float.valueOf(str.substring("PRIM_".length() + 2));
                case 'D':
                    return (T)Double.valueOf(str.substring("PRIM_".length() + 2));
            }
        return (T)str;
    }
}
