package com.sap.sepp.api.metadata;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Metadata: 07.04.2020 15:57 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

public interface Metadata {
    boolean containsKey(String paramString);

    boolean containsValue(Object paramObject);

    @Deprecated
    boolean has(String paramString);

    <T> Optional<T> get(String paramString) throws ClassCastException;

    <T> void set(String paramString, T paramT);

    Metadata createStructure(String paramString) throws IllegalArgumentException;

    boolean remove(String paramString);

    @Deprecated
    ImmutableSet<String> getAllKeys();

    ImmutableSet<String> keySet();

    ImmutableCollection<?> values();

    ImmutableSet<? extends Map.Entry<String, ?>> entrySet();

    void clear();
}
