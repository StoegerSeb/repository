package com.sap.sepp.api.config;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ConfigNode: 07.04.2020 17:06 by sebip
*/

import com.google.common.collect.ImmutableSet;
import com.sap.sepp.api.arena.SpawningMode;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.round.LifecycleStage;
import com.sap.sepp.api.utils.unsafe.UnsafeUtil;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class ConfigNode<T> {
    public static final ConfigNode<ImmutableSet<LifecycleStage>> DEFAULT_LIFECYCLE_STAGES = new ConfigNode(
            ImmutableSet.of());

    public static final ConfigNode<Boolean> ENABLE_LOBBY_WIZARD = new ConfigNode(Boolean.valueOf(true));

    public static final ConfigNode<Boolean> REQUIRE_SNEAK_TO_DESTROY_LOBBY = new ConfigNode(Boolean.valueOf(true));

    public static final RoundConfigNode<Integer> MAX_PLAYERS = new RoundConfigNode<>(Integer.valueOf(32));

    public static final RoundConfigNode<Boolean> ALLOW_EXIT_BOUNDARY = new RoundConfigNode<>(Boolean.valueOf(false));

    public static final RoundConfigNode<Boolean> ALLOW_DAMAGE = new RoundConfigNode<>(Boolean.valueOf(true));

    public static final RoundConfigNode<Boolean> ALLOW_FRIENDLY_FIRE = new RoundConfigNode<>(Boolean.valueOf(true));

    public static final RoundConfigNode<Boolean> SEPARATE_TEAM_CHATS = new RoundConfigNode<>(Boolean.valueOf(false));

    public static final RoundConfigNode<Boolean> SEPARATE_ROUND_CHATS = new RoundConfigNode<>(Boolean.valueOf(true));

    public static final RoundConfigNode<Boolean> WITHHOLD_SPECTATOR_CHAT = new RoundConfigNode<>(Boolean.valueOf(true));

    public static final RoundConfigNode<Boolean> ROLLBACK_ON_END = new RoundConfigNode<>(Boolean.valueOf(true));

    public static final RoundConfigNode<SpawningMode> SPAWNING_MODE = new RoundConfigNode<>(SpawningMode.SEQUENTIAL);

    @Deprecated
    public static final RoundConfigNode<Boolean> RANDOM_SPAWNING = new RoundConfigNode<>(Boolean.valueOf(false));

    public static final RoundConfigNode<Set<String>> FORBIDDEN_COMMANDS = new RoundConfigNode<>(new HashSet<>());

    public static final RoundConfigNode<LobbySignPopulator> STATUS_LOBBY_SIGN_POPULATOR = new RoundConfigNode<>(
            UnsafeUtil.instance().getDefaultStatusLobbySignPopulator());

    public static final RoundConfigNode<LobbySignPopulator> CHALLENGER_LISTING_LOBBY_SIGN_POPULATOR = new RoundConfigNode<>(
            UnsafeUtil.instance().getDefaultChallengerListingLobbySignPopulator());

    private final UUID uuid;

    private final T defaultValue;

    protected ConfigNode(T defaultValue) {
        this.uuid = UUID.randomUUID();
        this.defaultValue = defaultValue;
    }

    public T getDefaultValue() {
        return this.defaultValue;
    }

    public boolean equals(Object other) {
        return (other instanceof ConfigNode && this.uuid.equals(((ConfigNode)other).uuid));
    }

    public int hashCode() {
        return Objects.hash(getClass(), this.uuid);
    }
}
