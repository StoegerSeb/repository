package com.sap.sepp.api.common.challenger;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonTeam: 07.04.2020 16:47 by sebip
*/

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.metadata.CommonMetadataHolder;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.round.Round;

import java.util.ArrayList;
import java.util.List;

public class CommonTeam extends CommonMetadataHolder implements Team, CommonComponent<Round> {
    private final String id;

    private final Round round;

    private boolean orphan;

    private String name;

    private final List<Challenger> challengers = new ArrayList<>();

    public CommonTeam(String id, Round round) throws IllegalArgumentException {
        assert id != null;
        assert round != null;
        if (round.getTeam(id).isPresent())
            throw new IllegalArgumentException("Team \"" + id + "\" already exists");
        this.id = id;
        this.name = id;
        this.round = round;
    }

    public Round getOwner() throws OrphanedComponentException {
        checkState();
        return this.round;
    }

    public Round getRound() throws OrphanedComponentException {
        return getOwner();
    }

    public String getId() throws OrphanedComponentException {
        checkState();
        return this.id;
    }

    public String getName() throws OrphanedComponentException {
        checkState();
        return this.name;
    }

    public void setName(String name) throws OrphanedComponentException {
        checkState();
        this.name = name;
    }

    public ImmutableList<Challenger> getChallengers() throws OrphanedComponentException {
        checkState();
        return ImmutableList.copyOf(this.challengers);
    }

    public void addChallenger(Challenger challenger) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        Preconditions.checkArgument((challenger.getRound() == getRound()), "Cannot add challenger to team: round mismatch");
        if (challenger.getTeam().isPresent())
            challenger.getTeam().get().removeChallenger(challenger);
        this.challengers.add(challenger);
        ((CommonChallenger)challenger).justSetTeam(this);
    }

    public void removeChallenger(Challenger challenger) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        Preconditions.checkArgument(this.challengers.contains(challenger), "Cannot remove challenger from team: not present");
        this.challengers.remove(challenger);
        ((CommonChallenger)challenger).justSetTeam(null);
    }

    public void checkState() throws OrphanedComponentException {
        if (this.orphan)
            throw new OrphanedComponentException(this);
    }

    public void orphan() {
        CommonCore.orphan(this);
    }

    public void setOrphanFlag() {
        this.orphan = true;
    }
}