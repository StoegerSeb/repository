package com.sap.sepp.api.common.challenger;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonChallenger: 07.04.2020 16:47 by sebip
*/

import com.google.common.base.Optional;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.metadata.CommonMetadataHolder;
import com.sap.sepp.api.common.round.CommonRound;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.round.Round;

import java.util.UUID;

public abstract class CommonChallenger extends CommonMetadataHolder implements Challenger, CommonComponent<Round> {
    private final UUID uuid;

    private final String name;

    private final CommonRound round;

    private boolean orphan;

    private Team team;

    private boolean spectating = false;

    private boolean leaving = false;

    protected CommonChallenger(UUID playerUuid, String playerName, CommonRound round) {
        assert playerUuid != null;
        assert playerName != null;
        assert round != null;
        this.uuid = playerUuid;
        this.name = playerName;
        this.round = round;
    }

    public Round getOwner() throws OrphanedComponentException {
        checkState();
        return (Round)this.round;
    }

    public Round getRound() throws OrphanedComponentException {
        return getOwner();
    }

    public String getName() throws OrphanedComponentException {
        checkState();
        return this.name;
    }

    public UUID getUniqueId() throws OrphanedComponentException {
        checkState();
        return this.uuid;
    }

    public void removeFromRound() throws OrphanedComponentException {
        checkState();
        this.round.removeChallenger(this);
    }

    public Optional<Team> getTeam() throws OrphanedComponentException {
        checkState();
        return Optional.fromNullable(this.team);
    }

    public void setTeam(Team team) {
        if (team == null) {
            if (getTeam().isPresent())
                getTeam().get().removeChallenger(this);
            return;
        }
        team.addChallenger(this);
    }

    public void justSetTeam(Team team) {
        this.team = team;
    }

    public boolean isSpectating() throws OrphanedComponentException {
        checkState();
        return this.spectating;
    }

    public void setSpectating(boolean spectating) throws OrphanedComponentException {
        checkState();
        if (this.spectating != spectating)
            this.spectating = spectating;
    }

    public boolean isLeaving() {
        return this.leaving;
    }

    public void setLeavingFlag() {
        this.leaving = true;
    }

    public void checkState() throws OrphanedComponentException {
        if (this.orphan)
            throw new OrphanedComponentException();
    }

    public void orphan() {
        CommonCore.orphan(this);
    }

    public void setOrphanFlag() {
        this.orphan = true;
    }
}