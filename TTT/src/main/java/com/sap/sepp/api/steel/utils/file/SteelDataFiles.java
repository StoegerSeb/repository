package com.sap.sepp.api.steel.utils.file;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelDataFiles: 07.04.2020 16:19 by sebip
*/

public class SteelDataFiles extends CommonDataFiles {
    public static final CoreDataFile TELEMETRY_UUID_STORE = new CoreDataFile("uuid.txt");

    public static final CoreDataFile CORE_OLD_DATA_DIR = new CoreDataFile("old", true, false);

    public static final CoreDataFile OLD_OFFLINE_PLAYER_STORE = new CoreDataFile("offline_players.yml", false, false);

    public static final CoreDataFile OLD_PLAYER_LOCATION_STORE = new CoreDataFile("locs.yml", false, false);

    public static final MinigameDataFile MG_OLD_DATA_DIR = new MinigameDataFile("old", true, false);

    public static final MinigameDataFile OLD_ARENA_STORE = new MinigameDataFile("arenas.yml", false, false);

    public static final MinigameDataFile OLD_LOBBY_STORE = new MinigameDataFile("lobbies.yml", false, false);
}
