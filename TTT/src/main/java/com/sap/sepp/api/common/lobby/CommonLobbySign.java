package com.sap.sepp.api.common.lobby;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonLobbySign: 07.04.2020 16:51 by sebip
*/

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.utils.file.CommonDataFiles;
import com.sap.sepp.api.common.utils.helper.JsonHelper;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.config.RoundConfigNode;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.popular.LobbySignPopulator;
import com.sap.sepp.api.lobby.type.ChallengerListingLobbySign;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Location3D;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class CommonLobbySign implements LobbySign, CommonComponent<Arena> {
    public static final String PERSIST_TYPE_KEY = "type";

    public static final String PERSIST_TYPE_STATUS = "status";

    public static final String PERSIST_TYPE_LISTING = "listing";

    public static final String PERSIST_INDEX_KEY = "index";

    private final Location3D location;

    private final CommonArena arena;

    private final LobbySign.Type type;

    private boolean orphan = false;

    protected CommonLobbySign(Location3D location, CommonArena arena, LobbySign.Type type) {
        this.location = location;
        this.arena = arena;
        this.type = type;
    }

    public Arena getOwner() throws OrphanedComponentException {
        checkState();
        return this.arena;
    }

    public Arena getArena() throws OrphanedComponentException {
        return getOwner();
    }

    public Location3D getLocation() throws OrphanedComponentException {
        checkState();
        return this.location;
    }

    public LobbySign.Type getType() {
        return this.type;
    }

    public void unregister() throws OrphanedComponentException {
        checkState();
        this.arena.unregisterLobbySign(this.location);
    }

    public void update() {
        RoundConfigNode<LobbySignPopulator> node;
        checkState();
        if (getType() == LobbySign.Type.STATUS) {
            node = ConfigNode.STATUS_LOBBY_SIGN_POPULATOR;
        } else if (getType() == LobbySign.Type.CHALLENGER_LISTING) {
            node = ConfigNode.CHALLENGER_LISTING_LOBBY_SIGN_POPULATOR;
        } else {
            throw new AssertionError();
        }
        LobbySignPopulator pop = getArena().getRound().isPresent() ? getArena().getRound().get().getConfigValue(node) : (LobbySignPopulator)getArena().getMinigame().getConfigValue((ConfigNode)node);
        updatePhysicalSign(pop.first(this), pop.second(this), pop.third(this), pop.fourth(this));
    }

    private void store(boolean remove) throws Throwable {
        try {
            Throwable throwable = null;
            File store = CommonDataFiles.LOBBY_STORE.getFile(getArena().getMinigame());
            JsonObject json = JsonHelper.readOrCreateJson(store);
            JsonObject arena = json.getAsJsonObject(getArena().getId());
            if (arena == null)
                if (!remove) {
                    arena = new JsonObject();
                    json.add(getArena().getId(), (JsonElement)arena);
                } else {
                    CommonCore.logWarning("Anomaly: Engine requested removal of lobby sign from store, but arena was not defined");
                    return;
                }
            String locSerial = getLocation().serialize();
            if (remove) {
                if (arena.has(locSerial)) {
                    arena.remove(locSerial);
                } else {
                    CommonCore.logWarning("Engine requested removal of lobby sign from store, but respective section was not defined");
                }
            } else {
                String str;
                JsonObject sign = new JsonObject();
                arena.add(locSerial, (JsonElement)sign);
                if (this instanceof com.sap.sepp.api.lobby.type.StatusLobbySign) {
                    str = "status";
                } else if (this instanceof ChallengerListingLobbySign) {
                    str = "listing";
                } else {
                    throw new AssertionError("Invalid LobbySign object. Report this immediately.");
                }
                sign.addProperty("type", str);
                if (this instanceof ChallengerListingLobbySign)
                    sign.addProperty("index", ((ChallengerListingLobbySign) this).getIndex());
            }
            FileWriter writer = new FileWriter(store);
            String type = null;
            try {
                writer.write(json.toString());
            } catch (Throwable throwable1) {
                throwable = throwable1 = null;
                throw throwable1;
            } finally {
                if (writer != null)
                    if (throwable != null) {
                        try {
                            writer.close();
                        } catch (Throwable throwable1) {
                            throwable.addSuppressed(throwable1);
                        }
                    } else {
                        writer.close();
                    }
            }
        } catch (IOException ex) {
            CommonCore.logSevere("Failed to write to lobby sign store");
            ex.printStackTrace();
        }
    }

    public void store() {
        store(false);
    }

    public void unstore() {
        store(true);
    }

    protected String[] getChallengerListingText() {
        if (!validate()) {
            unregister();
            CommonCore.logWarning("Cannot update lobby sign at (\"" + this.location.getWorld() + "\", " + this.location
                    .getX() + ", " + this.location.getY() + ", " + this.location.getZ() + "): not a sign. Removing...");
        }
        int startIndex = ((ChallengerListingLobbySign)this).getIndex() * getSignSize();
        boolean round = getArena().getRound().isPresent();
        String[] lines = new String[getSignSize()];
        for (int i = 0; i < getSignSize(); i++) {
            if (round && startIndex + i < getArena().getRound().get().getChallengers().size()) {
                lines[i] = ((Challenger) getArena().getRound().get().getChallengers().get(startIndex + i)).getName();
            } else {
                lines[i] = "";
            }
        }
        return lines;
    }

    protected String[] getStatusText() {
        if (!validate()) {
            unregister();
            throw new IllegalStateException("Cannot update lobby sign: not a sign. Removing...");
        }
        String[] lines = new String[getSignSize()];
        lines[0] = getArena().getDisplayName();
        if (getArena().getRound().isPresent()) {
            lines[1] = getArena().getRound().get().getLifecycleStage().getId().toUpperCase();
            long seconds = (getArena().getRound().get().getRemainingTime() != -1L) ? getArena().getRound().get().getRemainingTime() : getArena().getRound().get().getTime();
            String time = (seconds / 60L) + ":" + ((seconds % 60L >= 10L) ? (String)Long.valueOf(seconds % 60L) : ("0" + (seconds % 60L)));
            lines[2] = time;
            int maxPlayers = getArena().getRound().get().getConfigValue(ConfigNode.MAX_PLAYERS).intValue();
            String players = getArena().getRound().get().getChallengers().size() + "/" + ((maxPlayers > 0) ? (String)Integer.valueOf(maxPlayers) : ");
            players = players + ((players.length() <= 5) ? " players" : ((players.length() <= 7) ? " plyrs" : ""));
            lines[3] = players;
        } else {
            for (int i = 1; i < getSignSize(); i++)
                lines[i] = "";
        }
        return lines;
    }

    public void checkState() throws OrphanedComponentException {
        if (this.orphan)
            throw new OrphanedComponentException(this);
    }

    public void orphan() {
        CommonCore.orphan(this);
    }

    public void setOrphanFlag() {
        this.orphan = true;
    }

    protected abstract void updatePhysicalSign(String... paramVarArgs);

    protected abstract boolean validate();

    protected abstract int getSignSize();
}
