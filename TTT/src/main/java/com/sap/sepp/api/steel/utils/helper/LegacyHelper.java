package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LegacyHelper: 07.04.2020 16:20 by sebip
*/

import com.google.common.base.Preconditions;
import com.sap.sepp.api.steel.SteelCore;
import org.bukkit.block.Block;

import java.lang.reflect.Method;

public class LegacyHelper {
    private static boolean initialized;

    static {
        Preconditions.checkState(SteelCore.isLegacy(), "Cannot use legacy helper class on non-legacy platform!");
        try {
            method_Block_setData = Block.class.getMethod("setData", byte.class);
        } catch (NoSuchMethodException e) {
            working = false;
            throw new RuntimeException("Failed to access Block#setData (is legacy detection broken?)");
        }
    }

    private static boolean working = true;

    private static final Method method_Block_setData;

    public LegacyHelper() {
        Preconditions.checkState(!initialized, "Cannot initialize singleton class LegacyHelper more than once.");
        initialized = true;
    }

    public void updateData(Block b, byte data) {
        Preconditions.checkState(SteelCore.isLegacy(), "Cannot use legacy helper class on non-legacy platform!");
        if (!working)
            return;
        try {
            method_Block_setData.invoke(b, data);
        } catch (IllegalAccessException|java.lang.reflect.InvocationTargetException ex) {
            throw new RuntimeException("Failed to set block data (is legacy detection broken?)");
        }
    }
}
