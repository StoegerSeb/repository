package com.sap.sepp.api.steel.lobby;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelLobbySign: 07.04.2020 16:34 by sebip
*/

import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.lobby.CommonLobbySign;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.steel.SteelCore;
import com.sap.sepp.api.steel.SteelMain;
import com.sap.sepp.api.steel.utils.helper.LocationHelper;
import com.sap.sepp.api.utils.physical.Location3D;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.plugin.Plugin;

public abstract class SteelLobbySign extends CommonLobbySign {
    private static final int SIGN_SIZE = 4;

    public SteelLobbySign(Location3D location, CommonArena arena, LobbySign.Type type) {
        super(location, arena, type);
        Bukkit.getScheduler().runTask((Plugin) SteelMain.getInstance(), new Runnable() {
            public void run() {
                SteelLobbySign.this.update();
            }
        });
    }

    public void unregister() {
        super.unregister();
        World world = Bukkit.getWorld((String)getLocation().getWorld().get());
        if (world == null)
            SteelCore.logVerbose("Cannot blank unregistered lobby sign: world is not loaded");
        Block block = LocationHelper.convertLocation(getLocation()).getBlock();
        if (block.getState() instanceof Sign)
            for (int i = 0; i < (((Sign)block.getState()).getLines()).length; i++)
                ((Sign)block.getState()).setLine(i, "");
        orphan();
    }

    protected void updatePhysicalSign(String... lines) {
        assert lines.length == getSignSize();
        Block block = LocationHelper.convertLocation(getLocation()).getBlock();
        if (block.getState() instanceof Sign) {
            final Sign sign = (Sign)block.getState();
            for (int i = 0; i < getSignSize(); i++)
                sign.setLine(i, lines[i]);
            Bukkit.getScheduler().runTask((Plugin)SteelMain.getInstance(), new Runnable() {
                public void run() {
                    sign.update(true);
                }
            });
        }
    }

    protected boolean validate() {
        Block b = getBlock();
        return b.getState() instanceof Sign;
    }

    protected int getSignSize() {
        return 4;
    }

    public Block getBlock() {
        World world = Bukkit.getWorld((String)getLocation().getWorld().get());
        if (world == null)
            throw new IllegalStateException("Cannot get world \"" + (String)getLocation().getWorld().get() + "\" for lobby sign");
        return world.getBlockAt((int)getLocation().getX(), (int)getLocation().getY(), (int)getLocation().getZ());
    }
}