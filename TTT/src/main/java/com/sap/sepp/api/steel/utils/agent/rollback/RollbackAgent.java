package com.sap.sepp.api.steel.utils.agent.rollback;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class RollbackAgent: 07.04.2020 16:11 by sebip
*/

import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.utils.agent.rollback.CommonRollbackAgent;
import com.sap.sepp.api.common.utils.agent.rollback.RollbackRecord;
import com.sap.sepp.api.steel.SteelCore;
import com.sap.sepp.api.steel.SteelMain;
import com.sap.sepp.api.steel.arena.SteelArena;
import com.sap.sepp.api.steel.utils.agent.rollback.serialization.BlockStateSerializer;
import com.sap.sepp.api.steel.utils.agent.rollback.serialization.EntityStateSerializer;
import com.sap.sepp.api.steel.utils.helper.LocationHelper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Event;
import org.bukkit.inventory.InventoryHolder;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public final class RollbackAgent extends CommonRollbackAgent {
    private HashMap<UUID, Entity> entities;

    public RollbackAgent(CommonArena arena) {
        super(arena);
    }

    protected void delay(Runnable runnable) {
        Bukkit.getScheduler().runTask(SteelMain.getInstance(), runnable);
    }

    public void logBlockChange(Block block) throws IOException, SQLException {
        String state = BlockStateSerializer.serializeState(block).orNull();
        logChange(RollbackRecord.createBlockRecord(-1, LocationHelper.convertLocation(block.getLocation()), block
                .getState().getType().name(), block.getState().getRawData(), state));
    }

    private void logEntityCreation(Entity entity) throws IOException, SQLException {
        logEntitySomething(entity, true);
    }

    private void logEntityChange(Entity entity) throws IOException, SQLException {
        logEntitySomething(entity, false);
    }

    private void logEntitySomething(Entity entity, boolean newlyCreated) throws IOException, SQLException {
        String state = !newlyCreated ? EntityStateSerializer.serializeState(entity) : null;
        if (newlyCreated) {
            logChange(RollbackRecord.createEntityCreationRecord(-1, entity.getUniqueId(), entity.getWorld().getName()));
        } else {
            logChange(RollbackRecord.createEntityChangeRecord(-1, entity.getUniqueId(),
                    LocationHelper.convertLocation(entity.getLocation()), entity.getType().name(), state));
        }
    }

    public static void checkBlockChange(Block block, Event event) {
        List<Arena> arenas = checkChangeAtLocation(LocationHelper.convertLocation(block.getLocation()));
        for (Arena arena : arenas) {
            try {
                ((SteelArena)arena).getRollbackAgent().logBlockChange(block);
            } catch (IOException|SQLException ex) {
                throw new RuntimeException("Failed to log " + event.getEventName() + " for rollback in arena " + arena
                        .getDisplayName(), ex);
            }
        }
    }

    public static void checkEntityChange(Entity entity, boolean newlyCreated, Event event) {
        List<Arena> arenas = checkChangeAtLocation(LocationHelper.convertLocation(entity.getLocation()));
        for (Arena arena : arenas) {
            try {
                if (newlyCreated) {
                    ((SteelArena)arena).getRollbackAgent().logEntityCreation(entity);
                    continue;
                }
                ((SteelArena)arena).getRollbackAgent().logEntityChange(entity);
            } catch (IOException|SQLException ex) {
                throw new RuntimeException("Failed to log " + event.getEventName() + " for rollback in arena " + arena
                        .getDisplayName(), ex);
            }
        }
    }

    public void rollbackBlock(RollbackRecord record) throws IOException {
        Material m;
        Block b = LocationHelper.convertLocation(record.getLocation()).getBlock();
        try {
            m = Material.valueOf(record.getTypeData());
        } catch (IllegalArgumentException ex) {
            SteelCore.logWarning("Rollback record with ID " + record.getId() + " in arena " +
                    getArena().getId() + " cannot be matched to a Material");
            return;
        }
        if (b.getState() instanceof InventoryHolder)
            ((InventoryHolder)b.getState()).getInventory().clear();
        b.setType(m);
        if (SteelCore.isLegacy())
            SteelCore.getLegacyHelper().updateData(b, (byte)record.getData());
        if (record.getStateSerial() != null)
            try {
                BlockStateSerializer.deserializeState(b, record.getStateSerial());
            } catch (InvalidConfigurationException ex) {
                throw new IOException(ex);
            }
    }

    public void rollbackEntityCreation(RollbackRecord record) {
        if (this.entities.containsKey(record.getUuid()))
            this.entities.get(record.getUuid()).remove();
    }

    public void rollbackEntityChange(final RollbackRecord record) throws IOException {
        final EntityType entityType;
        try {
            entityType = EntityType.valueOf(record.getTypeData());
        } catch (IllegalArgumentException ex) {
            CommonCore.logWarning("Invalid entity type for rollback record with ID " + record.getId() + " in arena " +
                    getArena().getId());
            return;
        }
        if (this.entities.containsKey(record.getUuid())) {
            Entity e = this.entities.get(record.getUuid());
            e.teleport(e.getLocation().subtract(0.0D, e.getLocation().getY() + 1.0D, 0.0D));
            e.remove();
        }
        Bukkit.getScheduler().runTask(SteelMain.getInstance(), new Runnable() {
            public void run() {
                Location loc = LocationHelper.convertLocation(record.getLocation());
                Entity e = loc.getWorld().spawnEntity(loc, entityType);
                if (record.getStateSerial() != null)
                    try {
                        EntityStateSerializer.deserializeState(e, record.getStateSerial());
                    } catch (InvalidConfigurationException ex) {
                        throw new RuntimeException(ex);
                    }
            }
        });
    }

    public void cacheEntities() {
        World w = Bukkit.getWorld(getArena().getWorld());
        this.entities = new HashMap<>();
        for (Entity entity : w.getEntities())
            this.entities.put(entity.getUniqueId(), entity);
    }
}