package com.sap.sepp.api.common.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonRoundWorker: 07.04.2020 16:57 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.sap.sepp.api.common.event.round.CommonRoundTimerTickEvent;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.round.Round;

public abstract class CommonRoundWorker implements Runnable {
    private final CommonRound round;

    protected CommonRoundWorker(CommonRound round) {
        this.round = round;
    }

    public CommonRound getRound() {
        return this.round;
    }

    public void run() {
        if (this.round.isTimerTicking())
            handleTick();
        if (!this.round.isOrphaned()) {
            checkPlayerLocations();
            for (UnmodifiableIterator<LobbySign> unmodifiableIterator = this.round.getArena().getLobbySigns().iterator(); unmodifiableIterator.hasNext(); ) {
                LobbySign sign = unmodifiableIterator.next();
                if (sign.getType() == LobbySign.Type.STATUS)
                    sign.update();
            }
        }
    }

    private void handleTick() {
        boolean stageSwitch = (this.round.getLifecycleStage().getDuration() > 0 && this.round.getTime() >= this.round.getLifecycleStage().getDuration());
        if (stageSwitch) {
            if (this.round.getNextLifecycleStage().isPresent()) {
                this.round.nextLifecycleStage();
            } else {
                this.round.end(CommonRound.NaturalEnd.NATURAL);
                return;
            }
        } else {
            this.round.setTime(this.round.getTime() + 1L, false);
        }
        this.round.getArena().getMinigame().getEventBus().post(new CommonRoundTimerTickEvent(this.round, this.round.getTime() - 1L, stageSwitch ? 0L : this.round
                .getTime()));
    }

    protected abstract void checkPlayerLocations();
}
