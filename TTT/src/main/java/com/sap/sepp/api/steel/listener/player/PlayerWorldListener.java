package com.sap.sepp.api.steel.listener.player;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class PlayerWorldListener: 07.04.2020 16:30 by sebip
*/

public class PlayerWorldListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        if (event.getFrom().getX() != event.getTo().getX() || event
                .getFrom().getY() != event.getTo().getY() || event
                .getFrom().getZ() != event.getTo().getZ()) {
            Optional<Challenger> challenger = CommonCore.getChallenger(event.getPlayer().getUniqueId());
            if (challenger.isPresent() && !((CommonChallenger)challenger.get()).isLeaving()) {
                Boundary bound = ((Challenger)challenger.get()).getRound().getArena().getBoundary();
                if (!bound.contains(LocationHelper.convertLocation(event.getTo())))
                    if (((Boolean)((Challenger)challenger.get()).getRound().getConfigValue(ConfigNode.ALLOW_EXIT_BOUNDARY)).booleanValue()) {
                        ((Challenger)challenger.get()).removeFromRound();
                    } else {
                        event.setCancelled(true);
                    }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        for (Minigame mg : SteelCore.getMinigames().values()) {
            IWizardManager wm = ((SteelMinigame)mg).getLobbyWizardManager();
            if (wm.hasPlayer(event.getPlayer().getUniqueId())) {
                event.setCancelled(true);
                event.getPlayer().sendMessage("<" + event.getPlayer().getDisplayName() + "> " + event.getMessage());
                String[] response = wm.accept(event.getPlayer().getUniqueId(), event.getMessage());
                event.getPlayer().sendMessage(response);
                return;
            }
            Iterator<Player> it = event.getRecipients().iterator();
            while (it.hasNext()) {
                Player recip = it.next();
                if (((SteelMinigame)mg).getLobbyWizardManager().hasPlayer(recip.getUniqueId())) {
                    ((SteelMinigame)mg).getLobbyWizardManager().withholdMessage(recip.getUniqueId(), event
                            .getPlayer().getDisplayName(), event.getMessage());
                    it.remove();
                    continue;
                }
                if (ChatHelper.isBarrierPresent(event.getPlayer(), recip))
                    it.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        boolean cancelled = false;
        if (event.getEntity().getType() == EntityType.PLAYER && event.getDamager().getType() == EntityType.PLAYER) {
            Optional<Challenger> challenger = CommonCore.getChallenger(event.getEntity().getUniqueId());
            Optional<Challenger> damager = CommonCore.getChallenger(event.getDamager().getUniqueId());
            if ((challenger.isPresent() && ((Challenger)challenger.get()).isSpectating()) || (damager
                    .isPresent() && ((Challenger)damager.get()).isSpectating())) {
                event.setCancelled(true);
                return;
            }
            if (challenger.isPresent() && damager.isPresent()) {
                if (((Challenger)challenger.get()).getRound() == ((Challenger)damager.get()).getRound()) {
                    if (!((Boolean)((Challenger)challenger.get()).getRound().getConfigValue(ConfigNode.ALLOW_DAMAGE)).booleanValue()) {
                        cancelled = true;
                    } else if (!((Boolean)((Challenger)challenger.get()).getRound().getConfigValue(ConfigNode.ALLOW_FRIENDLY_FIRE)).booleanValue()) {
                        if (((Challenger)challenger.get()).getTeam().orNull() == ((Challenger)damager.get()).getTeam().orNull())
                            cancelled = true;
                    }
                } else {
                    cancelled = true;
                }
            } else if (challenger.isPresent() != damager.isPresent()) {
                cancelled = true;
            }
        }
        if (cancelled)
            event.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        processEvent((Cancellable)event, event.getPlayer());
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        processEvent((Cancellable)event, event.getPlayer());
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getClickedBlock() == null || event
                .getAction() != Action.RIGHT_CLICK_BLOCK ||
                !(event.getClickedBlock().getState() instanceof org.bukkit.inventory.InventoryHolder))
            processEvent((Cancellable)event, event.getPlayer());
    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
        if (event.getEntity().getType() == EntityType.PLAYER)
            processEvent((Cancellable)event, (Player)event.getEntity());
    }

    @EventHandler
    public void onInventoryInteract(InventoryInteractEvent event) {
        if (event.getInventory().getHolder() instanceof org.bukkit.block.Block)
            processEvent((Cancellable)event, (Player)event.getWhoClicked());
    }

    @EventHandler
    public void onPlayerCommandPreprocessEvent(PlayerCommandPreprocessEvent event) {
        if (event.getMessage().startsWith("/suicide") || event.getMessage().startsWith("/kill")) {
            Player pl = event.getMessage().startsWith("/kill ") ? Bukkit.getPlayer(event.getMessage().split(" ")[1]) : event.getPlayer();
            if (pl == null)
                return;
            UUID uuid = pl.getUniqueId();
            if (CommonCore.getChallenger(uuid).isPresent()) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED + "You may not run this command while in a minigame round");
                return;
            }
        }
        Optional<Challenger> ch = CommonCore.getChallenger(event.getPlayer().getUniqueId());
        if (ch.isPresent() && (
                (Set)((Challenger)ch.get()).getRound().getConfigValue(ConfigNode.FORBIDDEN_COMMANDS))
                .contains(event.getMessage().split(" ")[0].substring(1))) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.RED + "You may not run this command while in a minigame round");
        }
    }

    private void processEvent(Cancellable event, Player player) {
        if (!SteelCore.SPECTATOR_SUPPORT) {
            Optional<Challenger> ch = CommonCore.getChallenger(player.getUniqueId());
            if (ch.isPresent() && ((Challenger)ch.get()).isSpectating()) {
                event.setCancelled(true);
                return;
            }
        }
    }
}
