package com.sap.sepp.api.steel.utils.factory;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class LobbySignFactory: 07.04.2020 16:18 by sebip
*/

import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.utils.physical.Location3D;

public class LobbySignFactory implements ILobbySignFactory {
    public LobbySign createLobbySign(Location3D location, Arena arena, JsonObject json) throws IllegalArgumentException {
        if (json.has("type")) {
            int index;
            String type = json.get("type").getAsString();
            switch (type) {
                case "status":
                    return (LobbySign)new SteelStatusLobbySign(location, (CommonArena)arena);
                case "listing":
                    if (!json.has("index"))
                        break;
                    index = json.get("index").getAsInt();
                    return (LobbySign)new SteelChallengerListingLobbySign(location, (CommonArena)arena, index);
            }
        }
        throw new IllegalArgumentException("Invalid configuration for LobbySign");
    }
}
