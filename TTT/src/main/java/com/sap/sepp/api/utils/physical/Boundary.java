package com.sap.sepp.api.utils.physical;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Boundary: 07.04.2020 15:50 by sebip
*/

import com.google.common.base.Preconditions;

public class Boundary {
    public static final Boundary INFINITE = new Boundary(new Location3D(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY), new Location3D(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY));

    private final String world;

    private final Location3D lowerBound;

    private final Location3D upperBound;

    public Boundary(final Location3D corner1, final Location3D corner2) throws IllegalArgumentException {
        Preconditions.checkState((corner1 != null), "Boundary corner cannot be null");
        Preconditions.checkState((corner2 != null), "Boundary corner cannot be null");
        Preconditions.checkState((!corner1.getWorld().isPresent() || !corner2.getWorld().isPresent() || corner1 .getWorld().get().equals(corner2.getWorld().get())), "Boundary corners cannot have mismatching worlds");

        if (corner1.getWorld().isPresent())  this.world = corner1.getWorld().get();
         else if (corner2.getWorld().isPresent())  this.world = corner2.getWorld().get();
         else this.world = null;

        this.lowerBound = new Location3D(this.world, Math.min(corner1.getX(), corner2.getX()), Math.min(corner1.getY(), corner2.getY()), Math.min(corner1.getZ(), corner2.getZ()));
        this.upperBound = new Location3D(this.world, Math.max(corner1.getX(), corner2.getX()), Math.max(corner1.getY(), corner2.getY()), Math.max(corner1.getZ(), corner2.getZ()));
    }

    @Deprecated
    public Boundary(final Location3D corner1, final Location3D corner2, final boolean ignoreDifferentWorlds) throws IllegalArgumentException {
        this(corner1, corner2);
    }

    public Location3D getLowerBound() {
        return this.lowerBound;
    }

    public Location3D getUpperBound() {
        return this.upperBound;
    }

    public boolean contains(final Location3D location) {
        if (this.world != null && location.getWorld().isPresent() && !this.world.equals(location.getWorld().get()))
            return false;
        return (location.getX() >= getLowerBound().getX() && location.getX() <= getUpperBound().getX() && location
                .getY() >= getLowerBound().getY() && location.getY() <= getUpperBound().getY() && location
                .getZ() >= getLowerBound().getZ() && location.getZ() <= getUpperBound().getZ());
    }
}
