package com.sap.sepp.api.steel;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelMain: 07.04.2020 16:08 by sebip
*/

import com.sap.sepp.api.steel.utils.file.SteelDataFiles;
import com.sap.sepp.api.steel.utils.helper.ConfigHelper;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

public class SteelMain extends JavaPlugin {
    private static SteelMain instance;

    public void onEnable() {
        instance = this;
        SteelCore.initializeSteel();
        registerEvents();
        saveDefaultConfig();
        try {
            ConfigHelper.addMissingKeys();
        } catch (InvalidConfigurationException |java.io.IOException ex) {
            ex.printStackTrace();
            SteelCore.logWarning("Failed to write missing config keys");
        }
        SteelDataFiles.createCoreDataFiles();
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
            getLogger().severe("Failed to load SQL driver");
            ex.printStackTrace();
        }
        (new CoreDataMigrationAgent()).migrateData();
        initMetrics();
        initTelemetry();
        UpdateHelper.run();
    }

    public void onDisable() {}

    public File getFile() {
        return super.getFile();
    }

    public static SteelMain getInstance() {
        return instance;
    }

    public void initMetrics() {
        if (getConfig().getBoolean("enable-metrics")) {
            Metrics metrics = new Metrics(this);
            metrics.addCustomChart((Metrics.CustomChart)new Metrics.SimplePie("flint_api_level", new Callable<String>() {
                public String call() {
                    return Integer.toString(FlintCore.getApiRevision());
                }
            }));
        }
    }

    public void initTelemetry() {
        if (getConfig().getBoolean("enable-metrics"))
            Bukkit.getScheduler().runTask((Plugin)this, (Runnable)new TelemetryRunner());
    }

    public void registerEvents() {
        Bukkit.getPluginManager().registerEvents((Listener)new PlayerConnectionListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new PlayerWorldListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new PluginListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new RollbackBlockListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new RollbackEntityListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new RollbackInventoryListener(), (Plugin)getInstance());
        Bukkit.getPluginManager().registerEvents((Listener)new LobbyListener(), (Plugin)getInstance());
    }
}