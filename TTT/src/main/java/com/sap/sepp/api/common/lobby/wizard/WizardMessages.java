package com.sap.sepp.api.common.lobby.wizard;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class WizardMessages: 07.04.2020 16:54 by sebip
*/

public class WizardMessages {
    public static final String INFO_COLOR = ";

    public static final String ERROR_COLOR = ";

    public static final String EM_COLOR = ";

    public static final String WELCOME = "to the lobby sign wizard!";

    public static final String CHAT_WITHHOLDING = "messages will be withheld until the wizard is complete.";

    public static final String GET_ARENA = "start, please type the name of the arena you would like to create a lobby sign for. (You may type cancel at any time to exit the wizard.";

    public static final String GET_TYPE = "please select the type of lobby sign you would like to create from the list below (type a number):";

    public static final String GET_TYPE_STATUS = "- Status signs display basic information about the round contained by an arena such as timer info and player count.";

    public static final String GET_TYPE_LISTING = "Listing - Player listing signs display a portion of the players in a round. If this is selected, you will be asked next to define which portion of players to display.";

    public static final String GET_INDEX = "Next, please select which portion of players you would like the player listing sign to display. (will display players will display players and so on.)";

    public static final String CONFIRM_1 = "Your lobby sign will be created with the following info:";

    public static final String CONFIRM_2 = "this okay? (Type or ;

    public static final String RESET = "wizard will now reset.";

    public static final String FINISH = "lobby sign was successfully created! The wizard will now exit.";

    public static final String MESSAGE_PLAYBACK = "back withheld chat messages...";

    public static final String CANCELLED = "sign creation cancelled.";

    public static final String BAD_ARENA = "arena by that ID exists! Please enter another arena ID.";

    public static final String BAD_TYPE = "sign type! Please select a valid option.";

    public static final String BAD_INDEX = "sign index! Please enter a number greater than or equal to 1.";

    public static final String BAD_CONFIRMATION = "type or ;

    public static final String ARENA_REMOVED = "selected arena has been removed. The wizard will now exit.";

    public static final String GENERIC_ERROR = "internal exception occurred while creating the lobby sign. The wizard will now exit.";

    public static final String DIVIDER;

    static {
        int dividerLength = 36;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 36; i++)
            sb.append("-");
        DIVIDER = sb.toString();
    }
}