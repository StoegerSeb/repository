package com.sap.sepp.api.steel;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelCore: 07.04.2020 16:08 by sebip
*/

import com.google.common.base.Preconditions;
import com.sap.sepp.api.FlintCore;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.utils.PlatformUtils;
import com.sap.sepp.api.common.utils.agent.chat.IChatAgent;
import com.sap.sepp.api.common.utils.agent.rollback.IRollbackAgent;
import com.sap.sepp.api.common.utils.factory.Factory;
import com.sap.sepp.api.common.utils.factory.FactoryRegistry;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.minigame.Minigame;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.steel.utils.SteelUtils;
import com.sap.sepp.api.steel.utils.agent.chat.ChatAgent;
import com.sap.sepp.api.steel.utils.factory.*;
import com.sap.sepp.api.steel.utils.helper.LegacyHelper;
import com.sap.sepp.api.steel.utils.unsafe.SteelUnsafeUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.plugin.Plugin;

public class SteelCore extends CommonCore {
    public static final boolean SPECTATOR_SUPPORT;

    private static boolean VERBOSE_LOGGING;

    private static final ChatAgent CHAT_AGENT = new ChatAgent();

    public static final int MC_113_TRANSFORMED = 1013000;

    public static final int MC_114_TRANSFORMED = 1014000;

    private static int mcVersion;

    private static boolean legacyMcVersion;

    private static LegacyHelper legacyHelper;

    static {
        INSTANCE = new SteelCore();
        boolean javacIsStupid = false;
        try {
            GameMode.valueOf("SPECTATOR");
            javacIsStupid = true;
        } catch (IllegalArgumentException illegalArgumentException) {}
        SPECTATOR_SUPPORT = javacIsStupid;
    }

    static void initializeSteel() {
        checkIfLegacyMinecraftVersion();
        VERBOSE_LOGGING = SteelMain.getInstance().getConfig().getBoolean("verbose-logging");
        CommonCore.initializeCommon();
        registerFactories();
        SteelUnsafeUtil.initialize();
        PLATFORM_UTILS = (PlatformUtils)new SteelUtils();
    }

    private static void checkIfLegacyMinecraftVersion() {
        String[] mcVersions = Bukkit.getBukkitVersion().split("-")[0].split("\\.");
        mcVersion = Integer.parseInt(mcVersions[0]) * 1000000 + Integer.parseInt(mcVersions[1]) * 1000 + ((mcVersions.length > 2) ? Integer.parseInt(mcVersions[2]) : 0);
        legacyMcVersion = (mcVersion < 1013000);
        if (legacyMcVersion) {
            legacyHelper = new LegacyHelper();
        } else {
            //logWarning("This server is running Minecraft version 1.13 or later.");
            //ogWarning("Steel's support for this version may be incomplete or unstable.");
            //logWarning("Please report any issues at https://github.com/caseif/Steel/issues");
        }
    }

    private static void registerFactories() {
        FactoryRegistry.registerFactory(Arena.class, (Factory)new ArenaFactory());
        FactoryRegistry.registerFactory(LobbySign.class, (Factory)new LobbySignFactory());
        FactoryRegistry.registerFactory(Minigame.class, (Factory)new MinigameFactory());
        FactoryRegistry.registerFactory(IRollbackAgent.class, (Factory)new RollbackAgentFactory());
        FactoryRegistry.registerFactory(Round.class, (Factory)new RoundFactory());
    }

    public static boolean isLegacy() {
        return legacyMcVersion;
    }

    public static int getMcVersion() {
        return mcVersion;
    }

    public static LegacyHelper getLegacyHelper() {
        Preconditions.checkState(legacyMcVersion, "Cannot get legacy helper on non-legacy platform!");
        return legacyHelper;
    }

    protected String getImplementationName0() {
        return SteelMain.getInstance().getName();
    }

    protected void logInfo0(String message) {
        SteelMain.getInstance().getLogger().info(message);
    }

    protected void logWarning0(String message) {
        SteelMain.getInstance().getLogger().warning(message);
    }

    protected void logSevere0(String message) {
        SteelMain.getInstance().getLogger().severe(message);
    }

    protected void logVerbose0(String message) {
        if (VERBOSE_LOGGING)
            logInfo0("[VERBOSE] " + message);
    }

    protected void orphan0(final CommonComponent<?> component) {
        Bukkit.getScheduler().runTask(SteelMain.getInstance(), new Runnable() {
            public void run() {
                component.setOrphanFlag();
            }
        });
    }

    protected IChatAgent getChatAgent0() {
        return (IChatAgent)CHAT_AGENT;
    }
}
