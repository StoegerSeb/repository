package com.sap.sepp.api.steel.lobby.type;

import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.lobby.type.ChallengerListingLobbySign;
import com.sap.sepp.api.steel.lobby.SteelLobbySign;
import com.sap.sepp.api.utils.physical.Location3D;

/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelChallengerListingLobbySign: 07.04.2020 16:34 by sebip
*/
public class SteelChallengerListingLobbySign extends SteelLobbySign implements ChallengerListingLobbySign {
    private final int index;

    public SteelChallengerListingLobbySign(Location3D location, CommonArena arena, int index) {
        super(location, arena, LobbySign.Type.CHALLENGER_LISTING);
        this.index = index;
    }

    public LobbySign.Type getType() {
        return LobbySign.Type.CHALLENGER_LISTING;
    }

    public int getIndex() throws OrphanedComponentException {
        checkState();
        return this.index;
    }
}
