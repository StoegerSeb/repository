package com.sap.sepp.api.common.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonPlayerHelper: 07.04.2020 16:44 by sebip
*/

import com.google.common.base.Optional;
import com.google.gson.*;
import com.sap.sepp.api.common.CommonCore;
import com.sap.sepp.api.common.utils.file.CommonDataFiles;
import com.sap.sepp.api.utils.physical.Location3D;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

public class CommonPlayerHelper {
    public static void setOfflineFlag(UUID player) {
        try {
            JsonArray json;
            File playerStore = CommonDataFiles.OFFLINE_PLAYER_STORE.getFile();
            boolean isNew = false;
            if (!playerStore.exists())
                playerStore.createNewFile();
            try (FileReader reader = new FileReader(playerStore)) {
                JsonElement el = (new JsonParser()).parse(reader);
                json = el.isJsonArray() ? el.getAsJsonArray() : new JsonArray();
                json.add(new JsonPrimitive(player.toString()));
            }
            try (FileWriter writer = new FileWriter(playerStore)) {
                writer.write(json.toString());
            }
        } catch (IOException ex) {
            CommonCore.logSevere("Failed to mark player as offline!");
            ex.printStackTrace();
        }
    }

    public static boolean checkOfflineFlag(UUID player) {
        try {
            JsonArray json;
            File playerStore = CommonDataFiles.OFFLINE_PLAYER_STORE.getFile();
            if (!playerStore.exists())
                return false;
            try (FileReader reader = new FileReader(playerStore)) {
                JsonElement el = (new JsonParser()).parse(reader);
                if (!el.isJsonArray())
                    return false;
                json = el.getAsJsonArray();
            }
            if (json.size() > 0) {
                JsonArray newArray = new JsonArray();
                Iterator<JsonElement> it = json.iterator();
                boolean found = false;
                while (it.hasNext()) {
                    JsonElement el = it.next();
                    if (el.getAsString().equals(player.toString())) {
                        found = true;
                        continue;
                    }
                    newArray.add(el);
                }
                if (found) {
                    try (FileWriter writer = new FileWriter(playerStore)) {
                        writer.write(newArray.toString());
                    }
                    return true;
                }
            }
            return false;
        } catch (IOException ex) {
            CommonCore.logSevere("Failed to mark player as offline!");
            throw new RuntimeException(ex);
        }
    }

    public static void storeLocation(UUID player, Location3D location) throws IOException {
        File store = CommonDataFiles.PLAYER_LOCATION_STORE.getFile();
        JsonObject json = JsonHelper.readOrCreateJson(store);
        json.add(player.toString(), JsonSerializer.serializeLocation(location));
        try (FileWriter writer = new FileWriter(store)) {
            writer.append(json.toString());
        }
    }

    public static Optional<Location3D> getReturnLocation(UUID player) throws IllegalArgumentException, IOException {
        JsonObject json;
        File store = CommonDataFiles.PLAYER_LOCATION_STORE.getFile();
        if (store.exists()) {
            try (FileReader reader = new FileReader(store)) {
                JsonElement el = (new JsonParser()).parse(reader);
                if (!el.isJsonObject())
                    return Optional.absent();
                json = el.getAsJsonObject();
            }
        } else {
            return Optional.absent();
        }
        if (json.has(player.toString())) {
            Location3D l3d = JsonSerializer.deserializeLocation(json.getAsJsonObject(player.toString()));
            json.remove(player.toString());
            try (FileWriter writer = new FileWriter(store)) {
                writer.append(json.toString());
            }
            if (!l3d.getWorld().isPresent())
                throw new IllegalArgumentException("World not present in stored location of player " + player);
            return Optional.of(l3d);
        }
        return Optional.absent();
    }
}