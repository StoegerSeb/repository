package com.sap.sepp.api.common.metadata.persist;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonPersistentMetadataHolder: 07.04.2020 16:56 by sebip
*/

import com.sap.sepp.api.common.metadata.CommonMetadataHolder;
import com.sap.sepp.api.metadata.persist.PersistentMetadata;
import com.sap.sepp.api.metadata.persist.PersistentMetadataHolder;

public class CommonPersistentMetadataHolder extends CommonMetadataHolder implements PersistentMetadataHolder {
    private PersistentMetadata persistentMetadata = new CommonPersistentMetadata();

    public PersistentMetadata getPersistentMetadata() {
        return this.persistentMetadata;
    }

    @Deprecated
    public PersistentMetadata getPersistableMetadata() {
        return getPersistentMetadata();
    }
}
