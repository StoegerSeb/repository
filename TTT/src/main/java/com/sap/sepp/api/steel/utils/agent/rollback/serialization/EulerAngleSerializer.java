package com.sap.sepp.api.steel.utils.agent.rollback.serialization;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class EulerAnglerSerializer: 07.04.2020 16:14 by sebip
*/

import com.sap.sepp.api.serialization.Serializer;
import org.bukkit.util.EulerAngle;

public class EulerAngleSerializer implements Serializer<EulerAngle> {
    private static EulerAngleSerializer instance;

    public static EulerAngleSerializer getInstance() {
        return (instance != null) ? instance : (instance = new EulerAngleSerializer());
    }

    public String serialize(EulerAngle angle) {
        return "(" + angle.getX() + "," + angle.getY() + "," + angle.getZ() + ")";
    }

    public EulerAngle deserialize(String serial) throws IllegalArgumentException {
        if (serial.startsWith("(") && serial.endsWith(")")) {
            String[] arr = serial.substring(1, serial.length() - 1).split(",");
            if (arr.length == 3)
                try {
                    double x = Double.parseDouble(arr[0]);
                    double y = Double.parseDouble(arr[0]);
                    double z = Double.parseDouble(arr[0]);
                    return new EulerAngle(x, y, z);
                } catch (NumberFormatException numberFormatException) {}
        }
        throw new IllegalArgumentException("Invalid serial for EulerAngle");
    }
}
