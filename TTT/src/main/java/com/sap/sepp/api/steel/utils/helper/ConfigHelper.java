package com.sap.sepp.api.steel.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class ConfigHelper: 07.04.2020 16:20 by sebip
*/

public class ConfigHelper {
    public static void addMissingKeys() throws InvalidConfigurationException, IOException {
        BufferedReader is = new BufferedReader(new InputStreamReader(ConfigHelper.class.getResourceAsStream("/config.yml")));
        File configYml = new File(SteelMain.getInstance().getDataFolder(), "config.yml");
        YamlConfiguration yml = new YamlConfiguration();
        yml.load(configYml);
        StringBuilder sb = new StringBuilder();
        char NEWLINE_CHAR = '\n';
        String line;
        while ((line = is.readLine()) != null) {
            if (!line.startsWith("#") &&
                    line.contains(":")) {
                String key = line.split(":")[0];
                String value = line.substring(key.length() + 1, line.length()).trim();
                String newValue = yml.contains(key.trim()) ? yml.getString(key.trim()) : value;
                boolean equal = false;
                try {
                    equal = NumberFormat.getInstance().parse(value).equals(NumberFormat.getInstance().parse(newValue));
                } catch (ParseException ex) {
                    equal = value.equals(newValue);
                }
                if (!equal) {
                    String writeValue = yml.getString(key.trim());
                    try {
                        double d = Double.parseDouble(writeValue);
                        writeValue = BigDecimal.valueOf(d).stripTrailingZeros().toPlainString();
                    } catch (NumberFormatException numberFormatException) {}
                    sb.append(key).append(": ").append(writeValue).append('\n');
                    continue;
                }
            }
            sb.append(line).append('\n');
        }
        if (!configYml.renameTo(new File(configYml.getParentFile(), "config.yml.old"))) {
            Files.copy(configYml, new File(configYml.getParentFile(), "config.yml.old"));
            configYml.delete();
        }
        configYml.createNewFile();
        FileWriter w = new FileWriter(configYml);
        w.append(sb.toString());
        w.flush();
    }
}