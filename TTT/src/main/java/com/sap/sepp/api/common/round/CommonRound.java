package com.sap.sepp.api.common.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class CommonRound: 07.04.2020 16:57 by sebip
*/

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import com.sap.sepp.api.arena.Arena;
import com.sap.sepp.api.arena.SpawningMode;
import com.sap.sepp.api.challenger.Challenger;
import com.sap.sepp.api.challenger.Team;
import com.sap.sepp.api.common.arena.CommonArena;
import com.sap.sepp.api.common.challenger.CommonChallenger;
import com.sap.sepp.api.common.challenger.CommonTeam;
import com.sap.sepp.api.common.component.CommonComponent;
import com.sap.sepp.api.common.event.round.*;
import com.sap.sepp.api.common.metadata.CommonMetadataHolder;
import com.sap.sepp.api.common.minigame.CommonMinigame;
import com.sap.sepp.api.component.exception.OrphanedComponentException;
import com.sap.sepp.api.config.ConfigNode;
import com.sap.sepp.api.config.RoundConfigNode;
import com.sap.sepp.api.lobby.LobbySign;
import com.sap.sepp.api.round.LifecycleStage;
import com.sap.sepp.api.round.Round;
import com.sap.sepp.api.utils.physical.Location3D;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class CommonRound extends CommonMetadataHolder implements Round, CommonComponent<Arena> {
    private AtomicInteger nextSpawn = new AtomicInteger();

    private final CommonArena arena;

    private final BiMap<UUID, Challenger> challengers = HashBiMap.create();

    private final BiMap<String, Team> teams = HashBiMap.create();

    private final HashMap<RoundConfigNode<?>, Object> config = new HashMap<>();

    private final ImmutableSet<LifecycleStage> stages;

    protected boolean orphan = false;

    protected boolean ending;

    protected int currentStage = 0;

    private long time;

    private boolean timerTicking = true;

    public CommonRound(CommonArena arena, ImmutableSet<LifecycleStage> stages) {
        assert arena != null;
        assert stages != null;
        this.arena = arena;
        this.stages = stages;
    }

    public Arena getOwner() {
        checkState();
        return this.arena;
    }

    public Arena getArena() {
        return getOwner();
    }

    public boolean isEnding() {
        return this.ending;
    }

    public ImmutableList<Challenger> getChallengers() {
        checkState();
        return ImmutableList.copyOf(this.challengers.values());
    }

    public Optional<Challenger> getChallenger(UUID uuid) throws OrphanedComponentException {
        checkState();
        return Optional.fromNullable(this.challengers.get(uuid));
    }

    public void removeChallenger(UUID uuid) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        Challenger c = this.challengers.get(uuid);
        if (c == null)
            throw new IllegalArgumentException("Could not get challenger from UUID " + uuid);
        removeChallenger(c);
    }

    public void removeChallenger(Challenger challenger, boolean isDisconnecting, boolean updateSigns) throws OrphanedComponentException {
        checkState();
        if (challenger.getRound() != this)
            throw new IllegalArgumentException("Cannot remove challenger: round mismatch");
        ((CommonChallenger)challenger).setLeavingFlag();
        if (!isEnding()) {
            this.challengers.remove(challenger.getUniqueId());
            if (updateSigns)
                for (UnmodifiableIterator<LobbySign> unmodifiableIterator = getArena().getLobbySigns().iterator(); unmodifiableIterator.hasNext(); ) {
                    LobbySign sign = unmodifiableIterator.next();
                    sign.update();
                }
        }
        challenger.setSpectating(false);
        challenger.setTeam(null);
    }

    public void removeChallenger(Challenger challenger) throws OrphanedComponentException {
        removeChallenger(challenger, false, true);
    }

    public Location3D nextSpawnPoint() {
        int spawnIndex;
        switch (getConfigValue(ConfigNode.SPAWNING_MODE)) {
            case RANDOM:
                return getArena().getSpawnPoints().values().asList()
                        .get((int)Math.floor(Math.random() * getArena().getSpawnPoints().size()));
            case SEQUENTIAL:
                spawnIndex = this.nextSpawn.getAndIncrement();
                if (this.nextSpawn.intValue() == getArena().getSpawnPoints().size())
                    this.nextSpawn.set(0);
                return getArena().getSpawnPoints().values().asList().get(spawnIndex);
            case SHUFFLE:
                spawnIndex = this.nextSpawn.getAndIncrement();
                if (this.nextSpawn.intValue() == getArena().getSpawnPoints().size())
                    this.nextSpawn.set(0);
                return this.arena.getShuffledSpawnPoints().asList().get(spawnIndex);
        }
        throw new UnsupportedOperationException();
    }

    public ImmutableList<Team> getTeams() throws OrphanedComponentException {
        checkState();
        return ImmutableList.copyOf(this.teams.values());
    }

    public Optional<Team> getTeam(String id) throws OrphanedComponentException {
        checkState();
        return Optional.fromNullable(this.teams.get(id));
    }

    public Team createTeam(String id) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        if (this.teams.containsKey(id))
            throw new IllegalArgumentException("Team \"" + id + "\" already exists");
        CommonTeam commonTeam = new CommonTeam(id, this);
        this.teams.put(id, commonTeam);
        return commonTeam;
    }

    public Team getOrCreateTeam(String id) throws OrphanedComponentException {
        checkState();
        Optional<Team> team = getTeam(id);
        return team.isPresent() ? team.get() : createTeam(id);
    }

    public void removeTeam(String id) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        Team team = this.teams.get(id);
        if (team == null)
            throw new IllegalArgumentException("Cannot get team with ID " + id + " in round in " + this.arena.getId());
        removeTeam(team);
    }

    public void removeTeam(Team team) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        if (this.teams.get(team.getId()) != team)
            throw new IllegalArgumentException("Team " + team.getId() + " is owned by a different round");
        this.teams.remove(team.getId());
        ((CommonTeam)team).orphan();
    }

    public ImmutableList<Challenger> getSpectators() throws OrphanedComponentException {
        checkState();
        return ImmutableList.copyOf(Collections2.filter((Collection)getChallengers(), new Predicate<Challenger>() {
            public boolean apply(Challenger challenger) {
                return challenger.isSpectating();
            }
        }));
    }

    public ImmutableSet<LifecycleStage> getLifecycleStages() throws OrphanedComponentException {
        checkState();
        return this.stages;
    }

    public LifecycleStage getLifecycleStage() throws OrphanedComponentException {
        checkState();
        return (LifecycleStage)getLifecycleStages().toArray()[this.currentStage];
    }

    public void setLifecycleStage(LifecycleStage stage, boolean resetTimer) throws IllegalArgumentException, OrphanedComponentException {
        checkState();
        if (this.stages.contains(stage)) {
            if (!stage.equals(getLifecycleStage())) {
                UnmodifiableIterator<LifecycleStage> unmodifiableIterator = this.stages.iterator();
                int i = 0;
                while (unmodifiableIterator.hasNext() &&
                        !unmodifiableIterator.next().equals(stage))
                    i++;
                this.currentStage = i;
                if (resetTimer)
                    this.time = 0L;
                getArena().getMinigame().getEventBus()
                        .post(new CommonRoundChangeLifecycleStageEvent(this, getLifecycleStage(), stage));
            }
        } else {
            throw new IllegalArgumentException("Invalid lifecycle stage");
        }
    }

    public void setLifecycleStage(LifecycleStage stage) throws IllegalArgumentException, OrphanedComponentException {
        setLifecycleStage(stage, false);
    }

    public Optional<LifecycleStage> getLifecycleStage(String id) throws OrphanedComponentException {
        checkState();
        for (UnmodifiableIterator<LifecycleStage> unmodifiableIterator = this.stages.iterator(); unmodifiableIterator.hasNext(); ) {
            LifecycleStage stage = unmodifiableIterator.next();
            if (stage.getId().equals(id))
                return Optional.of(stage);
        }
        return Optional.absent();
    }

    public LifecycleStage getLifecycleStage(int index) throws OrphanedComponentException {
        checkState();
        if (index >= this.stages.size())
            throw new IndexOutOfBoundsException();
        return this.stages.asList().get(index);
    }

    public Optional<LifecycleStage> getNextLifecycleStage() throws OrphanedComponentException {
        checkState();
        return Optional.fromNullable(
                (this.currentStage < this.stages.size() - 1) ?
                        getLifecycleStages().toArray()[this.currentStage + 1] : null);
    }

    public void nextLifecycleStage() throws IllegalStateException {
        checkState();
        Optional<LifecycleStage> next = getNextLifecycleStage();
        if (!next.isPresent())
            throw new IllegalStateException("Current lifecycle stage is last defined");
        setLifecycleStage(next.get());
        setTime(0L);
    }

    public long getTime() throws OrphanedComponentException {
        checkState();
        return this.time;
    }

    public void setTime(long time) throws OrphanedComponentException {
        checkState();
        setTime(time, true);
    }

    public void setTime(long time, boolean callEvent) throws OrphanedComponentException {
        checkState();
        this.time = time;
        if (callEvent)
            getArena().getMinigame().getEventBus().post(new CommonRoundTimerChangeEvent(this, getTime(), time));
    }

    public long getRemainingTime() throws OrphanedComponentException {
        checkState();
        return (getLifecycleStage().getDuration() == -1) ? -1L : (getLifecycleStage().getDuration() - this.time);
    }

    public void resetTimer() throws OrphanedComponentException {
        checkState();
        setTimerTicking(false);
        this.time = 0L;
        setLifecycleStage(getLifecycleStages().asList().get(0));
    }

    public void end() throws IllegalStateException {
        checkState();
        end(new Round.EndParameter[0]);
    }

    public void end(boolean rollback) throws IllegalStateException {
        checkState();
        end(rollback ? EndParameter.RollbackBehavior.DO_ROLLBACK : EndParameter.RollbackBehavior.SKIP_ROLLBACK);
    }

    public boolean isTimerTicking() throws OrphanedComponentException {
        checkState();
        return this.timerTicking;
    }

    public void setTimerTicking(boolean ticking) throws OrphanedComponentException {
        checkState();
        if (ticking != isTimerTicking()) {
            this.timerTicking = ticking;
            getArena().getMinigame().getEventBus()
                    .post(ticking ? new CommonRoundTimerStartEvent(this) : new CommonRoundTimerStopEvent(this));
        }
    }

    public void end(Round.EndParameter... params) throws IllegalStateException {
        checkState();
        if (this.ending)
            throw new IllegalStateException("Cannot invoke end() on a round more than once");
        this.ending = true;
        cancelTimerTask();
        for (UnmodifiableIterator<Challenger> unmodifiableIterator1 = getChallengers().iterator(); unmodifiableIterator1.hasNext(); ) {
            Challenger challenger = unmodifiableIterator1.next();
            removeChallenger(challenger, false, false);
        }
        List<EndParameter> paramList = Arrays.asList(params);
        if (getConfigValue(ConfigNode.ROLLBACK_ON_END).booleanValue() || paramList
                .contains(Round.EndParameter.RollbackBehavior.DO_ROLLBACK))
            getArena().rollback();
        getArena().getMinigame().getEventBus()
                .post(new CommonRoundEndEvent(this, paramList.contains(NaturalEnd.NATURAL)));
        UnmodifiableIterator<Challenger> unmodifiableIterator2;
        for (unmodifiableIterator2 = getChallengers().iterator(); unmodifiableIterator2.hasNext(); ) {
            Challenger challenger = unmodifiableIterator2.next();
            ((CommonChallenger)challenger).orphan();
        }
        ((CommonMinigame)getArena().getMinigame()).getRoundMap().remove(getArena());
        for (unmodifiableIterator2 = getArena().getLobbySigns().iterator(); unmodifiableIterator2.hasNext(); ) {
            LobbySign ls = (LobbySign)unmodifiableIterator2.next();
            ls.update();
        }
        orphan();
    }

    public <T> T getConfigValue(RoundConfigNode<T> node) throws OrphanedComponentException {
        checkState();
        return this.config.containsKey(node) ? (T)this.config.get(node) : (T)getArena().getMinigame().getConfigValue((ConfigNode)node);
    }

    public <T> void setConfigValue(RoundConfigNode<T> node, T value) throws OrphanedComponentException {
        checkState();
        Preconditions.checkNotNull(node, "node");
        Preconditions.checkNotNull(value, "value");
        this.config.put(node, value);
        if (node == ConfigNode.RANDOM_SPAWNING)
            this.config.put(ConfigNode.SPAWNING_MODE, ((Boolean)value).booleanValue() ? SpawningMode.RANDOM : SpawningMode.SEQUENTIAL);
    }

    public Map<UUID, Challenger> getChallengerMap() {
        checkState();
        return (Map<UUID, Challenger>)this.challengers;
    }

    public Map<String, Team> getTeamMap() {
        checkState();
        return (Map<String, Team>)this.teams;
    }

    public void checkState() throws OrphanedComponentException {
        if (this.orphan)
            throw new OrphanedComponentException(this);
    }

    public void orphan() {
        CommonCore.orphan(this);
    }

    public void setOrphanFlag() {
        this.orphan = true;
    }

    public boolean isOrphaned() {
        return this.orphan;
    }

    protected abstract void cancelTimerTask();

    public static class NaturalEnd implements Round.EndParameter {
        public static final NaturalEnd NATURAL = new NaturalEnd();
    }
}