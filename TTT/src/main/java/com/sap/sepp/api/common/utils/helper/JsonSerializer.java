package com.sap.sepp.api.common.utils.helper;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class JsonSerializer: 07.04.2020 16:45 by sebip
*/

import com.google.common.collect.UnmodifiableIterator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sap.sepp.api.metadata.persist.PersistentMetadata;
import com.sap.sepp.api.utils.physical.Location3D;

import java.util.Map;

public class JsonSerializer {
    private static String LOC_WORLD_KEY = "world";

    private static String LOC_X_KEY = "x";

    private static String LOC_Y_KEY = "y";

    private static String LOC_Z_KEY = "z";

    public static void serializeMetadata(JsonObject json, PersistentMetadata data) {
        for (UnmodifiableIterator<String> unmodifiableIterator = data.getAllKeys().iterator(); unmodifiableIterator.hasNext(); ) {
            String key = unmodifiableIterator.next();
            if (data.get(key).get() instanceof String) {
                json.addProperty(key, (String)data.get(key).get());
                continue;
            }
            if (data.get(key).get() instanceof PersistentMetadata) {
                JsonObject subsection = new JsonObject();
                serializeMetadata(subsection, (PersistentMetadata)data.get(key).get());
                json.add(key, subsection);
            }
        }
    }

    public static void deserializeMetadata(JsonObject json, PersistentMetadata parent) {
        for (Map.Entry<String, JsonElement> entry : json.entrySet()) {
            if (json.get(entry.getKey()).isJsonObject()) {
                deserializeMetadata(json.getAsJsonObject(entry.getKey()), parent.createStructure(entry.getKey()));
                continue;
            }
            if (json.get(entry.getKey()).isJsonPrimitive())
                parent.set(entry.getKey(), json.get(entry.getKey()).getAsString());
        }
    }

    public static JsonObject serializeLocation(Location3D location) {
        JsonObject json = new JsonObject();
        if (location.getWorld().isPresent())
            json.addProperty(LOC_WORLD_KEY, location.getWorld().get());
        json.addProperty(LOC_X_KEY, location.getX());
        json.addProperty(LOC_Y_KEY, location.getY());
        json.addProperty(LOC_Z_KEY, location.getZ());
        return json;
    }

    public static Location3D deserializeLocation(JsonObject json) {
        String world = json.has(LOC_WORLD_KEY) ? json.get(LOC_WORLD_KEY).getAsString() : null;
        double x = json.get(LOC_X_KEY).getAsDouble();
        double y = json.get(LOC_Y_KEY).getAsDouble();
        double z = json.get(LOC_Z_KEY).getAsDouble();
        return new Location3D(world, x, y, z);
    }
}