package com.sap.sepp.api.steel.round;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class SteelRoundWorker: 07.04.2020 16:36 by sebip
*/


class SteelRoundWorker extends CommonRoundWorker {
    SteelRoundWorker(CommonRound round) {
        super(round);
    }

    protected void checkPlayerLocations() {
        Boundary bound = getRound().getArena().getBoundary();
        for (UnmodifiableIterator<Challenger> unmodifiableIterator = getRound().getChallengers().iterator(); unmodifiableIterator.hasNext(); ) {
            Challenger challenger = unmodifiableIterator.next();
            if (((CommonChallenger)challenger).isLeaving())
                return;
            Player player = Bukkit.getPlayer(challenger.getUniqueId());
            Location3D loc = LocationHelper.convertLocation(player.getLocation());
            if (!bound.contains(loc)) {
                if (((Boolean)getRound().getConfigValue(ConfigNode.ALLOW_EXIT_BOUNDARY)).booleanValue()) {
                    challenger.removeFromRound();
                    continue;
                }
                if (loc.getY() < 0.0D) {
                    Location3D nextSpawn = challenger.getRound().nextSpawnPoint();
                    player.setFallDistance(0.0F);
                    player.teleport(new Location(player.getWorld(), nextSpawn.getX(), nextSpawn.getY(), nextSpawn
                            .getZ(), player.getLocation().getYaw(), player.getLocation().getPitch()));
                    continue;
                }
                double x = (loc.getX() > bound.getUpperBound().getX()) ? bound.getUpperBound().getX() : ((loc.getX() < bound.getLowerBound().getX()) ? bound.getLowerBound().getX() : loc.getX());
                double y = (loc.getY() > bound.getUpperBound().getY()) ? bound.getUpperBound().getY() : ((loc.getY() < bound.getLowerBound().getY()) ? bound.getLowerBound().getY() : loc.getY());
                double z = (loc.getZ() > bound.getUpperBound().getZ()) ? bound.getUpperBound().getZ() : ((loc.getZ() < bound.getLowerBound().getZ()) ? bound.getLowerBound().getZ() : loc.getZ());
                player.teleport(new Location(player.getWorld(), x, y, z, player
                        .getLocation().getYaw(), player.getLocation().getPitch()));
            }
        }
    }
}