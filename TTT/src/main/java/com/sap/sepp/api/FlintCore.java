package com.sap.sepp.api;
/*
# (C) Copyright 2020 TTT (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The TTT-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class FlintCore: 07.04.2020 15:44 by sebip
*/

import com.sap.sepp.api.minigame.Minigame;

public abstract class FlintCore {
    protected static FlintCore INSTANCE;

    private static final int API_REVISION = 5;

    private static final int MAJOR_VERSION = 1;

    private static final String CODENAME = "Dagur";

    public static int getApiRevision() {
        return 5;
    }

    public static int getMajorVersion() {
        return 1;
    }

    protected static String getCodename() {
        return "Dagur";
    }

    public static String getImplementationName() {
        return INSTANCE.getImplementationName0();
    }

    protected abstract String getImplementationName0();

    public static Minigame registerPlugin(String pluginId) throws IllegalStateException {
        return INSTANCE.registerPlugin0(pluginId);
    }

    protected abstract Minigame registerPlugin0(String paramString) throws IllegalStateException;
}
