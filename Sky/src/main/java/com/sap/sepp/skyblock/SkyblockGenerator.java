package com.sap.sepp.skyblock;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

class SkyblockGenerator extends ChunkGenerator {
    public byte[][] blockSections;

    public ChunkGenerator.ChunkData generateChunkData(World world, Random random, int cx, int cz, ChunkGenerator.BiomeGrid biomeGrid) {
        Biome biome;
        ChunkGenerator.ChunkData chunkData = createChunkData(world);
        Config config = IridiumSkyblock.getConfiguration();
        String worldName = world.getName();
        if (worldName.equals(config.worldName)) {
            biome = config.defaultBiome.parseBiome();
        } else if (worldName.equals(config.netherWorldName)) {
            biome = config.defaultNetherBiome.parseBiome();
        } else {
            return chunkData;
        }
        for (int x = 0; x <= 15; x++) {
            for (int z = 0; z <= 15; z++)
                biomeGrid.setBiome(x, z, biome);
        }
        return chunkData;
    }

    public byte[][] generateBlockSections(World world, Random random, int x, int z, ChunkGenerator.BiomeGrid biomes) {
        if (this.blockSections == null)
            this.blockSections = new byte[world.getMaxHeight() / 16][];
        return this.blockSections;
    }

    public boolean canSpawn(World world, int x, int z) {
        return true;
    }

    public List<BlockPopulator> getDefaultPopulators(World world) {
        return Collections.emptyList();
    }
}
