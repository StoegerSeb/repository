package com.sap.sepp.skyblock;

public enum Color {
    Blue, Green, Red, Off;
}
