package com.sap.sepp.skyblock;

import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.inventory.ItemStack;
import org.jnbt.ByteArrayTag;
import org.jnbt.ByteTag;
import org.jnbt.CompoundTag;
import org.jnbt.IntArrayTag;
import org.jnbt.IntTag;
import org.jnbt.ListTag;
import org.jnbt.NBTInputStream;
import org.jnbt.ShortTag;
import org.jnbt.StringTag;
import org.jnbt.Tag;

public class Schematic {
    public static Map<String, Schematic> cache = new HashMap<>();

    private final short width;

    private final short length;

    private final short height;

    private List<Tag> tileEntities;

    private byte[] blocks;

    private byte[] data;

    private byte[] blockdata;

    private Map<String, Tag> palette;

    private final File file;

    private Integer version;

    private final SchematicVersion schematicVersion;

    public enum SchematicVersion {
        v1_13, v_1_8;
    }

    public short getWidth() {
        return this.width;
    }

    public short getLength() {
        return this.length;
    }

    public short getHeight() {
        return this.height;
    }

    public byte[] getBlocks() {
        return this.blocks;
    }

    public byte[] getData() {
        return this.data;
    }

    public Schematic(File file, short width, short length, short height, List<Tag> tileEntities, byte[] blocks, byte[] data, List<Tag> entities) {
        this.blocks = blocks;
        this.data = data;
        this.width = width;
        this.length = length;
        this.height = height;
        this.tileEntities = tileEntities;
        this.schematicVersion = SchematicVersion.v_1_8;
        this.file = file;
    }

    public Schematic(File file, short width, short length, short height, byte[] blockdata, Map<String, Tag> palette, int version) {
        this.width = width;
        this.length = length;
        this.height = height;
        this.palette = palette;
        this.blockdata = blockdata;
        this.schematicVersion = SchematicVersion.v1_13;
        this.version = Integer.valueOf(version);
        this.file = file;
    }

    public Schematic(File file, short width, short length, short height, List<Tag> tileEntities, byte[] blockdata, Map<String, Tag> palette, int version) {
        this.width = width;
        this.length = length;
        this.height = height;
        this.palette = palette;
        this.blockdata = blockdata;
        this.schematicVersion = SchematicVersion.v1_13;
        this.tileEntities = tileEntities;
        this.version = Integer.valueOf(version);
        this.file = file;
    }

    public void pasteSchematic(Location loc, Island island) {
        short length = getLength();
        short width = getWidth();
        short height = getHeight();
        loc.subtract(width / 2.0D, height / 2.0D, length / 2.0D);
        if (this.schematicVersion == SchematicVersion.v_1_8) {
            if (IridiumSkyblock.worldEdit != null &&
                    IridiumSkyblock.worldEdit.version() == 6) {
                IridiumSkyblock.worldEdit.paste(this.file, loc, island);
                return;
            }
            byte[] blocks = getBlocks();
            byte[] blockData = getData();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    for (int z = 0; z < length; z++) {
                        int index = y * width * length + z * width + x;
                        Block block = (new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ())).getBlock();
                        IridiumSkyblock.nms.setBlockFast(block, blocks[index], blockData[index]);
                    }
                }
            }
            if (this.tileEntities != null)
                for (Tag tag : this.tileEntities) {
                    if (!(tag instanceof CompoundTag))
                        continue;
                    CompoundTag t = (CompoundTag) tag;
                    Map<String, Tag> tags = t.getValue();
                    int i = ((IntTag) getChildTag(tags, "x", IntTag.class)).getValue().intValue();
                    int y = ((IntTag) getChildTag(tags, "y", IntTag.class)).getValue().intValue();
                    int z = ((IntTag) getChildTag(tags, "z", IntTag.class)).getValue().intValue();
                    Block block = (new Location(loc.getWorld(), i + loc.getX(), y + loc.getY(), z + loc.getZ())).getBlock();
                    String id = ((StringTag) getChildTag(tags, "id", StringTag.class)).getValue().toLowerCase().replace("minecraft:", "");
                    if (id.equalsIgnoreCase("chest")) {
                        List<Tag> items = ((ListTag) getChildTag(tags, "Items", ListTag.class)).getValue();
                        if (block.getState() instanceof Chest) {
                            Chest chest = (Chest) block.getState();
                            for (Tag item : items) {
                                if (!(item instanceof CompoundTag))
                                    continue;
                                Map<String, Tag> itemtag = ((CompoundTag) item).getValue();
                                byte slot = ((ByteTag) getChildTag(itemtag, "Slot", ByteTag.class)).getValue().byteValue();
                                String name = ((StringTag) getChildTag(itemtag, "id", StringTag.class)).getValue().toLowerCase().replace("minecraft:", "");
                                Byte amount = ((ByteTag) getChildTag(itemtag, "Count", ByteTag.class)).getValue();
                                short damage = ((ShortTag) getChildTag(itemtag, "Damage", ShortTag.class)).getValue().shortValue();
                                XMaterial material = XMaterial.requestOldXMaterial(name.toUpperCase(), (byte) damage);
                                if (material != null) {
                                    ItemStack itemStack = material.parseItem(true);
                                    if (itemStack != null) {
                                        itemStack.setAmount(amount.byteValue());
                                        chest.getBlockInventory().setItem(slot, itemStack);
                                    }
                                }
                            }
                        }
                        continue;
                    }
                    if (id.equalsIgnoreCase("sign") &&
                            block.getState() instanceof Sign) {
                        Sign sign = (Sign) block.getState();
                        JsonParser parser = new JsonParser();
                        String line1 = parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsString().isEmpty() ? "" : parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                        String line2 = parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsString().isEmpty() ? "" : parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                        String line3 = parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsString().isEmpty() ? "" : parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                        String line4 = parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsString().isEmpty() ? "" : parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                        if (!parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsString().isEmpty() && parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                            line1 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line1;
                        if (!parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsString().isEmpty() && parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                            line2 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line2;
                        if (!parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsString().isEmpty() && parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                            line3 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line3;
                        if (!parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsString().isEmpty() && parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                            line4 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line4;
                        sign.setLine(0, line1);
                        sign.setLine(1, line2);
                        sign.setLine(2, line3);
                        sign.setLine(3, line4);
                        sign.update(true);
                    }
                }
        } else {
            if (IridiumSkyblock.worldEdit != null &&
                    IridiumSkyblock.worldEdit.version() == 7) {
                IridiumSkyblock.worldEdit.paste(this.file, loc, island);
                return;
            }
            if (XMaterial.ISFLAT) {
                try {
                    for (int x = 0; x < width; x++) {
                        for (int y = 0; y < height; y++) {
                            for (int z = 0; z < length; z++) {
                                int index = y * width * length + z * width + x;
                                Block block = (new Location(loc.getWorld(), x + loc.getX(), y + loc.getY(), z + loc.getZ())).getBlock();
                                for (String s : this.palette.keySet()) {
                                    int i = ((IntTag) getChildTag(this.palette, s, IntTag.class)).getValue().intValue();
                                    if (this.blockdata[index] == i)
                                        block.setBlockData(Bukkit.createBlockData(s), false);
                                }
                            }
                        }
                    }
                    if (this.version.intValue() == 2)
                        if (this.tileEntities != null)
                            for (Tag tag : this.tileEntities) {
                                if (!(tag instanceof CompoundTag))
                                    continue;
                                CompoundTag t = (CompoundTag) tag;
                                Map<String, Tag> tags = t.getValue();
                                int[] pos = ((IntArrayTag) getChildTag(tags, "Pos", IntArrayTag.class)).getValue();
                                int i = pos[0];
                                int y = pos[1];
                                int z = pos[2];
                                Block block = (new Location(loc.getWorld(), i + loc.getX(), y + loc.getY(), z + loc.getZ())).getBlock();
                                String id = ((StringTag) getChildTag(tags, "Id", StringTag.class)).getValue().toLowerCase().replace("minecraft:", "");
                                if (id.equalsIgnoreCase("chest")) {
                                    List<Tag> items = ((ListTag) getChildTag(tags, "Items", ListTag.class)).getValue();
                                    if (block.getState() instanceof Chest) {
                                        Chest chest = (Chest) block.getState();
                                        for (Tag item : items) {
                                            if (!(item instanceof CompoundTag))
                                                continue;
                                            Map<String, Tag> itemtag = ((CompoundTag) item).getValue();
                                            byte slot = ((ByteTag) getChildTag(itemtag, "Slot", ByteTag.class)).getValue().byteValue();
                                            String name = ((StringTag) getChildTag(itemtag, "id", StringTag.class)).getValue().toLowerCase().replace("minecraft:", "");
                                            Byte amount = ((ByteTag) getChildTag(itemtag, "Count", ByteTag.class)).getValue();
                                            XMaterial material = XMaterial.requestOldXMaterial(name.toUpperCase(), (byte) -1);
                                            if (material != null) {
                                                ItemStack itemStack = material.parseItem(true);
                                                if (itemStack != null) {
                                                    itemStack.setAmount(amount.byteValue());
                                                    chest.getBlockInventory().setItem(slot, itemStack);
                                                }
                                            }
                                        }
                                    }
                                    continue;
                                }
                                if (id.equalsIgnoreCase("sign") &&
                                        block.getState() instanceof Sign) {
                                    Sign sign = (Sign) block.getState();
                                    JsonParser parser = new JsonParser();
                                    String line1 = parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                                    String line2 = parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                                    String line3 = parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                                    String line4 = parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("text").getAsString().replace("[ISLAND_OWNER]", (User.getUser(island.getOwner())).name);
                                    if (parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                                        line1 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text1", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line1;
                                    if (parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                                        line2 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text2", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line2;
                                    if (parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                                        line3 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text3", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line3;
                                    if (parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().has("color"))
                                        line4 = ChatColor.valueOf(parser.parse(((StringTag) getChildTag(tags, "Text4", StringTag.class)).getValue()).getAsJsonObject().get("extra").getAsJsonArray().get(0).getAsJsonObject().get("color").getAsString().toUpperCase()) + line4;
                                    sign.setLine(0, line1);
                                    sign.setLine(1, line2);
                                    sign.setLine(2, line3);
                                    sign.setLine(3, line4);
                                    sign.update(true);
                                }
                            }
                } catch (Exception e) {
                    IridiumSkyblock.getInstance().sendErrorMessage(e);
                }
            } else {
                loc.getBlock().setType(Material.STONE);
                IridiumSkyblock.getInstance().getLogger().warning("Tried to load a 1.13+ schematic in a native minecraft version");
            }
        }
    }

    public static void debugSchematic(File file) throws IOException {
        FileInputStream stream = new FileInputStream(file);
        NBTInputStream nbtStream = new NBTInputStream(stream);
        CompoundTag schematicTag = (CompoundTag) nbtStream.readTag();
        stream.close();
        nbtStream.close();
        Map<String, Tag> schematic = schematicTag.getValue();
        for (String s : schematic.keySet())
            System.out.println(s + " - " + schematic.get(s));
    }

    public static Schematic loadSchematic(File file) throws IOException {
        if (cache.containsKey(file.getAbsolutePath()))
            return cache.get(file.getAbsolutePath());
        FileInputStream stream = new FileInputStream(file);
        NBTInputStream nbtStream = new NBTInputStream(stream);
        CompoundTag schematicTag = (CompoundTag) nbtStream.readTag();
        stream.close();
        nbtStream.close();
        Map<String, Tag> schematic = schematicTag.getValue();
        short width = ((ShortTag) getChildTag(schematic, "Width", ShortTag.class)).getValue().shortValue();
        short length = ((ShortTag) getChildTag(schematic, "Length", ShortTag.class)).getValue().shortValue();
        short height = ((ShortTag) getChildTag(schematic, "Height", ShortTag.class)).getValue().shortValue();
        if (!schematic.containsKey("Blocks")) {
            int version = ((IntTag) getChildTag(schematic, "Version", IntTag.class)).getValue().intValue();
            Map<String, Tag> palette = ((CompoundTag) getChildTag(schematic, "Palette", CompoundTag.class)).getValue();
            byte[] blockdata = ((ByteArrayTag) getChildTag(schematic, "BlockData", ByteArrayTag.class)).getValue();
            if (version == 1) {
                List<Tag> list = ((ListTag) getChildTag(schematic, "TileEntities", ListTag.class)).getValue();
                cache.put(file.getAbsolutePath(), new Schematic(file, width, length, height, list, blockdata, palette, version));
                return new Schematic(file, width, length, height, list, blockdata, palette, version);
            }
            if (version == 2) {
                List<Tag> BlockEntities = ((ListTag) getChildTag(schematic, "BlockEntities", ListTag.class)).getValue();
                cache.put(file.getAbsolutePath(), new Schematic(file, width, length, height, blockdata, palette, version));
                return new Schematic(file, width, length, height, BlockEntities, blockdata, palette, version);
            }
            cache.put(file.getAbsolutePath(), new Schematic(file, width, length, height, blockdata, palette, version));
            return new Schematic(file, width, length, height, blockdata, palette, version);
        }
        List<Tag> TileEntities = ((ListTag) getChildTag(schematic, "TileEntities", ListTag.class)).getValue();
        String materials = ((StringTag) getChildTag(schematic, "Materials", StringTag.class)).getValue();
        if (!materials.equals("Alpha"))
            throw new IllegalArgumentException("Schematic file is not an Alpha schematic");
        byte[] blocks = ((ByteArrayTag) getChildTag(schematic, "Blocks", ByteArrayTag.class)).getValue();
        byte[] blockData = ((ByteArrayTag) getChildTag(schematic, "Data", ByteArrayTag.class)).getValue();
        List<Tag> entities = ((ListTag) getChildTag(schematic, "Entities", ListTag.class)).getValue();
        cache.put(file.getAbsolutePath(), new Schematic(file, width, length, height, TileEntities, blocks, blockData, entities));
        return new Schematic(file, width, length, height, TileEntities, blocks, blockData, entities);
    }

    public static <T extends Tag> T getChildTag(Map<String, Tag> items, String key, Class<T> expected) throws IllegalArgumentException {
        if (!items.containsKey(key))
            throw new IllegalArgumentException("Schematic file is missing a \"" + key + "\" tag");
        Tag tag = items.get(key);
        if (!expected.isInstance(tag))
            throw new IllegalArgumentException(key + " tag is not of tag type " + expected.getName());
        return expected.cast(tag);
    }
}
