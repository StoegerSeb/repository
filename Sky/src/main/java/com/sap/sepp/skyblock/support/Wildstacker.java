package com.sap.sepp.skyblock.support;

import com.bgsoftware.wildstacker.api.WildStackerAPI;
import com.sap.sepp.skyblock.IridiumSkyblock;
import org.bukkit.block.CreatureSpawner;

public class Wildstacker {

    public static boolean enabled = false;

    public Wildstacker() {
        IridiumSkyblock.getInstance().getLogger().info("Wildstacker support loaded");
        enabled = true;
    }

    public static int getSpawnerAmount(CreatureSpawner spawner) {
        return WildStackerAPI.getSpawnersAmount(spawner);
    }
}