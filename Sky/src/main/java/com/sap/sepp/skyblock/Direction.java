package com.sap.sepp.skyblock;

public enum Direction {
    NORTH, EAST, SOUTH, WEST;

    public Direction next() {
        if (this == NORTH)
            return EAST;
        if (this == EAST)
            return SOUTH;
        if (this == SOUTH)
            return WEST;
        return NORTH;
    }
}
