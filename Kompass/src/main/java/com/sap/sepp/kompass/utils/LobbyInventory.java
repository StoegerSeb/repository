package com.sap.sepp.kompass.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LobbyInventory {
    private Player player;

    public LobbyInventory(final Player player) {
        this.player = player;
    }

    public void setInventory() {
        final ItemStack navigator = new ItemManager(Material.COMPASS).setDisplayName("§4Navigator").build();
        final ItemStack hide = new ItemManager(Material.BLAZE_ROD).setDisplayName("§9Spieler verstecken").build();
        final ItemStack noGadget = new ItemManager(Material.BARRIER).setDisplayName("§cKein Gadget ausgewählt!").build();
        final ItemStack gadgets = new ItemManager(Material.CHEST).setDisplayName("§6Gadgets").build();
        final ItemStack profile = new ItemManager(Material.SLIME_BALL).setDisplayName("§aDein Profil").build();

        player.getInventory().setItem(0, navigator);
        player.getInventory().setItem(2, hide);
        player.getInventory().setItem(4, noGadget);
        player.getInventory().setItem(6, gadgets);
        player.getInventory().setItem(8, profile);
    }
}
