package com.sap.sepp.kompass.listener;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.Constants;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginManager;

public class PlayerHider implements Listener {

    public PlayerHider(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);

    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        try {
            if (e.getItem().getItemMeta().getDisplayName().equals("§9Spieler verstecken")) {
                Constants.hide.add(e.getPlayer());

                e.getPlayer().sendMessage( "§3Lobby§7>> §3Du siehst nun keine Spieler mehr!");

                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (e.getPlayer() != player)
                        e.getPlayer().hidePlayer(player);
                });
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CLICK, 3, 1);
                e.getPlayer().getInventory().setItem(2, new ItemManager(Material.STICK).setDisplayName("§9Spieler anzeigen").build());
                e.getPlayer().updateInventory();

            } else if (e.getItem().getItemMeta().getDisplayName().equals("§9Spieler anzeigen")) {
                Constants.hide.remove(e.getPlayer());
                e.getPlayer().sendMessage("§3Lobby§7>> §aDu siehst nun wieder alle Spieler!");

                Bukkit.getOnlinePlayers().forEach(player -> {
                    if (e.getPlayer() != player)
                        e.getPlayer().showPlayer(player);
                });
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CLICK, 3, 1);
                e.getPlayer().getInventory().setItem(2, new ItemManager(Material.BLAZE_ROD).setDisplayName("§9Spieler verstecken").build());
                e.getPlayer().updateInventory();
            }
        } catch (NullPointerException ex) {
        }
    }
}
