package com.sap.sepp.kompass.utils;

import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemManager {

    private ItemStack item;
    private List<String> lore = new ArrayList<>();
    private ItemMeta meta;

    public ItemManager(Material material, short subID, int noe) {
        item = new ItemStack(material, noe, subID);
        meta = item.getItemMeta();
    }

    public ItemManager(ItemStack item) {
        this.item = item;
        this.meta = item.getItemMeta();
    }

    public ItemManager(Material material, short subID) {
        item = new ItemStack(material, 1, subID);
        meta = item.getItemMeta();
    }

    public ItemManager(Material material, int noe) {
        item = new ItemStack(material, noe, (short)0);
    }

    public ItemManager(Material material) {
        item = new ItemStack(material, 1, (short)0);
        meta = item.getItemMeta();
    }

    public ItemManager setAmount(int noe) {
        item.setAmount(noe);
        return this;
    }

    public ItemManager setDefaultName() {
        meta.setDisplayName(" ");
        return this;
    }

    public ItemManager setGlow() {
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return this;
    }

    public ItemManager setData(short data) {
        item.setDurability(data);
        return this;
    }

    public ItemManager addLoreLine(String line) {
        lore.add(line);
        return this;
    }

    public ItemManager addLoreArray(String[] lines) {
        for(int i = 0; i < lines.length; i++)
            lore.add(lines[i]);
        return this;
    }

    public ItemManager addLoreAll(List<String> lines) {
        lore.addAll(lines);
        return this;
    }

    public ItemManager setDisplayName(String name) {
        meta.setDisplayName(name);
        return this;
    }

    public ItemManager setSkullOwner(String owner) {
        ((SkullMeta)meta).setOwner(owner);
        return this;
    }

    public ItemManager setColor(Color color) {
        ((LeatherArmorMeta)meta).setColor(color);
        return this;
    }

    public ItemManager setBannerColor(DyeColor color) {
        ((BannerMeta)meta).setBaseColor(color);
        return this;
    }

    public ItemManager addEnchantment(Enchantment enchantment, int lvl) {
        meta.addEnchant(enchantment, lvl, true);
        return this;
    }

    public ItemManager addItemFlag(ItemFlag flag) {
        meta.addItemFlags(flag);
        return this;
    }

    public ItemManager addLeatherColor(Color color) {
        ((LeatherArmorMeta)meta).setColor(color);
        return this;
    }

    public ItemStack build() {
        if(!lore.isEmpty()) {
            meta.setLore(lore);
        }
        item.setItemMeta(meta);
        return item;
    }
}