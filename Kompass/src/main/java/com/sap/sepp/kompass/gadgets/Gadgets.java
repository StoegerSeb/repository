package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

public class Gadgets implements Listener {

    public Gadgets(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    private Inventory inventory = Bukkit.createInventory(null, 9, "§6Gadgets");
    private Inventory profileInventory = Bukkit.createInventory(null, 9, "§6Profil");

    private final ItemStack enderpearl = new ItemManager(Material.ENDER_PEARL).setDisplayName("§bEnderperle").build();
    private final ItemStack firework = new ItemManager(Material.FIREWORK).setDisplayName("§bFeuerwerk").build();
    private final ItemStack fishingrod = new ItemManager(Material.FISHING_ROD).setDisplayName("§bEnterharken").build();
    private final ItemStack jumpBoost = new ItemManager(Material.FEATHER).setDisplayName("§bJumpboost").build();
    private final ItemStack noGadget = new ItemManager(Material.BARRIER).setDisplayName("§cKein Gadget ausgewählt!").build();
    private final ItemStack back = new ItemManager(Material.BARRIER).setDisplayName("§cZurück").build();
    private final ItemStack hats = new ItemManager(Material.CHAINMAIL_HELMET).setDisplayName("§aHüte").build();
    private final ItemStack boots = new ItemManager(Material.CHAINMAIL_BOOTS).setDisplayName("§aSchuhe").build();
    private final ItemStack nametag = new ItemManager(Material.NAME_TAG).setDisplayName("§aFliegen").build();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        try {
            if (e.getItem().getItemMeta().getDisplayName().equals("§6Gadgets")) {
                e.getPlayer().openInventory(inventory);
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_OPEN, 3, 1);
                e.setCancelled(true);

                inventory.setItem(0, enderpearl);
                inventory.setItem(1, firework);
                inventory.setItem(2, fishingrod);
                inventory.setItem(3, jumpBoost);
                inventory.setItem(8, back);
            } else if(e.getItem().getItemMeta().getDisplayName().equals("§aDein Profil")) {
                e.getPlayer().openInventory(profileInventory);
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.CHEST_CLOSE, 3, 1);
                e.setCancelled(true);
                profileInventory.setItem(0, hats);
                profileInventory.setItem(1, boots);
                profileInventory.setItem(8, back);
            }
        } catch(NullPointerException ex) {}
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getView().getTitle().equals("§6Gadgets")) {
            Player player = (Player)e.getWhoClicked();

            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                Material material = e.getCurrentItem().getType();

                if(material == Material.ENDER_PEARL) {
                    player.sendMessage("§3Lobby§7>> §cNoch nicht verfügbar!");
                } else if(material == Material.FIREWORK) {
                    player.getInventory().setItem(4, firework);
                } else if(material == Material.FISHING_ROD) {
                    player.getInventory().setItem(4, fishingrod);
                } else if(material == Material.BARRIER) {
                    player.getInventory().setItem(4, noGadget);
                } else if(material == Material.FEATHER) {
                    player.getInventory().setItem(4, jumpBoost);
                } else {
                    player.sendMessage("§3Lobby§7>> §cNoch nicht verfügbar!");
                }
                player.closeInventory();
                player.updateInventory();
                player.playSound(player.getLocation(), Sound.CHEST_CLOSE, 3, 1);
            }
        }
    }
}