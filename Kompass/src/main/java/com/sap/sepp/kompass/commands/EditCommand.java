package com.sap.sepp.kompass.commands;

import com.sap.sepp.kompass.utils.Constants;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EditCommand implements CommandExecutor {

    public EditCommand(final String command) {
        Bukkit.getPluginCommand(command).setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 0) {
            if(sender instanceof Player) {
                Player player = (Player) sender;

                if(player.hasPermission("ehrengames.lobby.edit")) {
                    if(!Constants.edit.contains(player)) {
                        Constants.edit.add(player);
                        player.sendMessage("§3Lobby§7>> §3Du kannst die Lobby nun bearbeiten!");
                    } else {
                        Constants.edit.remove(player);
                        player.sendMessage("§3Lobby§7>> §3Du kannst die Lobby nicht mehr bearbeiten!");
                    }
                } else {
                    return false;
                }
            } else {
                sender.sendMessage("§3Lobby§7>> §3Funktioniert nicht in der Konsole!");
                return false;
            }
        } else {
            return false;
        }
        return false;
    }
}