package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

public class Hats implements Listener {

    public Hats(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    private Inventory hatsInventory = Bukkit.createInventory(null, 9, "§aDeine Hüte");

    private final ItemStack chain = new ItemManager(Material.CHAINMAIL_HELMET).setDisplayName("§aKettenhut").build();
    private final ItemStack diamond = new ItemManager(Material.DIAMOND_HELMET).setDisplayName("§aDiamanthelm").build();
    private final ItemStack gold = new ItemManager(Material.GOLD_HELMET).setDisplayName("§aGoldhelm").build();
    private final ItemStack chest = new ItemManager(Material.CHEST).setDisplayName("§aKistenhut").build();
    private final ItemStack dirt = new ItemManager(Material.DIRT).setDisplayName("§aDreckshut").build();
    private final ItemStack back = new ItemManager(Material.BARRIER).setDisplayName("§cZurück").build();
    private final ItemStack spawner = new ItemManager(Material.MOB_SPAWNER).setDisplayName("§aMonsterhut").build();
    private final ItemStack barrierHat = new ItemManager(Material.BARRIER).setDisplayName("§aGrenzhut").build();
    private final ItemStack endCrystal = new ItemManager(Material.ENDER_PORTAL).setDisplayName("§aVoidHut").build();

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player)e.getWhoClicked();
        hatsInventory.setItem(0, chain);
        hatsInventory.setItem(1, diamond);
        hatsInventory.setItem(2, gold);
        hatsInventory.setItem(3, chest);
        hatsInventory.setItem(4, dirt);
        hatsInventory.setItem(5, spawner);
        hatsInventory.setItem(6, barrierHat);
        hatsInventory.setItem(7, endCrystal);
        hatsInventory.setItem(8, back);

        if (e.getView().getTitle().equals("§6Profil")) {
            Material material = e.getCurrentItem().getType();

            if(material == Material.CHAINMAIL_HELMET) {
                player.openInventory(hatsInventory);
                player.playSound(player.getLocation(), Sound.CHEST_OPEN, 3, 1);
                e.setCancelled(true);
            }
        }

        if(e.getView().getTitle().equals("§aDeine Hüte")) {
            Player p = (Player)e.getWhoClicked();

            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                Material material = e.getCurrentItem().getType();

                if(material != Material.BARRIER) {
                    p.getInventory().setHelmet(e.getCurrentItem());
                } else {
                    p.getInventory().setHelmet(null);
                }
                p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 3, 1);
                p.closeInventory();
                p.updateInventory();
                e.setCancelled(true);
            }
        }
    }
}