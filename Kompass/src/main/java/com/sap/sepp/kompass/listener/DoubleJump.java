package com.sap.sepp.kompass.listener;

import com.sap.sepp.kompass.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.util.Vector;

public class DoubleJump implements Listener {

    public DoubleJump(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onFly(PlayerToggleFlightEvent e) {
        Player player = e.getPlayer();

        if(player.getGameMode() == GameMode.SURVIVAL) {
            e.setCancelled(true);

            player.setAllowFlight(false);
            player.setFlying(false);
            player.setVelocity(player.getLocation().getDirection().multiply(2).add(new Vector(0, 1.5, 0)));
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player player = e.getPlayer();

        if(player.getGameMode() == GameMode.SURVIVAL) {
            if(player.getLocation().add(0, -1, 0).getBlock().getType() != Material.AIR) {
                player.setAllowFlight(true);
                player.setFlying(false);
            }
        }
    }
}