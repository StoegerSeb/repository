package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.*;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.PluginManager;

public class Firework implements Listener {

    public Firework(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    final ItemStack idle = new ItemManager(Material.FIREWORK).setDisplayName("§7Feuerwerk").addLoreLine("§4Abklingzeit..").build();
    private final ItemStack fireworK = new ItemManager(Material.FIREWORK).setDisplayName("§bFeuerwerk").build();

    @EventHandler
    public void onFirework(PlayerInteractEvent e) {
        if (e.getItem() != null && e.getItem().getType() == Material.FIREWORK) {
            if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Player player = e.getPlayer();


                Location location = player.getLocation();

                if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    location = e.getClickedBlock().getRelative(BlockFace.UP).getLocation();
                }

                FireworkMeta fireworkMeta = (FireworkMeta) e.getItem().getItemMeta();
                fireworkMeta.setPower(3);
                fireworkMeta.addEffect(FireworkEffect.builder().withFade(Color.AQUA).withColor(Color.GREEN).trail(true).build());

                org.bukkit.entity.Firework firework = location.getWorld().spawn(location, org.bukkit.entity.Firework.class);
                firework.setFireworkMeta(fireworkMeta);

                player.getInventory().setItem(4, idle);
                player.updateInventory();

                Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        player.getInventory().setItem(4, fireworK);
                        player.updateInventory();
                    }
                }, 20 * 5);
            }
        }
    }
}