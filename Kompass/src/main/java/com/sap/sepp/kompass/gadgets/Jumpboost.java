package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.util.Vector;

public class Jumpboost implements Listener {

    public Jumpboost(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    final ItemStack idle = new ItemManager(Material.FIREWORK_CHARGE).setDisplayName("§7Jumpboost").addLoreLine("§4Abklingzeit..").build();
    final ItemStack jumpboost = new ItemManager(Material.FEATHER).setDisplayName("§bJumpboost").build();

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        try {
            if (e.getItem().getType() == Material.FEATHER) {
                if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    e.getPlayer().setVelocity(new Vector(0, 1.1, 0));

                    e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENDERDRAGON_WINGS, 3, 1);
                    e.getPlayer().playEffect(e.getPlayer().getLocation(), Effect.MOBSPAWNER_FLAMES, 3);

                    e.getPlayer().getInventory().setItem(4, idle);
                    e.getPlayer().updateInventory();

                    Bukkit.getScheduler().runTaskLater(LobbySystem.getInstance(), new Runnable() {
                        @Override
                        public void run() {
                            e.getPlayer().getInventory().setItem(4, jumpboost);
                            e.getPlayer().updateInventory();
                        }
                    }, 20 * 5);
                }
            }
        } catch (NullPointerException ex) {
        }
    }
}