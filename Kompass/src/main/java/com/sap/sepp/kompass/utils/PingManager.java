package com.sap.sepp.kompass.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class PingManager {
    public static Map<String, String> maxPlayers = new HashMap<>();
    public static Map<String, String> currentPlayers = new HashMap<>();
    public static Map<String, String> serverMotd = new HashMap<>();

    public static void pingServer(String ipAddress, int port, String serverName) {
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(ipAddress, port));
            OutputStream out = socket.getOutputStream();
            InputStream in = socket.getInputStream();
            out.write(254);
            StringBuilder str = new StringBuilder();
            int b;
            while ((b = in.read()) != -1) {
                if ((b != 0) && (b > 16) && (b != 255) && (b != 23) && (b != 24)) {
                    str.append((char) b);
                }
            }
            String[] data = str.toString().split("§");
            maxPlayers.put(serverName, data[2]);
            currentPlayers.put(serverName, data[1]);
            serverMotd.put(serverName, data[0]);
            socket.close();
            return;
        } catch (Exception e1) {
            maxPlayers.put(serverName, "0");
            currentPlayers.put(serverName, "0");
            serverMotd.put(serverName, "OFFLINE:0:0");
            try {
                socket.close();
            } catch (IOException e) {
            }
        }
    }
}
