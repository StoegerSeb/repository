package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

public class Boots implements Listener {

    public Boots(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    private Inventory bootsInventory = Bukkit.createInventory(null, 9, "§aDeine Schuhe");
    private final ItemStack love = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§4Liebesschuhe").build();
    private final ItemStack fire = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§6Feurige Schuhe").build();
    private final ItemStack water = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§bDelfinschuhe").build();
    private final ItemStack smoke = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§7Raucher Schuhe").build();
    private final ItemStack magic = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§aMagische Schuhe").build();
    private final ItemStack lsd = new ItemManager(Material.LEATHER_BOOTS).setDisplayName("§a§kL§c§kS§b§kD §7Schuhe").build();
    private final ItemStack back = new ItemManager(Material.BARRIER).setDisplayName("§cZurück").build();

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        bootsInventory.setItem(0, love);
        bootsInventory.setItem(1, fire);
        bootsInventory.setItem(2, water);
        bootsInventory.setItem(3, smoke);
        bootsInventory.setItem(4, magic);
        bootsInventory.setItem(5, lsd);
        bootsInventory.setItem(8, back);
        if (e.getView().getTitle().equals("§6Profil")) {
            Material material = e.getCurrentItem().getType();

            if (material == Material.CHAINMAIL_BOOTS) {
                player.closeInventory();
                player.openInventory(bootsInventory);
                player.playSound(player.getLocation(), Sound.CHEST_OPEN, 3, 1);
                e.setCancelled(true);
            }
        }


        if (e.getView().getTitle().equals("§aDeine Schuhe")) {
            Player p = (Player) e.getWhoClicked();

            if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
                Material material = e.getCurrentItem().getType();

                if (material != Material.BARRIER) {
                    p.getInventory().setBoots(e.getCurrentItem());
                } else {
                    p.getInventory().setBoots(null);
                }
                p.playSound(p.getLocation(), Sound.CHEST_CLOSE, 3, 1);
                p.closeInventory();
                p.updateInventory();
                e.setCancelled(true);
            }
        }
    }
}