package com.sap.sepp.kompass.gadgets;

import com.sap.sepp.kompass.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.util.Vector;

public class GrapplingIron implements Listener {

    public GrapplingIron(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onPlayerFis(PlayerFishEvent e) {
        Player player = e.getPlayer();
        FishHook fishHook = e.getHook();

        if (fishHook.getLocation().subtract(0, 1, 0).getBlock().getType() != Material.AIR) {
            Location playerLocation = player.getLocation();
            Location hookLocation = fishHook.getLocation();

            Vector vector = player.getVelocity();
            double distance = playerLocation.distance(hookLocation);

            vector.setX((1.08D * distance) * (hookLocation.getX() - playerLocation.getX()) / distance);
            vector.setY((1D * distance) * (hookLocation.getY() - playerLocation.getY()) / distance - -0.05D * distance);
            vector.setZ((1.08D * distance) * (hookLocation.getZ() - playerLocation.getZ()) / distance);

            player.setVelocity(vector);
            player.getInventory().getItemInHand().setDurability((short)0);
            player.updateInventory();
        }
    }
}