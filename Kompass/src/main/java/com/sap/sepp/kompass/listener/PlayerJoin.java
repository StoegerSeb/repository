package com.sap.sepp.kompass.listener;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.LobbyInventory;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginManager;

public class PlayerJoin implements Listener {
    public PlayerJoin(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();

        player.getInventory().clear();
        new LobbyInventory(player).setInventory();
        player.setAllowFlight(true);
        player.setFlying(false);
    }
}
