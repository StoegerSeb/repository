package com.sap.sepp.kompass.listener;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.Constants;
import com.sap.sepp.kompass.utils.LobbyInventory;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.PluginManager;

public class MiscListener implements Listener {

    public MiscListener(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
        e.setFoodLevel(20);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();

        if (!Constants.edit.contains(player))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onWeathearChange(WeatherChangeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (!Constants.edit.contains(e.getPlayer()))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (!Constants.edit.contains(e.getPlayer()))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onGamemodeChange(PlayerGameModeChangeEvent e) {
        e.getPlayer().getInventory().clear();
        e.getPlayer().getInventory().setArmorContents(null);
        e.getPlayer().updateInventory();

        if (e.getNewGameMode() != GameMode.CREATIVE) {
            new LobbyInventory(e.getPlayer()).setInventory();
        }
    }

    @EventHandler
    public void onDropItems(PlayerDropItemEvent e) {
        if (!Constants.edit.contains(e.getPlayer()))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onExplose(EntityExplodeEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDoorBreak(EntityBreakDoorEvent e) {
        Player player = e.getEntity().getKiller();

        if(!Constants.edit.contains(player))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        e.getEntity().spigot().respawn();
        e.getDrops();
        e.setDroppedExp(0);
        e.getDrops().clear();
    }

    @EventHandler
    public void onPlayerPickUp(PlayerPickupItemEvent e) {
        if(!Constants.edit.contains(e.getPlayer()))
            e.setCancelled(true);
        else
            e.setCancelled(false);
    }

    @EventHandler
    public void onFireTick(BlockIgniteEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamageEntity(EntityDamageByEntityEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onDamageBlock(EntityDamageByBlockEvent e) {
        e.setCancelled(true);
    }
}
