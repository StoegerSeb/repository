package com.sap.sepp.kompass;

import com.sap.sepp.kompass.commands.EditCommand;
import com.sap.sepp.kompass.gadgets.*;
import com.sap.sepp.kompass.listener.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class LobbySystem extends JavaPlugin {
    
    private static LobbySystem instance;

    @Override
    public void onEnable() {
        instance = this;

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        registerListener();
        new EditCommand("edit");
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public static LobbySystem getInstance() {
        return instance;
    }

    private void registerListener() {
        new PlayerJoin(getInstance());
        new Navigator(getInstance());
        new PlayerQuit(getInstance());
        new DoubleJump(getInstance());
        new MiscListener(getInstance());
        new PlayerHider(getInstance());
        new Gadgets(getInstance());
        new Boots(getInstance());
        new Firework(getInstance());
        new GrapplingIron(getInstance());
        new Hats(getInstance());
        new Jumpboost(getInstance());
    }
}
