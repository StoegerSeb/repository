package com.sap.sepp.kompass.listener;

import com.sap.sepp.kompass.LobbySystem;
import com.sap.sepp.kompass.utils.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Navigator implements Listener {

    public Navigator(final LobbySystem instance) {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(this, instance);
    }

    @EventHandler
    public void onNavigatorInteract(PlayerInteractEvent e) {
        int players = Bukkit.getOnlinePlayers().size();

        try {
            if (e.getItem().getItemMeta().getDisplayName().equals("§4Navigator")) {
                Inventory navInv = Bukkit.createInventory(null, 9 * 3, "§4Navigator");

                final ItemStack cb1 = new ItemManager(Material.BRICK).setDisplayName("§6CityBuild-1").build();
                final ItemStack minigames = new ItemManager(Material.DIAMOND_SWORD).setDisplayName("§4Minigames").build();
                final ItemStack adventure = new ItemManager(Material.STONE).setDisplayName("§3Adventure").build();
                final ItemStack survival = new ItemManager(Material.BOW).setDisplayName("§7Survival").build();
                final ItemStack spawn = new ItemManager(Material.SLIME_BALL).setDisplayName("§aSpawn").build();

                navInv.setItem(1, cb1);
                navInv.setItem(7, minigames);
                navInv.setItem(13, spawn);
                navInv.setItem(19, adventure);
                navInv.setItem(25, survival);

                if (e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                    e.getPlayer().openInventory(navInv);
            }
        } catch (NullPointerException ex) {
        }
    }

    @EventHandler
    public void onNavInvClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();

        if (e.getView().getTitle().equals("§4Navigator")) {
            try {
                if (e.getCurrentItem().getType().equals(Material.BRICK)) {
                    player.closeInventory();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                    try {
                        dataOutputStream.writeUTF("Connect");
                        dataOutputStream.writeUTF("CityBuild-1");

                        player.sendPluginMessage(LobbySystem.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
                        player.sendMessage("§3Kompass§7>> §3Verbindung zu §eCityBuild-1 §3hergestellt!");
                    } catch (IOException ex) {
                    }
                } else if (e.getCurrentItem().getType().equals(Material.DIAMOND_SWORD)) {
                    player.closeInventory();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                    try {
                        dataOutputStream.writeUTF("Connect");
                        dataOutputStream.writeUTF("Minigames-1");

                        player.sendPluginMessage(LobbySystem.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
                        player.sendMessage("§3Kompass§7>> §3Verbindung zu §eMiniGames-1 §3hergestellt!");
                    } catch (IOException ex) {
                    }
                } else if (e.getCurrentItem().getType().equals(Material.STONE)) {
                    player.closeInventory();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                    try {
                        dataOutputStream.writeUTF("Connect");
                        dataOutputStream.writeUTF("Adventure-1");

                        player.sendPluginMessage(LobbySystem.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
                        player.sendMessage("§3Kompass§7>> §3Verbindung zu §eAdventure-1 §3hergestellt!");
                    } catch (IOException ex) {
                    }
                } else if (e.getCurrentItem().getType().equals(Material.BOW)) {

                    player.closeInventory();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                    try {
                        dataOutputStream.writeUTF("Connect");
                        dataOutputStream.writeUTF("Survival-1");

                        player.sendPluginMessage(LobbySystem.getInstance(), "BungeeCord", byteArrayOutputStream.toByteArray());
                        player.sendMessage("§3Kompass§7>> §3Verbindung zu §eSurvival-1 §3hergestellt!");
                    } catch (IOException ex) {
                    }
                }else if (e.getCurrentItem().getType().equals(Material.SLIME_BALL)) {
                    player.closeInventory();
                    Bukkit.dispatchCommand(player, "spawn");
                }
            } catch (NullPointerException ex) {
            }
            e.setCancelled(true);
        }
    }
}
