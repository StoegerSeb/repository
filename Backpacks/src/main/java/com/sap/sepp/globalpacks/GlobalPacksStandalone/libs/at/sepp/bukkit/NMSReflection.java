package com.sap.sepp.globalpacks.GlobalPacksStandalone.libs.at.sepp.bukkit;
/*
# (C) Copyright 2020 Backpacks (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Backpacks-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class NMSReflection: 13.04.2020 19:33 by sebip
*/

public class NMSReflection extends OBCReflection implements NmsReflector {
    private static final String NMS_CLASS_PATH = "net.minecraft.server." + BUKKIT_VERSION + ".";

    static {
        if (Bukkit.getServer().getName().toLowerCase(Locale.ROOT).contains("cauldron") || Bukkit.getServer().getName().toLowerCase(Locale.ROOT).contains("uranium"))
            throw new RuntimeException("Using Bukkit Reflections on Cauldron / Uranium based server!");
    }

    @Nullable
    public static Class<?> getNMSClass(@NotNull String className) {
        try {
            return Class.forName(NMS_CLASS_PATH + className);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Method getNMSMethod(@NotNull String className, @NotNull String name, @Nullable Class<?>... args) {
        return getNMSMethod(getNMSClass(className), name, args);
    }

    @Contract("null, _, _ -> null")
    @Nullable
    public static Method getNMSMethod(@Nullable Class<?> clazz, @NotNull String name, @Nullable Class<?>... args) {
        return (clazz == null) ? null : getMethod(clazz, name, args);
    }

    @Nullable
    public static Field getNMSField(@NotNull String className, @NotNull String name) {
        return getNMSField(getNMSClass(className), name);
    }

    @Contract("null, _ -> null")
    @Nullable
    public static Field getNMSField(@Nullable Class<?> clazz, @NotNull String name) {
        return (clazz == null) ? null : getField(clazz, name);
    }

    @Nullable
    public static Enum<?> getNMSEnum(@NotNull String enumClassAndEnumName) {
        return getEnum(NMS_CLASS_PATH + enumClassAndEnumName);
    }

    @Nullable
    public static Enum<?> getNMSEnum(@NotNull String enumClass, @NotNull String enumName) {
        return getEnum(NMS_CLASS_PATH + enumClass + "." + enumName);
    }

    @Nullable
    public static Object getHandle(@NotNull Object obj) {
        try {
            return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public Class<?> getNmsClass(@NotNull String className) {
        return getNMSClass(className);
    }

    @Nullable
    public Method getNmsMethod(@NotNull String className, @NotNull String name, @NotNull Class<?>... args) {
        return getNMSMethod(className, name, args);
    }

    @Nullable
    public Method getNmsMethod(@Nullable Class<?> clazz, @NotNull String name, @NotNull Class<?>... args) {
        return getNMSMethod(clazz, name, args);
    }

    @Nullable
    public Field getNmsField(@NotNull String className, @NotNull String name) {
        return getNMSField(className, name);
    }

    @Nullable
    public Field getNmsField(@Nullable Class<?> clazz, @NotNull String name) {
        return getNMSField(clazz, name);
    }

    @Nullable
    public Enum<?> getNmsEnum(@NotNull String enumClassAndEnumName) {
        return getNMSEnum(enumClassAndEnumName);
    }

    @Nullable
    public Enum<?> getNmsEnum(@NotNull String enumClass, @NotNull String enumName) {
        return getNMSEnum(enumClass, enumName);
    }

    @Nullable
    public Object getNmsHandle(@NotNull Object obj) {
        return getHandle(obj);
    }
}