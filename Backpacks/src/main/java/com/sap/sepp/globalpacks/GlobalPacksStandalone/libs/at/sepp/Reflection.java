package com.sap.sepp.globalpacks.GlobalPacksStandalone.libs.at.sepp;
/*
# (C) Copyright 2020 Backpacks (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Backpacks-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class Reflection: 13.04.2020 19:34 by sebip
*/

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Reflection {
    @Nullable
    public static Class<?> getClass(@NotNull String classPath) {
        try {
            return Class.forName(classPath);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Class<?> getInnerClass(@NotNull Class clazz, @NotNull String className) {
        try {
            Class[] classes = clazz.getClasses();
            for (Class<?> innerClass : classes) {
                if (innerClass.getSimpleName().equals(className))
                    return innerClass;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void setStaticField(@NotNull Class<?> clazz, @NotNull String field, @Nullable Object value) {
        setStaticField(getField(clazz, field), value);
    }

    public static void setStaticField(@NotNull Field field, @Nullable Object value) {
        setValue((Object) field, null, value);
    }

    public static void setValue(@NotNull Field field, @Nullable Object instance, @Nullable Object value) {
        try {
            field.setAccessible(true);
            field.set(instance, value);
            field.setAccessible(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setValue(@NotNull Object instance, @NotNull String fieldName, @Nullable Object value) {
        setValue(getField(instance.getClass(), fieldName), instance, value);
    }

    @Nullable
    public static Enum<?> getEnum(@NotNull String enumFullName) {
        String[] x = enumFullName.split("\\.(?=[^.]+$)");
        if (x.length == 2)
            try {
                return (Enum<? extends >)Enum.valueOf(Class.forName(x[0]), x[1]);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        return null;
    }

    @Nullable
    public static Enum<?> getEnum(@NotNull Class<Enum<?>> clazz, @NotNull String enumName) {
        try {
            return Enum.valueOf(clazz, enumName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Field getField(@NotNull Class<?> clazz, @NotNull String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    public static Field getFieldIncludeParents(@NotNull Class<?> clazz, @NotNull String name) {
        try {
            Field field = clazz.getDeclaredField(name);
            field.setAccessible(true);
            return field;
        } catch (NoSuchFieldException ignored) {
            if (clazz.getSuperclass() != null)
                return getFieldIncludeParents(clazz.getSuperclass(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static Method getMethod(@NotNull Class<?> clazz, @NotNull String name, @Nullable Class<?>... args) {
        Method method = null;
        try {
            method = clazz.getDeclaredMethod(name, args);
            method.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return method;
    }

    @Nullable
    public static Method getMethodIncludeParents(@NotNull Class<?> clazz, @NotNull String name, @Nullable Class<?>... args) {
        Method method = null;
        try {
            method = clazz.getDeclaredMethod(name, args);
            method.setAccessible(true);
        } catch (NoSuchMethodException ignored) {
            if (clazz.getSuperclass() != null)
                return getMethodIncludeParents(clazz.getSuperclass(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return method;
    }

    @Nullable
    public static Constructor<?> getConstructor(@NotNull Class<?> clazz, @Nullable Class<?>... args) {
        try {
            return clazz.getConstructor(args);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean classListEqual(@NotNull Class<?>[] l1, @NotNull Class<?>[] l2) {
        if (l1.length != l2.length)
            return false;
        for (int i = 0; i < l1.length; i++) {
            if (l1[i] != l2[i])
                return false;
        }
        return true;
    }
}
