package com.sap.sepp.globalpacks.GlobalPacksStandalone.libs.at.sepp.bukkit;
/*
# (C) Copyright 2020 Backpacks (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Backpacks-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class OBCReflection: 13.04.2020 19:33 by sebip
*/

public class OBCReflection extends Reflection {
    protected static final String BUKKIT_VERSION = getBukkitVersion();

    private static final String OBC_CLASS_PATH = "org.bukkit.craftbukkit." + BUKKIT_VERSION + ".";

    private static String getBukkitVersion() {
        try {
            return Bukkit.getServer().getClass().getName().split("\\.")[3];
        } catch (Throwable throwable) {
            return "unknown";
        }
    }

    public static String getVersion() {
        return BUKKIT_VERSION;
    }

    @Nullable
    public static Class<?> getOBCClass(@NotNull String className) {
        return getClass(OBC_CLASS_PATH + className);
    }

    @Nullable
    public static Method getOBCMethod(@NotNull String className, @NotNull String name, @Nullable Class<?>... args) {
        Class<?> clazz = getOBCClass(className);
        return (clazz == null) ? null : getMethod(clazz, name, args);
    }

    @Nullable
    public static Field getOBCField(@NotNull String className, @NotNull String name) {
        Class<?> clazz = getOBCClass(className);
        return (clazz == null) ? null : getField(clazz, name);
    }
}
