package com.sap.sepp.globalpacks.GlobalPacksStandalone.libs.at.sepp.bukkit;
/*
# (C) Copyright 2020 Backpacks (Sepp_xGMx)
#
# @author Sebastian Stögerer
# @since 19.03.2020 21:46
# @Website https://bitbucket.org/StoegerSeb/repository/src/master/
#
# The Backpacks-Project is under the Apache License, version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Creation of Class MCVersion: 13.04.2020 19:32 by sebip
*/

import org.bukkit.Bukkit;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum MCVersion {
    UNKNOWN(0, -1, "", "UNKNOWN"),
    MC_1_7(11, 3, "1_7", "1.7", false),
    MC_1_7_1(11, 3, "1_7", "1.7.1", MC_1_7, false),
    MC_1_7_2(11, 4, "1_7", "1.7.2", MC_1_7, false),
    MC_1_7_3(11, 4, "1_7", "1.7.3", MC_1_7, false),
    MC_1_7_4(11, 4, "1_7", "1.7.4", MC_1_7, false),
    MC_NMS_1_7_R1(11, 4, "1_7", "1.7_NMS_R1", MC_1_7, false),
    MC_1_7_5(12, 5, "1_7", "1.7.5", MC_1_7),
    MC_1_7_6(12, 5, "1_7", "1.7.6", MC_1_7),
    MC_1_7_7(12, 5, "1_7", "1.7.7", MC_1_7),
    MC_NMS_1_7_R2(12, 5, "1_7", "1.7_NMS_R2", MC_1_7),
    MC_1_7_8(13, 5, "1_7", "1.7.8", MC_1_7),
    MC_1_7_9(13, 5, "1_7", "1.7.9", MC_1_7),
    MC_NMS_1_7_R3(13, 5, "1_7", "1.7_NMS_R3", MC_1_7),
    MC_1_7_10(14, 5, "1_7", "1.7.10", MC_1_7),
    MC_NMS_1_7_R4(14, 5, "1_7", "1.7_NMS_R4", MC_1_7),
    MC_1_8(21, 47, "1_8", "1.8"),
    MC_1_8_1(21, 47, "1_8", "1.8.1", MC_1_8),
    MC_1_8_2(21, 47, "1_8", "1.8.2", MC_1_8),
    MC_NMS_1_8_R1(21, 47, "1_8", "1.8_NMS_R1", MC_1_8),
    MC_1_8_3(22, 47, "1_8", "1.8.3", MC_1_8),
    MC_1_8_4(22, 47, "1_8", "1.8.4", MC_1_8),
    MC_1_8_5(22, 47, "1_8", "1.8.5", MC_1_8),
    MC_1_8_6(22, 47, "1_8", "1.8.6", MC_1_8),
    MC_1_8_7(22, 47, "1_8", "1.8.7", MC_1_8),
    MC_NMS_1_8_R2(22, 47, "1_8", "1.8_NMS_R2", MC_1_8),
    MC_1_8_8(23, 47, "1_8", "1.8.8", MC_1_8),
    MC_1_8_9(23, 47, "1_8", "1.8.9", MC_1_8),
    MC_NMS_1_8_R3(23, 47, "1_8", "1.8_NMS_R3", MC_1_8),
    MC_1_9(31, 107, "1_9", "1.9"),
    MC_1_9_1(31, 108, "1_9", "1.9.1", MC_1_9),
    MC_1_9_2(31, 109, "1_9", "1.9.2", MC_1_9),
    MC_NMS_1_9_R1(31, 109, "1_9", "1.9_NMS_R1", MC_1_9),
    MC_1_9_3(32, 110, "1_9", "1.9.3", MC_1_9),
    MC_1_9_4(32, 110, "1_9", "1.9.4", MC_1_9),
    MC_NMS_1_9_R2(32, 110, "1_9", "1.9_NMS_R2", MC_1_9),
    MC_1_10(41, 210, "1_10", "1.10"),
    MC_1_10_1(41, 210, "1_10", "1.10.1", MC_1_10),
    MC_1_10_2(41, 210, "1_10", "1.10.2", MC_1_10),
    MC_NMS_1_10_R1(41, 210, "1_10", "1.10_NMS_R1", MC_1_10),
    MC_1_11(51, 315, "1_11", "1.11"),
    MC_1_11_1(51, 316, "1_11", "1.11.1", MC_1_11),
    MC_1_11_2(51, 316, "1_11", "1.11.2", MC_1_11),
    MC_NMS_1_11_R1(51, 316, "1_11", "1.11_NMS_R1"),
    MC_1_12(61, 335, "1_12", "1.12"),
    MC_1_12_1(61, 338, "1_12", "1.12.1", MC_1_12),
    MC_1_12_2(61, 340, "1_12", "1.12.2", MC_1_12),
    MC_NMS_1_12_R1(61, 340, "1_12", "1.12_NMS_R1", MC_1_12),
    MC_1_13(71, 393, "1_13", "1.13"),
    MC_NMS_1_13_R1(71, 393, "1_13", "1.13_NMS_R1", MC_1_13),
    MC_1_13_1(72, 401, "1_13", "1.13.1", MC_1_13),
    MC_1_13_2(72, 404, "1_13", "1.13.2", MC_1_13),
    MC_NMS_1_13_R2(72, 404, "1_13", "1.13_NMS_R2", MC_1_13),
    MC_1_14(81, 477, "1_14", "1.14"),
    MC_NMS_1_14_R1(81, 498, "1_14", "1.14_NMS_R1", MC_1_14),
    MC_1_14_1(81, 480, "1_14", "1.14.1", MC_1_14),
    MC_1_14_2(81, 485, "1_14", "1.14.2", MC_1_14),
    MC_1_14_3(81, 490, "1_14", "1.14.3", MC_1_14),
    MC_1_14_4(81, 498, "1_14", "1.14.4", MC_1_14),
    MC_1_15(91, 573, "1_15", "1.15"),
    MC_1_15_1(91, 575, "1_15", "1.15.1", MC_1_15),
    MC_1_15_2(91, 578, "1_15", "1.15.2", MC_1_15),
    MC_NMS_1_15_R1(91, 578, "1_15", "1.15_NMS_R1", MC_1_15),
    MC_1_16(101, 2147483647, "1_16", "1.16"),
    MC_NMS_1_16_R1(101, 2147483647, "1_16", "1.16_NMS_R1", MC_1_16);

    private static final Map<String, MCVersion> VERSION_MAP;

    private static final Map<String, MCVersion> NMS_VERSION_MAP;

    private static final Map<Integer, MCVersion> PROTOCOL_VERSION_MAP;

    private static final Pattern VERSION_PATTERN;

    public static final MCVersion CURRENT_VERSION;

    private final int versionID;

    private final int protocolVersion;

    private final String identifier;

    private final MCVersion mainVersion;

    private final boolean supportsUUIDs;

    private final boolean dualWielding;

    private final String name;

    static {
        VERSION_MAP = new HashMap<>();
        NMS_VERSION_MAP = new HashMap<>();
        PROTOCOL_VERSION_MAP = new HashMap<>();
        VERSION_PATTERN = Pattern.compile("\\(MC: (?<version>\\d.\\d+(.\\d+)?)\\)");
        for (MCVersion version : values()) {
            if (version.name().contains("NMS")) {
                NMS_VERSION_MAP.put(version.identifier, version);
            } else {
                VERSION_MAP.put(version.name, version);
                PROTOCOL_VERSION_MAP.put(version.protocolVersion, version);
            }
        }
        MCVersion currentVersion = UNKNOWN;
        try {
            Matcher matcher = VERSION_PATTERN.matcher(Bukkit.getVersion());
            if (matcher.find()) {
                currentVersion = getFromVersionName(matcher.group("version"));
            } else {
                currentVersion = getFromServerVersion(NMSReflection.getVersion());
            }
        } catch (Throwable ignored) {
            System.out.println("Failed to obtain server version!");
        }
        CURRENT_VERSION = currentVersion;
    }

    public static Collection<MCVersion> getProtocolVersions() {
        return PROTOCOL_VERSION_MAP.values();
    }

    public static Collection<MCVersion> getVersions() {
        return VERSION_MAP.values();
    }

    public int getProtocolVersion() {
        return this.protocolVersion;
    }

    public boolean isDualWielding() {
        return this.dualWielding;
    }

    public String getName() {
        return this.name;
    }

    MCVersion(int versionID, int protocolVersion, String mainVersionString, String versionString, boolean supportsUUIDs) {
        this.versionID = versionID;
        this.protocolVersion = protocolVersion;
        this.identifier = mainVersionString + "_R" + (versionID % 10);
        this.mainVersion = this;
        this.supportsUUIDs = supportsUUIDs;
        this.name = versionString;
        this.dualWielding = (protocolVersion >= 107);
    }

    MCVersion(int versionID, int protocolVersion, String mainVersionString, String versionString, MCVersion mainVersion, boolean supportsUUIDs) {
        this.versionID = versionID;
        this.protocolVersion = protocolVersion;
        this.identifier = mainVersionString + "_R" + (versionID % 10);
        this.mainVersion = mainVersion;
        this.supportsUUIDs = supportsUUIDs;
        this.name = versionString;
        this.dualWielding = (protocolVersion >= 107);
    }

    public MCVersion getMajorMinecraftVersion() {
        return this.mainVersion;
    }

    public boolean areUUIDsSupported() {
        return this.supportsUUIDs;
    }

    public boolean isSame(MCVersion other) {
        return (this.versionID == other.versionID);
    }

    public boolean newerThan(MCVersion other) {
        return (this.versionID > other.versionID && other != UNKNOWN);
    }

    public boolean newerOrEqualThan(MCVersion other) {
        return (isSame(other) || newerThan(other));
    }

    public boolean olderThan(MCVersion other) {
        return (this.versionID < other.versionID && this != UNKNOWN);
    }

    public boolean olderOrEqualThan(MCVersion other) {
        return (isSame(other) || olderThan(other));
    }

    public boolean isSameMajorVersion(MCVersion other) {
        return (this.versionID / 10 == other.versionID / 10);
    }

    public static boolean isAny(MCVersion other) {
        return CURRENT_VERSION.isSameMajorVersion(other);
    }

    public static boolean is(MCVersion other) {
        return (CURRENT_VERSION.versionID == other.versionID);
    }

    public static boolean isNewerThan(MCVersion other) {
        return (CURRENT_VERSION.versionID > other.versionID && other != UNKNOWN);
    }

    public static boolean isNewerOrEqualThan(MCVersion other) {
        return (is(other) || isNewerThan(other));
    }

    public static boolean isOlderThan(MCVersion other) {
        return (CURRENT_VERSION.versionID < other.versionID && CURRENT_VERSION != UNKNOWN);
    }

    public static boolean isOlderOrEqualThan(MCVersion other) {
        return (is(other) || isOlderThan(other));
    }

    public static boolean isUUIDsSupportAvailable() {
        return CURRENT_VERSION.areUUIDsSupported();
    }

    public static boolean isDualWieldingMC() {
        return CURRENT_VERSION.isDualWielding();
    }

    @NotNull
    public static MCVersion getFromServerVersion(@NotNull String serverVersion) {
        for (Map.Entry<String, MCVersion> entry : NMS_VERSION_MAP.entrySet()) {
            if (serverVersion.contains(entry.getKey()))
                return entry.getValue();
        }
        return UNKNOWN;
    }

    @NotNull
    public static MCVersion getFromProtocolVersion(int protocolVersion) {
        MCVersion version = PROTOCOL_VERSION_MAP.get(Integer.valueOf(protocolVersion));
        if (version == null)
            version = UNKNOWN;
        return version;
    }

    @NotNull
    public static MCVersion getFromVersionName(String versionName) {
        MCVersion version = VERSION_MAP.get(versionName);
        if (version == null)
            version = UNKNOWN;
        return version;
    }

    public String toString() {
        return this.name;
    }
}
