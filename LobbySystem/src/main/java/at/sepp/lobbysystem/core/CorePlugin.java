package at.sepp.lobbysystem.core;

import org.bukkit.plugin.java.JavaPlugin;

public interface CorePlugin {
    CoreLog getLog();
    JavaPlugin getInstance();
}
