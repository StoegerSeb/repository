package at.sepp.lobbysystem.core;

import org.bukkit.inventory.ItemStack;

public class CoreNBTAttribute {
    public static Class<?> CraftItemStackClass = CoreReflection.getCraftClass("inventory.CraftItemStack");

    public static Class<?> NBTTagListClass = CoreReflection.getNMSClass("NBTTagList");

    public static Class<?> NBTTagCompoundClass = CoreReflection.getNMSClass("NBTTagCompound");

    public static Class<?> ItemStackClass = CoreReflection.getNMSClass("ItemStack");

    public static Class<?> NBTBaseClass = CoreReflection.getNMSClass("NBTBase");

    public static ItemStack removeAttributesPotion(ItemStack item) {
        try {
            Object itemStack = CraftItemStackClass.getMethod("asNMSCopy", new Class[] { ItemStack.class }).invoke(null, item);
            Object nBTTagCompound = CoreReflection.getNMSClass("NBTTagCompound").newInstance();
            Object tag = null;
            boolean hasTag = (Boolean) ItemStackClass.getMethod("hasTag", new Class[0]).invoke(itemStack, new Object[0]);
            if (!hasTag) {
                tag = nBTTagCompound;
                ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            }
            if (tag == null)
                tag = ItemStackClass.getMethod("getTag", new Class[0]).invoke(itemStack, new Object[0]);
            Object modifiers = NBTTagListClass.newInstance();
            NBTTagCompoundClass.getMethod("set", new Class[] { String.class, NBTBaseClass }).invoke(tag, "CustomPotionEffects", modifiers);
            ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            return (ItemStack)CraftItemStackClass.getMethod("asCraftMirror", new Class[] { ItemStackClass }).invoke(null, new Object[] { itemStack });
        } catch (Exception e) {
            e.printStackTrace();
            return item;
        }
    }

    public static ItemStack removeAttributes(ItemStack item) {
        try {
            Object itemStack = CraftItemStackClass.getMethod("asNMSCopy", new Class[] { ItemStack.class }).invoke(null, item);
            Object nBTTagCompound = CoreReflection.getNMSClass("NBTTagCompound").newInstance();
            Object tag = null;
            boolean hasTag = (Boolean) ItemStackClass.getMethod("hasTag", new Class[0]).invoke(itemStack, new Object[0]);
            if (!hasTag) {
                tag = nBTTagCompound;
                ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            }
            if (tag == null)
                tag = ItemStackClass.getMethod("getTag", new Class[0]).invoke(itemStack);
            Object modifiers = NBTTagListClass.newInstance();
            NBTTagCompoundClass.getMethod("set", new Class[] { String.class, NBTBaseClass }).invoke(tag, "AttributeModifiers", modifiers);
            ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            return (ItemStack)CraftItemStackClass.getMethod("asCraftMirror", new Class[] { ItemStackClass }).invoke(null, new Object[] { itemStack });
        } catch (Exception e) {
            e.printStackTrace();
            return item;
        }
    }

    public static ItemStack addGlow(ItemStack item) {
        try {
            Object itemStack = CraftItemStackClass.getMethod("asNMSCopy", new Class[] { ItemStack.class }).invoke(null, item);
            Object nBTTagCompound = CoreReflection.getNMSClass("NBTTagCompound").newInstance();
            Object tag = null;
            boolean hasTag = (Boolean) ItemStackClass.getMethod("hasTag", new Class[0]).invoke(itemStack, new Object[0]);
            if (!hasTag) {
                tag = nBTTagCompound;
                ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            }
            if (tag == null)
                tag = ItemStackClass.getMethod("getTag", new Class[0]).invoke(itemStack);
            Object modifiers = NBTTagListClass.newInstance();
            NBTTagCompoundClass.getMethod("set", new Class[] { String.class, NBTBaseClass }).invoke(tag, "ench", modifiers);
            ItemStackClass.getMethod("setTag", new Class[] { NBTTagCompoundClass }).invoke(itemStack, NBTTagCompoundClass.cast(tag));
            return (ItemStack)CraftItemStackClass.getMethod("asCraftMirror", new Class[] { ItemStackClass }).invoke(null, new Object[] { itemStack });
        } catch (Exception e) {
            e.printStackTrace();
            return item;
        }
    }
}