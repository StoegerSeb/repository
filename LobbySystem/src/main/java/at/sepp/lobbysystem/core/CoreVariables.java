package at.sepp.lobbysystem.core;

import me.clip.placeholderapi.PlaceholderAPI;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CoreVariables {
    private static Permission permission = null;

    private static Chat chat = null;

    public static List<String> vu = new ArrayList<>();

    private static boolean placeholderAPI = false;

    public static void permission(Permission per) {
        permission = per;
    }

    public static void chat(Chat cha) {
        chat = cha;
    }

    public static void placeholderAPI(boolean api) {
        placeholderAPI = api;
    }

    public static String replace(String string, Player p) {
        if (string == null)
            return "";
        String newString = string;
        if (newString.contains("<player>"))
            newString = newString.replaceAll("<player>", p.getName());
        if (newString.contains("<server>"))
            newString = newString.replaceAll("<server>", p.getServer().getName());
        if (newString.contains("<displayname>"))
            newString = newString.replaceAll("<displayname>", p.getDisplayName());
        if (newString.contains("<world>"))
            newString = newString.replaceAll("<world>", p.getWorld().getName());
        if (newString.contains("<online>"))
            newString = newString.replaceAll("<online>", String.valueOf(Bukkit.getOnlinePlayers().size()));
        if (newString.contains("<player-x>"))
            newString = newString.replaceAll("<player-x>", String.valueOf(p.getLocation().getBlockX()));
        if (newString.contains("<player-y>"))
            newString = newString.replaceAll("<player-y>", String.valueOf(p.getLocation().getBlockY()));
        if (newString.contains("<player-z>"))
            newString = newString.replaceAll("<player-z>", String.valueOf(p.getLocation().getBlockZ()));
        if (permission != null) {
            String g = permission.getPrimaryGroup(p);
            if (newString.contains("<rank>"))
                newString = newString.replaceAll("<rank>", g);
        }
        if (chat != null) {
            if (newString.contains("<prefix>"))
                newString = newString.replaceAll("<prefix>", chat.getPlayerPrefix(p));
            if (newString.contains("<suffix>"))
                newString = newString.replaceAll("<suffix>", chat.getPlayerSuffix(p));
        }
        newString = replaceUcode(newString);
        if (placeholderAPI)
            newString = PlaceholderAPI.setPlaceholders(p, newString);
        newString = CoreColor.colorCodes(newString);
        return newString;
    }

    public static List<String> replaceList(List<String> list, Player p) {
        List<String> localList = new ArrayList<>(list);
        for (int i = 0; i < localList.size(); i++)
            localList.set(i, replace(localList.get(i), p));
        return localList;
    }

    public static String replaceConsole(String string, CommandSender p) {
        String newString = string;
        if (newString.contains("<player>"))
            newString = newString.replaceAll("<player>", p.getName());
        newString = replaceUcode(newString);
        return newString;
    }

    public static String replaceUcode(String string) {
        String newString = string;
        while (newString.contains("<ucode")) {
            String code = newString.split("<ucode")[1].split(">")[0];
            newString = newString.replaceAll("<ucode" + code + ">",
                    String.valueOf((char)Integer.parseInt(code, 16)));
        }
        if (newString.contains("<a>"))
            newString = newString.replaceAll("<a>", "á");
        if (newString.contains("<e>"))
            newString = newString.replaceAll("<e>", "é");
        if (newString.contains("<i>"))
            newString = newString.replaceAll("<i>", "í");
        if (newString.contains("<o>"))
            newString = newString.replaceAll("<o>", "ó");
        if (newString.contains("<u>"))
            newString = newString.replaceAll("<u>", "ú");
        int s = 1;
        for (String v : vu) {
            if (newString.contains("<" + s + ">"))
                newString = newString.replaceAll("<" + s + ">", v);
            s++;
        }
        return newString;
    }
}