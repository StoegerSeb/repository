package at.sepp.lobbysystem.core;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CoreMenuItem {
    private Material material;

    private Short data = null;

    private String noPermissionMessage = "Keine Berechtigung!";
    private String name = null;
    private String permission = null;
    private String skull = null;

    private Boolean opened;
    private boolean hasPermissionToView = false;
    private boolean enchantmentGlowing;
    private boolean checkVersion;

    private Integer price;
    private Integer slot = null;
    private int maximumRows;

    private List<String> lore;
    private List<String> commands;
    private List<String> version_list;
    private String invalidVersionMessage = "It is only available in the <version> versions.";

    public CoreMenuItem(Material material) {
        this.material = material;
    }

    public void setMaximumRows(Integer s) {
        if (s == null || s == 0) {
            this.maximumRows = 6;
            return;
        }
        this.maximumRows = s;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if (name == null || name.length() == 0) {
            this.name = null;
            return;
        }
        this.name = name;
    }

    public void setVersionList(List<String> version) {
        this.version_list = version;
    }

    public void setVersionCheck(boolean version_check) {
        this.checkVersion = version_check;
    }

    public boolean getVersionCheck() {
        return this.checkVersion;
    }

    public void setNoVersionMessage(String no_version_message) {
        if (no_version_message != null &&
                !no_version_message.isEmpty())
            this.invalidVersionMessage = no_version_message;
    }

    public void setNoPermisionMessage(String no_permision_message) {
        if (no_permision_message != null &&
                !no_permision_message.isEmpty())
            this.noPermissionMessage = no_permision_message;
    }

    public String getNoVersionMessage() {
        return this.invalidVersionMessage;
    }

    public List<String> getVersionList() {
        return this.version_list;
    }

    public void setLore(List<String> lore) {
        if (lore == null || lore.size() == 0) {
            this.lore = null;
            return;
        }
        this.lore = lore;
    }

    public void setPrice(Integer s) {
        if (s == null || s == 0) {
            this.price = null;
            return;
        }
        this.price = s;
    }

    public int getPrice() {
        if (this.price == null)
            return 0;
        return this.price;
    }

    public void setSlot(Integer s) {
        if (s == null || s == 0) {
            this.slot = null;
            return;
        }
        if (s < 1)
            s = 1;
        if (s > getMaxSlot(this.maximumRows))
            s = getMaxSlot(this.maximumRows);
        s = s - 1;
        this.slot = s;
    }

    public void setPermissionToView(boolean permission_to_view) {
        this.hasPermissionToView = permission_to_view;
    }

    public boolean getPermissionToView() {
        return this.hasPermissionToView;
    }

    public void setOpened(Boolean s) {
        this.opened = s;
    }

    public void setSkull(String skull) {
        if (skull == null || skull.length() == 0) {
            this.skull = null;
        } else {
            this.skull = skull;
        }
    }

    public void setEnchantGlow(boolean enchant_glow) {
        this.enchantmentGlowing = enchant_glow;
    }

    public void setPerm(String permission) {
        if (permission == null || permission.length() == 0) {
            this.permission = null;
        } else {
            this.permission = permission;
        }
    }

    public boolean like(ItemStack s, Player player) {
        if (s == null)
            return false;
        if (s.getType() != this.material)
            return false;
        ItemMeta meta = s.getItemMeta();
        if (this.name == null && meta.hasDisplayName())
            return false;
        if (this.name != null && !meta.hasDisplayName())
            return false;
        if (!meta.getDisplayName().equals(CoreVariables.replace(this.name, player)))
            return false;
        return this.data == null || this.data == s.getDurability();
    }

    private class MojangTexture extends BukkitRunnable {
        private SkullMeta skullMeta;

        private ItemStack itemStack;

        private Player player;

        private CoreLog log;

        private Inventory menu;

        private String playerTextureName;

        private MojangTexture(ItemStack itemStack, Player player, String playerTextureName, Inventory menu, CoreLog pluginLog, CoreLog log) {
            this.skullMeta = (SkullMeta) itemStack.getItemMeta();
            this.itemStack = itemStack;
            this.player = player;
            this.log = log;
            this.menu = menu;
            this.playerTextureName = playerTextureName;
        }

        public void run() {
            String textures = this.playerTextureName;
            String[] mojangTextures = CoreUtils.getTextureFromMojang(textures, this.log);
            if (mojangTextures != null) {
                GameProfile localProfile = new GameProfile(UUID.randomUUID(), CoreMenuItem.this.skull);
                localProfile.getProperties().put("textures", new Property("textures", mojangTextures[0], mojangTextures[1]));
                Field profileField;
                try {
                    profileField = this.skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(this.skullMeta, localProfile);
                    if (CoreMenuItem.this.name != null)
                        this.skullMeta.setDisplayName(CoreVariables.replace(CoreMenuItem.this.name, this.player));
                    if (CoreMenuItem.this.lore != null)
                        this.skullMeta.setLore(CoreVariables.replaceList(CoreMenuItem.this.lore, this.player));
                    this.itemStack.setItemMeta(this.skullMeta);
                    this.itemStack = CoreNBTAttribute.removeAttributes(this.itemStack);
                    if (CoreMenuItem.this.enchantmentGlowing)
                        this.itemStack = CoreNBTAttribute.addGlow(this.itemStack);
                    this.menu.setItem(CoreMenuItem.this.slot, this.itemStack);
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                    this.log.error("Error al dar formato al gameprofile con texturas", e);
                }
            }
        }
    }

    public ItemStack create(Player player, Inventory menu, CorePlugin plugin) {
        ItemStack item = new ItemStack(this.material);
        item = CoreNBTAttribute.removeAttributesPotion(item);
        if (this.data != null)
            item.setDurability(this.data);
        if (this.material.equals(CoreMaterial.pre113.SKULL_ITEM.get()) && ((this.data == null) ? 0 : this.data) == 3) {
            SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
            if (this.name != null) {
                String localName = this.name;
                skullMeta.setDisplayName(CoreVariables.replace(localName.replaceAll("<price>", String.valueOf(getPrice())), player));
            }
            if (this.lore != null) {
                List<String> localLore = new ArrayList<>();
                for (String l : this.lore)
                    localLore.add(l.replaceAll("<price>", String.valueOf(getPrice())));
                if (!hasPerm(player)) {
                    localLore.add(" ");
                    localLore.add(CoreColor.colorCodes(this.noPermissionMessage));
                    skullMeta.setLore(CoreVariables.replaceList(localLore, player));
                } else {
                    skullMeta.setLore(CoreVariables.replaceList(localLore, player));
                }
            } else if (!hasPerm(player)) {
                List<String> localLore = new ArrayList<>();
                localLore.add(" ");
                localLore.add(CoreColor.colorCodes(this.noPermissionMessage));
                skullMeta.setLore(CoreVariables.replaceList(localLore, player));
            }
            if (this.skull.contains("url:")) {
                String localString = this.skull;
                String url = localString.split("url:")[1].trim();
                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", new Object[]{url}).getBytes());
                profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
                Field profileField;
                try {
                    profileField = skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(skullMeta, profile);
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            } else if (this.skull.contains("textures:")) {
                String localString = this.skull;
                String textures = localString.split("textures:")[1].trim();
                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                profile.getProperties().put("textures", new Property("textures", textures));
                Field profileField;
                try {
                    profileField = skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(skullMeta, profile);
                } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            } else if (this.skull.contains("web:")) {
                String localString = this.skull;
                String playerTextureName = CoreVariables.replace(localString.split("web:")[1].trim(), player);
                plugin.getLog().debug("web: obteniendo texturas para " + playerTextureName + " desde mojan.");
                if (CoreUtils.playerTexturesCache.containsKey(playerTextureName)) {
                    plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + playerTextureName);
                    String[] cachedTextures = CoreUtils.playerTexturesCache.get(playerTextureName);
                    GameProfile localProfile = new GameProfile(UUID.randomUUID(), this.skull);
                    localProfile.getProperties().put("textures", new Property("textures", cachedTextures[0], cachedTextures[1]));
                    Field profileField = null;
                    try {
                        profileField = skullMeta.getClass().getDeclaredField("profile");
                        profileField.setAccessible(true);
                        profileField.set(skullMeta, localProfile);
                    } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                        plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                    }
                } else {
                    (new MojangTexture(item, player, playerTextureName, menu, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                }
            } else {
                Player bukkitPlayer = Bukkit.getPlayerExact(CoreVariables.replace(this.skull, player));
                if (bukkitPlayer != null) {
                    plugin.getLog().debug("Jugador en linea intentando obtener texturas del profile local.");
                    String[] localTextures = CoreUtils.getPlayerTextureLocal(bukkitPlayer, plugin.getLog());
                    if (localTextures != null) {
                        GameProfile localprofile = new GameProfile(UUID.randomUUID(), this.skull);
                        localprofile.getProperties().put("textures", new Property("textures", localTextures[0], localTextures[1]));
                        Field profileField = null;
                        try {
                            profileField = skullMeta.getClass().getDeclaredField("profile");
                            profileField.setAccessible(true);
                            profileField.set(skullMeta, localprofile);
                        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                            plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                        }
                    } else {
                        plugin.getLog().debug("Intentando obtener texturas desde mojan.");
                        String textureplayername = CoreVariables.replace(this.skull, player);
                        if (CoreUtils.playerTexturesCache.containsKey(textureplayername)) {
                            plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + textureplayername);
                            String[] cachetextures = CoreUtils.playerTexturesCache.get(textureplayername);
                            GameProfile localprofile = new GameProfile(UUID.randomUUID(), this.skull);
                            localprofile.getProperties().put("textures", new Property("textures", cachetextures[0], cachetextures[1]));
                            Field profileField;
                            try {
                                profileField = skullMeta.getClass().getDeclaredField("profile");
                                profileField.setAccessible(true);
                                profileField.set(skullMeta, localprofile);
                            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                                plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                            }
                        } else {
                            (new MojangTexture(item, player, textureplayername, menu, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                        }
                    }
                } else {
                    plugin.getLog().debug("Jugador fuera de linea intentando obtener texturas desde mojan.");
                    String playerTextureName = CoreVariables.replace(this.skull, player);
                    if (CoreUtils.playerTexturesCache.containsKey(playerTextureName)) {
                        plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + playerTextureName);
                        String[] cachedTextures = CoreUtils.playerTexturesCache.get(playerTextureName);
                        GameProfile localProfile = new GameProfile(UUID.randomUUID(), this.skull);
                        localProfile.getProperties().put("textures", new Property("textures", cachedTextures[0], cachedTextures[1]));
                        Field profileField;
                        try {
                            profileField = skullMeta.getClass().getDeclaredField("profile");
                            profileField.setAccessible(true);
                            profileField.set(skullMeta, localProfile);
                        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
                            plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                        }
                    } else {
                        (new MojangTexture(item, player, playerTextureName, menu, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                    }
                }
            }
            item.setItemMeta(skullMeta);
        } else {
            ItemMeta meta = item.getItemMeta();
            if (this.name != null) {
                String localName = this.name;
                meta.setDisplayName(CoreVariables.replace(localName.replaceAll("<price>", String.valueOf(getPrice())), player));
            }
            if (this.lore != null) {
                List<String> locallore = new ArrayList<>();
                for (String l : this.lore)
                    locallore.add(l.replaceAll("<price>", String.valueOf(getPrice())));
                if (!hasPerm(player)) {
                    locallore.add(" ");
                    locallore.add(CoreColor.colorCodes(this.noPermissionMessage));
                    meta.setLore(CoreVariables.replaceList(locallore, player));
                } else {
                    meta.setLore(CoreVariables.replaceList(locallore, player));
                }
            } else if (!hasPerm(player)) {
                List<String> locallore = new ArrayList<>();
                locallore.add(" ");
                locallore.add(CoreColor.colorCodes(this.noPermissionMessage));
                meta.setLore(CoreVariables.replaceList(locallore, player));
            }
            item.setItemMeta(meta);
        }
        this.material = item.getType();
        item = CoreNBTAttribute.removeAttributes(item);
        if (this.enchantmentGlowing)
            return CoreNBTAttribute.addGlow(item);
        return item;
    }

    public void executeCommands(Player p, JavaPlugin plugin, String prefix) {
        if (this.commands != null && this.commands.size() > 0)
            for (String s : this.commands) {
                CoreExecuteComands c = new CoreExecuteComands(p, s, plugin, prefix);
                c.execute();
            }
    }

    public boolean hasPerm(Player s) {
        if (this.permission == null)
            return true;
        return s.hasPermission(this.permission);
    }

    public void setData(Short s) {
        if (s == null || s == 0) {
            this.data = null;
            return;
        }
        this.data = s;
    }

    public void setCommands(List<String> c) {
        if (c == null || c.size() == 0) {
            this.commands = null;
            return;
        }
        this.commands = new ArrayList<>();
        this.commands.addAll(c);
    }

    public boolean isCommand() {
        return this.commands != null;
    }

    public boolean isOpened() {
        if (this.opened == null)
            return false;
        return this.opened;
    }

    public int getSlot() {
        if (this.slot == null)
            return 0;
        return this.slot;
    }

    private int getMaxSlot(int rows) {
        if (rows <= 0) {
            int i = 9;
            return i;
        }
        if (rows > 6) {
            int i = 54;
            return i;
        }
        return rows * 9;
    }
}
