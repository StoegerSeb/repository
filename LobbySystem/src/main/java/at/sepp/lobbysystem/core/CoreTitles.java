package at.sepp.lobbysystem.core;

import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;

public class CoreTitles {
    public static void sendTitles(Player player, String title) {
        send(player, 20, 50, 10, CoreColor.colorCodes(title), " ");
    }

    public static void sendTitles(Player player, String title, String subtitle) {
        send(player, 20, 50, 10, CoreColor.colorCodes(title), CoreColor.colorCodes(subtitle));
    }

    public static void sendTitles(Player player, Integer stay, String title, String subtitle) {
        send(player, 20, (stay <= 0) ? 50 : stay, 10, CoreColor.colorCodes(title), CoreColor.colorCodes(subtitle));
    }

    public static void sendTitles(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        send(player, (fadeIn <= 0) ? 20 : fadeIn, (stay <= 0) ? 50 : stay, (fadeOut <= 0) ? 10 : fadeOut, CoreColor.colorCodes(title), CoreColor.colorCodes(subtitle));
    }

    private static void send(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        try {
            Object chat = CoreReflection.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, "{\"text\":\"" + title + "\"}");
            Constructor<?> Constructor = CoreReflection.getNMSClass("PacketPlayOutTitle").getConstructor(CoreReflection.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], CoreReflection.getNMSClass("IChatBaseComponent"), int.class, int.class, int.class);
            Object timePacket = Constructor.newInstance(CoreReflection.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TIMES").get(null), chat, fadeIn, stay, fadeOut);
            Constructor = CoreReflection.getNMSClass("PacketPlayOutTitle").getConstructor(CoreReflection.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0], CoreReflection.getNMSClass("IChatBaseComponent"));
            Object titlePacket = Constructor.newInstance(CoreReflection.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("TITLE").get(null), chat);
            chat = CoreReflection.getNMSClass("IChatBaseComponent").getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, "{\"text\":\"" + subtitle + "\"}");
            Object subtitlePacket = Constructor.newInstance(CoreReflection.getNMSClass("PacketPlayOutTitle").getDeclaredClasses()[0].getField("SUBTITLE").get(null), chat);
            CoreReflection.sendPacket(player, timePacket);
            CoreReflection.sendPacket(player, titlePacket);
            CoreReflection.sendPacket(player, subtitlePacket);
        } catch (Exception var11) {
            var11.printStackTrace();
        }
    }
}
