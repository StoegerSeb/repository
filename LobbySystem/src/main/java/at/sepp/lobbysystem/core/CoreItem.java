package at.sepp.lobbysystem.core;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.*;

public class CoreItem {
    private Material material;
    private Short data = null;
    private String name = null;
    private String permission = null;
    private Integer slot = null;
    private List<String> lores;
    private List<String> commands;
    private int delay;
    private Map<String, Long> coolDowns = new HashMap<>();
    private String skull = null;
    private boolean enchantmentGlowing;
    private boolean checkVersion;
    private List<String> versionList;
    private String noVersionMessaging = "It is only available in the <version> versions.";

    public Map<String, Long> getCoolDowns() {
        return this.coolDowns;
    }

    public CoreItem(final Material material) {
        this.material = material;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        if (name == null || name.length() == 0) {
            this.name = null;
            return;
        }
        this.name = name;
    }

    public void setVersionList(final List<String> version) {
        this.versionList = version;
    }

    public void setVersionCheck(final boolean checkVersion) {
        this.checkVersion = checkVersion;
    }

    public boolean getVersionCheck() {
        return this.checkVersion;
    }

    public void setNoVersionMessage(final String noVersionMessage) {
        if (noVersionMessage != null &&
                !noVersionMessage.isEmpty())
            this.noVersionMessaging = noVersionMessage;
    }

    public String getNoVersionMessage() {
        return this.noVersionMessaging;
    }

    public List<String> getVersionList() {
        return this.versionList;
    }

    public void setLores(final List<String> lores) {
        if (lores == null || lores.size() == 0) {
            this.lores = null;
            return;
        }
        this.lores = lores;
    }

    public void setDelay(final Integer delay) {
        if (delay == null || delay == 0) {
            this.delay = 1;
            return;
        }
        if (delay < 1 &&
                delay != -1) {
            this.delay = 1;
            return;
        }
        this.delay = delay;
    }

    public void setSlot(Integer slot) {
        if (slot == null || slot == 0) {
            this.slot = null;
            return;
        }
        if (slot < 1)
            slot = 1;
        if (slot > 9)
            slot = 9;
        slot = slot - 1;
        this.slot = slot;
    }

    public void setSkull(final String skull) {
        if (skull == null || skull.length() == 0) {
            this.skull = null;
        } else {
            this.skull = skull;
        }
    }

    public void setEnchantGlow(final boolean enchantmentGlowing) {
        this.enchantmentGlowing = enchantmentGlowing;
    }

    public void setPerm(final String permission) {
        if (permission == null || permission.length() == 0) {
            this.permission = null;
        } else {
            this.permission = permission;
        }
    }

    public boolean like(final ItemStack itemStack, final Player player) {
        if (itemStack == null) return false;
        if (itemStack.getType() != this.material) return false;

        ItemMeta meta = itemStack.getItemMeta();

        if (this.name == null && meta.hasDisplayName()) return false;
        if (this.name != null && !meta.hasDisplayName()) return false;
        if (!meta.getDisplayName().equals(CoreVariables.replace(this.name, player))) return false;
        return this.data == null || this.data == itemStack.getDurability();
    }

    private class MojangTexture extends BukkitRunnable {
        private SkullMeta skullMeta;
        private ItemStack itemStack;
        private Player player;
        private CoreLog log;
        private String playerTextureName;

        private MojangTexture(final ItemStack itemStack, final Player player, final String playersTextureName, CoreLog pluginLog, final CoreLog log) {
            this.skullMeta = (SkullMeta)itemStack.getItemMeta();
            this.itemStack = itemStack;
            this.player = player;
            this.log = log;
            this.playerTextureName = playersTextureName;
        }

        public void run() {
            String textures = this.playerTextureName;
            String[] mojangTextures = CoreUtils.getTextureFromMojang(textures, this.log);
            if (mojangTextures != null) {
                GameProfile localProfile = new GameProfile(UUID.randomUUID(), CoreItem.this.skull);
                localProfile.getProperties().put("textures", new Property("textures", mojangTextures[0], mojangTextures[1]));
                Field profileField;

                try {
                    profileField = this.skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(this.skullMeta, localProfile);
                    if (CoreItem.this.name != null)
                        this.skullMeta.setDisplayName(CoreVariables.replace(CoreItem.this.name, this.player));
                    if (CoreItem.this.lores != null)
                        this.skullMeta.setLore(CoreVariables.replaceList(CoreItem.this.lores, this.player));
                    this.itemStack.setItemMeta(this.skullMeta);
                    this.itemStack = CoreNBTAttribute.removeAttributes(this.itemStack);
                    if (CoreItem.this.enchantmentGlowing)
                        this.itemStack = CoreNBTAttribute.addGlow(this.itemStack);
                    this.player.getInventory().setItem(CoreItem.this.slot, this.itemStack);
                } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e) {
                    this.log.error("Error loading GameProfile-Textures", e);
                }
            }
        }
    }

    public void give(final Player player, final CorePlugin plugin) {
        ItemStack item = new ItemStack(this.material);
        item = CoreNBTAttribute.removeAttributesPotion(item);
        if (this.data != null)
            item.setDurability(this.data);
        if (this.material.equals(CoreMaterial.pre113.SKULL_ITEM.get()) && this.data == 3) {
            SkullMeta skullMeta = (SkullMeta)item.getItemMeta();
            if (this.name != null)
                skullMeta.setDisplayName(CoreVariables.replace(this.name, player));
            if (this.lores != null)
                skullMeta.setLore(CoreVariables.replaceList(this.lores, player));
            if (this.skull.contains("url:")) {
                String localString = this.skull;
                String url = localString.split("url:")[1].trim();
                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", new Object[] { url }).getBytes());
                profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
                Field profileField;
                try {
                    profileField = skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(skullMeta, profile);
                } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            } else if (this.skull.contains("textures:")) {
                String localString = this.skull;
                String textures = localString.split("textures:")[1].trim();
                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                profile.getProperties().put("textures", new Property("textures", textures));
                Field profileField;
                try {
                    profileField = skullMeta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(skullMeta, profile);
                } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            } else if (this.skull.contains("web:")) {
                String localString = this.skull;
                String playerTextureName = CoreVariables.replace(localString.split("web:")[1].trim(), player);
                plugin.getLog().debug("web: obteniendo texturas para " + playerTextureName + " desde mojan.");
                if (CoreUtils.playerTexturesCache.containsKey(playerTextureName)) {
                    plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + playerTextureName);
                    String[] cachetextures = CoreUtils.playerTexturesCache.get(playerTextureName);
                    GameProfile localprofile = new GameProfile(UUID.randomUUID(), this.skull);
                    localprofile.getProperties().put("textures", new Property("textures", cachetextures[0], cachetextures[1]));
                    Field profileField = null;
                    try {
                        profileField = skullMeta.getClass().getDeclaredField("profile");
                        profileField.setAccessible(true);
                        profileField.set(skullMeta, localprofile);
                    } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e) {
                        plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                    }
                } else {
                    (new MojangTexture(item, player, playerTextureName, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                }
            } else {
                Player playerBukkit = Bukkit.getPlayerExact(CoreVariables.replace(this.skull, player));
                if (playerBukkit != null) {
                    plugin.getLog().debug("Jugador en linea intentando obtener texturas del profile local.");
                    String[] localTextures = CoreUtils.getPlayerTextureLocal(playerBukkit, plugin.getLog());
                    if (localTextures != null) {
                        GameProfile localProfile = new GameProfile(UUID.randomUUID(), this.skull);
                        localProfile.getProperties().put("textures", new Property("textures", localTextures[0], localTextures[1]));
                        Field profileField = null;
                        try {
                            profileField = skullMeta.getClass().getDeclaredField("profile");
                            profileField.setAccessible(true);
                            profileField.set(skullMeta, localProfile);
                        } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e) {
                            plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                        }
                    } else {
                        plugin.getLog().debug("Intentando obtener texturas desde mojan.");
                        String playerTextureName = CoreVariables.replace(this.skull, player);
                        if (CoreUtils.playerTexturesCache.containsKey(playerTextureName)) {
                            plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + playerTextureName);
                            String[] cachetextures = CoreUtils.playerTexturesCache.get(playerTextureName);
                            GameProfile localprofile = new GameProfile(UUID.randomUUID(), this.skull);
                            localprofile.getProperties().put("textures", new Property("textures", cachetextures[0], cachetextures[1]));
                            Field profileField;
                            try {
                                profileField = skullMeta.getClass().getDeclaredField("profile");
                                profileField.setAccessible(true);
                                profileField.set(skullMeta, localprofile);
                            } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e) {
                                plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                            }
                        } else {
                            (new MojangTexture(item, player, playerTextureName, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                        }
                    }
                } else {
                    plugin.getLog().debug("Couldn't receive Information of Offline-GameProfile.");
                    String textureplayername = CoreVariables.replace(this.skull, player);
                    if (CoreUtils.playerTexturesCache.containsKey(textureplayername)) {
                        plugin.getLog().debug("Textura obtenida desde el cache de mojan para: " + textureplayername);
                        String[] cachetextures = CoreUtils.playerTexturesCache.get(textureplayername);
                        GameProfile localprofile = new GameProfile(UUID.randomUUID(), this.skull);
                        localprofile.getProperties().put("textures", new Property("textures", cachetextures[0], cachetextures[1]));
                        Field profileField = null;
                        try {
                            profileField = skullMeta.getClass().getDeclaredField("profile");
                            profileField.setAccessible(true);
                            profileField.set(skullMeta, localprofile);
                        } catch (NoSuchFieldException|IllegalArgumentException|IllegalAccessException e) {
                            plugin.getLog().error("Error al dar formato al gameprofile con texturas", e);
                        }
                    } else {
                        (new MojangTexture(item, player, textureplayername, plugin.getLog(), null)).runTaskAsynchronously(plugin.getInstance());
                    }
                }
            }
            item.setItemMeta(skullMeta);
        } else {
            ItemMeta meta = item.getItemMeta();
            if (this.name != null)
                meta.setDisplayName(CoreVariables.replace(this.name, player));
            if (this.lores != null)
                meta.setLore(CoreVariables.replaceList(this.lores, player));
            item.setItemMeta(meta);
        }
        this.material = item.getType();
        item = CoreNBTAttribute.removeAttributes(item);
        if (this.enchantmentGlowing)
            item = CoreNBTAttribute.addGlow(item);
        player.getInventory().setItem(this.slot, item);
    }

    public void executeCommands(Player s, CorePlugin plugin, String click_wait, String prefix) {
        if (this.commands != null && this.commands.size() > 0)
            if (this.delay == -1) {
                for (String ss : this.commands) {
                    CoreExecuteComands c = new CoreExecuteComands(s, ss, plugin.getInstance(), prefix);
                    c.execute();
                }
            } else if (!this.coolDowns.containsKey(s.getName())) {
                long t = System.currentTimeMillis();
                this.coolDowns.put(s.getName(), t);
                for (String ss : this.commands) {
                    CoreExecuteComands c = new CoreExecuteComands(s, ss, plugin.getInstance(), prefix);
                    c.execute();
                }
            } else {
                int seconds = this.delay;
                int mil = 1000;
                long dm = (seconds * mil);
                long t = System.currentTimeMillis();
                long ptime = t - dm;
                long las = this.coolDowns.get(s.getName());
                if (las < ptime) {
                    for (String ss : this.commands) {
                        CoreExecuteComands c = new CoreExecuteComands(s, ss, plugin.getInstance(), prefix);
                        c.execute();
                    }
                    this.coolDowns.put(s.getName(), t);
                } else {
                    long left = las - ptime;
                    double leftSeng = (double) left / 1000.0D;
                    String m = click_wait.replaceAll("<time>", String.valueOf(leftSeng));
                    CoreColor.message(s, m);
                }
            }
    }

    public boolean hasPerm(Player s) {
        if (this.permission == null)
            return true;
        return s.hasPermission(this.permission);
    }

    public void setData(Short s) {
        if (s == null || s == 0) {
            this.data = null;
            return;
        }
        this.data = s;
    }

    public void setCommands(List<String> c) {
        if (c == null || c.size() == 0) {
            this.commands = null;
            return;
        }
        this.commands = new ArrayList<>();
        this.commands.addAll(c);
    }
}