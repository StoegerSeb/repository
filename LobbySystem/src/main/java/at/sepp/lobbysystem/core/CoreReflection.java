package at.sepp.lobbysystem.core;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class CoreReflection {
    public static Class<?> getNMSClass(final String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        return getClass("net.minecraft.server." + version + "." + name);
    }

    public static Class<?> getCraftClass(final String name) {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        return getClass("org.bukkit.craftbukkit." + version + "." + name);
    }

    public static void sendPacket(final Player player, final Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle", new Class[0]).invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", new Class[] { getNMSClass("Packet") }).invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Enum<?> getEnum(final String enumFullName) {
        String[] x = enumFullName.split("\\.(?=[^\\.]+$)");
        if (x.length == 2) {
            String enumClassName = x[0];
            String enumName = x[1];
            Class<Enum> cl = (Class)getClass(enumClassName);
            return Enum.valueOf(cl, enumName);
        }
        return null;
    }

    public static Class<?> getClass(final String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
