package at.sepp.lobbysystem.core;

import org.bukkit.command.CommandSender;

public abstract class CoreSubCommand {
    public abstract boolean onSubCommand(CommandSender paramCommandSender, String[] paramArrayOfString);
    public abstract String getErrorNoPermission();
    public abstract String getPermission();

    public boolean hasPermission(CommandSender sender) {
        if (getPermission() == null)
            return true;
        return sender.hasPermission(getPermission());
    }

    public boolean run(CommandSender sender, String cmd, String[] args) {
        if (!hasPermission(sender)) {
            sender.sendMessage(CoreColor.colorCodes(getErrorNoPermission()));
            return true;
        }
        onSubCommand(sender, args);
        return false;
    }
}
