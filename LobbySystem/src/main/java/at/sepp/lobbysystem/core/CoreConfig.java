package at.sepp.lobbysystem.core;

import org.apache.commons.lang.Validate;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;

public class CoreConfig {
    private FileConfiguration config;
    private File file;
    private String configFileName;
    private CoreLog log;

    public CoreConfig(final JavaPlugin plugin, final String configFile, final CoreLog log, final InputStream defaultData, final boolean update) {
        try {
            Validate.notNull(plugin);
            Validate.notNull(configFile);
            Validate.notNull(log);
            Validate.notNull(defaultData);
        } catch (IllegalArgumentException e) {
            log.fatalError("Error on create Object CoreConfig", e);
        }
        this.config = new YamlConfiguration();
        this.file = new File(plugin.getDataFolder(), String.valueOf(configFile) + ".yml");
        this.configFileName = configFile;
        this.log = log;
        if (!plugin.getDataFolder().exists())
            plugin.getDataFolder().mkdirs();
        YamlConfiguration defaultConfig = new YamlConfiguration();
        try {
            defaultConfig.load(new InputStreamReader(defaultData, StandardCharsets.UTF_8));
        } catch (IOException | InvalidConfigurationException e) {
            log.fatalError("Error loading  " + this.configFileName + ".yml.", e);
        }
        if (exists()) {
            load();
            if (update) {
                for (String nodeString : defaultConfig.getKeys(true))
                    add(nodeString, defaultConfig.get(nodeString));
                header(defaultConfig.options().header());
                silentSave();
            }
        } else {
            for (String nodeString : defaultConfig.getKeys(true))
                add(nodeString, defaultConfig.get(nodeString));
            header(defaultConfig.options().header());
            create();
        }
    }


    public boolean exists() {
        if (this.file.exists())
            return true;
        return false;
    }

    public void save() {
        String configTex = this.config.saveToString();
        try {
            Writer fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file), StandardCharsets.UTF_8));
            fileWriter.write(configTex);
            fileWriter.close();
            this.log.alert(String.valueOf(this.configFileName) + ".yml  save.");
        } catch (IOException e) {
            this.log.fatalError("Error on save " + this.configFileName + ".yml.", e);
        }
    }

    public void silentSave() {
        String configTex = this.config.saveToString();
        try {
            Writer fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file), StandardCharsets.UTF_8));
            fileWriter.write(configTex);
            fileWriter.close();
        } catch (IOException e) {
            this.log.fatalError("Error on save " + this.configFileName + ".yml.", e);
        }
    }

    public void load() {
        this.log.info("Load " + this.configFileName + ".yml");
        try {
            FileInputStream fileinputstream = new FileInputStream(this.file);
            this.config.load(new InputStreamReader(fileinputstream, StandardCharsets.UTF_8));
            this.log.alert(String.valueOf(this.configFileName) + ".yml loaded.");
        } catch (IOException | InvalidConfigurationException e) {
            this.log.fatalError("Error on loaded " + this.configFileName + ".yml.", e);
        }
    }


    public void create() {
        String configTex = this.config.saveToString();
        this.log.alert("The " + this.configFileName + ".yml file does not exist.");
        this.log.info("Creating and loading file " + this.configFileName + ".yml.");
        try {
            Writer fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file), StandardCharsets.UTF_8));
            fileWriter.write(configTex);
            fileWriter.close();
            this.log.alert(this.configFileName + ".yml  created.");
        } catch (IOException e) {
            this.log.fatalError("Error creating " + this.configFileName + ".yml.", e);
            e.printStackTrace();
        }
    }


    public void add(final String path, final String value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final long value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final boolean value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final List<String> value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final int value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final double value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public void add(final String path, final Object value) {
        if (!this.config.isSet(path))
            this.config.set(path, value);
    }

    public boolean getBoolean(final String path) {
        return this.config.getBoolean(path);
    }

    public String getString(final String path) {
        return this.config.getString(path);
    }

    public int getInt(final String path) {
        return this.config.getInt(path);
    }

    public List<String> getStringList(final String path) {
        return this.config.getStringList(path);
    }

    public ConfigurationSection getConfigurationSection(final String path) {
        return this.config.getConfigurationSection(path);
    }

    public Double getDouble(final String path) {
        return this.config.getDouble(path);
    }

    public Set<String> getKeys(final Boolean bo) {
        return this.config.getKeys(bo);
    }

    public void set(final String path, final String value) {
        this.config.set(path, value);
    }

    public void setNull(final String path) {
        this.config.set(path, null);
    }

    public void set(final String path, final double value) {
        this.config.set(path, value);
    }

    public void set(final String path, final List<String> value) {
        this.config.set(path, value);
    }

    public void set(final String path, final int value) {
        this.config.set(path, value);
    }

    public void set(final String path, final boolean value) {
        this.config.set(path, value);
    }

    public boolean isSet(final String path) {
        return this.config.isSet(path);
    }

    public boolean contains(final String path) {
        return this.config.contains(path);
    }

    public void header(final String header) {
        this.config.options().header(header);
    }
}