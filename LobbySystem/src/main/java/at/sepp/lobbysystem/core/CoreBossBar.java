package at.sepp.lobbysystem.core;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class CoreBossBar {
    public static float bossBarProgress = 0.5F;

    protected interface BossBar {
        void setName(final String paramString);
        void destroyPacket();
        void send();
        float getProgress();
        void update();
        boolean isSend();
        void setProgress(final float paramFloat);
    }

    protected static class BossBarPos1_9 implements BossBar {
        private UUID uuid;
        private String name;
        private Player player;
        private boolean send = false;
        private Object PacketPlayOutBoss;
        private float progress;
        private boolean isProgress;
        private Class<?> BossBattle_BarColorClass = CoreReflection.getNMSClass("BossBattle$BarColor");
        private Class<?> BossBattle_BarStyleClass = CoreReflection.getNMSClass("BossBattle$BarStyle");
        private Class<?> PacketPlayOutBoss_ActionClass = CoreReflection.getNMSClass("PacketPlayOutBoss$Action");
        private Class<?> PacketPlayOutBossClass = CoreReflection.getNMSClass("PacketPlayOutBoss");
        Class<?> ChatSerializerClass = CoreReflection.getNMSClass("IChatBaseComponent$ChatSerializer");
        private CoreBossBar.Color color;
        private CoreBossBar.Style style;

        public BossBarPos1_9(final Player player, final String title, final CoreBossBar.Color color, final CoreBossBar.Style style, final float pro) {
            this.name = title;
            this.uuid = UUID.randomUUID();
            this.player = player;
            this.color = color;
            this.style = style;

            if (pro > 0.0F) {
                this.isProgress = true;
                this.progress = pro;
            } else {
                this.isProgress = false;
                this.progress = 1.0F;
            }
            try {
                createBossBattleServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void createBossBattleServer() throws Exception {
            this.PacketPlayOutBoss = this.PacketPlayOutBossClass.newInstance();
            Field aField = this.PacketPlayOutBoss.getClass().getDeclaredField("a");
            aField.setAccessible(true);
            aField.set(this.PacketPlayOutBoss, this.uuid);
            Field bField = this.PacketPlayOutBoss.getClass().getDeclaredField("b");
            bField.setAccessible(true);
            bField.set(this.PacketPlayOutBoss, this.PacketPlayOutBoss_ActionClass.getEnumConstants()[0]);
            Field cField = this.PacketPlayOutBoss.getClass().getDeclaredField("c");
            cField.setAccessible(true);
            cField.set(this.PacketPlayOutBoss, this.ChatSerializerClass.getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\": \"" + this.name + "\"}"));
            Field dField = this.PacketPlayOutBoss.getClass().getDeclaredField("d");
            dField.setAccessible(true);
            dField.set(this.PacketPlayOutBoss, this.progress);
            Field eField = this.PacketPlayOutBoss.getClass().getDeclaredField("e");
            eField.setAccessible(true);
            eField.set(this.PacketPlayOutBoss, this.BossBattle_BarColorClass.getEnumConstants()[this.color.ordinal()]);
            Field fField = this.PacketPlayOutBoss.getClass().getDeclaredField("f");
            fField.setAccessible(true);
            fField.set(this.PacketPlayOutBoss, this.BossBattle_BarStyleClass.getEnumConstants()[this.style.ordinal()]);
            Field gField = this.PacketPlayOutBoss.getClass().getDeclaredField("g");
            gField.setAccessible(true);
            gField.set(this.PacketPlayOutBoss, Boolean.FALSE);
            Field hField = this.PacketPlayOutBoss.getClass().getDeclaredField("h");
            hField.setAccessible(true);
            hField.set(this.PacketPlayOutBoss, Boolean.FALSE);
            Field iField = this.PacketPlayOutBoss.getClass().getDeclaredField("i");
            iField.setAccessible(true);
            iField.set(this.PacketPlayOutBoss, Boolean.FALSE);
        }

        public void setName(final String paramString) {
            this.name = paramString;
            try {
                Field cField = this.PacketPlayOutBoss.getClass().getDeclaredField("c");
                cField.setAccessible(true);
                cField.set(this.PacketPlayOutBoss, this.ChatSerializerClass.getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\": \"" + this.name + "\"}"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void destroyPacket() {
            try {
                Field bField = this.PacketPlayOutBoss.getClass().getDeclaredField("b");
                bField.setAccessible(true);
                bField.set(this.PacketPlayOutBoss, this.PacketPlayOutBoss_ActionClass.getEnumConstants()[1]);
                CoreReflection.sendPacket(this.player, this.PacketPlayOutBoss);
                this.send = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void send() {
            try {
                Field bField = this.PacketPlayOutBoss.getClass().getDeclaredField("b");
                bField.setAccessible(true);
                bField.set(this.PacketPlayOutBoss, this.PacketPlayOutBoss_ActionClass.getEnumConstants()[0]);
                CoreReflection.sendPacket(this.player, this.PacketPlayOutBoss);
                this.send = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void update() {
            try {
                if (this.isProgress) {
                    Field bField = this.PacketPlayOutBoss.getClass().getDeclaredField("b");
                    bField.setAccessible(true);
                    bField.set(this.PacketPlayOutBoss, this.PacketPlayOutBoss_ActionClass.getEnumConstants()[2]);
                    CoreReflection.sendPacket(this.player, this.PacketPlayOutBoss);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setProgress(float progress) {
            if (progress > 1.0F)
                progress /= 100.0F;
            if (progress != this.progress) {
                this.progress = progress;
                if (this.isProgress)
                    try {
                        Field dField = this.PacketPlayOutBoss.getClass().getDeclaredField("d");
                        dField.setAccessible(true);
                        dField.set(this.PacketPlayOutBoss, this.progress);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }

        public boolean isSend() {
            return this.send;
        }

        public float getProgress() {
            return this.progress;
        }
    }

    protected static class BossBarPre1_9 implements BossBar {
        private int id = (new Random()).nextInt();
        private String name;
        private float health;
        private Object PacketPlayOutSpawnEntityLivin;
        private Player player;
        private boolean isProgress;
        private Class<?> EntityClass = CoreReflection.getNMSClass("Entity");
        private Class<?> DataWatcherClass = CoreReflection.getNMSClass("DataWatcher");
        private Class<?> PacketPlayOutSpawnEntityLivinClass = CoreReflection.getNMSClass("PacketPlayOutSpawnEntityLiving");
        private boolean send = false;

        public BossBarPre1_9(final Player player, final String title, final float pro) {
            this.name = title;
            this.player = player;
            if (pro > 0.0F) {
                this.isProgress = true;
                this.health = pro;
            } else {
                this.isProgress = false;
                this.health = 300.0F;
            }
            try {
                createDragon();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void createDragon() throws Exception {
            this.PacketPlayOutSpawnEntityLivin = this.PacketPlayOutSpawnEntityLivinClass.newInstance();
            Location location = makeLocation(this.player.getLocation());
            Field aField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("a");
            aField.setAccessible(true);
            aField.set(this.PacketPlayOutSpawnEntityLivin, this.id);
            Field bField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("b");
            bField.setAccessible(true);
            bField.set(this.PacketPlayOutSpawnEntityLivin, 64);
            Field cField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("c");
            cField.setAccessible(true);
            cField.set(this.PacketPlayOutSpawnEntityLivin, location.getBlockX() * 32);
            Field dField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("d");
            dField.setAccessible(true);
            dField.set(this.PacketPlayOutSpawnEntityLivin, location.getBlockY() * 32);
            Field eField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("e");
            eField.setAccessible(true);
            eField.set(this.PacketPlayOutSpawnEntityLivin, location.getBlockZ() * 32);
            Field iField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("i");
            iField.setAccessible(true);
            iField.set(this.PacketPlayOutSpawnEntityLivin, (byte) ((int) location.getYaw() * 256 / 360));
            Field jField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("j");
            jField.setAccessible(true);
            jField.set(this.PacketPlayOutSpawnEntityLivin, (byte) ((int) location.getPitch() * 256 / 360));
            Field kField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("k");
            kField.setAccessible(true);
            kField.set(this.PacketPlayOutSpawnEntityLivin, (byte) ((int) location.getPitch() * 256 / 360));
            Field lField = this.PacketPlayOutSpawnEntityLivin.getClass().getDeclaredField("l");
            lField.setAccessible(true);
            lField.set(this.PacketPlayOutSpawnEntityLivin, getWatcher());
        }

        public void setName(final String paramString) {
            try {
                this.name = paramString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void destroyPacket() {
            try {
                Class<?> PacketPlayOutEntityDestroy = CoreReflection.getNMSClass("PacketPlayOutEntityDestroy");
                Object packet = PacketPlayOutEntityDestroy.newInstance();
                Field a = PacketPlayOutEntityDestroy.getDeclaredField("a");
                a.setAccessible(true);
                a.set(packet, new int[] { this.id});
                CoreReflection.sendPacket(this.player, packet);
                this.send = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void send() {
            try {
                CoreReflection.sendPacket(this.player, this.PacketPlayOutSpawnEntityLivin);
                teleport();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void teleport() throws Exception {
            Location loc = makeLocation(this.player.getLocation());
            Constructor<?> constructorPacketPlayOutEntityTeleport = CoreReflection.getNMSClass("PacketPlayOutEntityTeleport").getConstructor(int.class, int.class, int.class, int.class, byte.class, byte.class, boolean.class);
            Object packet = constructorPacketPlayOutEntityTeleport.newInstance(this.id, loc.getBlockX() * 32, loc.getBlockY() * 32, loc.getBlockZ() * 32, (byte) ((int) loc.getYaw() * 256 / 360), (byte) ((int) loc.getPitch() * 256 / 360), Boolean.FALSE);
            CoreReflection.sendPacket(this.player, packet);
        }

        private void sendMetaPacket() throws Exception {
            Constructor<?> constructorMeta = CoreReflection.getNMSClass("PacketPlayOutEntityMetadata").getConstructor(int.class, this.DataWatcherClass, boolean.class);
            Object packet = constructorMeta.newInstance(this.id, getWatcher(), Boolean.TRUE);
            CoreReflection.sendPacket(this.player, packet);
        }

        public Object getWatcher() {
            Object watcher = null;
            try {
                Object entyti = null;
                watcher = this.DataWatcherClass.getConstructor(new Class[] { this.EntityClass }).newInstance(entyti);
                Method a = this.DataWatcherClass.getMethod("a", int.class, Object.class);
                a.invoke(watcher, 0, (byte) 32);
                a.invoke(watcher, 6, this.isProgress ? this.health : 300.0F);
                a.invoke(watcher, 7, 0);
                a.invoke(watcher, 8, (byte) 0);
                a.invoke(watcher, 10, this.name);
                a.invoke(watcher, 2, this.name);
                a.invoke(watcher, 11, (byte) 1);
                a.invoke(watcher, 17, 0);
                a.invoke(watcher, 18, 0);
                a.invoke(watcher, 19, 0);
                a.invoke(watcher, 20, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return watcher;
        }


        protected Location makeLocation(final Location base) {
            return base.getDirection().multiply(32).add(base.toVector()).toLocation(base.getWorld());
        }

        private static BlockFace getDirection(final Location loc) {
            float dir = Math.round(loc.getYaw() / 90.0F);
            if (dir == -4.0F || dir == 0.0F || dir == 4.0F)
                return BlockFace.SOUTH;
            if (dir == -1.0F || dir == 3.0F)
                return BlockFace.EAST;
            if (dir == -2.0F || dir == 2.0F)
                return BlockFace.NORTH;
            if (dir == -3.0F || dir == 1.0F)
                return BlockFace.WEST;
            return null;
        }

        public void update() {
            try {
                if (this.isProgress)
                    sendMetaPacket();
                teleport();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public boolean isSend() {
            return this.send;
        }

        public float getProgress() {
            return this.health;
        }

        public void setProgress(final float progress) {
            this.health = progress;
        }
    }

    private static Map<UUID, BossBar> bossBar = new ConcurrentHashMap<>();

    private static HashMap<UUID, Integer> timers = new HashMap<>();

    public enum Color {
        PINK, BLUE, RED, GREEN, YELLOW, PURPLE, WHITE
    }

    public enum Style {
        PROGRESS, NOTCHED_6, NOTCHED_10, NOTCHED_12, NOTCHED_20
    }

    public static void sendBossBar(final Player player, final String title, final String color, final String style, final int seconds, final Plugin plugin) {
        Color c;
        Style s;
        try {
            c = Color.valueOf(color);
            s = Style.valueOf(style);
        } catch (IllegalArgumentException e) {
            c = Color.BLUE;
            s = Style.PROGRESS;
        }
        sendBossBar(player, title, c, s, seconds, 3, plugin);
    }

    private static void sendBossBar(final Player player, final String message, final Color color, final Style style, final int seconds, final int interval, final Plugin plugin) {
        if (hasSendBar(player))
            removeSendBar(player);
        if (CoreUtils.mc1_7 || CoreUtils.mc1_8) {
            sendPre1_9(player, message, seconds, 0.0F, interval, plugin);
        } else {
            sendPos1_9(player, message, color, style, seconds, 0.0F, interval, plugin);
        }
    }

    private static void sendPos1_9(final Player player, final String message, final Color color, final Style style, final int seconds, final float pro, final int interval, final Plugin plugin) {
        BossBar bar = getBossBar(player, cleanMessage(message), color, style, pro);
        bar.send();
        final float progressMinus = bar.getProgress() / (seconds * 20 / interval);
        cancelTimer(player);
        timers.put(player.getUniqueId(), Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {
            public void run() {
                BossBar bar = CoreBossBar.bossBar.get(player.getUniqueId());
                float newProgress = bar.getProgress() - progressMinus;
                if (newProgress <= 0.0F) {
                    CoreBossBar.cancelBossBar(player);
                    CoreBossBar.cancelTimer(player);
                } else {
                    bar.setProgress(newProgress);
                    bar.update();
                }
            }
        }, interval, interval).getTaskId());
    }

    private static void sendPre1_9(final Player player, final String message, final int seconds, final float pro, final int interval, final Plugin plugin) {
        BossBar bar = getBossBar(player, cleanMessage(message), null, null, pro);
        bar.send();
        final float progressMinus = bar.getProgress() / (seconds * 20 / interval);
        cancelTimer(player);
        timers.put(player.getUniqueId(), Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {
            public void run() {
                BossBar bar = CoreBossBar.bossBar.get(player.getUniqueId());
                float newProgress = bar.getProgress() - progressMinus;
                if (newProgress <= 1.0F) {
                    CoreBossBar.cancelBossBar(player);
                    CoreBossBar.cancelTimer(player);
                } else {
                    bar.setProgress(newProgress);
                    bar.update();
                }
            }
        }, interval, interval).getTaskId());
    }

    private static void cancelBossBar(final Player player) {
        if (bossBar.containsKey(player.getUniqueId())) {
            BossBar bar = bossBar.get(player.getUniqueId());
            bar.destroyPacket();
            bossBar.remove(player.getUniqueId());
        }
    }

    private static BossBar getBossBar(final Player player, final String name, final Color color, final Style style, final float pro) {
        BossBar bossBar;
        if (CoreUtils.mc1_7 || CoreUtils.mc1_8) {
            bossBar = new BossBarPre1_9(player, name, pro);
        } else {
            bossBar = new BossBarPos1_9(player, name, color, style, pro);
        }
        CoreBossBar.bossBar.put(player.getUniqueId(), bossBar);
        return bossBar;
    }

    private static String cleanMessage(String message) {
        if (message.length() > 64)
            message = message.substring(0, 63);
        return message;
    }

    private static void cancelTimer(final Player player) {
        Integer timerID = timers.remove(player.getUniqueId());
        if (timerID != null)
            Bukkit.getScheduler().cancelTask(timerID);
    }

    private static void removeSendBar(final Player player) {
        if (!hasSendBar(player))
            return;
        cancelBossBar(player);
        cancelTimer(player);
    }

    private static boolean hasSendBar(final Player player) {
        return (bossBar.get(player.getUniqueId()) != null);
    }
}
