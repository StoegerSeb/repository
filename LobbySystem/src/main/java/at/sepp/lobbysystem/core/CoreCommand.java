package at.sepp.lobbysystem.core;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CoreCommand implements CommandExecutor {
    public abstract boolean onCommand(CommandSender paramCommandSender, String paramString, String[] paramArrayOfString);

    public abstract String getErrorNoPermission();

    public abstract String getPerm();

    public boolean hasPerm(CommandSender sender) {
        if (getPerm() == null)
            return true;
        return sender.hasPermission(getPerm());
    }

    private Map<List<String>, CoreSubCommand> subCommands = new HashMap<>();

    public void addSubCommand(List<String> subCommands, CoreSubCommand s) {
        this.subCommands.put(subCommands, s);
    }

    public boolean onCommand(final CommandSender sender, final Command command, final String arg2, final String[] arg3) {
        if (!hasPerm(sender)) {
            sender.sendMessage(CoreColor.colorCodes(getErrorNoPermission()));
            return true;
        }
        boolean retVal = false;
        if (arg3.length == 0)
            return onCommand(sender, command.getName(), arg3);
        for (List<String> s : this.subCommands.keySet()) {
            if (s.contains(arg3[0].toLowerCase())) {
                String[] args = new String[arg3.length - 1];
                System.arraycopy(arg3, 1, args, 0, arg3.length - 1);
                retVal = (this.subCommands.get(s)).onSubCommand(sender, args);
                break;
            }
        }
        if (!retVal)
            return onCommand(sender, command.getName(), arg3);
        return retVal;
    }
}