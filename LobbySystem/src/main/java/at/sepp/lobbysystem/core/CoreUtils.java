package at.sepp.lobbysystem.core;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class CoreUtils {
    public static boolean mc1_7 = Bukkit.getBukkitVersion().split("-")[0].contains("1.7");
    public static boolean mc1_8 = Bukkit.getBukkitVersion().split("-")[0].contains("1.8");
    public static boolean mc1_9 = Bukkit.getBukkitVersion().split("-")[0].contains("1.9");
    public static boolean mc1_10 = Bukkit.getBukkitVersion().split("-")[0].contains("1.10");
    public static boolean mc1_11 = Bukkit.getBukkitVersion().split("-")[0].contains("1.11");
    public static boolean mc1_12 = Bukkit.getBukkitVersion().split("-")[0].contains("1.12");
    public static boolean mc1_13_0 = Bukkit.getBukkitVersion().split("-")[0].equalsIgnoreCase("1.13");

    public static Map<String, String[]> playerTexturesCache = new HashMap<>();

    public static String[] getPlayerTextureLocal(Player playerBukkit, CoreLog log) {
        String[] a = null;
        Class<?> strClass = CoreReflection.getCraftClass("entity.CraftPlayer");
        try {
            GameProfile profile = (GameProfile) strClass.cast(playerBukkit).getClass().getMethod("getProfile", new Class[0]).invoke(strClass.cast(playerBukkit), new Object[0]);
            Property property = profile.getProperties().get("textures").iterator().next();
            String texture = property.getValue();
            String signature = property.getSignature();
            a = new String[]{texture, signature};
            log.debug("Textura obtenida localmente para: " + playerBukkit.getName());
        } catch (Exception e) {
            log.debug("Error al intentar obtener texturas localmente para " + playerBukkit.getName(), e);
        }
        return a;
    }

    public static String[] getTextureFromMojang(String name, CoreLog log) {
        try {
            if (playerTexturesCache.containsKey(name)) {
                log.debug("Textura obtenida desde el cache de mojan para: " + name);
                return playerTexturesCache.get(name);
            }
            URL url_0 = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
            InputStreamReader reader_0 = new InputStreamReader(url_0.openStream());
            String uuid = (new JsonParser()).parse(reader_0).getAsJsonObject().get("id").getAsString();
            URL url_1 = new URL("https://sessionserver.mojang.com/session/minecraft/profile/" + uuid + "?unsigned=false");
            InputStreamReader reader_1 = new InputStreamReader(url_1.openStream());
            JsonObject textureProperty = (new JsonParser()).parse(reader_1).getAsJsonObject().get("properties").getAsJsonArray().get(0).getAsJsonObject();
            String texture = textureProperty.get("value").getAsString();
            String signature = textureProperty.get("signature").getAsString();
            log.debug("Textura obtenida desde la web de mojan para: " + name);
            String[] playerTexture = {texture, signature};
            playerTexturesCache.put(name, playerTexture);
            return playerTexture;
        } catch (IOException | IllegalStateException e) {
            log.debug("Error al intentar obtener texturas desde la web mojan para " + name, e);
            return null;
        }
    }

    public static boolean isDouble(String s) {
        try {
            double i = Double.parseDouble(s);
            return true;
        } catch (NumberFormatException er) {
            return false;
        }
    }

    public static boolean isInt(String s) {
        try {
            int i = Integer.parseInt(s);
            return true;
        } catch (NumberFormatException er) {
            return false;
        }
    }

    public static boolean isPre1_13() {
        return mc1_8 || mc1_9 || mc1_10 || mc1_11 || mc1_12;
    }
}