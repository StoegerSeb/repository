package at.sepp.lobbysystem.core;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class CoreLog {

    private JavaPlugin javaPlugin;
    private Color color;
    private String prefix;

    public enum Color {
        AQUA(ChatColor.AQUA),
        BLACK(ChatColor.BLACK),
        BLUE(ChatColor.BLUE),
        DARK_AQUA(ChatColor.DARK_AQUA),
        DARK_BLUE(ChatColor.DARK_BLUE),
        DARK_GRAY(ChatColor.DARK_GRAY),
        DARK_GREEN(ChatColor.DARK_GREEN),
        DARK_PURPLE(ChatColor.DARK_PURPLE),
        DARK_RED(ChatColor.DARK_RED),
        GOLD(ChatColor.GOLD),
        GRAY(ChatColor.GRAY),
        GREEN(ChatColor.GREEN),
        LIGHT_PURPLE(ChatColor.LIGHT_PURPLE),
        RED(ChatColor.RED),
        WHITE(ChatColor.WHITE),
        YELLOW(ChatColor.YELLOW);

        private ChatColor chatColor;

        Color(final ChatColor chatColor) {
            this.chatColor = chatColor;
        }

        public ChatColor get() {
            return this.chatColor;
        }
    }

    private boolean debug = false;

    public CoreLog(final JavaPlugin javaPlugin, final Color color) {
        this.javaPlugin = javaPlugin;
        this.color = color;
        this.prefix = this.color.get() + "[" + Color.GRAY.get() + this.javaPlugin.getName() + this.color.get() + "]";
    }

    public void setDebug(final boolean debug) {
        this.debug = debug;
    }

    public void info(final String info) {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes("&7 " + info));
    }

    public void alert(final String alert) {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes("&8 " + alert));
    }

    public void error(final String info, final Throwable e) {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes(" &4Error:&c " + info));
        e.printStackTrace();
    }

    public void fatalError(final String info, final Throwable e) {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes(" &4Fatal-Error:&c " + info));
        e.printStackTrace();
        this.javaPlugin.getServer().getPluginManager().disablePlugin(this.javaPlugin);
    }

    public void error(final String info) {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes("&c " + info));
    }

    public void debug(final String info) {
        if (this.debug)
            this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes(" &8Debug:&c " + info));
    }

    public void debug(final String info, final  Throwable e) {
        if (this.debug) {
            this.javaPlugin.getServer().getConsoleSender().sendMessage(this.prefix + CoreColor.colorCodes(" &8Debug:&c " + info));
            e.printStackTrace();
        }
    }

    public void line() {
        this.javaPlugin.getServer().getConsoleSender().sendMessage(this.color.get() + "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
    }
}
