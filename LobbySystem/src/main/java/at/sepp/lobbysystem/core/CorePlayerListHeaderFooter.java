package at.sepp.lobbysystem.core;

import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class CorePlayerListHeaderFooter {

    public static void sendHeaderFooter(Player player, String header, String footer) {
        try {
            if (CoreUtils.mc1_7 || CoreUtils.mc1_8 || CoreUtils.mc1_9 || CoreUtils.mc1_10 || CoreUtils.mc1_11 || CoreUtils.mc1_12 || CoreUtils.mc1_13_0) {
                sendPre1_13_1(player, header, footer);
            } else {
                sendPos1_13_1(player, header, footer);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void sendPos1_13_1(Player player, String header, String footer) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, InstantiationException {
        Class<?> chat = CoreReflection.getNMSClass("IChatBaseComponent");
        Object tabHeader = chat.getDeclaredClasses()[0].getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\":\"" + header + "\"}");
        Object tabFooter = chat.getDeclaredClasses()[0].getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\":\"" + footer + "\"}");
        Constructor<?> constructor = CoreReflection.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor();
        Object packet = constructor.newInstance();
        Field aField = packet.getClass().getDeclaredField("header");
        aField.setAccessible(true);
        aField.set(packet, tabHeader);
        Field bField = packet.getClass().getDeclaredField("footer");
        bField.setAccessible(true);
        bField.set(packet, tabFooter);
        CoreReflection.sendPacket(player, packet);
    }

    private static void sendPre1_13_1(Player player, String header, String footer) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, InstantiationException {
        Class<?> chat = CoreReflection.getNMSClass("IChatBaseComponent");
        Object tabHeader = chat.getDeclaredClasses()[0].getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\":\"" + header + "\"}");
        Object tabFooter = chat.getDeclaredClasses()[0].getMethod("a", new Class[] { String.class }).invoke(null, "{\"text\":\"" + footer + "\"}");
        Constructor<?> constructor = CoreReflection.getNMSClass("PacketPlayOutPlayerListHeaderFooter").getConstructor();
        Object packet = constructor.newInstance();
        Field aField = packet.getClass().getDeclaredField("a");
        aField.setAccessible(true);
        aField.set(packet, tabHeader);
        Field bField = packet.getClass().getDeclaredField("b");
        bField.setAccessible(true);
        bField.set(packet, tabFooter);
        CoreReflection.sendPacket(player, packet);
    }
}
