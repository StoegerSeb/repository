package at.sepp.lobbysystem.core;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CoreColor {

    public static String colorCodes(final String nonColoredText) {
        return ChatColor.translateAlternateColorCodes('&', nonColoredText);
    }

    public static void message(final Player player, final String paramString) {
        player.sendMessage(colorCodes(paramString));
    }

    public static void message(final CommandSender player, final String paramString) {
        player.sendMessage(colorCodes(paramString));
    }

    public static String colorizeTextComponent(final String string) {
        if (string == null || string.length() == 0) return " ";

        String localString = colorCodes(string.trim());
        String newString = "";
        String last = "§7";

        if (localString.contains(" ")) {
            String[] phrases = localString.split(" ");

            for (String s : phrases) {
                String phrase = s.trim();

                if (phrase.startsWith("§")) {
                    newString = newString + " " + phrase;
                } else {
                    newString = newString + " " + last + phrase;
                }

                for (int j = 0; j < phrase.length(); j++) {
                    char c = phrase.charAt(j);
                    char m = (phrase.length() > 2) ? phrase.charAt(j + 2) : ' ';

                    if (c == '§' && m == '§') {
                        last = "§" + phrase.charAt(j + 1) + "§" + phrase.charAt(j + 3);
                        j = 3;
                    } else if (c == '§') {
                        last = "§" + phrase.charAt(j + 1);
                        j = 1;
                    }
                }
            }
            return newString.trim();
        }
        return localString;
    }

    public static List<String> getColorLis() {
        List<String> codes = new ArrayList<>();
        codes.add("&0");
        codes.add("&1");
        codes.add("&2");
        codes.add("&3");
        codes.add("&4");
        codes.add("&5");
        codes.add("&6");
        codes.add("&7");
        codes.add("&8");
        codes.add("&9");
        codes.add("&a");
        codes.add("&b");
        codes.add("&c");
        codes.add("&d");
        codes.add("&e");
        codes.add("&f");
        codes.add("&k");
        codes.add("&l");
        codes.add("&m");
        codes.add("&n");
        codes.add("&o");
        codes.add("&r");
        codes.add("&A");
        codes.add("&B");
        codes.add("&C");
        codes.add("&D");
        codes.add("&E");
        codes.add("&F");
        codes.add("&K");
        codes.add("&L");
        codes.add("&M");
        codes.add("&N");
        codes.add("&O");
        codes.add("&R");
        return codes;
    }
}
