package at.sepp.lobbysystem.core;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CoreActionBar {
    public static Map<UUID, Integer> timers = new HashMap<>();

    private static class SendRun extends BukkitRunnable {
        private int duration;
        private Player player;
        private String message;

        public SendRun(final int duration, final Player player, final String message) {
            this.duration = duration;
            this.player = player;
            this.message = message;
        }

        public void run() {
            if (this.duration > 0) {
                CoreActionBar.sendActionBar(this.player, this.message);
                this.duration -= 20;
            } else {
                cancel();
                CoreActionBar.timers.remove(this.player.getUniqueId());
            }
        }
    }

    private static void cancelTimer(final Player player) {
        Integer timerID = timers.remove(player.getUniqueId());
        if (timerID != null)
            Bukkit.getScheduler().cancelTask(timerID);
    }

    public static void sendActionBar(final Player player, final String message, final int duration, final Plugin plugin) {
        if (timers.containsKey(player.getUniqueId()))
            cancelTimer(player);
        timers.put(player.getUniqueId(), (new SendRun(duration, player, message)).runTaskTimer(plugin, 0L, 20L).getTaskId());
    }

    public static void sendActionBar(final Player player, final String message) {
        if (!player.isOnline())
            return;
        boolean mc18 = Bukkit.getBukkitVersion().split("-")[0].contains("1.8");
        boolean mc19 = Bukkit.getBukkitVersion().split("-")[0].contains("1.9");
        boolean mc110 = Bukkit.getBukkitVersion().split("-")[0].contains("1.10");
        boolean mc111 = Bukkit.getBukkitVersion().split("-")[0].contains("1.11");

        if (mc18 || mc19 || mc110 || mc111)
            sendPre12(player, message);
        else
            sendPo12(player, message);

    }

    private static void sendPo12(final Player player, final String message) {
        try {
            Class<?> chat = CoreReflection.getNMSClass("IChatBaseComponent");
            Object actionMessage = chat.getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, "{\"text\":\"" + message + "\"}");
            Class<?> chatMessageTypeClass = CoreReflection.getNMSClass("ChatMessageType");
            Constructor<?> constructor = CoreReflection.getNMSClass("PacketPlayOutChat").getConstructor(chat, chatMessageTypeClass);
            Object packet = constructor.newInstance(actionMessage, CoreReflection.getNMSClass("ChatMessageType").getField("GAME_INFO").get(null));
            CoreReflection.sendPacket(player, packet);
        } catch (Exception var11) {
            var11.printStackTrace();
        }
    }

    private static void sendPre12(final Player player, final String message) {
        try {
            Class<?> chat = CoreReflection.getNMSClass("IChatBaseComponent");
            Object actionMessage = chat.getDeclaredClasses()[0].getMethod("a", new Class[]{String.class}).invoke(null, "{\"text\":\"" + message + "\"}");
            Constructor<?> constructor = CoreReflection.getNMSClass("PacketPlayOutChat").getConstructor(chat, byte.class);
            Object packet = constructor.newInstance(actionMessage, (byte) 2);
            CoreReflection.sendPacket(player, packet);
        } catch (Exception var11) {
            var11.printStackTrace();
        }
    }
}