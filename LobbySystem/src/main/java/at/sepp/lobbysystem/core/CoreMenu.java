package at.sepp.lobbysystem.core;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CoreMenu {
    private String name;
    private String world;
    private String command;
    private String perm;

    private boolean glass_enable;

    private int glassColor = 0;
    private int rows;

    private Sound sound;

    public List<CoreMenuItem> items = new ArrayList<>();

    private ConfigurationSection sectionItems;

    private CorePlugin plugin;

    public CoreMenu(final String name, final ConfigurationSection sectionItems, final CorePlugin plugin) {
        this.name = name;
        this.sectionItems = sectionItems;
        this.plugin = plugin;
    }

    public void create(final int row, final Player player) {
        int slot = getSlot(row);
        if (!CoreUtils.mc1_8 && !CoreUtils.mc1_9 && !CoreUtils.mc1_10 && !CoreUtils.mc1_11 && !CoreUtils.mc1_12) {
            for (String color : CoreColor.getColorLis()) {
                if (this.name.contains(color))
                    this.name = this.name.replaceAll(color, "");
            }
        }
        this.name = CoreColor.colorCodes(this.name);
        Inventory inventory = Bukkit.createInventory(null, slot, this.name);
        if (getGlassEnable()) {
            int color = getClassColor();
            for (int i = 0; i < slot; i++) {
                ItemStack itemStack = createItem(" ", 160, color);
                inventory.setItem(i, itemStack);
            }
        }
        for (CoreMenuItem item : this.items) {
            if (item.getPermissionToView()) {
                if (item.hasPerm(player))
                    inventory.setItem(item.getSlot(), item.create(player, inventory, this.plugin));
                continue;
            }
            inventory.setItem(item.getSlot(), item.create(player, inventory, this.plugin));
        }
        player.openInventory(inventory);
    }

    public String getPerm() {
        return this.perm;
    }

    public String getWorld() {
        return this.world;
    }

    public Sound getSound() {
        return this.sound;
    }

    public void setPerm(final String perm) {
        this.perm = perm;
    }

    public void setWorld(final String world) {
        this.world = world;
    }

    public void setGlassColor(int color) {
        this.glassColor = color;
    }

    public int getClassColor() {
        return this.glassColor;
    }

    public void setGlassEnable(boolean enable) {
        this.glass_enable = enable;
    }

    public boolean getGlassEnable() {
        return this.glass_enable;
    }

    public void setSound(String sound) {
        if (sound == null || sound.isEmpty()) {
            this.sound = null;
        } else {
            try {
                this.sound = Sound.valueOf(sound);
            } catch (Exception ex) {
                this.sound = null;
            }
        }
    }

    public String getCommands() {
        return this.command;
    }

    public void setCommands(String command) {
        this.command = command;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getName() {
        return this.name;
    }

    public void open(Player p) {
        create(this.rows, p);
    }

    private static ItemStack createItem(String name, int mate, int shrt) {
        ItemStack i = new ItemStack(CoreMaterial.getMaterial(mate), 1, (short) shrt);
        ItemMeta itemMeta = i.getItemMeta();
        String n = CoreColor.colorCodes(name);
        itemMeta.setDisplayName(n);
        i.setItemMeta(itemMeta);
        return i;
    }

    private int getSlot(int rows) {
        if (rows <= 0) {
            return 9;
        }
        if (rows > 6) {
            return 54;
        }
        return rows * 9;
    }

    public void load() {
        Set<String> keys = this.sectionItems.getKeys(false);
        for (String key : keys) {
            String mate_data, mate;
            short data;
            ConfigurationSection configNode = this.sectionItems.getConfigurationSection(key);
            if (!configNode.isSet("name")) {
                this.plugin.getLog().error("Menu-Item: The item " + key + " has no name!");
                continue;
            }
            if (!configNode.isSet("material") && !configNode.isSet("material-old")) {
                this.plugin.getLog().debug("Menu-Item: The item " + key + " does not have a defined material value, using id instead.!");
                if (!configNode.isSet("id")) {
                    this.plugin.getLog().error("Menu-Item: The item " + key + " has no Material or ID!");
                    continue;
                }
                if (configNode.getInt("id") == 0 || CoreMaterial.getMaterial(configNode.getInt("id")) == null) {
                    this.plugin.getLog().error("Menu-Item: The item " + key + " has an invalid item ID: " + configNode.getInt("id") + ".");
                    continue;
                }
                CoreMenuItem coreMenuItem = new CoreMenuItem(CoreMaterial.getMaterial(configNode.getInt("id")));
                coreMenuItem.setPerm(configNode.getString("permission"));
                coreMenuItem.setName(configNode.getString("name"));
                coreMenuItem.setMaximumRows(this.rows);
                coreMenuItem.setSlot(configNode.getInt("slot"));
                coreMenuItem.setEnchantGlow(configNode.getBoolean("enchant-glow"));
                coreMenuItem.setData((short) configNode.getInt("data"));
                coreMenuItem.setPrice(configNode.getInt("price"));
                coreMenuItem.setOpened(configNode.getBoolean("keep-open"));
                coreMenuItem.setPermissionToView(configNode.getBoolean("permission-to-view"));
                coreMenuItem.setVersionCheck(configNode.getBoolean("version-check"));
                coreMenuItem.setVersionList(configNode.getStringList("version-list"));
                coreMenuItem.setNoVersionMessage(configNode.getString("no-version-message"));
                coreMenuItem.setNoPermisionMessage(configNode.getString("no-permision-message"));
                if (configNode.isSet("lore") && configNode.isList("lore"))
                    coreMenuItem.setLore(configNode.getStringList("lore"));
                if (configNode.contains("skull") && configNode.isSet("skull"))
                    coreMenuItem.setSkull(configNode.getString("skull"));
                if (configNode.isSet("commands") && configNode.isList("commands"))
                    coreMenuItem.setCommands(configNode.getStringList("commands"));
                this.items.add(coreMenuItem);
                continue;
            }
            if (CoreUtils.isPre1_13()) {
                if (configNode.isSet("material-old")) {
                    mate_data = configNode.getString("material-old");
                } else {
                    mate_data = configNode.getString("material");
                }
            } else if (configNode.isSet("material")) {
                mate_data = configNode.getString("material");
            } else {
                mate_data = configNode.getString("material-old");
            }
            if (Material.getMaterial(mate_data.contains(":") ? mate_data.split(":")[0].trim() : mate_data) == null) {
                this.plugin.getLog().error("Menu-Item: The item " + key + " has an invalid item Material: " + (mate_data.contains(":") ? mate_data.split(":")[0].trim() : mate_data) + ".");
                continue;
            }
            if (mate_data.contains(":")) {
                mate = mate_data.split(":")[0].trim();
                data = Short.parseShort(mate_data.split(":")[1].trim());
            } else {
                mate = mate_data;
                data = (short) configNode.getInt("data");
            }
            CoreMenuItem item = new CoreMenuItem(Material.getMaterial(mate));
            item.setPerm(configNode.getString("permission"));
            item.setName(configNode.getString("name"));
            item.setMaximumRows(this.rows);
            item.setSlot(configNode.getInt("slot"));
            item.setEnchantGlow(configNode.getBoolean("enchant-glow"));
            item.setData(data);
            item.setPrice(configNode.getInt("price"));
            item.setOpened(configNode.getBoolean("keep-open"));
            item.setPermissionToView(configNode.getBoolean("permission-to-view"));
            item.setVersionCheck(configNode.getBoolean("version-check"));
            item.setVersionList(configNode.getStringList("version-list"));
            item.setNoVersionMessage(configNode.getString("no-version-message"));
            item.setNoPermisionMessage(configNode.getString("no-permision-message"));
            if (configNode.isSet("lore") && configNode.isList("lore"))
                item.setLore(configNode.getStringList("lore"));
            if (configNode.contains("skull") && configNode.isSet("skull"))
                item.setSkull(configNode.getString("skull"));
            if (configNode.isSet("commands") && configNode.isList("commands"))
                item.setCommands(configNode.getStringList("commands"));
            this.items.add(item);
        }
    }
}
