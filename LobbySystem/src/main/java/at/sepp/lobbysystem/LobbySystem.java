package at.sepp.lobbysystem;

import java.io.File;
import java.io.IOException;
import java.util.*;

import at.sepp.lobbysystem.core.*;
import at.sepp.lobbysystem.listener.BlockListener;
import at.sepp.lobbysystem.listener.EntityListener;
import at.sepp.lobbysystem.listener.OthersListener;
import at.sepp.lobbysystem.listener.PlayerListener;
import at.sepp.lobbysystem.runnable.AlwaysDayRunnable;
import at.sepp.lobbysystem.runnable.BoardAnimationRunnable;
import at.sepp.lobbysystem.runnable.TabListUpdateRunnable;
import at.sepp.lobbysystem.subcommand.HelpCommand;
import at.sepp.lobbysystem.subcommand.ReloadCommand;
import at.sepp.lobbysystem.subcommand.RemoveSpawnCommand;
import at.sepp.lobbysystem.subcommand.SetSpawnCommand;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class LobbySystem extends JavaPlugin implements CorePlugin {
    public CoreConfig config;
    public CoreConfig configSpawn;
    public CoreConfig configItem;
    public CoreConfig configChat;
    public CoreConfig configInfoCommands;
    public CoreConfig configCustomEvents;
    public CoreConfig configMenus;
    public CoreConfig configBoard;
    public CoreConfig configAnnouncer;
    public CoreConfig configMessages;

    public CoreLog log;

    public Permission permission = null;
    public Economy economy = null;
    public Chat chat = null;

    public List<InfoCommand> infoCommands = new ArrayList<>();
    public List<CustomEvents> customEvents = new ArrayList<>();
    public List<CoreItem> items = new ArrayList<>();

    public Map<String, CoreMenu> menus = new HashMap<>();
    public Map<Player, PlayerBoard> playerBoards = new HashMap<>();

    public Announcer announcer;
    public LobbySystem instance;

    public int spawns = 0;

    public void onEnable() {
        this.instance = this;
        this.log = new CoreLog(this, CoreLog.Color.YELLOW);

        this.log.info("Loading configuration...");
        YamlConfiguration yamlConfiguration = new YamlConfiguration();
        File oldFile = new File(getDataFolder(), "config.yml");
        File backFile = new File(getDataFolder(), "old-config.yml");
        if (oldFile.exists()) {
            try {
                yamlConfiguration.load(oldFile);
            } catch (IOException | org.bukkit.configuration.InvalidConfigurationException e) {
                this.log.error("&cError on loaded old config.yml.", e);
            }
            if (!yamlConfiguration.getBoolean("newconfig")) {
                try {
                    yamlConfiguration.save(backFile);
                } catch (IOException e) {
                    this.log.error("Error on save old-config.yml.", e);
                }
                oldFile.delete();
            }
        }
        this.config = new CoreConfig(this, "config", this.log, getResource("config.yml"), true);
        this.log.setDebug(this.config.getBoolean("debug"));
        loadMessages();
        AllString.load(this.config, this.configMessages);
        this.configSpawn = new CoreConfig(this, "spawn", this.log, getResource("spawn.yml"), false);
        this.configChat = new CoreConfig(this, "chat", this.log, getResource("chat.yml"), false);
        this.configInfoCommands = new CoreConfig(this, "infocommands", this.log, getResource("infocommands.yml"), false);
        loadInfoCommands();
        this.configCustomEvents = new CoreConfig(this, "customevents", this.log, getResource("customevents.yml"), false);
        loadCustomEvents();
        this.configItem = new CoreConfig(this, "items", this.log, getResource("items.yml"), false);
        loadItems();
        this.configMenus = new CoreConfig(this, "menus", this.log, getResource("menus.yml"), false);
        loadMenus();
        this.configBoard = new CoreConfig(this, "board", this.log, getResource("board.yml"), false);
        this.configAnnouncer = new CoreConfig(this, "announcer", this.log, getResource("announcer.yml"), false);
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.log.info("Register commands...");
        LobbyCommand mainCommand = new LobbyCommand(this);
        mainCommand.addSubCommand(Collections.singletonList("setspawn"), new SetSpawnCommand(this));
        mainCommand.addSubCommand(Collections.singletonList("remspawn"), new RemoveSpawnCommand(this));
        mainCommand.addSubCommand(Collections.singletonList("reload"), new ReloadCommand(this));
        mainCommand.addSubCommand(Arrays.asList("help", "?"), new HelpCommand(this));
        getCommand("superlobby").setExecutor(mainCommand);
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(this), this);
        pm.registerEvents(new BlockListener(this), this);
        pm.registerEvents(new EntityListener(this), this);
        pm.registerEvents(new OthersListener(this), this);
        (new AlwaysDayRunnable(this)).runTaskTimer(this, 0L, 1000L);
        //CoreVariables.iniUcode(); //TODO: Add later
        this.permission = setupVaultPermissions();
        if (this.permission != null) {
            this.log.alert("Hooked Vault Permissions");
            CoreVariables.permission(this.permission);
        }
        this.chat = setupChat();
        if (this.chat != null) {
            this.log.alert("Hooked Vault Chat");
            CoreVariables.chat(this.chat);
        }
        this.economy = setupEconomy();
        if (this.economy != null) {
            this.log.alert("Hooked Vault economy");
            CoreExecuteComands.economy(this.economy);
        }
        //this.playerPoints = setupPlayerPoints();
        //if (this.playerPoints != null) {
        //  this.log.alert("Hooked PlayerPoints");
        //CoreExecuteComands.playerPoints(this.playerPoints);
        //}
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            this.log.alert("Hooked PlaceholderAPI");
            CoreVariables.placeholderAPI(true);
        }
        //BukkitViaAPI api = setupViaVersionApi();
        //if (api != null) {
        //  this.log.alert("Hooked ViaVersion");
        //CoreViaVersion.viaApi(api);
        //}
        if (this.config.getInt("join-tab.update") > 0) {
            long pe = this.config.getInt("JoinTab.Update");
            (new TabListUpdateRunnable(this)).runTaskTimer(this, 0L, pe);
        }
        if (this.configBoard.getBoolean("settings-enable"))
            (new BoardAnimationRunnable(this)).runTaskTimer(this, 0L, 1L);
        this.announcer = new Announcer(this);
        //Metrics metrics = new Metrics(this);
        checkForUpdates();
        this.log.info("&7A total of &b" + this.menus.size() + " &7menus were loaded.");
        this.log.line();
    }

    public void checkForUpdates() {
        if (this.config.getBoolean("update-check"))
            (new BukkitRunnable() {
                public void run() {
                    CoreSpigotUpdater updater = new CoreSpigotUpdater(LobbySystem.this.instance, 20400);
                    try {
                        if (updater.checkForUpdates())
                            LobbySystem.this.log.alert("An update was found! for SuperLobby. Please update to recieve latest version. download: " + updater.getResourceURL());
                    } catch (Exception e) {
                        LobbySystem.this.log.error("Failed to check for a update on spigot.");
                    }
                }
            }).runTask(this);
    }

    public void teleportToSpawn(final Player p) {
        Set<String> k = this.configSpawn.getKeys(Boolean.FALSE);
        if (k == null || k.isEmpty()) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
                public void run() {
                    CoreColor.message(p, AllString.prefix + AllString.spawnNotSetError);
                }
            }, 10L);
        } else {
            ArrayList<String> arrayList = new ArrayList<>(k);
            if (this.spawns < arrayList.size()) {
                String s = arrayList.get(this.spawns);
                World w = getServer().getWorld(this.configSpawn.getString(s + ".world"));
                double x = this.configSpawn.getDouble(s + ".x");
                double y = this.configSpawn.getDouble(s + ".y");
                double z = this.configSpawn.getDouble(s + ".z");
                int yaw = this.configSpawn.getInt(s + ".yaw");
                int pi = this.configSpawn.getInt(s + ".pi");
                p.teleport(new Location(w, x, y, z, yaw, pi));
                this.spawns++;
            } else {
                this.spawns = 0;
                String s = arrayList.get(this.spawns);
                World w = getServer().getWorld(this.configSpawn.getString(s + ".world"));
                double x = this.configSpawn.getDouble(s + ".x");
                double y = this.configSpawn.getDouble(s + ".y");
                double z = this.configSpawn.getDouble(s + ".z");
                int yaw = this.configSpawn.getInt(s + ".yaw");
                int pi = this.configSpawn.getInt(s + ".pi");
                p.teleport(new Location(w, x, y, z, yaw, pi));
            }
        }
    }

    public void loadMenus() {
        this.menus = new HashMap<>();
        Set<String> keymenu = this.configMenus.getKeys(Boolean.FALSE);
        for (String nodomenu : keymenu) {
            ConfigurationSection sectionmenu = this.configMenus.getConfigurationSection(nodomenu);
            CoreMenu menu = new CoreMenu(sectionmenu.getString("settings-name"), sectionmenu.getConfigurationSection("items"), this);
            menu.setPerm(sectionmenu.getString("settings-permission"));
            menu.setRows(sectionmenu.getInt("settings-rows"));
            menu.setCommands(sectionmenu.getString("settings-open-commands"));
            menu.setSound(sectionmenu.getString("settings-open-sound"));
            menu.setWorld(sectionmenu.getString("settings-world"));
            menu.setGlassEnable(sectionmenu.getBoolean("settings-glass-enable"));
            menu.setGlassColor(sectionmenu.getInt("settings-glass-color"));
            menu.load();
            this.menus.put(nodomenu, menu);
        }
    }

    public void loadItems() {
        this.items = new ArrayList<>();
        Set<String> keys = this.configItem.getKeys(Boolean.FALSE);
        for (String key : keys) {
            String metaData, mate;
            short data;
            ConfigurationSection configNode = this.configItem.getConfigurationSection(key);
            if (!configNode.isSet("name")) {
                this.log.error("Lobby-Item: The item " + key + " has no name!");
                continue;
            }
            if (!configNode.isSet("material") && !configNode.isSet("material-old")) {
                this.log.debug("Lobby-Item: The item " + key + " does not have a defined material value, using id instead.!");
                if (!configNode.isSet("id")) {
                    this.log.error("Lobby-Item: The item " + key + " has no Material or ID!");
                    continue;
                }
                if (configNode.getInt("id") == 0 || CoreMaterial.getMaterial(configNode.getInt("id")) == null) {
                    this.log.error("Lobby-Item: The item " + key + " has an invalid item ID: " + configNode.getInt("id") + ".");
                    continue;
                }
                CoreItem coreItem = new CoreItem(CoreMaterial.getMaterial(configNode.getInt("id")));
                coreItem.setDelay(configNode.getInt("delay"));
                coreItem.setPerm(configNode.getString("permission"));
                coreItem.setName(configNode.getString("name"));
                coreItem.setSlot(configNode.getInt("slot"));
                coreItem.setEnchantGlow(configNode.getBoolean("enchant-glow"));
                coreItem.setData((short) configNode.getInt("data"));
                coreItem.setVersionCheck(configNode.getBoolean("version-check"));
                coreItem.setVersionList(configNode.getStringList("version-list"));
                coreItem.setNoVersionMessage(configNode.getString("no-version-message"));
                if (configNode.isSet("lore") && configNode.isList("lore"))
                    coreItem.setLores(configNode.getStringList("lore"));
                if (configNode.contains("skull") && configNode.isSet("skull"))
                    coreItem.setSkull(configNode.getString("skull"));
                if (configNode.isSet("commands") && configNode.isList("commands"))
                    coreItem.setCommands(configNode.getStringList("commands"));
                this.items.add(coreItem);
                continue;
            }
            if (CoreUtils.isPre1_13()) {
                if (configNode.isSet("material-old")) {
                    metaData = configNode.getString("material-old");
                } else {
                    metaData = configNode.getString("material");
                }
            } else if (configNode.isSet("material")) {
                metaData = configNode.getString("material");
            } else {
                metaData = configNode.getString("material-old");
            }
            if (Material.getMaterial(metaData.contains(":") ? metaData.split(":")[0].trim() : metaData) == null) {
                this.log.error("Lobby-Item: The item " + key + " has an invalid item Material: " + (metaData.contains(":") ? metaData.split(":")[0].trim() : metaData) + ".");
                continue;
            }
            if (metaData.contains(":")) {
                mate = metaData.split(":")[0].trim();
                data = Short.parseShort(metaData.split(":")[1].trim());
            } else {
                mate = metaData;
                data = (short) configNode.getInt("data");
            }
            CoreItem item = new CoreItem(Material.getMaterial(mate));
            item.setDelay(configNode.getInt("delay"));
            item.setPerm(configNode.getString("permission"));
            item.setName(configNode.getString("name"));
            item.setSlot(configNode.getInt("slot"));
            item.setEnchantGlow(configNode.getBoolean("enchant-glow"));
            item.setData(data);
            if (configNode.isSet("lore") && configNode.isList("lore"))
                item.setLores(configNode.getStringList("lore"));
            if (configNode.contains("skull") && configNode.isSet("skull"))
                item.setSkull(configNode.getString("skull"));
            if (configNode.isSet("commands") && configNode.isList("commands"))
                item.setCommands(configNode.getStringList("commands"));
            this.items.add(item);
        }
    }

    public void loadInfoCommands() {
        this.infoCommands = new ArrayList<>();
        Set<String> key = this.configInfoCommands.getKeys(Boolean.FALSE);
        for (String nodo : key) {
            ConfigurationSection a = this.configInfoCommands.getConfigurationSection(nodo);
            if (!a.isSet("command"))
                this.log.alert("InfoCommands: The nodo " + nodo + " has no command!");
            InfoCommand infoc = new InfoCommand(a.getString("command"));
            infoc.setPermission(a.getString("permission"));
            if (a.isSet("info") && a.isList("info")) {
                List<String> lista = a.getStringList("info");
                infoc.setInfo(lista);
            }
            this.infoCommands.add(infoc);
        }
    }

    public void loadCustomEvents() {
        this.customEvents = new ArrayList<>();
        Set<String> key = this.configCustomEvents.getKeys(Boolean.FALSE);
        for (String nodo : key) {
            ConfigurationSection a = this.configCustomEvents.getConfigurationSection(nodo);
            if (!a.isSet("permission"))
                this.log.alert("CustomEvents: The customjoin " + nodo + " has no permission!");
            CustomEvents cj = new CustomEvents(a.getString("permission"), this);
            if (a.isSet("join-message"))
                cj.setJoinMessage(a.getString("join-message"));
            if (a.isSet("quit-message"))
                cj.setQuitMessage(a.getString("quit-message"));
            if (a.isSet("join-firework"))
                cj.setJoinFirework(a.getString("join-firework"));
            if (a.isSet("join-sound-old")) {
                if (!Utils.isEnum(Sound.class, a.getString("join-sound-old").toUpperCase()) && !a.getString("join-sound-old").equalsIgnoreCase("random")) {
                    if (a.isSet("join-sound"))
                        cj.setJoinSound(a.getString("join-sound"));
                } else {
                    cj.setJoinSound(a.getString("join-sound-old"));
                }
            } else if (a.isSet("join-sound")) {
                cj.setJoinSound(a.getString("join-sound"));
            }
            if (a.isSet("join-gamemode"))
                cj.setJoinGamemode(a.getString("join-gamemode"));
            if (a.isSet("join-comands"))
                cj.setJoinComands(a.getStringList("join-comands"));
            this.customEvents.add(cj);
        }
    }

    public Permission setupVaultPermissions() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null)
            return null;
        Permission retVal;
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        try {
            retVal = rsp.getProvider();
        } catch (NullPointerException e) {
            this.log.debug("Failed to get the vault permissions provider.", e);
            retVal = null;
        }
        return retVal;
    }

    public Chat setupChat() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null)
            return null;
        Chat retorno = null;
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        try {
            retorno = rsp.getProvider();
        } catch (NullPointerException e) {
            this.log.debug("Failed to get the vault chat provider.", e);
        }
        return retorno;
    }

    /*public BukkitViaAPI setupViaVersionApi() {
        if (Bukkit.getPluginManager().getPlugin("ViaVersion") == null)
            return null;
        ViaVersionPlugin viaVersionPlugin = (ViaVersionPlugin)Bukkit.getPluginManager().getPlugin("ViaVersion");
        if (viaVersionPlugin == null)
            return null;
        BukkitViaAPI api = (BukkitViaAPI)viaVersionPlugin.getApi();
        return api;
    }*/

    public Economy setupEconomy() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null)
            return null;
        Economy retorno = null;
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServicesManager().getRegistration(Economy.class);
        try {
            retorno = rsp.getProvider();
        } catch (NullPointerException e) {
            this.log.debug("Failed to get the vault economy provider.", e);
        }
        return retorno;
    }

    /*public PlayerPoints setupPlayerPoints() {
        if (Bukkit.getPluginManager().getPlugin("PlayerPoints") == null)
            return null;
        PlayerPoints pointsPlugin = (PlayerPoints)Bukkit.getPluginManager().getPlugin("PlayerPoints");
        return pointsPlugin;
    }*/

    public void loadMessages() {
        String m = this.config.getString("messages");
        String str1;
        switch ((str1 = m.toUpperCase()).hashCode()) {
            case 2217:
                if (!str1.equals("EN"))
                    break;
                this.configMessages = new CoreConfig(this, "messages_EN", this.log, getResource("messages_EN.yml"), true);
                return;
            case 2222:
                if (!str1.equals("ES"))
                    break;
                this.configMessages = new CoreConfig(this, "messages_ES", this.log, getResource("messages_ES.yml"), true);
                return;
            case 2494:
                if (!str1.equals("NL"))
                    break;
                this.configMessages = new CoreConfig(this, "messages_NL", this.log, getResource("messages_NL.yml"), true);
                return;
            case 2627:
                if (!str1.equals("RU"))
                    break;
                this.configMessages = new CoreConfig(this, "messages_RU", this.log, getResource("messages_RU.yml"), true);
                return;
            case 2862:
                if (!str1.equals("ZH"))
                    break;
                this.configMessages = new CoreConfig(this, "messages_ZH", this.log, getResource("messages_ZH.yml"), true);
                return;
        }
        this.configMessages = new CoreConfig(this, "messages_EN", this.log, getResource("messages_EN.yml"), true);
    }

    public void reloadTab() {
        if (this.config.getBoolean("join-tab.enable"))
            for (Player p : Bukkit.getOnlinePlayers()) {
                String h = this.config.getString("join-tab.header");
                h = CoreVariables.replace(h, p);
                String f = this.config.getString("JoinTab.Footer");
                f = CoreVariables.replace(f, p);
                CorePlayerListHeaderFooter.sendHeaderFooter(p, h, f);
            }
    }

    public void reloadItems() {
        if (this.config.getBoolean("lobby-items.enable"))
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (this.config.getStringList("lobby-items.world").contains(p.getPlayer().getWorld().getName()))
                    for (CoreItem item : this.items) {
                        if (item.hasPerm(p)) {
                            item.give(p, this);
                            continue;
                        }
                        this.log.debug("The player " + p.getName() + " do not have permission to receive the item " + item.getName());
                    }
            }
    }

    public CoreLog getLog() {
        return this.log;
    }

    public JavaPlugin getInstance() {
        return this;
    }
}
