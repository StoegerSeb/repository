package at.sepp.lobbysystem.subcommand;

import at.sepp.lobbysystem.AllString;
import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand extends CoreSubCommand {
    private LobbySystem plugin;

    public SetSpawnCommand(LobbySystem plugin) {
        this.plugin = plugin;
        this.plugin.log.info("Register sub-command setspawn");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            CoreColor.message(sender, AllString.prefix + AllString.noConsoleError);
            return true;
        }
        if (args.length <= 0) {
            CoreColor.message(sender, AllString.prefix + AllString.commandSetSpawnUsage);
            return true;
        }
        String name = args[0];
        Player p = (Player)sender;
        this.plugin.configSpawn.set(name + ".world", p.getLocation().getWorld().getName());
        this.plugin.configSpawn.set(name + ".x", p.getLocation().getX());
        this.plugin.configSpawn.set(name + ".y", p.getLocation().getY());
        this.plugin.configSpawn.set(name + ".z", p.getLocation().getZ());
        this.plugin.configSpawn.set(name + ".yaw", p.getLocation().getYaw());
        this.plugin.configSpawn.set(name + ".pi", p.getLocation().getPitch());
        this.plugin.configSpawn.save();
        CoreColor.message(sender, AllString.prefix + AllString.setSpawn.replaceAll("<spawn>", name.toLowerCase()));
        return true;
    }

    @Override
    public String getErrorNoPermission() {
        return AllString.prefix + AllString.noPermissionError;
    }

    @Override
    public String getPermission() {
        return "lobby.admin";
    }
}
