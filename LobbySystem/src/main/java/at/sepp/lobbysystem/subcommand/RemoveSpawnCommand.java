package at.sepp.lobbysystem.subcommand;

import at.sepp.lobbysystem.AllString;
import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Set;

public class RemoveSpawnCommand extends CoreSubCommand {
    private LobbySystem plugin;

    public RemoveSpawnCommand(LobbySystem plugin) {
        this.plugin = plugin;
        this.plugin.log.info("Register sub-command remspawn");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            CoreColor.message(sender, AllString.prefix + AllString.noConsoleError);
            return true;
        }
        if (args.length <= 0) {
            CoreColor.message(sender, AllString.prefix + AllString.commandRemoveSpawnUsage);
            return true;
        }
        if (args[0].equalsIgnoreCase("all")) {
            Set<String> k = this.plugin.configSpawn.getKeys(Boolean.FALSE);
            if (k == null || k.isEmpty()) {
                CoreColor.message(sender, AllString.prefix + AllString.noSpawnError);
                return true;
            }
            for (String s : k) {
                if (this.plugin.configSpawn.contains(s.toLowerCase())) {
                    this.plugin.configSpawn.setNull(s.toLowerCase());
                    this.plugin.configSpawn.save();
                    CoreColor.message(sender, AllString.prefix + AllString.removeSpawn.replaceAll("<spawn>", s.toLowerCase()));
                }
            }
            return true;
        }
        if (!this.plugin.configSpawn.contains(args[0])) {
            CoreColor.message(sender, AllString.prefix + AllString.noSpawnError);
            return true;
        }
        this.plugin.configSpawn.setNull(args[0].toLowerCase());
        this.plugin.configSpawn.save();
        CoreColor.message(sender, AllString.prefix + AllString.removeSpawn.replaceAll("<spawn>", args[0].toLowerCase()));
        return true;
    }

    @Override
    public String getErrorNoPermission() {
        return AllString.prefix + AllString.noPermissionError;
    }

    @Override
    public String getPermission() {
        return "lobby.admin";
    }
}
