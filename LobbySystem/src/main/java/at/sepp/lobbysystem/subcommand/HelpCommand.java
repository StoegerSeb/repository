package at.sepp.lobbysystem.subcommand;

import at.sepp.lobbysystem.AllString;
import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreSubCommand;
import org.bukkit.command.CommandSender;

public class HelpCommand extends CoreSubCommand {

    public HelpCommand(LobbySystem plugin) {
        plugin.log.info("Register sub-command help");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        CoreColor.message(sender, AllString.prefix + "&e&m---------------------------------------------------");
        CoreColor.message(sender, (new StringBuilder(String.valueOf(AllString.prefix))).toString());
        CoreColor.message(sender, AllString.prefix + AllString.commandSetSpawnHelp);
        CoreColor.message(sender, AllString.prefix + AllString.descriptionSetSpawnHelp);
        CoreColor.message(sender, AllString.prefix + AllString.commandRemoveSpawnHelp);
        CoreColor.message(sender, AllString.prefix + AllString.descriptionRemoveSpawnHelp);
        CoreColor.message(sender, AllString.prefix + AllString.reloadCommandHelp);
        CoreColor.message(sender, AllString.prefix + AllString.reloadDescriptionHelp);
        CoreColor.message(sender, (new StringBuilder(String.valueOf(AllString.prefix))).toString());
        CoreColor.message(sender, AllString.prefix + "&e&m---------------------------------------------------");
        return true;
    }

    @Override
    public String getErrorNoPermission() {
        return AllString.prefix + AllString.noPermissionError;
    }

    @Override
    public String getPermission() {
        return null;
    }
}
