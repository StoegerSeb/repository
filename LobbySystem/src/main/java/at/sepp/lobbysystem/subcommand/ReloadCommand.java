package at.sepp.lobbysystem.subcommand;

import at.sepp.lobbysystem.AllString;
import at.sepp.lobbysystem.BoardLine;
import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.PlayerBoard;
import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreConfig;
import at.sepp.lobbysystem.core.CoreSubCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

public class ReloadCommand extends CoreSubCommand {
    private LobbySystem plugin;

    public ReloadCommand(LobbySystem plugin) {
        this.plugin = plugin;
        this.plugin.log.info("Register sub-command reload");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        this.plugin.config = new CoreConfig(this.plugin, "config", this.plugin.log, this.plugin.getResource("config.yml"), true);
        this.plugin.reloadTab();
        this.plugin.loadMessages();
        AllString.load(this.plugin.config, this.plugin.configMessages);
        this.plugin.configItem = new CoreConfig(this.plugin, "items", this.plugin.log, this.plugin.getResource("items.yml"), false);
        this.plugin.loadItems();
        this.plugin.reloadItems();
        this.plugin.configMenus = new CoreConfig(this.plugin, "menus", this.plugin.log, this.plugin.getResource("menus.yml"), false);
        this.plugin.loadMenus();
        this.plugin.configSpawn = new CoreConfig(this.plugin, "spawn", this.plugin.log, this.plugin.getResource("spawn.yml"), false);
        this.plugin.configInfoCommands = new CoreConfig(this.plugin, "infocommands", this.plugin.log, this.plugin.getResource("infocommands.yml"), false);
        this.plugin.loadInfoCommands();
        this.plugin.configCustomEvents = new CoreConfig(this.plugin, "customevents", this.plugin.log, this.plugin.getResource("customevents.yml"), false);
        this.plugin.loadCustomEvents();
        this.plugin.configAnnouncer = new CoreConfig(this.plugin, "announcer", this.plugin.log, this.plugin.getResource("announcer.yml"), false);
        this.plugin.announcer.reload();
        this.plugin.configBoard = new CoreConfig(this.plugin, "board", this.plugin.log, this.plugin.getResource("board.yml"), false);
        this.plugin.playerBoards = new HashMap<>();
        if (this.plugin.configBoard.getBoolean("settings-enable"))
            for (Player p : Bukkit.getOnlinePlayers()) {
                PlayerBoard board = new PlayerBoard(p, this.plugin.configBoard.getStringList("settings-title"), this.plugin.configBoard.getInt("settings-update-title"));
                int index = this.plugin.configBoard.getConfigurationSection("board").getKeys(false).size() - 1;
                for (String nodo : this.plugin.configBoard.getConfigurationSection("board").getKeys(false)) {
                    Team team = board.get().registerNewTeam("text-" + (index + 1));
                    team.addEntry(String.valueOf(ChatColor.values()[index]));
                    board.getObjective().getScore(String.valueOf(ChatColor.values()[index])).setScore(index);
                    BoardLine line = new BoardLine(p, team, this.plugin.configBoard.getStringList("board." + nodo + ".content"), this.plugin.configBoard.getInt("board." + nodo + ".update"));
                    board.add(line);
                    index--;
                }
                board.send();
                this.plugin.playerBoards.put(p, board);
            }
        CoreColor.message(sender, AllString.prefix + AllString.configLoaded);
        return true;
    }

    @Override
    public String getErrorNoPermission() {
        return AllString.prefix + AllString.noPermissionError;
    }

    @Override
    public String getPermission() {
        return "lobby.admin";
    }
}