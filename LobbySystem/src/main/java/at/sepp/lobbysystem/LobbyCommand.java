package at.sepp.lobbysystem;

import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreCommand;
import org.bukkit.command.CommandSender;

public class LobbyCommand extends CoreCommand {
    public LobbyCommand(LobbySystem plugin) {
        plugin.log.info("Register main command superlobby");
    }

    public boolean onCommand(CommandSender sender, String command, String[] args) {
        CoreColor.message(sender, AllString.prefix + AllString.commandUsage);
        return true;
    }

    public String getErrorNoPermission() {
        return AllString.prefix + AllString.noPermissionError;
    }

    public String getPerm() {
        return "lobby.use";
    }
}
