package at.sepp.lobbysystem;

import at.sepp.lobbysystem.core.CoreExecuteComands;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Random;

public class CustomEvents {
    private String permission;
    private String joinMessage;
    private String quitMessage;
    private String joinGamemode;
    private String joinFirework;
    private String join_sound;

    private List<String> joinCommands;

    private JavaPlugin plugin;

    private int num;

    public CustomEvents(String permission, JavaPlugin plugin) {
        this.permission = permission;
        this.plugin = plugin;
    }

    public String getPermission() {
        return this.permission;
    }

    public boolean hasJoinMessage() {
        return (this.joinMessage != null);
    }

    public boolean hasQuitMessage() {
        return (this.quitMessage != null);
    }

    public boolean hasJoinFirework() {
        return (this.joinFirework != null);
    }

    public boolean hasJoinSound() {
        return (this.join_sound != null);
    }

    public boolean hasJoinGamemode() {
        return (this.joinGamemode != null);
    }

    public boolean hasJoinComands() {
        return (this.joinCommands != null);
    }

    public void setJoinMessage(String join_message) {
        this.joinMessage = join_message;
    }

    public void setQuitMessage(String quit_message) {
        this.quitMessage = quit_message;
    }

    public String getJoinMessage() {
        return this.joinMessage;
    }

    public String getQuitMessage() {
        return this.quitMessage;
    }

    public void sendJoinFirework(final Player p) {
        for (int i = 0; i < this.num; i++) {
            int m = i * 10;
            this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, () -> CustomEvents.this.fireword(p), m);
        }
    }

    public void sendJoinSound(Player p) {
        if (this.join_sound.equalsIgnoreCase("random")) {
            Random r = new Random();
            Sound[] so = Sound.values();
            int v = so.length;
            int rt = r.nextInt(v) + 1;
            p.getWorld().playSound(p.getLocation(), so[rt], 1.0F, 1.0F);
        } else {
            p.getWorld().playSound(p.getLocation(), Sound.valueOf(this.join_sound.toUpperCase()), 1.0F, 1.0F);
        }
    }

    public void sendJoinGamemode(Player p) {
        p.setGameMode(GameMode.valueOf(this.joinGamemode.toUpperCase()));
    }

    public void sendJoinComands(Player p) {
        for (String commands : this.joinCommands) {
            CoreExecuteComands c = new CoreExecuteComands(p, CoreVariables.replace(commands, p), this.plugin, AllString.prefix);
            c.execute();
        }
    }

    public void fireword(Player p) {
        Firework fw = (Firework) p.getWorld().spawnEntity(p.getLocation(), EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();
        Random r = new Random();
        FireworkEffect.Type type = FireworkEffect.Type.BALL;
        if (this.joinFirework.equalsIgnoreCase("random")) {
            int rt = r.nextInt(5) + 1;
            if (rt == 1)
                type = FireworkEffect.Type.BALL;
            if (rt == 2)
                type = FireworkEffect.Type.BALL_LARGE;
            if (rt == 3)
                type = FireworkEffect.Type.BURST;
            if (rt == 4)
                type = FireworkEffect.Type.CREEPER;
            if (rt == 5)
                type = FireworkEffect.Type.STAR;
        } else {
            type = FireworkEffect.Type.valueOf(this.joinFirework.toUpperCase());
        }
        int a = r.nextInt(256);
        int b = r.nextInt(256);
        int g = r.nextInt(256);
        Color c1 = Color.fromRGB(a, g, b);
        a = r.nextInt(256);
        b = r.nextInt(256);
        g = r.nextInt(256);
        Color c2 = Color.fromRGB(a, g, b);
        FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
        fwm.addEffect(effect);
        int rp = r.nextInt(2) + 1;
        fwm.setPower(rp);
        fw.setFireworkMeta(fwm);
    }

    public void setJoinFirework(String firework) {
        if (firework.trim().contains(",")) {
            String[] data = firework.trim().split(",");
            this.joinFirework = data[0].trim();
            this.num = Integer.parseInt(data[1].trim());
        } else {
            this.joinFirework = firework;
            this.num = 2;
        }
    }

    public void setJoinSound(String sound) {
        this.join_sound = sound;
    }

    public void setJoinGamemode(String gamemode) {
        this.joinGamemode = gamemode;
    }

    public void setJoinComands(List<String> comands) {
        this.joinCommands = comands;
    }
}
