package at.sepp.lobbysystem.listener;

import at.sepp.lobbysystem.LobbySystem;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {
    public LobbySystem plugin;

    public BlockListener(LobbySystem plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (this.plugin.config.getBoolean("disable-block-break.enable") &&
                this.plugin.config.getStringList("disable-block-break.world").contains(e.getBlock().getWorld().getName()) && !e.isCancelled())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (this.plugin.config.getBoolean("disable-block-place.enable") &&
                this.plugin.config.getStringList("disable-block-place.world").contains(e.getBlock().getWorld().getName()) && !e.isCancelled())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent e) {
        if (this.plugin.config.getBoolean("disable-block-burn.enable") &&
                this.plugin.config.getStringList("disable-block-burn.world").contains(e.getBlock().getWorld().getName()) && !e.isCancelled())
            e.setCancelled(true);
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent e) {
        if (this.plugin.config.getBoolean("disable-block-burn.enable") &&
                this.plugin.config.getStringList("disable-block-burn.world").contains(e.getBlock().getWorld().getName()) && !e.isCancelled())
            e.setCancelled(true);
    }
}
