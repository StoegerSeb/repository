package at.sepp.lobbysystem.listener;

import at.sepp.lobbysystem.*;
import at.sepp.lobbysystem.core.*;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Team;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class PlayerListener implements Listener {
    public LobbySystem plugin;

    public PlayerListener(LobbySystem plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onDropItem(PlayerDropItemEvent e) {
        if (this.plugin.config.getBoolean("disable-drop-item.enable") &&
                this.plugin.config.getStringList("disable-drop-item.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onPickUpItems(PlayerPickupItemEvent e) {
        if (this.plugin.config.getBoolean("disable-pick-up-items.enable") &&
                this.plugin.config.getStringList("disable-pick-up-items.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoinClear(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("join-clear-inventory"))
            e.getPlayer().getInventory().clear();
        if (this.plugin.config.getBoolean("join-clear-chat"))
            for (int i = 0; i < 120; i++) {
                Player p = e.getPlayer();
                p.sendMessage("");
            }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoinSpawn(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("join-spawn"))
            this.plugin.teleportToSpawn(e.getPlayer());
    }

    @EventHandler
    public void onCommandSpawn(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        if (e.getMessage().startsWith("/spawn") &&
                this.plugin.config.getBoolean("command-spawn")) {
            e.setCancelled(true);
            if (p.hasPermission("superlobby.use")) {
                this.plugin.teleportToSpawn(p);
            } else {
                CoreColor.message(p, AllString.prefix + AllString.noPermissionError);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoinLobbyItem(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("lobby-items.enable") && this.plugin.config.getBoolean("lobby-items.join-server") &&
                this.plugin.config.getStringList("lobby-items.world").contains(e.getPlayer().getWorld().getName()))
            for (CoreItem item : this.plugin.items) {
                if (item.hasPerm(e.getPlayer())) {
                    item.give(e.getPlayer(), this.plugin);
                    continue;
                }
                this.plugin.log.debug("The player " + e.getPlayer().getName() + " do not have permission to receive the item " + item.getName());
            }
    }

    public void onRespawnLobbyItem(final PlayerRespawnEvent e) {
        if (this.plugin.config.getBoolean("lobby-items.respawn"))
            Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> {
                if (!e.getPlayer().isOnline())
                    return;
                if (PlayerListener.this.plugin.config.getStringList("lobby-items.world").contains(e.getPlayer().getWorld().getName()))
                    for (CoreItem item : PlayerListener.this.plugin.items) {
                        if (item.hasPerm(e.getPlayer())) {
                            item.give(e.getPlayer(), PlayerListener.this.plugin);
                            continue;
                        }
                        PlayerListener.this.plugin.log.debug("The player " + e.getPlayer().getName() + " do not have permission to receive the item " + item.getName());
                    }
            }, 2L);
    }

    @EventHandler
    public void changeWorldLobbyItem(final PlayerChangedWorldEvent e) {
        if (this.plugin.config.getBoolean("lobby-items.join-world"))
            Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, () -> {
                if (!e.getPlayer().isOnline())
                    return;
                if (PlayerListener.this.plugin.config.getStringList("lobby-items.world").contains(e.getPlayer().getWorld().getName()))
                    for (CoreItem item : PlayerListener.this.plugin.items) {
                        if (item.hasPerm(e.getPlayer())) {
                            item.give(e.getPlayer(), PlayerListener.this.plugin);
                            continue;
                        }
                        PlayerListener.this.plugin.log.debug("The player " + e.getPlayer().getName() + " do not have permission to receive the item " + item.getName());
                    }
            }, 2L);
    }

    @EventHandler
    public void onLobbyItemClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("lobby-items.enable") &&
                this.plugin.config.getStringList("lobby-items.world").contains(e.getPlayer().getWorld().getName()) && (
                e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
            ItemStack itemInHand = e.getItem();
            for (CoreItem item : this.plugin.items) {
                if (item.like(itemInHand, e.getPlayer())) {
                    e.setCancelled(true);
                    if (item.getVersionCheck()) {
                        boolean vali = false;

                        if (vali) {
                            if (item.hasPerm(e.getPlayer())) {
                                item.executeCommands(e.getPlayer(), this.plugin, AllString.prefix + AllString.clickWaitInterval, AllString.prefix);
                                e.getPlayer().updateInventory();
                                break;
                            }
                            e.getPlayer().sendMessage(CoreColor.colorCodes(AllString.prefix + AllString.noPermissionError));
                            break;
                        }
                        String versionformat = "";
                        for (String local : item.getVersionList())
                            versionformat = versionformat + local + " ";
                        CoreColor.message(e.getPlayer(), AllString.prefix + item.getNoVersionMessage());
                        break;
                    }
                    if (item.hasPerm(e.getPlayer())) {
                        item.executeCommands(e.getPlayer(), this.plugin, AllString.prefix + AllString.clickWaitInterval, AllString.prefix);
                        e.getPlayer().updateInventory();
                        break;
                    }
                    e.getPlayer().sendMessage(CoreColor.colorCodes(AllString.prefix + AllString.noPermissionError));
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onVoidTP(PlayerMoveEvent e) {
        if (this.plugin.config.getBoolean("void-tp.enable") &&
                this.plugin.config.getStringList("void-tp.world").contains(e.getPlayer().getWorld().getName()) &&
                e.getPlayer().getLocation().getY() < this.plugin.config.getInt("void-tp.level")) {
            Set<String> k = this.plugin.configSpawn.getKeys(Boolean.FALSE);
            if (k == null || k.isEmpty()) {
                e.getPlayer().getWorld().getSpawnLocation();
                e.getPlayer().teleport(e.getPlayer().getWorld().getSpawnLocation());
                this.plugin.log.debug("Void-TP: The spawn has not been established, trying to send to the spawn by defeto of the world.");
            } else {
                this.plugin.teleportToSpawn(e.getPlayer());
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        if (e.isCancelled())
            return;
        if (this.plugin.config.getBoolean("frame-protect.enable") &&
                e.getRightClicked() instanceof org.bukkit.entity.ItemFrame &&
                this.plugin.config.getStringList("frame-protect.world").contains(e.getPlayer().getWorld().getName()))
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onSoilChangePlayer(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("farm-protect.enable") &&
                this.plugin.config.getStringList("farm-protect.world").contains(e.getPlayer().getWorld().getName()) &&
                e.getAction() == Action.PHYSICAL && e.getClickedBlock().getType() == CoreMaterial.pre113.SOIL.get())
            e.setCancelled(true);
    }

    @EventHandler
    public void onJumpPlate(PlayerMoveEvent e) {
        if (this.plugin.config.getBoolean("jump-pads.enable")) {
            Material mat1 = Material.getMaterial(this.plugin.config.getString("jump-pads.plate-block").toUpperCase());
            if (mat1 == null)
                mat1 = Material.getMaterial(this.plugin.config.getString("jump-pads.plate-block-old").toUpperCase());
            if (e.getPlayer().getLocation().getBlock().getType() == mat1) {
                Vector vector = e.getPlayer().getLocation().getDirection().multiply(1.5D).setY(1.0D);
                e.getPlayer().setVelocity(vector);
                e.getPlayer().setFallDistance(-9999.0F);
                Sound sound = Sound.valueOf(this.plugin.config.getString("jump-pads.sound").toUpperCase());
                if (sound == null)
                    sound = Sound.valueOf(this.plugin.config.getString("jump-pads.sound-old").toUpperCase());
                e.getPlayer().playSound(e.getPlayer().getLocation(), sound, 1.0F, 1.0F);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoinEffets(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("join-boss-bar.enable")) {
            String mesage = CoreVariables.replace(this.plugin.config.getString("join-boss-bar.message"), e.getPlayer());
            String bc = this.plugin.config.getString("join-boss-bar.color");
            String bs = this.plugin.config.getString("join-boss-bar.style");
            int time = this.plugin.config.getInt("join-boss-bar.seconds");
            CoreBossBar.sendBossBar(e.getPlayer(), mesage, bc, bs, Math.max(time, 1), this.plugin);
        }
        if (this.plugin.config.getBoolean("join-tab.enable")) {
            String h = CoreVariables.replace(this.plugin.config.getString("join-tab.header"), e.getPlayer());
            String f = CoreVariables.replace(this.plugin.config.getString("join-tab.footer"), e.getPlayer());
            CorePlayerListHeaderFooter.sendHeaderFooter(e.getPlayer(), h, f);
        }
        if (this.plugin.config.getBoolean("join-titles.enable")) {
            int fadeIn = this.plugin.config.getInt("join-titles.fade-in");
            int stay = this.plugin.config.getInt("join-titles.stay");
            int fadeOut = this.plugin.config.getInt("join-titles.fade-out");
            String t = CoreVariables.replace(this.plugin.config.getString("join-titles.title"), e.getPlayer());
            String st = CoreVariables.replace(this.plugin.config.getString("join-titles.sub-title"), e.getPlayer());
            CoreTitles.sendTitles(e.getPlayer(), fadeIn, stay, fadeOut, t, st);
        }
        if (this.plugin.config.getBoolean("join-motd.enable")) {
            List<String> motd = CoreVariables.replaceList(this.plugin.config.getStringList("join-motd.message"), e.getPlayer());
            for (int i = 0; i < motd.size(); i++)
                e.getPlayer().sendMessage(motd.get(i));
        }
        if (this.plugin.config.getBoolean("join-action-bar.enable")) {
            String msg = CoreVariables.replace(this.plugin.config.getString("join-action-bar.message"), e.getPlayer());
            int time = this.plugin.config.getInt("join-action-bar.seconds");
            CoreActionBar.sendActionBar(e.getPlayer(), msg, time * 20, this.plugin);
        }
    }

    @EventHandler
    public void onCcommandBloker(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        Player p = e.getPlayer();
        if (this.plugin.config.getBoolean("commands-blacklist.enable") &&
                !p.hasPermission(this.plugin.config.getString("commands-blacklist.bypass")) &&
                this.plugin.config.getStringList("commands-blacklist.list") != null)
            for (String commands : this.plugin.config.getStringList("commands-blacklist.list")) {
                if (e.getMessage().split(" ")[0].equalsIgnoreCase("/" + commands)) {
                    e.setCancelled(true);
                    CoreColor.message(e.getPlayer(), AllString.prefix + AllString.noCommandError);
                    break;
                }
            }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (this.plugin.configChat.getBoolean("chat-format.enable")) {
            Player player = e.getPlayer();
            if (!player.hasPermission("superLobby.chat.magic") && (
                    e.getMessage().contains("&k") || e.getMessage().contains("§k")))
                            e.getMessage().replaceAll("&k", "").replaceAll("§k", "");
            if (player.hasPermission("superLobby.chat.color"))
                e.setMessage(CoreColor.colorCodes(e.getMessage()));
            String format = "";
            if (this.plugin.permission != null) {
                String g = this.plugin.permission.getPrimaryGroup(player);
                if (g == null)
                    throw new NullPointerException("PrimaryGroup = null.");
                if (this.plugin.configChat.isSet("chat-format.format." + g)) {
                    format = this.plugin.configChat.getString("chat-format.format." + g);
                } else {
                    format = this.plugin.configChat.getString("chat-format.format.default");
                }
                format = format.replace("{RANK}", g);
            } else {
                format = this.plugin.configChat.getString("chat-format.format.default");
            }
            format = format.replace("{PLAYER_NAME}", player.getName());
            format = format.replace("{DISPLAY_NAME}", "%s");
            format = format.replace("{WORLD}", player.getWorld().getName());
            if (this.plugin.chat != null) {
                format = format.replace("{PREFIX}", this.plugin.chat.getPlayerPrefix(player));
                format = format.replace("{SUFFIX}", this.plugin.chat.getPlayerSuffix(player));
            }
            format = CoreVariables.replace(format, player);
            format = format.replace("%", "%%");
            format = format.replace("{MESSAGE}", "%2$s");
            e.setFormat(format);
        }
    }

    @EventHandler
    public void onInfocommands(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        if (this.plugin.config.getBoolean("info-commands"))
            for (InfoCommand info : this.plugin.infoCommands) {
                if (e.getMessage().equalsIgnoreCase("/" + info.getCommand())) {
                    e.setCancelled(true);
                    if (info.getInfo() != null)
                        for (String m : info.getInfo())
                            e.getPlayer().sendMessage(CoreVariables.replace(m, e.getPlayer()));
                    break;
                }
            }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCustomJoin(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("custom-events"))
            for (CustomEvents cj : this.plugin.customEvents) {
                if (e.getPlayer().hasPermission(cj.getPermission())) {
                    if (cj.hasJoinMessage())
                        if (cj.getJoinMessage().equalsIgnoreCase("none")) {
                            e.setJoinMessage(null);
                        } else {
                            e.setJoinMessage(CoreVariables.replace(cj.getJoinMessage(), e.getPlayer()));
                        }
                    if (cj.hasJoinFirework())
                        cj.sendJoinFirework(e.getPlayer());
                    if (cj.hasJoinSound())
                        cj.sendJoinSound(e.getPlayer());
                    if (cj.hasJoinGamemode())
                        cj.sendJoinGamemode(e.getPlayer());
                    if (cj.hasJoinComands())
                        cj.sendJoinComands(e.getPlayer());
                    break;
                }
            }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCustomQuit(PlayerQuitEvent e) {
        if (this.plugin.config.getBoolean("custom-events"))
            for (CustomEvents cj : this.plugin.customEvents) {
                if (e.getPlayer().hasPermission(cj.getPermission())) {
                    if (cj.hasQuitMessage()) {
                        if (cj.getQuitMessage().equalsIgnoreCase("none")) {
                            e.setQuitMessage(null);
                            break;
                        }
                        e.setQuitMessage(CoreVariables.replace(cj.getQuitMessage(), e.getPlayer()));
                    }
                    break;
                }
            }
    }

    @EventHandler
    public void onLeaveCache(PlayerQuitEvent e) {
        for (CoreItem item : this.plugin.items) {
            item.getCoolDowns().remove(e.getPlayer().getName());
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onOpenMenu(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        if (!this.plugin.config.getBoolean("menu"))
            return;
        Player p = e.getPlayer();
        if (!e.getMessage().startsWith("/slmenuopen#"))
            return;
        String menuname = e.getMessage().split("#")[1].trim();
        if (this.plugin.menus.containsKey(menuname)) {
            e.setCancelled(true);
            CoreMenu m = this.plugin.menus.get(menuname);
            if (m.getWorld() != null &&
                    !m.getWorld().equalsIgnoreCase(p.getWorld().getName())) {
                CoreColor.message(e.getPlayer(), AllString.prefix + AllString.noWorldError);
                return;
            }
            if (m.getPerm() == null || m.getPerm().equals("")) {
                if (m.getSound() != null)
                    p.playSound(p.getLocation(), m.getSound(), 1.0F, 1.0F);
                m.open(p);
                return;
            }
            if (!p.hasPermission(m.getPerm())) {
                CoreColor.message(e.getPlayer(), AllString.prefix + AllString.noPermissionError);
                return;
            }
            if (m.getSound() != null)
                p.playSound(p.getLocation(), m.getSound(), 1.0F, 1.0F);
            m.open(p);
        } else {
            CoreColor.message(e.getPlayer(), AllString.prefix + "Menu not found. Please inform the staff.");
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onCommandMenu(PlayerCommandPreprocessEvent e) {
        if (e.isCancelled())
            return;
        if (!this.plugin.config.getBoolean("menu"))
            return;
        Player p = e.getPlayer();
        for (String menu : this.plugin.menus.keySet()) {
            if (this.plugin.menus.get(menu).getCommands() != null && e.getMessage().equalsIgnoreCase("/" + this.plugin.menus.get(menu).getCommands())) {
                e.setCancelled(true);
                CoreMenu m = this.plugin.menus.get(menu);
                if (m.getWorld() != null &&
                        !m.getWorld().equalsIgnoreCase(p.getWorld().getName())) {
                    CoreColor.message(p, AllString.prefix + AllString.noWorldError);
                    break;
                }
                if (m.getPerm() == null || m.getPerm().equals("")) {
                    if (m.getSound() != null)
                        p.playSound(p.getLocation(), m.getSound(), 1.0F, 1.0F);
                    m.open(p);
                    break;
                }
                if (!p.hasPermission(m.getPerm())) {
                    CoreColor.message(p, AllString.prefix + AllString.noPermissionError);
                    break;
                }
                if (m.getSound() != null)
                    p.playSound(p.getLocation(), m.getSound(), 1.0F, 1.0F);
                m.open(p);
                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoinBoard(PlayerJoinEvent e) {
        if (this.plugin.configBoard.getBoolean("settings-enable") &&
                this.plugin.configBoard.getStringList("settings-world").contains(e.getPlayer().getWorld().getName())) {
            PlayerBoard board = new PlayerBoard(e.getPlayer(), this.plugin.configBoard.getStringList("settings-title"), this.plugin.configBoard.getInt("settings-update-title"));
            int index = this.plugin.configBoard.getConfigurationSection("board").getKeys(false).size() - 1;
            for (String nodo : this.plugin.configBoard.getConfigurationSection("board").getKeys(false)) {
                Team team = board.get().registerNewTeam("text-" + (index + 1));
                team.addEntry(String.valueOf(ChatColor.values()[index]));
                board.getObjective().getScore(String.valueOf(ChatColor.values()[index])).setScore(index);
                BoardLine line = new BoardLine(e.getPlayer(), team, this.plugin.configBoard.getStringList("board." + nodo + ".content"), this.plugin.configBoard.getInt("board." + nodo + ".update"));
                board.add(line);
                index--;
            }
            board.send();
            this.plugin.playerBoards.put(e.getPlayer(), board);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onQuitBoard(PlayerQuitEvent e) {
        if (this.plugin.playerBoards.containsKey(e.getPlayer())) {
            this.plugin.playerBoards.remove(e.getPlayer());
            e.getPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        }
    }

    @EventHandler
    public void onChangeWorldBoard(PlayerChangedWorldEvent e) {
        if (this.plugin.configBoard.getBoolean("settings-enable")) {
            final Player p = e.getPlayer();
            Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
                public void run() {
                    if (PlayerListener.this.plugin.playerBoards.containsKey(p)) {
                        if (!PlayerListener.this.plugin.playerBoards.get(p).get().equals(p.getScoreboard()) &&
                                PlayerListener.this.plugin.configBoard.getStringList("settings-world").contains(p.getWorld().getName())) {
                            PlayerBoard board = new PlayerBoard(p, PlayerListener.this.plugin.configBoard.getStringList("settings-title"), PlayerListener.this.plugin.configBoard.getInt("settings-update-title"));
                            int index = PlayerListener.this.plugin.configBoard.getConfigurationSection("board").getKeys(false).size() - 1;
                            for (String nodo : PlayerListener.this.plugin.configBoard.getConfigurationSection("board").getKeys(false)) {
                                Team team = board.get().registerNewTeam("text-" + (index + 1));
                                team.addEntry(String.valueOf(ChatColor.values()[index]));
                                board.getObjective().getScore(String.valueOf(ChatColor.values()[index])).setScore(index);
                                BoardLine line = new BoardLine(p, team, PlayerListener.this.plugin.configBoard.getStringList("board." + nodo + ".content"), PlayerListener.this.plugin.configBoard.getInt("board." + nodo + ".update"));
                                board.add(line);
                                index--;
                            }
                            board.send();
                            PlayerListener.this.plugin.playerBoards.replace(p, board);
                        }
                    } else if (PlayerListener.this.plugin.configBoard.getStringList("settings-world").contains(p.getWorld().getName())) {
                        PlayerBoard board = new PlayerBoard(p, PlayerListener.this.plugin.configBoard.getStringList("settings-title"), PlayerListener.this.plugin.configBoard.getInt("settings-update-title"));
                        int index = PlayerListener.this.plugin.configBoard.getConfigurationSection("board").getKeys(false).size() - 1;
                        for (String nodo : PlayerListener.this.plugin.configBoard.getConfigurationSection("board").getKeys(false)) {
                            Team team = board.get().registerNewTeam("text-" + (index + 1));
                            team.addEntry(String.valueOf(ChatColor.values()[index]));
                            board.getObjective().getScore(String.valueOf(ChatColor.values()[index])).setScore(index);
                            BoardLine line = new BoardLine(p, team, PlayerListener.this.plugin.configBoard.getStringList("board." + nodo + ".content"), PlayerListener.this.plugin.configBoard.getInt("board." + nodo + ".update"));
                            board.add(line);
                            index--;
                        }
                        board.send();
                        PlayerListener.this.plugin.playerBoards.put(p, board);
                    }
                }
            }, 2L);
        }
    }

    @EventHandler
    public void PlayerJoinUpdateCheck(PlayerJoinEvent e) {
        if (this.plugin.config.getBoolean("update-check")) {
            final Player p = e.getPlayer();
            if (p.isOp())
                (new BukkitRunnable() {
                    public void run() {
                        CoreSpigotUpdater updater = new CoreSpigotUpdater(PlayerListener.this.plugin, 20400);
                        try {
                            if (updater.checkForUpdates()) {
                                CoreColor.message(p, AllString.prefix + "&7Update avaliable for SuperLobby. Please update to recieve latest version.");
                                CoreColor.message(p, AllString.prefix + "&7" + updater.getResourceURL());
                            }
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }
                    }
                }).runTaskAsynchronously(this.plugin);
        }
    }

    @EventHandler
    public void onDispenserClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-dispenser-interaction.enable") &&
                this.plugin.config.getStringList("disable-dispenser-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK &&
                    e.getClickedBlock().getType() == CoreMaterial.pre113.DISPENSER.get())
                if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                    if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                        e.setCancelled(false);
                    } else {
                        e.setCancelled(true);
                    }
                } else {
                    e.setCancelled(true);
                }
    }

    @EventHandler
    public void onNoteBlockClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-noteblock-interaction.enable") &&
                this.plugin.config.getStringList("disable-noteblock-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled() && (
                e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) &&
                e.getClickedBlock().getType() == CoreMaterial.pre113.NOTE_BLOCK.get())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onButtonClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-button-interaction.enable") &&
                this.plugin.config.getStringList("disable-button-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled() &&
                e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            List<String> mates;
            if (CoreUtils.mc1_7 || CoreUtils.mc1_8 || CoreUtils.mc1_9 || CoreUtils.mc1_10 || CoreUtils.mc1_11 || CoreUtils.mc1_12) {
                mates = Arrays.asList("WOOD_BUTTON", "STONE_BUTTON");
            } else {
                mates = Arrays.asList("ACACIA_BUTTON", "BIRCH_BUTTON", "DARK_OAK_BUTTON", "JUNGLE_BUTTON", "OAK_BUTTON", "SPRUCE_BUTTON", "STONE_BUTTON");
            }
            for (String mate : mates) {
                if (e.getClickedBlock().getType() == Material.valueOf(mate)) {
                    if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                        if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                            e.setCancelled(false);
                            break;
                        }
                        e.setCancelled(true);
                        break;
                    }
                    e.setCancelled(true);
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onTrapdoorClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-trapdoor-interaction.enable") &&
                this.plugin.config.getStringList("disable-trapdoor-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled() &&
                e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            List<String> mates;
            if (CoreUtils.mc1_7 || CoreUtils.mc1_8 || CoreUtils.mc1_9 || CoreUtils.mc1_10 || CoreUtils.mc1_11 || CoreUtils.mc1_12) {
                mates = Arrays.asList("IRON_TRAPDOOR", "TRAP_DOOR");
            } else {
                mates = Arrays.asList("ACACIA_TRAPDOOR", "BIRCH_TRAPDOOR", "DARK_OAK_TRAPDOOR", "IRON_TRAPDOOR", "JUNGLE_TRAPDOOR", "OAK_TRAPDOOR", "SPRUCE_TRAPDOOR");
            }
            for (String mate : mates) {
                if (e.getClickedBlock().getType() == Material.valueOf(mate)) {
                    if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                        if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                            e.setCancelled(false);
                            break;
                        }
                        e.setCancelled(true);
                        break;
                    }
                    e.setCancelled(true);
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onFenceGateClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-fence-gate-interaction.enable") &&
                this.plugin.config.getStringList("disable-fence-gate-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled() &&
                e.getAction() == Action.RIGHT_CLICK_BLOCK && (
                e.getClickedBlock().getType() == CoreMaterial.pre113.ACACIA_FENCE_GATE.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.BIRCH_FENCE_GATE.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.DARK_OAK_FENCE_GATE.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.JUNGLE_FENCE_GATE.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.FENCE_GATE.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.SPRUCE_FENCE_GATE.get()))
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onHopperClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-hopper-interaction.enable") &&
                this.plugin.config.getStringList("disable-hopper-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled() &&
                e.getAction() == Action.RIGHT_CLICK_BLOCK && (
                e.getClickedBlock().getType() == CoreMaterial.pre113.HOPPER.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.HOPPER_MINECART.get()))
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onDropperClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-dropper-interaction.enable") &&
                this.plugin.config.getStringList("disable-dropper-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK &&
                    e.getClickedBlock().getType() == CoreMaterial.pre113.DROPPER.get())
                if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                    if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                        e.setCancelled(false);
                    } else {
                        e.setCancelled(true);
                    }
                } else {
                    e.setCancelled(true);
                }
    }

    @EventHandler
    public void onDaylightSensorClick(PlayerInteractEvent e) {
        if (this.plugin.config.getBoolean("disable-daylight-sensor-interaction.enable") &&
                this.plugin.config.getStringList("disable-daylight-sensor-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getAction() == Action.RIGHT_CLICK_BLOCK && (
                    e.getClickedBlock().getType() == CoreMaterial.pre113.DAYLIGHT_DETECTOR.get() || e.getClickedBlock().getType() == CoreMaterial.pre113.DAYLIGHT_DETECTOR_INVERTED.get()))
                if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                    if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                        e.setCancelled(false);
                    } else {
                        e.setCancelled(true);
                    }
                } else {
                    e.setCancelled(true);
                }
    }

    @EventHandler
    public void onArmorStandsClick(PlayerArmorStandManipulateEvent e) {
        if (this.plugin.config.getBoolean("disable-armorstands-interaction.enable") &&
                this.plugin.config.getStringList("disable-armorstands-interaction.world").contains(e.getPlayer().getWorld().getName()) && !e.isCancelled())
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }
}