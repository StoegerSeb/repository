package at.sepp.lobbysystem.listener;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreMaterial;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;

public class EntityListener implements Listener {
    public LobbySystem plugin;

    public EntityListener(LobbySystem plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onItemDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof org.bukkit.entity.Item &&
                this.plugin.config.getBoolean("disable-item-damage.enable") &&
                this.plugin.config.getStringList("disable-item-damage.world").contains(e.getEntity().getWorld().getName()) && !e.isCancelled())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityExplode(EntityExplodeEvent e) {
        if (this.plugin.config.getBoolean("disable-entity-explode.enable") &&
                this.plugin.config.getStringList("disable-entity-explode.world").contains(e.getEntity().getWorld().getName()) && !e.isCancelled())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onEntityDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        if (this.plugin.config.getBoolean("disable-damage.enable") &&
                this.plugin.config.getStringList("disable-damage.world").contains(e.getEntity().getWorld().getName()) && !e.isCancelled())
            e.setCancelled(true);
    }

    @EventHandler
    public void onHunger(FoodLevelChangeEvent e) {
        if (!(e.getEntity() instanceof Player))
            return;
        if (this.plugin.config.getBoolean("disable-hunger.enable") &&
                this.plugin.config.getStringList("disable-hunger.world").contains(e.getEntity().getWorld().getName()) && !e.isCancelled()) {
            e.setCancelled(true);
            ((Player)e.getEntity()).setFoodLevel(20);
        }
    }

    @EventHandler
    public void onPlayerDeathMessage(PlayerDeathEvent e) {
        if (this.plugin.config.getBoolean("disable-death-message.enable") &&
                this.plugin.config.getStringList("disable-death-message.world").contains(e.getEntity().getWorld().getName()))
            e.setDeathMessage(null);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onCreatureSpawn(CreatureSpawnEvent e) {
        if (e.isCancelled())
            return;
        if (e.getEntity() == null)
            return;
        if (e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.NATURAL)
            return;
        if (this.plugin.config.getBoolean("disable-creature-spawn.enable") &&
                this.plugin.config.getStringList("disable-creature-spawn.world").contains(e.getEntity().getWorld().getName()))
            e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDeathDrops(PlayerDeathEvent e) {
        if (this.plugin.config.getBoolean("clear-drops-on-death.enable") &&
                this.plugin.config.getStringList("clear-drops-on-death.world").contains(e.getEntity().getWorld().getName()))
            e.getDrops().clear();
    }

    @EventHandler
    public void soilChangeEntity(EntityInteractEvent e) {
        if (this.plugin.config.getBoolean("farm-protect.enable") &&
                this.plugin.config.getStringList("farm-protect.world").contains(e.getEntity().getWorld().getName()) &&
                e.getEntityType() != EntityType.PLAYER && e.getBlock().getType() == CoreMaterial.pre113.SOIL.get())
            e.setCancelled(true);
    }
}