package at.sepp.lobbysystem.listener;

import at.sepp.lobbysystem.AllString;
import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.Utils;
import at.sepp.lobbysystem.core.CoreColor;
import at.sepp.lobbysystem.core.CoreMenu;
import at.sepp.lobbysystem.core.CoreMenuItem;
import org.bukkit.GameMode;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class OthersListener implements Listener {
    public LobbySystem plugin;

    public OthersListener(LobbySystem plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onRain(WeatherChangeEvent e) {
        if (this.plugin.config.getBoolean("disable-rain.enable") &&
                this.plugin.config.getStringList("disable-rain.world").contains(e.getWorld().getName()) && !e.isCancelled())
            e.setCancelled(e.toWeatherState());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onItemMove(InventoryClickEvent e) {
        if (this.plugin.config.getBoolean("disable-item-move.enable") &&
                this.plugin.config.getStringList("disable-item-move.world").contains(e.getWhoClicked().getWorld().getName()) && !e.isCancelled())
            if (e.getWhoClicked().isOp() || e.getWhoClicked().hasPermission("superlobby.staff")) {
                if (e.getWhoClicked().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
        if (e.isCancelled())
            return;
        if (this.plugin.config.getBoolean("frame-protect.enable") &&
                e.getRemover() instanceof Player &&
                this.plugin.config.getStringList("frame-protect.world").contains(e.getRemover().getWorld().getName()))
            if (e.getRemover().isOp() || e.getRemover().hasPermission("superlobby.staff")) {
                if (((HumanEntity) e.getRemover()).getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onHangingPlace(HangingPlaceEvent e) {
        if (e.isCancelled())
            return;
        if (this.plugin.config.getBoolean("frame-protect.enable") &&
                this.plugin.config.getStringList("frame-protect.world").contains(e.getPlayer().getWorld().getName()))
            if (e.getPlayer().isOp() || e.getPlayer().hasPermission("superlobby.staff")) {
                if (e.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    e.setCancelled(false);
                } else {
                    e.setCancelled(true);
                }
            } else {
                e.setCancelled(true);
            }
    }

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {
        if (e.isCancelled())
            return;
        for (String menu : this.plugin.menus.keySet()) {
            CoreMenu m = (CoreMenu) this.plugin.menus.get(menu);
            Player p = (Player) e.getWhoClicked();
            if (e.getView().getTitle().equals(CoreColor.colorCodes(m.getName()))) {
                ItemStack itemInHand = e.getCurrentItem();
                if (itemInHand == null)
                    break;
                e.setCancelled(true);
                for (CoreMenuItem item : m.items) {
                    if (item.like(itemInHand, p)) {
                        if (item.isCommand())
                            if (item.getVersionCheck()) {
                                boolean vali = false;
                                if (vali) {
                                    if (item.hasPerm(p)) {
                                        if (this.plugin.economy != null) {
                                            double money = this.plugin.economy.getBalance(p.getName());
                                            if (money >= item.getPrice() || item.getPrice() == 0) {
                                                double t = money - item.getPrice();
                                                Utils.setMoney(t, p, this.plugin);
                                                item.executeCommands(p, (JavaPlugin) this.plugin, AllString.prefix);
                                            } else {
                                                p.sendMessage(CoreColor.colorCodes(String.valueOf(AllString.prefix) + AllString.noMoneyError));
                                            }
                                        } else {
                                            item.executeCommands(p, (JavaPlugin) this.plugin, AllString.prefix);
                                        }
                                    } else {
                                        p.sendMessage(CoreColor.colorCodes(String.valueOf(AllString.prefix) + AllString.noPermissionError));
                                    }
                                } else {
                                    String versionformat = "";
                                    for (String local : item.getVersionList())
                                        versionformat = String.valueOf(versionformat) + local + " ";
                                    p.sendMessage(CoreColor.colorCodes(item.getNoVersionMessage().replaceAll("<version>", versionformat)));
                                }
                            } else if (item.hasPerm(p)) {
                                if (this.plugin.economy != null) {
                                    double money = this.plugin.economy.getBalance(p.getName());
                                    if (money >= item.getPrice() || item.getPrice() == 0) {
                                        double t = money - item.getPrice();
                                        Utils.setMoney(t, p, this.plugin);
                                        item.executeCommands(p, (JavaPlugin) this.plugin, AllString.prefix);
                                    } else {
                                        p.sendMessage(CoreColor.colorCodes(String.valueOf(AllString.prefix) + AllString.noMoneyError));
                                    }
                                } else {
                                    item.executeCommands(p, (JavaPlugin) this.plugin, AllString.prefix);
                                }
                            } else {
                                p.sendMessage(CoreColor.colorCodes(String.valueOf(AllString.prefix) + AllString.noPermissionError));
                            }
                        if (!item.isOpened())
                            p.closeInventory();
                        break;
                    }
                }
                break;
            }
        }
    }
}
