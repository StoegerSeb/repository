package at.sepp.lobbysystem;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class Utils {
    public static void setMoney(double money, Player p, LobbySystem plugin) {
        double i = money - plugin.economy.getBalance(p.getName());
        if (i > 0.0D) {
            plugin.economy.depositPlayer((OfflinePlayer)p, i);
        } else {
            plugin.economy.withdrawPlayer((OfflinePlayer)p, -i);
        }
    }

    public static boolean isEnum(Class<? extends Enum> class1, String value) {
        try {
            Object obj = Enum.valueOf(class1, value);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }
}
