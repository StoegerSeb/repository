package at.sepp.lobbysystem;

import at.sepp.lobbysystem.runnable.ActionBarAnnouncerRunnable;
import at.sepp.lobbysystem.runnable.BossBarAnnouncerRunnable;
import at.sepp.lobbysystem.runnable.MessageAnnouncerRunnable;
import at.sepp.lobbysystem.runnable.TitleAnnouncerRunnable;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public class Announcer {
    private LobbySystem plugin;

    private List<Integer> timers = new ArrayList<>();

    public Announcer(LobbySystem plugin) {
        this.plugin = plugin;
        if (plugin.configAnnouncer.getBoolean("message.enable")) {
            int interval = plugin.configAnnouncer.getInt("message.interval");
            this.timers.add((new MessageAnnouncerRunnable(plugin)).runTaskTimer(plugin, 0L, interval).getTaskId());
        }
        if (plugin.configAnnouncer.getBoolean("bossbar.enable")) {
            int interval = plugin.configAnnouncer.getInt("bossbar.interval");
            this.timers.add((new BossBarAnnouncerRunnable(plugin)).runTaskTimer(plugin, 0L, interval).getTaskId());
        }
        if (plugin.configAnnouncer.getBoolean("titles.enable")) {
            int interval = plugin.configAnnouncer.getInt("titles.interval");
            this.timers.add((new TitleAnnouncerRunnable(plugin)).runTaskTimer(plugin, 0L, interval).getTaskId());
        }
        if (plugin.configAnnouncer.getBoolean("actionbar.enable")) {
            int interval = plugin.configAnnouncer.getInt("actionbar.interval");
            this.timers.add((new ActionBarAnnouncerRunnable(plugin)).runTaskTimer(plugin, 0L, interval).getTaskId());
        }
    }

    public void reload() {
        cancelTimer();
        if (this.plugin.configAnnouncer.getBoolean("message.enable")) {
            int interval = this.plugin.configAnnouncer.getInt("message.interval");
            this.timers.add((new MessageAnnouncerRunnable(this.plugin)).runTaskTimer(this.plugin, 0L, interval).getTaskId());
        }
        if (this.plugin.configAnnouncer.getBoolean("bossbar.enable")) {
            int interval = this.plugin.configAnnouncer.getInt("bossbar.interval");
            this.timers.add((new BossBarAnnouncerRunnable(this.plugin)).runTaskTimer(this.plugin, 0L, interval).getTaskId());
        }
        if (this.plugin.configAnnouncer.getBoolean("titles.enable")) {
            int interval = this.plugin.configAnnouncer.getInt("titles.interval");
            this.timers.add((new TitleAnnouncerRunnable(this.plugin)).runTaskTimer(this.plugin, 0L, interval).getTaskId());
        }
        if (this.plugin.configAnnouncer.getBoolean("actionbar.enable")) {
            int interval = this.plugin.configAnnouncer.getInt("actionbar.interval");
            this.timers.add((new ActionBarAnnouncerRunnable(this.plugin)).runTaskTimer(this.plugin, 0L, interval).getTaskId());
        }
    }

    private void cancelTimer() {
        for (Integer timerID : this.timers) {
            if (timerID != null)
                Bukkit.getScheduler().cancelTask(timerID);
        }
        this.timers = new ArrayList<>();
    }
}