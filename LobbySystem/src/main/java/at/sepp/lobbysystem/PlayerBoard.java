package at.sepp.lobbysystem;

import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;

public class PlayerBoard {
    private Scoreboard board;
    private Objective score;

    private List<BoardLine> lines;
    private List<String> title;

    private int titleIndex = 0;
    private int titleInterval;
    private int titleIntervalTemp;

    private Player player;

    public PlayerBoard(Player player, List<String> anititle, int titleinterval) {
        this.title = anititle;
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.lines = new ArrayList<>();
        this.score = this.board.registerNewObjective("main", "dummy");
        this.score.setDisplayName(CoreVariables.replace(anititle.get(0), player));
        this.score.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.titleInterval = titleinterval;
        this.titleIntervalTemp = titleinterval;
        this.player = player;
    }

    public Scoreboard get() {
        return this.board;
    }

    public Objective getObjective() {
        return this.score;
    }

    public void update() {
        for (BoardLine line : this.lines)
            line.update();
        if (this.titleInterval <= 0) {
            updateTitle();
            this.titleInterval = this.titleIntervalTemp;
        }
        this.titleInterval--;
    }

    public void updateTitle() {
        if (this.titleIndex >= this.title.size())
            this.titleIndex = 0;
        this.score.setDisplayName(CoreVariables.replace(this.title.get(this.titleIndex), this.player));
        this.titleIndex++;
    }

    public void add(BoardLine param) {
        this.lines.add(param);
    }

    public void send() {
        this.player.setScoreboard(this.board);
    }
}
