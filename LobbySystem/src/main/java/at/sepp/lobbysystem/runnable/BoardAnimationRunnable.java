package at.sepp.lobbysystem.runnable;


import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.PlayerBoard;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class BoardAnimationRunnable extends BukkitRunnable {
    public LobbySystem plugin;

    public BoardAnimationRunnable(LobbySystem plugin) {
        this.plugin = plugin;
    }

    public void run() {
        for (Player onlinePlayer : this.plugin.playerBoards.keySet()) {
            PlayerBoard board = (PlayerBoard)this.plugin.playerBoards.get(onlinePlayer);
            board.update();
        }
    }
}
