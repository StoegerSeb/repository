package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class MessageAnnouncerRunnable extends BukkitRunnable {
    private LobbySystem plugin;

    public int messageCount = 0;

    private List<String> nodeList = new ArrayList<>();

    public MessageAnnouncerRunnable(LobbySystem plugin) {
        this.plugin = plugin;
        this.nodeList.addAll(plugin.configAnnouncer.getConfigurationSection("message.list").getKeys(false));
    }

    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            String nodo = this.nodeList.get(this.messageCount);
            List<String> messageLines = this.plugin.configAnnouncer.getStringList("message.list." + nodo);
            for (String s : messageLines) {
                s = CoreVariables.replace(s, p);
                p.sendMessage(s);
            }
        }
        if (this.messageCount >= this.nodeList.size() - 1) {
            this.messageCount = 0;
        } else {
            this.messageCount++;
        }
    }
}
