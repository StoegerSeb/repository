package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreBossBar;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class BossBarAnnouncerRunnable extends BukkitRunnable {
    private LobbySystem plugin;

    public int bossBarCount = 0;

    private List<String> nodoALL = new ArrayList<>();

    public BossBarAnnouncerRunnable(LobbySystem plugin) {
        this.plugin = plugin;
        this.nodoALL.addAll(plugin.configAnnouncer.getConfigurationSection("bossbar.list").getKeys(false));
    }

    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            String nodo = this.nodoALL.get(this.bossBarCount);
            String mesage = this.plugin.configAnnouncer.getString("bossbar.list." + nodo + ".message");
            mesage = CoreVariables.replace(mesage, p);
            String bc = this.plugin.configAnnouncer.getString("bossbar.list." + nodo + ".color");
            String bs = this.plugin.configAnnouncer.getString("bossbar.list." + nodo + ".style");
            int time = this.plugin.configAnnouncer.getInt("bossbar.list." + nodo + ".seconds");
            CoreBossBar.sendBossBar(p, mesage, bc, bs, Math.max(time, 1), (Plugin)this.plugin);
        }
        if (this.bossBarCount >= this.nodoALL.size() - 1) {
            this.bossBarCount = 0;
        } else {
            this.bossBarCount++;
        }
    }
}
