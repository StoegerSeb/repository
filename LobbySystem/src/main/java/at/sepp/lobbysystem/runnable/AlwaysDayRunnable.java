package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

public class AlwaysDayRunnable extends BukkitRunnable {
    public LobbySystem plugin;

    public AlwaysDayRunnable(LobbySystem plugin) {
        this.plugin = plugin;
    }

    public void run() {
        if (this.plugin.config.getBoolean("always-day.enable"))
            for (World w : Bukkit.getServer().getWorlds()) {
                if (this.plugin.config.getStringList("always-day.world").contains(w.getName()))
                    w.setTime(0L);
            }
    }
}
