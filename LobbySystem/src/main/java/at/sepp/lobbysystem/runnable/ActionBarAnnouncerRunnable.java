package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreActionBar;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class ActionBarAnnouncerRunnable extends BukkitRunnable {
    private LobbySystem plugin;

    public int actionCount = 0;

    private List<String> allNodes = new ArrayList<>();

    public ActionBarAnnouncerRunnable(LobbySystem plugin) {
        this.plugin = plugin;
        this.allNodes.addAll(plugin.configAnnouncer.getConfigurationSection("actionbar.list").getKeys(false));
    }

    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            String nodeString = this.allNodes.get(this.actionCount);
            int stay = this.plugin.configAnnouncer.getInt("actionbar.list." + nodeString + ".stay");
            String mesage = this.plugin.configAnnouncer.getString("actionbar.list." + nodeString + ".message");
            mesage = CoreVariables.replace(mesage, p);
            CoreActionBar.sendActionBar(p, mesage, stay, this.plugin);
        }
        if (this.actionCount >= this.allNodes.size() - 1) {
            this.actionCount = 0;
        } else {
            this.actionCount++;
        }
    }
}
