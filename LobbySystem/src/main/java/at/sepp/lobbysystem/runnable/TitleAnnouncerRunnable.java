package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CoreTitles;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class TitleAnnouncerRunnable extends BukkitRunnable {
    private LobbySystem plugin;

    public int titleCount = 0;

    private List<String> nodeList = new ArrayList<>();

    public TitleAnnouncerRunnable(LobbySystem plugin) {
        this.plugin = plugin;
        this.nodeList.addAll(plugin.configAnnouncer.getConfigurationSection("titles.list").getKeys(false));
    }

    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            String nodo = this.nodeList.get(this.titleCount);
            int fadeIn = this.plugin.configAnnouncer.getInt("titles.list." + nodo + ".fadein");
            int stay = this.plugin.configAnnouncer.getInt("titles.list." + nodo + ".stay");
            int fadeOut = this.plugin.configAnnouncer.getInt("titles.list." + nodo + ".fadeout");
            String title = this.plugin.configAnnouncer.getString("titles.list." + nodo + ".title");
            title = CoreVariables.replace(title, p);
            String subtitle = this.plugin.configAnnouncer.getString("titles.list." + nodo + ".subtitle");
            subtitle = CoreVariables.replace(subtitle, p);
            CoreTitles.sendTitles(p, fadeIn, stay, fadeOut, title, subtitle);
        }
        if (this.titleCount >= this.nodeList.size() - 1) {
            this.titleCount = 0;
        } else {
            this.titleCount++;
        }
    }
}
