package at.sepp.lobbysystem.runnable;

import at.sepp.lobbysystem.LobbySystem;
import at.sepp.lobbysystem.core.CorePlayerListHeaderFooter;
import at.sepp.lobbysystem.core.CoreVariables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class TabListUpdateRunnable extends BukkitRunnable {
    public LobbySystem plugin;

    public TabListUpdateRunnable(LobbySystem plugin) {
        this.plugin = plugin;
    }

    public void run() {
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            if (this.plugin.config.getBoolean("JoinTab.Enable")) {
                String h = this.plugin.config.getString("JoinTab.Header");
                h = CoreVariables.replace(h, onlinePlayer);
                String f = this.plugin.config.getString("JoinTab.Footer");
                f = CoreVariables.replace(f, onlinePlayer);
                CorePlayerListHeaderFooter.sendHeaderFooter(onlinePlayer, h, f);
            }
        }
    }
}
