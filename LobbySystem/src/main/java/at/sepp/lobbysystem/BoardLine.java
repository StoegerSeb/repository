package at.sepp.lobbysystem;

import at.sepp.lobbysystem.core.CoreVariables;
import com.google.common.base.Splitter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.Iterator;
import java.util.List;

public class BoardLine {
    private Team team;

    private List<String> list;

    private int index = 0;
    private int interval;
    private int intervalTemp;

    private Player player;

    public BoardLine(Player p, Team team, List<String> list, int interval) {
        this.team = team;
        this.list = list;
        this.interval = interval;
        this.intervalTemp = interval;
        this.player = p;
        String s = list.get(0);
        s = CoreVariables.replace(s, this.player);
        Iterator<String> iterator = Splitter.fixedLength(16).split(s).iterator();
        String iteratorString = iterator.next();
        team.setPrefix(iteratorString);
        if (s.length() > 16) {
            String str2 = iterator.next();
            String prefix = team.getPrefix();
            char c1 = team.getPrefix().charAt(prefix.length() - 1);
            char c2 = str2.charAt(0);
            if ((c1 == '&' || c1 == '§') && ChatColor.getByChar(c2) != null) {
                team.setPrefix(team.getPrefix().substring(0, prefix.length() - 1));
                str2 = c1 + str2;
            }
            String str1;
            if ((str1 = ChatColor.getLastColors(team.getPrefix())).equals(""))
                str1 = ChatColor.WHITE.toString();
            if ((str2 = str1 + str2).length() > 16)
                str2 = str2.substring(0, 15);
            team.setSuffix(str2);
        }
    }

    public void update() {
        if (this.interval <= 0) {
            next();
            this.interval = this.intervalTemp;
        }
        this.interval--;
    }

    private void next() {
        if (this.index >= this.list.size())
            this.index = 0;
        String s = this.list.get(this.index);
        s = CoreVariables.replace(s, this.player);
        Iterator<String> iterator = Splitter.fixedLength(16).split(s).iterator();
        String iteratorString = iterator.next();
        this.team.setPrefix(iteratorString);
        if (s.length() > 16) {
            String str2 = iterator.next();
            String prefix = this.team.getPrefix();
            char c1 = prefix.charAt(prefix.length() - 1);
            char c2 = str2.charAt(0);
            if ((c1 == '&' || c1 == '§') && ChatColor.getByChar(c2) != null) {
                this.team.setPrefix(this.team.getPrefix().substring(0, prefix.length() - 1));
                str2 = c1 + str2;
            }
            String str1;
            if ((str1 = ChatColor.getLastColors(this.team.getPrefix())).equals(""))
                str1 = ChatColor.WHITE.toString();
            if ((str2 = str1 + str2).length() > 16)
                str2 = str2.substring(0, 15);
            this.team.setSuffix(str2);
        }
        this.index++;
    }
}