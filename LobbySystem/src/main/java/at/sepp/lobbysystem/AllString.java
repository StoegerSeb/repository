package at.sepp.lobbysystem;

import at.sepp.lobbysystem.core.CoreConfig;

public class AllString {
    public static String prefix;

    public static String noConsoleError;
    public static String noPermissionError;
    public static String commandUsage;
    public static String commandSetSpawnUsage;
    public static String setSpawn;
    public static String spawnNotSetError;
    public static String clickWaitInterval;
    public static String noCommandError;
    public static String noWorldError;
    public static String noMoneyError;
    public static String commandRemoveSpawnUsage;
    public static String noSpawnError;
    public static String removeSpawn;
    public static String configLoaded;
    public static String reloadCommandHelp;
    public static String reloadDescriptionHelp;
    public static String commandSetSpawnHelp;
    public static String descriptionSetSpawnHelp;
    public static String commandRemoveSpawnHelp;
    public static String descriptionRemoveSpawnHelp;

    public static void load(CoreConfig config, CoreConfig messages) {
        prefix = config.getString("prefix");
        noConsoleError = messages.getString("error-no-console");
        noPermissionError = messages.getString("error-no-permission");
        commandUsage = messages.getString("use-command");
        commandSetSpawnUsage = messages.getString("use-command-setspawn");
        setSpawn = messages.getString("set-spawn");
        spawnNotSetError = messages.getString("error-spawn-not-set");
        clickWaitInterval = messages.getString("click-wait");
        noCommandError = messages.getString("error-no-command");
        noWorldError = messages.getString("error-no-world");
        noMoneyError = messages.getString("no-money");
        commandRemoveSpawnUsage = messages.getString("use-command-remspawn");
        noSpawnError = messages.getString("error-no-spawn");
        removeSpawn = messages.getString("rem-spawn");
        configLoaded = messages.getString("config-loaded");
        reloadCommandHelp = messages.getString("help-command-reload");
        reloadDescriptionHelp = messages.getString("help-description-reload");
        commandSetSpawnHelp = messages.getString("help-command-setSpawn");
        descriptionSetSpawnHelp = messages.getString("help-description-setSpawn");
        commandRemoveSpawnHelp = messages.getString("help-command-remSpawn");
        descriptionRemoveSpawnHelp = messages.getString("help-description-remSpawn");
    }
}
