package sepp.troll;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import sepp.troll.listener.EggCannon;
import sepp.troll.listener.JukeListener;
import sepp.troll.listener.PushListener;

public class TrollPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new EggCannon(), this);
        Bukkit.getConsoleSender().sendMessage("Test");

        getCommand("push").setExecutor(new PushListener());
        getCommand("juke").setExecutor(new JukeListener());
    }
}
