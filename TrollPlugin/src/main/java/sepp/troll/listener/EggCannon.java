package sepp.troll.listener;

import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class EggCannon implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();

        try {
            if (e.getItem().getType() == Material.STICK) {
                if (player.hasPermission("troll.admin")) {
                    if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK ||  e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK  ) {
                        e.setCancelled(true);
                        /*player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);
                        player.launchProjectile(Arrow.class);*/
                        player.launchProjectile(Fireball.class);
                        player.launchProjectile(Fireball.class);
                        player.launchProjectile(Fireball.class);
                        player.launchProjectile(Fireball.class);
                    }
                }
            }
        }catch(NullPointerException ex) {}
    }
}
