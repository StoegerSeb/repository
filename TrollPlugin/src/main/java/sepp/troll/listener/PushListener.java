package sepp.troll.listener;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;

public class PushListener implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);
            target.setVelocity(new Vector(1, 1, 1));
            target.sendMessage("§3Du wurdest angestubst :3");
        }

        return false;
    }
}
