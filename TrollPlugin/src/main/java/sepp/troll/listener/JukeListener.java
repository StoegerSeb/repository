package sepp.troll.listener;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class JukeListener implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);
            target.playSound(target.getLocation(), Sound.BAT_DEATH, 5F, 5F);
            target.playSound(target.getLocation(), Sound.BAT_HURT, 5F, 5F);
            target.playSound(target.getLocation(), Sound.BAT_LOOP, 5F, 5F);
            target.playSound(target.getLocation(), Sound.BAT_TAKEOFF, 5F, 5F);
            target.playSound(target.getLocation(), Sound.COW_HURT, 5F, 5F);
            target.playSound(target.getLocation(), Sound.ENDERDRAGON_GROWL, 5F, 5F);
            target.playSound(target.getLocation(), Sound.ENDERDRAGON_DEATH, 5F, 5F);
            target.playSound(target.getLocation(), Sound.WITHER_DEATH, 5F, 5F);
            target.playSound(target.getLocation(), Sound.PIG_DEATH, 5F, 5F);
            target.playSound(target.getLocation(), Sound.IRONGOLEM_DEATH, 5F, 5F);
            target.sendMessage("Orhendkrebs ^^");
        }
        return false;
    }
}
